#
# Copyright (C) 2020-2024  CZ.NIC, z. s. p. o.
#
# This file is part of FRED.
#
# FRED is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# FRED is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with FRED.  If not, see <https://www.gnu.org/licenses/>.

"""Card handler for CSOB Gateway."""

import logging
from datetime import timedelta
from enum import unique
from typing import Any, Dict, List, Optional, Tuple, cast
from urllib.parse import quote, urljoin

import requests

try:
    from enum import StrEnum  # type: ignore[attr-defined, unused-ignore]
except ImportError:  # pragma: no cover
    from backports.strenum import StrEnum  # type: ignore[assignment, unused-ignore]

from django.core.exceptions import ImproperlyConfigured
from django.utils import timezone
from djmoney.money import Money
from hermes import Email, Reference
from hermes.client import SendFailed
from pycsob import conf as CSOB
from pycsob.client import CsobClient

from django_pain.card_payment_handlers import (
    AbstractCardPaymentHandler,
    CartItem,
    CustomerData,
    PaymentHandlerAccountError,
    PaymentHandlerConnectionError,
    PaymentHandlerError,
    PaymentHandlerLimitError,
    RedirectUrl,
    check_payment_handler,
)
from django_pain.constants import MessageReference, MessageTemplate, PaymentState, PaymentType
from django_pain.models import BankAccount, BankPayment
from django_pain.settings import SETTINGS, get_email_client_instance
from django_pain.utils import local_date

LOGGER = logging.getLogger(__name__)


CSOB_GATEWAY_TO_PAYMENT_STATE_MAPPING = {
    CSOB.PAYMENT_STATUS_INIT: PaymentState.INITIALIZED,
    CSOB.PAYMENT_STATUS_PROCESS: PaymentState.INITIALIZED,
    CSOB.PAYMENT_STATUS_CANCELLED: PaymentState.CANCELED,
    CSOB.PAYMENT_STATUS_CONFIRMED: PaymentState.READY_TO_PROCESS,
    CSOB.PAYMENT_STATUS_REVERSED: PaymentState.CANCELED,
    CSOB.PAYMENT_STATUS_REJECTED: PaymentState.CANCELED,
    CSOB.PAYMENT_STATUS_WAITING: PaymentState.READY_TO_PROCESS,
    CSOB.PAYMENT_STATUS_RECOGNIZED: PaymentState.READY_TO_PROCESS,
    CSOB.PAYMENT_STATUS_RETURN_WAITING: PaymentState.CANCELED,
    CSOB.PAYMENT_STATUS_RETURNED: PaymentState.CANCELED,
}


@unique
class PayOperation(StrEnum):
    """Payment operation type."""

    PAYMENT = "payment"
    CUSTOM_PAYMENT = "customPayment"


@unique
class PayMethod(StrEnum):
    """Payment method type."""

    CARD = "card"


class BaseCSOBGatewayHandler(AbstractCardPaymentHandler):
    """Base CSOB gateway handler.

    This is abstract handler encapsulating common functionality of CSOB gateway for various
    payment operations/methods.

    Attributes:
        pay_operation: Type of payment operation.
        pay_method: Type of payment method.
    """

    pay_operation: PayOperation
    pay_method: PayMethod

    def __init__(self, name: str, gateway: str) -> None:
        super().__init__(name, gateway)
        self._client = None
        try:
            self._gateway = SETTINGS.csob_gateways[gateway]
        except KeyError as error:
            raise ImproperlyConfigured(f"Key `{gateway}` not found in PAIN_CSOB_GATEWAYS") from error

    @property
    def client(self):
        """Get CSOB Gateway Client."""
        if self._client is None:
            self._client = CsobClient(
                self._gateway["MERCHANT_ID"],
                self._gateway["API_URL"],
                str(self._gateway["MERCHANT_PRIVATE_KEY"]),
                str(self._gateway["API_PUBLIC_KEY"]),
            )
        return self._client

    def _init_payment(
        self,
        amount: Money,
        variable_symbol: str,
        return_url: str,
        return_method: str,
        cart: List[CartItem],
        language: str,
        customer: Optional[CustomerData],
        **kwargs,
    ) -> Dict[str, Any]:
        """Initialize card payment on the CSOB gateway, return gateway response."""
        dict_cart: List[dict] = []
        for item in cart:
            dict_item = item._asdict()
            # CSOB Gateway works with multiples of 100 of basic currency:
            dict_item["amount"] = int(dict_item["amount"] * 100)
            dict_cart.append(dict_item)

        # Init payment on CSOB Gateway
        # Note: It is responsibility of each handler to provide meaningful 'kwargs' only.
        try:
            response = self.client.payment_init(
                order_no=variable_symbol,
                total_amount=int(amount.amount * 100),
                currency=str(amount.currency),
                return_url=return_url,
                description="Dummy value",
                cart=dict_cart,
                return_method=return_method,
                language=language,
                pay_operation=self.pay_operation.value,
                pay_method=self.pay_method.value,
                # logo_version=PAYMENTS_SETTINGS.PAYMENTS_CSOB_LOGO_VERSION,
                # color_scheme_version=PAYMENTS_SETTINGS.PAYMENTS_CSOB_COLOR_SCHEME_VERSION,
                # merchant_data=merchant_data,
                **kwargs,
            )
        except requests.ConnectionError:
            raise PaymentHandlerConnectionError("Gateway connection error")

        data = self.client.gateway_return(response.json())
        if (
            data["resultCode"] == CSOB.RETURN_CODE_MERCHANT_BLOCKED
            and data["resultMessage"] == "Merchant trx limit exceeded"
        ):
            # The combination of result code and message is not documented in API, it is based on
            # real life observation and the statement from bank technical support.
            raise PaymentHandlerLimitError(f"Amount {amount.amount} {amount.currency} exceeds gateway limit")
        if data["resultCode"] != CSOB.RETURN_CODE_OK:
            raise PaymentHandlerError("init resultCode != OK", data)
        if data["paymentStatus"] != CSOB.PAYMENT_STATUS_INIT:
            raise PaymentHandlerError("Init paymentStatus != PAYMENT_STATUS_INIT", data)

        return dict(**data)

    def _save_payment(
        self,
        pay_id: str,
        amount: Money,
        description: str,
        card_payment_state: str,
        variable_symbol: str,
        processor: str,
        account: BankAccount,
        **kwargs,
    ) -> BankPayment:
        """Save newly initialized payment to database."""
        # Note: It is responsibility of each handler to provide meaningful 'kwargs' only.
        payment = BankPayment.objects.create(
            identifier=pay_id,
            payment_type=PaymentType.CARD_PAYMENT,
            account=account,
            amount=amount,
            description=description,
            state=PaymentState.INITIALIZED,
            card_payment_state=card_payment_state,
            variable_symbol=variable_symbol,
            processor=processor,
            card_handler=self.name,
            **kwargs,
        )
        return payment

    def _update_payments_state(self, payment: BankPayment) -> None:
        """Update status of the payment from CSOB Gateway and if newly paid, process the payment."""
        try:
            gateway_result = self.client.payment_status(payment.identifier).payload
        except requests.ConnectionError:
            raise PaymentHandlerConnectionError("Gateway connection error")
        if gateway_result["resultCode"] == CSOB.RETURN_CODE_OK:
            payment.card_payment_state = CSOB.PAYMENT_STATUSES[gateway_result["paymentStatus"]]
            # `state` attribute must not be updated unless it's INITIALIZED, as we would easily go from
            # PROCESSED to READY_TO_PROCESS again.
            if payment.state == PaymentState.INITIALIZED:
                payment.state = CSOB_GATEWAY_TO_PAYMENT_STATE_MAPPING[gateway_result["paymentStatus"]]
                if payment.state != PaymentState.INITIALIZED:  # payment state changed
                    payment.transaction_date = local_date(timezone.now())
            payment.save()
        else:
            LOGGER.error("payment_status resultCode != OK: %s", gateway_result)
            raise PaymentHandlerError("payment_status resultCode != OK", gateway_result)

    def _get_account(self, amount: Money) -> BankAccount:
        """Get account based on amount currency."""
        csob_accounts = self._gateway["ACCOUNT_NUMBERS"]
        try:
            account_number = csob_accounts[str(amount.currency)]
        except KeyError as error:
            raise PaymentHandlerAccountError("No account for currency {}.".format(amount.currency)) from error

        try:
            account = BankAccount.objects.get(account_number=account_number)
        except BankAccount.DoesNotExist as error:
            message = 'CSOBCardPaymentHandler configured with non-existing account "{}".'.format(account_number)
            raise PaymentHandlerAccountError(message) from error

        return account

    def _check_limit(self, amount: Money) -> None:
        """Check payment limit and raise `PaymentHandlerLimitError` when exceeded."""
        # Do not carry out checking when no limit is found in settings
        limit = self._gateway["ACCOUNT_LIMITS"].get(str(amount.currency))
        if limit is not None and amount.amount > limit:
            raise PaymentHandlerLimitError(f"Amount {amount.amount} {amount.currency} exceeds limit")


class CSOBCardPaymentHandler(BaseCSOBGatewayHandler):
    """CSOB gateway card payment handler."""

    pay_operation = cast(PayOperation, PayOperation.PAYMENT)
    pay_method = cast(PayMethod, PayMethod.CARD)

    def init_payment(
        self,
        amount: Money,
        variable_symbol: str,
        processor: str,
        return_url: str,
        return_method: str,
        cart: List[CartItem],
        language: str,
        **kwargs,
    ) -> Tuple[BankPayment, RedirectUrl]:
        """Initialize card payment on the CSOB gateway, see parent class for detailed description."""
        account = self._get_account(amount)
        self._check_limit(amount)
        init_data = self._init_payment(
            amount=amount,
            variable_symbol=variable_symbol,
            return_url=return_url,
            return_method=return_method,
            cart=cart,
            language=language,
            customer=kwargs.pop("customer", None),
        )
        pay_id = init_data["payId"]
        payment = self._save_payment(
            pay_id=pay_id,
            amount=amount,
            description=cart[0].name,
            card_payment_state=CSOB.PAYMENT_STATUSES[init_data["paymentStatus"]],
            variable_symbol=variable_symbol,
            processor=processor,
            account=account,
        )
        redirect_url = self.client.get_payment_process_url(pay_id)
        return payment, RedirectUrl(redirect_url)

    @check_payment_handler
    def update_payments_state(self, payment: BankPayment) -> None:
        """Update status of the payment from CSOB gateway and if newly paid, process the payment."""
        self._update_payments_state(payment=payment)


class CSOBCustomPaymentHandler(BaseCSOBGatewayHandler):
    """CSOB gateway custom payment handler."""

    pay_operation = cast(PayOperation, PayOperation.CUSTOM_PAYMENT)
    pay_method = cast(PayMethod, PayMethod.CARD)

    def __init__(self, name: str, gateway: str) -> None:
        super().__init__(name, gateway)
        self._email_client = get_email_client_instance()

    def init_payment(
        self,
        amount: Money,
        variable_symbol: str,
        processor: str,
        return_url: str,
        return_method: str,
        cart: List[CartItem],
        language: str,
        **kwargs,
    ) -> Tuple[BankPayment, RedirectUrl]:
        """Initialize custom payment on the CSOB gateway, see parent class for detailed description."""
        # Get additional parameters - customer email and payment expiry
        email = kwargs.get("email", None)
        custom_expiry = kwargs.get(
            "custom_expiry", timezone.now() + timedelta(seconds=SETTINGS.csob_custom_handler["max_validity"])
        )

        account = self._get_account(amount)
        self._check_limit(amount)
        init_data = self._init_payment(
            amount=amount,
            variable_symbol=variable_symbol,
            return_url=return_url,
            return_method=return_method,
            cart=cart,
            language=language,
            customer=kwargs.pop("customer", None),
            # custom payment specific parameters
            custom_expiry=custom_expiry.strftime("%Y%m%d%H%M%S"),
        )

        customer_code = init_data.get("customerCode", None)
        if not customer_code:
            raise PaymentHandlerError("No customer code generated for custom payment")  # should never happen

        payment = self._save_payment(
            pay_id=init_data["payId"],
            amount=amount,
            description=cart[0].name,
            card_payment_state=CSOB.PAYMENT_STATUSES[init_data["paymentStatus"]],
            variable_symbol=variable_symbol,
            processor=processor,
            account=account,
            # custom payment specific parameters
            email=email,
            custom_expiry=custom_expiry,
            customer_code=customer_code,
        )

        if email:
            self._send_customer_code(payment)
        redirect_url = urljoin(SETTINGS.csob_custom_handler["custom_payment_url"], quote(customer_code))
        return payment, RedirectUrl(redirect_url)

    @check_payment_handler
    def update_payments_state(self, payment: BankPayment) -> None:
        """Update status of the payment from CSOB gateway and if newly paid, process the payment."""
        self._update_payments_state(payment=payment)
        if payment.email and payment.state == PaymentState.INITIALIZED:
            if not payment.email_sent:
                self._send_customer_code(payment)
            elif self._should_resend_email(payment):
                self._send_customer_code(payment, resend=True)

    @check_payment_handler
    def get_payment_url(self, payment: BankPayment) -> Optional[RedirectUrl]:
        """Provide an URL on the bank gateway if the payment is ready to be paid."""
        result = None  # URL is not available by default
        if payment.state == PaymentState.INITIALIZED and timezone.now() <= payment.custom_expiry:
            result = urljoin(SETTINGS.csob_custom_handler["custom_payment_url"], quote(payment.customer_code))
        return result

    def _send_email(self, payment: BankPayment, resend: bool) -> str:
        context: Dict[str, Any] = {
            "payment": payment.identifier,
            "url_base": SETTINGS.csob_custom_handler["custom_payment_url"],
            "customer_code": payment.customer_code,
            "description": payment.description,
            "amount": str(payment.amount),
            "variable_symbol": payment.variable_symbol,
            "expiry": payment.custom_expiry.isoformat(),
            "resend": resend,
        }
        email = Email(
            recipient=payment.email,
            subject_template=MessageTemplate.CSOB_CUSTOMER_CODE_SUBJECT,
            body_template=MessageTemplate.CSOB_CUSTOMER_CODE_BODY,
            context=context,
        )
        message_id = self._email_client.send(
            email, references=[Reference(type=MessageReference.PAYMENT, value=str(payment.uuid))]
        )
        return message_id

    def _send_customer_code(self, payment: BankPayment, *, resend: bool = False) -> None:
        """Send/resend email with customer code and update payment info in database."""
        now = timezone.now()
        if now > payment.custom_expiry:
            return  # no need to send email, payment would be overdue anyway
        try:
            message_id = self._send_email(payment, resend)
            payment.email_sent = now  # update payment - mark email as sent
            payment.save()
            LOGGER.debug(
                "email with customer code for payment %s sent (message id: %s)", payment.identifier, message_id
            )
        except SendFailed:
            LOGGER.error("failed to sent email with customer code for payment %s", payment.identifier)

    def _should_resend_email(self, payment: BankPayment) -> bool:
        """Evaluate conditions for resending email with customer code."""
        result = False
        # Check if resending emails is active
        resend_settings = SETTINGS.csob_custom_handler["resend_email"]
        if resend_settings is not None:
            # Check if processor is applicable
            if (resend_settings["processors"] is None) or (payment.processor in resend_settings["processors"]):
                # Evaluate time points for events
                # Note: Reduced `min_delta` allows for delay in sending email (after payment creation or last
                #       resending event) for up to one hour.
                now = timezone.now()
                from_time = payment.email_sent or payment.create_time  # 'email_sent' should always be set
                min_delta = resend_settings["min_delta"] - timedelta(hours=1)
                for delta in resend_settings["time_deltas"]:
                    event_time = payment.create_time + delta if delta >= timedelta() else payment.custom_expiry + delta
                    # Check if event falls into active interval
                    if from_time + min_delta <= event_time <= now:
                        return True
        return result
