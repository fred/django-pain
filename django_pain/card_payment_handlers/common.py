#
# Copyright (C) 2020-2024  CZ.NIC, z. s. p. o.
#
# This file is part of FRED.
#
# FRED is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# FRED is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with FRED.  If not, see <https://www.gnu.org/licenses/>.

"""Base payment processor module."""

from abc import ABC, abstractmethod
from decimal import Decimal
from functools import wraps
from typing import Callable, List, NamedTuple, Optional, Tuple, TypeVar, Union

from djmoney.money import Money

from django_pain.models import BankPayment

CartItem = NamedTuple(
    "CartItem",
    [
        ("name", str),
        ("quantity", int),
        ("amount", Decimal),
        ("description", str),
    ],
)


CustomerData = NamedTuple(
    "CustomerData",
    [
        ("name", str),
        ("phone_number", Optional[str]),
        ("email", Optional[str]),
    ],
)


class PaymentHandlerError(Exception):
    """Generic payment handler error."""


class PaymentHandlerConnectionError(PaymentHandlerError):
    """Payment handler connection error."""


class PaymentHandlerAccountError(PaymentHandlerError):
    """Error related to bank account associated with payment gateway."""


class PaymentHandlerLimitError(PaymentHandlerError):
    """Payment amount exceeds an operation limit."""


RedirectUrl = str
CardHandler = TypeVar("CardHandler", bound="AbstractCardPaymentHandler")


def check_payment_handler(method: Callable[[CardHandler, BankPayment], Union[None, RedirectUrl]]):
    """Check that `BankPayment` parameter to card handler method has appropriate `card_handler` attribute."""

    @wraps(method)
    def _check(self, payment):
        if self.name != payment.card_handler:
            raise ValueError(
                f"Card handler got incompatible payment: got `{payment.card_handler}`, expected `{self.name}`."
            )
        return method(self, payment)

    return _check


class AbstractCardPaymentHandler(ABC):
    """Card payment handler."""

    def __init__(self, name: str, gateway: str) -> None:
        self.name = name

    @check_payment_handler
    def get_payment_url(self, payment: BankPayment) -> Optional[RedirectUrl]:
        """Provide an URL on the bank gateway if the payment is ready to be paid.

        Overwrite method if handler can provide "long-term" URL to make a payment.
        """
        return None

    @abstractmethod
    def init_payment(
        self,
        amount: Money,
        variable_symbol: str,
        processor: str,
        return_url: str,
        return_method: str,
        cart: List[CartItem],
        language: str,
        **kwargs,
    ) -> Tuple[BankPayment, RedirectUrl]:
        """Initialize card payment.

        Args:
            amount: Total amount of money to pay.
            variable_symbol: Symbol of the order (sometimes called order_id) to be seen in bank administration.
            processor: Name of the processor in settings.
            return_url: URL to wich Gateway redirects back after payment is done.
            return_method: HTTP method for the redirection (POST/GET).
            cart: List of CartItems of length from 1 to 2.
            language: ISO 639-1 language code e.g. 'en' for English.
            kwargs: other handler specific parameters

        Returns newly created BankPayment and CSOB gateway URL to redirect to.

        Each descendant has to implement this method.
        Returns newly created BankPayment and redirect URL to Card Payment Gateway.
        """

    @abstractmethod
    def update_payments_state(self, payment: BankPayment) -> None:
        """Update state of the payment from Card Gateway and if newly paid, process the payment."""
