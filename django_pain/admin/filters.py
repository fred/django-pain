#
# Copyright (C) 2018-2024  CZ.NIC, z. s. p. o.
#
# This file is part of FRED.
#
# FRED is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# FRED is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with FRED.  If not, see <https://www.gnu.org/licenses/>.

"""Admin filters."""

from django.contrib.admin import ChoicesFieldListFilter, SimpleListFilter
from django.utils import timezone
from django.utils.translation import gettext as _

from django_pain.constants import PaymentRequestStatus, PaymentState
from django_pain.models import PAYMENT_STATE_CHOICES


class PaymentStateListFilter(ChoicesFieldListFilter):
    """Custom filter for payment state."""

    REALIZED_STATES = (
        PaymentState.READY_TO_PROCESS,
        PaymentState.DEFERRED,
        PaymentState.PROCESSED,
        PaymentState.TO_REFUND,
        PaymentState.REFUNDED,
    )

    def choices(self, cl):
        """Return modified enum choices."""
        yield {
            "selected": self.lookup_val is None,
            "query_string": cl.get_query_string(remove=self.expected_parameters()),
            "display": _("Realized"),
        }
        yield {
            "selected": self.lookup_val == "all",
            "query_string": cl.get_query_string({self.lookup_kwarg: "all"}, [self.lookup_kwarg]),
            "display": _("All"),
        }
        for enum_value in list(PaymentState):
            yield {
                "selected": (enum_value == self.lookup_val),
                "query_string": cl.get_query_string({self.lookup_kwarg: enum_value}),
                "display": dict(PAYMENT_STATE_CHOICES)[enum_value],
            }

    def queryset(self, request, queryset):
        """Return filtered queryset."""
        if not self.used_parameters:
            return queryset.filter(state__in=self.REALIZED_STATES)
        elif self.used_parameters.get(self.lookup_kwarg) == "all":
            return queryset
        return super().queryset(request, queryset)


class PaymentRequestStatusFilter(SimpleListFilter):
    """Custom filter for payment request status."""

    title = _("Status")
    parameter_name = "status"

    def lookups(self, request, model_admin):
        """Return list of tuples - value in the URL query string and human-readable name."""
        return [(item.value, item.label) for item in PaymentRequestStatus]

    def queryset(self, request, queryset):
        """Return filtered queryset based on value provided in request query string."""
        filter_value = self.value()
        if filter_value == PaymentRequestStatus.REALIZED:
            return queryset.filter(realized_payment__isnull=False)
        if filter_value == PaymentRequestStatus.CLOSED:
            return queryset.filter(realized_payment__isnull=True, closed_at__isnull=False)
        now = timezone.now()
        if filter_value == PaymentRequestStatus.WAITING:
            return queryset.filter(realized_payment__isnull=True, closed_at__isnull=True, due_at_displayed__gte=now)
        if filter_value == PaymentRequestStatus.OVERDUE:
            return queryset.filter(realized_payment__isnull=True, closed_at__isnull=True, due_at_displayed__lt=now)
