#
# Copyright (C) 2018-2024  CZ.NIC, z. s. p. o.
#
# This file is part of FRED.
#
# FRED is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# FRED is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with FRED.  If not, see <https://www.gnu.org/licenses/>.

"""Admin interface for django_pain."""

from calendar import monthrange
from copy import deepcopy
from datetime import date

from django.contrib import admin, messages
from django.contrib.auth.admin import UserAdmin as DjangoUserAdmin
from django.db import transaction
from django.templatetags.static import static
from django.utils import timezone
from django.utils.formats import date_format
from django.utils.html import format_html
from django.utils.safestring import mark_safe
from django.utils.translation import get_language, gettext_lazy as _, to_locale
from djmoney.models.fields import Money
from moneyed.localization import format_money

from django_pain.constants import PaymentRequestStatus, PaymentState
from django_pain.models import BankPayment, Invoice, InvoiceItem, InvoiceRef, PaymentRequest, PaymentRequestItem
from django_pain.settings import get_processor_instance

from .filters import PaymentRequestStatusFilter, PaymentStateListFilter
from .forms import BankAccountForm, BankPaymentForm, PaymentRequestForm, UserCreationForm


def format_price(amount: Money) -> str:
    """Format money field for admin views."""
    locale = to_locale(get_language())
    amount = format_money(amount, locale=locale)
    return mark_safe(amount.replace(" ", "&nbsp;"))


class BankAccountAdmin(admin.ModelAdmin):
    """Model admin for BankAccount."""

    form = BankAccountForm

    def get_readonly_fields(self, request, obj):
        """Currency field should be editable only when creating new bank account."""
        if obj is None:
            return ()
        else:
            return ("currency",)

    @transaction.atomic
    def changelist_view(self, request, extra_context=None):
        """Wrap super's view in a transaction."""
        return super().changelist_view(request, extra_context=extra_context)

    @transaction.atomic
    def history_view(self, request, object_id, extra_context=None):
        """Wrap super's view in a transaction."""
        return super().history_view(request, object_id, extra_context=extra_context)

    def get_queryset(self, request):
        """Override super's get_queryset to add locking."""
        return super().get_queryset(request).select_for_update()


class InvoiceRefsInline(admin.TabularInline):
    """An inline model admin for invoice refs related to the payment."""

    model = InvoiceRef.payments.through

    can_delete = False

    fields = ("invoice_link", "invoice_type")
    readonly_fields = ("invoice_link", "invoice_type")
    extra = 0

    verbose_name = _("Invoice related to payment")
    verbose_name_plural = _("Invoices related to payment")

    @admin.display(description=_("Invoice number"))
    def invoice_link(self, obj):
        """Return invoice link."""
        processor = get_processor_instance(obj.bankpayment.processor)
        if hasattr(processor, "get_invoice_url"):
            return format_html('<a href="{}">{}</a>', processor.get_invoice_url(obj.invoiceref), obj.invoiceref.number)
        else:
            return obj.invoiceref.number

    @admin.display(description=_("Invoice type"))
    def invoice_type(self, obj):
        """Return invoice type."""
        return obj.invoiceref.get_invoice_type_display()

    def has_add_permission(self, request, obj=None):
        """Read only access."""
        return False

    def get_queryset(self, request):
        """Override super's get_queryset to add locking when needed."""
        queryset = super().get_queryset(request)
        return queryset.select_for_update() if request.method == "POST" else queryset


class BankPaymentAdmin(admin.ModelAdmin):
    """Model admin for BankPayment."""

    form = BankPaymentForm

    list_display = (
        "detail_link",
        "counter_account_number",
        "variable_symbol",
        "unbreakable_amount",
        "short_transaction_date",
        "client_link",
        "description",
        "advance_invoice_link",
        "counter_account_name",
        "account",
    )

    list_filter = (
        ("state", PaymentStateListFilter),
        "account__account_name",
        "transaction_date",
    )

    readonly_fields = (
        "identifier",
        "account",
        "create_time",
        "transaction_date",
        "counter_account_number",
        "counter_account_name",
        "unbreakable_amount",
        "description",
        "state",
        "constant_symbol",
        "variable_symbol",
        "specific_symbol",
        "objective",
        "client_link",
        "processing_error",
    )

    search_fields = (
        "variable_symbol",
        "counter_account_name",
        "description",
    )

    ordering = ("-transaction_date", "-create_time")

    inlines = (InvoiceRefsInline,)

    actions = ("mark_refunded",)

    @transaction.atomic
    def changelist_view(self, request, extra_context=None):
        """Wrap super's view in a transaction."""
        return super().changelist_view(request, extra_context=extra_context)

    @transaction.atomic
    def history_view(self, request, object_id, extra_context=None):
        """Wrap super's view in a transaction."""
        return super().history_view(request, object_id, extra_context=extra_context)

    def get_queryset(self, request):
        """Override super's get_queryset to add locking."""
        # Admin interface times out when accessed while processing payments so we lock only for POST requests.
        if request.method == "POST":
            return super().get_queryset(request).select_for_update()
        else:
            return super().get_queryset(request)

    class Media:
        """Media class."""

        js = ("django_pain/js/state_colors.js",)
        css = {
            "all": ("django_pain/css/admin.css",),
        }

    def get_form(self, request, obj=None, **kwargs):
        """Filter allowed processors for manual assignment of payments."""
        form = super().get_form(request, obj, **kwargs)
        allowed_choices = []
        for processor, label in BankPayment.objective_choices():
            if not processor or request.user.has_perm("django_pain.can_manually_assign_to_{}".format(processor)):
                allowed_choices.append((processor, label))

        processor_field = deepcopy(form.base_fields["processor"])
        processor_field.choices = allowed_choices
        form.base_fields["processor"] = processor_field

        if obj is not None:
            initial_tax_date = self._get_initial_tax_date(obj.transaction_date)
            if initial_tax_date is not None:
                tax_date_field = deepcopy(form.base_fields["tax_date"])
                tax_date_field.initial = initial_tax_date
                form.base_fields["tax_date"] = tax_date_field

        return form

    @staticmethod
    def _get_initial_tax_date(payment_date):
        """Get initial tax date.

        Tax date needs to be in the same month as payment date.
        Tax date can set at most 15 days into the past.
        """
        if payment_date is None:
            # Card payment in initialized state does not have payment date yet
            return None
        today = date.today()
        if payment_date > today:
            # payment_date is from the future
            # manual correction needed
            return None
        elif (today - payment_date).days <= 15:
            # payment_date is recent, use it as a tax date
            return payment_date
        elif (today.year * 12 + today.month) - (payment_date.year * 12 + payment_date.month) > 1:
            # payment_date is too old (not from the current or previous month)
            # manual correction needed
            return None
        elif today.month == payment_date.month:
            # payment_date is from the current month (but not within last 15 days)
            # use current date
            return today
        elif today.day > 15:
            # payment_date is from the last month and has been identified after 15th day of the current month
            # manual correction needed
            return None
        else:
            # payment_date is from the last month (and has been identified before 15th day of the current month)
            # return the last day of the last month
            return date(payment_date.year, payment_date.month, monthrange(payment_date.year, payment_date.month)[1])

    def get_fieldsets(self, request, obj=None):
        """Return form fieldsets.

        For imported or deferred payment, display form fields to manually assign
        payment. Otherwise, display payment objective.
        """
        if obj is not None and obj.processing_error:
            state = ("state", "processing_error")  # type: ignore
        else:
            state = "state"  # type: ignore

        if obj is not None and obj.state in (PaymentState.READY_TO_PROCESS, PaymentState.DEFERRED):
            return [
                (
                    None,
                    {
                        "fields": (
                            "counter_account_number",
                            "transaction_date",
                            "constant_symbol",
                            "variable_symbol",
                            "specific_symbol",
                            "unbreakable_amount",
                            "description",
                            "counter_account_name",
                            "create_time",
                            "account",
                            state,
                        )
                    },
                ),
                (_("Assign payment"), {"fields": ("processor", "client_id", "tax_date")}),
            ]
        else:
            return [
                (
                    None,
                    {
                        "fields": (
                            "counter_account_number",
                            "objective",
                            "client_link",
                            "transaction_date",
                            "constant_symbol",
                            "variable_symbol",
                            "specific_symbol",
                            "unbreakable_amount",
                            "description",
                            "counter_account_name",
                            "create_time",
                            "account",
                            state,
                        )
                    },
                ),
            ]

    @admin.display(description="")
    def detail_link(self, obj):
        """Object detail link."""
        return format_html(
            '<div class="state_{}"></div><img src="{}" class="open-detail-icon" />',
            PaymentState(obj.state).value,
            static("django_pain/images/folder-open.svg"),
        )

    @admin.display(description=_("Amount"))
    def unbreakable_amount(self, obj):
        """Correctly formatted amount with unbreakable spaces."""
        return format_price(obj.amount)

    @admin.display(description=_("Date"))
    def short_transaction_date(self, obj):
        """Short transaction date."""
        return date_format(obj.transaction_date, format="SHORT_DATE_FORMAT") if obj.transaction_date else ""

    @admin.display(description=_("Invoice"))
    def advance_invoice_link(self, obj):
        """Display advance invoice link.

        If there are any other invoices, number of remaining (not displayed)
        invoices is displayed as well.
        """
        advance_invoice = obj.advance_invoice
        invoices_count = obj.invoice_refs.count()
        if advance_invoice is not None:
            processor = get_processor_instance(obj.processor)
            if hasattr(processor, "get_invoice_url"):
                link = format_html(
                    '<a href="{}">{}</a>', processor.get_invoice_url(advance_invoice), advance_invoice.number
                )
            else:
                link = advance_invoice.number

            if invoices_count > 1:
                link = format_html("{}&nbsp;(+{})", link, invoices_count - 1)

            return link
        else:
            if invoices_count > 0:
                return "(+{})".format(invoices_count)
            else:
                return ""

    @admin.display(description=_("Client ID"))
    def client_link(self, obj):
        """Client link."""
        client = getattr(obj, "client_ref", None)
        if client is not None:
            processor = get_processor_instance(obj.processor)
            if hasattr(processor, "get_client_url"):
                return format_html('<a href="{}">{}</a>', processor.get_client_url(client), client.handle)
            else:
                return client.handle
        else:
            return ""

    def has_add_permission(self, request):
        """Forbid adding new payments through admin interface."""
        return False

    def has_delete_permission(self, request, obj=None):
        """Forbid deleting payments through admin interface."""
        return False

    @admin.action(permissions=["change"])
    def mark_refunded(self, request, queryset):
        """Mark selected payments as refunded."""
        marked = queryset.filter(state__in=[PaymentState.TO_REFUND, PaymentState.DEFERRED]).update(
            state=PaymentState.REFUNDED
        )
        self.message_user(request, _("Payments marked as refunded") + ": " + str(marked), messages.SUCCESS)


class PaymentImportHistoryAdmin(admin.ModelAdmin):
    """Model admin for PaymentImportHistory."""

    list_display = ("start_datetime", "origin", "filenames", "errors", "finished", "success")
    fields = ("start_datetime", "origin", "filenames", "errors", "finished", "success")
    readonly_fields = ("start_datetime", "origin", "filenames", "errors", "finished", "success")

    ordering = ("-start_datetime",)
    actions = None

    def has_add_permission(self, request, obj=None):
        """Set add permission."""
        return False

    def has_change_permission(self, request, obj=None):
        """Set change permission."""
        return False

    def has_delete_permission(self, request, obj=None):
        """Set delete permission."""
        return False


class CustomerAdmin(admin.ModelAdmin):
    """Model admin for Customer."""

    list_display = ("uid", "name", "organization", "street1", "city", "country_code")
    fields = (
        "uid",
        "name",
        "organization",
        "street1",
        "street2",
        "street3",
        "city",
        "postal_code",
        "state_or_province",
        "country_code",
        "email",
        "phone_number",
        "company_registration_number",
        "vat_number",
        "vies_vat_valid",
        "vies_checked_at",
    )
    actions = None
    search_fields = (
        "uid",
        "name",
        "organization",
        "street1",
        "street2",
        "street3",
        "city",
        "email",
        "phone_number",
        "company_registration_number",
        "vat_number",
    )

    class Media:
        """Media class."""

        css = {"all": ("django_pain/css/admin.css",)}

    def has_add_permission(self, request, obj=None):
        """Set add permission."""
        return False

    def has_change_permission(self, request, obj=None):
        """Set change permission."""
        return False

    def has_delete_permission(self, request, obj=None):
        """Set delete permission."""
        return False


class PaymentRequestItemInline(admin.TabularInline):
    """An inline model admin for payment request."""

    model = PaymentRequestItem

    can_delete = False

    fields = ("name", "item_type", "quantity", "unit_price", "item_price")
    readonly_fields = fields

    verbose_name = _("Item")
    verbose_name_plural = _("Items")

    @admin.display(description=_("Price per unit"))
    def unit_price(self, obj: PaymentRequestItem) -> str:
        """Format total amount to pay."""
        return format_price(Money(obj.price_per_unit, obj.payment_request.currency))

    @admin.display(description=_("Price total"))
    def item_price(self, obj: PaymentRequestItem) -> str:
        """Format total amount to pay."""
        return format_price(Money(obj.price_total, obj.payment_request.currency))

    def has_add_permission(self, request, obj=None):
        """Read only access."""
        return False

    def has_change_permission(self, request, obj=None):
        """Set change permission."""
        return False


class PaymentRequestAdmin(admin.ModelAdmin):
    """Model admin for PaymentRequest."""

    list_display = ("detail_link", "app_id", "customer_fmt", "price", "variable_symbol", "due_at_displayed", "status")
    actions = None
    readonly_fields = (
        "uuid",
        "app_id",
        "customer",
        "price",
        "variable_symbol",
        "bank_account",
        "created_at",
        "due_at",
        "due_at_displayed",
        "realized_payment",
        "closed_at",
        "invoice",
        "status",
        "name",
        "organization",
        "street1",
        "street2",
        "street3",
        "city",
        "postal_code",
        "state_or_province",
        "country_code",
        "company_registration_number",
        "vat_number",
    )
    ordering = ("-created_at",)
    list_filter = (
        "app_id",
        PaymentRequestStatusFilter,
    )
    search_fields = ("price_to_pay", "variable_symbol", "name", "organization", "items__name")
    inlines = (PaymentRequestItemInline,)
    form = PaymentRequestForm

    def get_fieldsets(self, request, obj=None):
        """Return form fieldsets."""
        fieldsets = [
            (
                None,
                {
                    "fields": (
                        "uuid",
                        "app_id",
                        "customer",
                        "price",
                        "variable_symbol",
                        "bank_account",
                        "created_at",
                        "due_at",
                        "due_at_displayed",
                        "realized_payment",
                        "closed_at",
                        "invoice",
                    ),
                },
            ),
            (
                _("Customer"),
                {
                    "fields": (
                        "name",
                        "organization",
                        "street1",
                        "street2",
                        "street3",
                        "city",
                        "postal_code",
                        "state_or_province",
                        "country_code",
                        "company_registration_number",
                        "vat_number",
                    ),
                    "classes": ("collapse",),
                },
            ),
        ]
        if obj is not None and obj.closed_at is None:
            fieldsets.append(
                (_("Close payment request").upper(), {"fields": ("close",)}),
            )
        return fieldsets

    class Media:
        """Media class."""

        css = {"all": ("django_pain/css/admin.css",)}

    @admin.display(description="")
    def detail_link(self, obj: PaymentRequest) -> str:
        """Object detail link."""
        return format_html('<img src="{}" class="open-detail-icon" />', static("django_pain/images/folder-open.svg"))

    @admin.display(description=_("Customer"))
    def customer_fmt(self, obj: PaymentRequest) -> str:
        """Format customer name and organization."""
        return ", ".join([i for i in (obj.name, obj.organization) if i])

    @admin.display(description=_("Price to pay"))
    def price(self, obj: PaymentRequest) -> str:
        """Format total amount to pay."""
        return format_price(Money(obj.price_to_pay, obj.currency))

    @admin.display(description=_("Status"))
    def status(self, obj: PaymentRequest) -> str:
        """Return status of payment request."""
        if obj.realized_payment:
            return _(PaymentRequestStatus.REALIZED.label)
        elif obj.closed_at:
            return _(PaymentRequestStatus.CLOSED.label)
        elif timezone.now() > obj.due_at_displayed:
            return _(PaymentRequestStatus.OVERDUE.label)
        else:
            return _(PaymentRequestStatus.WAITING.label)

    def has_add_permission(self, request, obj=None):
        """Set add permission."""
        return False

    def has_delete_permission(self, request, obj=None):
        """Set delete permission."""
        return False

    @transaction.atomic
    def changelist_view(self, request, extra_context=None):
        """Wrap super's view in a transaction."""
        return super().changelist_view(request, extra_context=extra_context)

    @transaction.atomic
    def history_view(self, request, object_id, extra_context=None):
        """Wrap super's view in a transaction."""
        return super().history_view(request, object_id, extra_context=extra_context)

    def get_queryset(self, request):
        """Override super's get_queryset to add locking."""
        queryset = super().get_queryset(request)
        return queryset.select_for_update() if request.method == "POST" else queryset


class InvoiceItemInline(admin.TabularInline):
    """An inline model admin for invoice."""

    model = InvoiceItem

    can_delete = False

    fields = (
        "name",
        "item_type",
        "quantity",
        "unit_price",
        "item_price_base",
        "vat_rate_fmt",
        "item_vat",
        "item_price",
    )
    readonly_fields = fields

    verbose_name = _("Item")
    verbose_name_plural = _("Items")

    @admin.display(description=_("VAT rate"))
    def vat_rate_fmt(self, obj: InvoiceItem) -> str:
        """Format VAT rate."""
        return mark_safe(f"{obj.vat_rate:.1f} %")

    @admin.display(description=_("Price per unit"))
    def unit_price(self, obj: InvoiceItem) -> str:
        """Format total amount to pay."""
        return format_price(Money(obj.price_per_unit, obj.invoice.currency))

    @admin.display(description=_("Total base price"))
    def item_price_base(self, obj: InvoiceItem) -> str:
        """Format total base price."""
        return format_price(Money(obj.price_total_base, obj.invoice.currency))

    @admin.display(description=_("Total VAT"))
    def item_vat(self, obj: InvoiceItem) -> str:
        """Format total VAT."""
        return format_price(Money(obj.price_total_vat, obj.invoice.currency))

    @admin.display(description=_("Total price with VAT"))
    def item_price(self, obj: InvoiceItem) -> str:
        """Format total price."""
        return format_price(Money(obj.price_total, obj.invoice.currency))

    def has_add_permission(self, request, obj=None):
        """Read only access."""
        return False

    def has_change_permission(self, request, obj=None):
        """Set change permission."""
        return False


class InvoiceAdmin(admin.ModelAdmin):
    """Model admin for Invoice."""

    list_display = ("number", "app_id", "customer_fmt", "price_total_fmt", "variable_symbol", "tax_at")
    actions = None
    fieldsets = [
        (
            None,
            {
                "fields": (
                    "number",
                    "app_id",
                    "customer",
                    "price_total_base_fmt",
                    "price_total_vat_fmt",
                    "price_total_fmt",
                    "variable_symbol",
                    "created_at",
                    "due_at",
                    "tax_at",
                    "paid_at",
                    "bank_account",
                    "payment_type",
                ),
            },
        ),
        (
            _("Customer"),
            {
                "fields": (
                    "name",
                    "organization",
                    "street1",
                    "street2",
                    "street3",
                    "city",
                    "postal_code",
                    "state_or_province",
                    "country_code",
                    "company_registration_number",
                    "vat_number",
                ),
                "classes": ("collapse",),
            },
        ),
    ]
    readonly_fields = (
        "number",
        "app_id",
        "customer",
        "variable_symbol",
        "price_total_base_fmt",
        "price_total_vat_fmt",
        "price_total_fmt",
        "created_at",
        "due_at",
        "tax_at",
        "paid_at",
        "bank_account",
        "payment_type",
        "name",
        "organization",
        "street1",
        "street2",
        "street3",
        "city",
        "postal_code",
        "state_or_province",
        "country_code",
        "company_registration_number",
        "vat_number",
    )
    ordering = ("-number",)
    list_filter = ("app_id", "tax_at")
    search_fields = ("number", "variable_symbol", "price_total", "name", "organization", "items__name")
    inlines = (InvoiceItemInline,)

    class Media:
        """Media class."""

        css = {"all": ("django_pain/css/admin.css",)}

    @admin.display(description=_("Customer"))
    def customer_fmt(self, obj: Invoice) -> str:
        """Format customer name and organization."""
        return ", ".join([i for i in (obj.name, obj.organization) if i])

    @admin.display(description=_("Total base price"))
    def price_total_base_fmt(self, obj: Invoice) -> str:
        """Format total base price."""
        return format_price(Money(obj.price_total_base, obj.currency))

    @admin.display(description=_("Total VAT"))
    def price_total_vat_fmt(self, obj: Invoice) -> str:
        """Format total VAT."""
        return format_price(Money(obj.price_total_vat, obj.currency))

    @admin.display(description=_("Total price with VAT"))
    def price_total_fmt(self, obj: Invoice) -> str:
        """Format total price with VAT."""
        return format_price(Money(obj.price_total, obj.currency))

    def has_add_permission(self, request, obj=None):
        """Set add permission."""
        return False

    def has_change_permission(self, request, obj=None):
        """Set change permission."""
        return False

    def has_delete_permission(self, request, obj=None):
        """Set delete permission."""
        return False

    @transaction.atomic
    def changelist_view(self, request, extra_context=None):
        """Wrap super's view in a transaction."""
        return super().changelist_view(request, extra_context=extra_context)


class UserAdmin(DjangoUserAdmin):
    """Model admin for Django user."""

    add_form = UserCreationForm

    add_fieldsets = (
        (
            None,
            {
                "fields": ("username",),
            },
        ),
        (
            _("Password"),
            {
                "description": _(
                    "If you use external authentication system such as LDAP, " "you don't have to choose a password."
                ),
                "fields": (
                    "password1",
                    "password2",
                ),
            },
        ),
    )

    def save_model(self, request, obj, form, change):
        """If password isn't provided, set unusable password."""
        if not change and (not form.cleaned_data["password1"] or not obj.has_usable_password()):
            obj.set_unusable_password()
        super().save_model(request, obj, form, change)
