#
# Copyright (C) 2024  CZ.NIC, z. s. p. o.
#
# This file is part of FRED.
#
# FRED is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# FRED is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with FRED.  If not, see <https://www.gnu.org/licenses/>.

"""TEDB client - VAT rates downloader."""

from datetime import date
from logging import getLogger
from pathlib import Path
from typing import Any, Dict, List, Optional, Union

from zeep import Client as SoapClient, Transport
from zeep.exceptions import Error, Fault

from django_pain.utils import country_code_to_vat_prefix

LOGGER = getLogger(__name__)

VAT_RATES_WSDL = "https://ec.europa.eu/taxation_customs/tedb/ws/VatRetrievalService.wsdl"
VAT_RATES_TIMEOUT = 10.0  # seconds
MAX_CPA_CODES = 1000  # maximum of CPA codes in one SOAP service call

# Zeep creates instances based on XSD
#  - Service messages: https://ec.europa.eu/taxation_customs/tedb/ws/VatRetrievalServiceMessage.xsd
#  - Message schemas: https://ec.europa.eu/taxation_customs/tedb/ws/VatRetrievalServiceType.xsd
VatRateResult = Any


class TedbClient:
    """Downloader of VAT rate records from EU SOAP service."""

    def __init__(self, wsdl: Union[str, Path], timeout: float = VAT_RATES_TIMEOUT) -> None:
        LOGGER.info("Creating SOAP client: %s", wsdl)
        transport = Transport(timeout=timeout, operation_timeout=timeout)
        self._client = SoapClient(str(wsdl), transport=transport)

    def _error_message(self, error: Fault) -> str:
        """Return error message from SOAP exception."""
        message = f"SOAP error: {error.message}, {error.code}"
        if error.detail is not None:  # detail data in XML element
            parts = []
            for item in error.detail.findall(
                ".//{urn:ec.europa.eu:taxud:tedb:services:v1:IVatRetrievalService:types}error"
            ):
                parts.append(": ".join(i.text for i in item))  # code, description
            message += " ({})".format(", ".join(parts))
        return message

    def _call_soap(self, params: Dict[str, Any]) -> List[VatRateResult]:
        LOGGER.info("Calling `retrieveVatRates` service with: %s", str(params))
        try:
            received = self._client.service.retrieveVatRates(**params).vatRateResults
        except Fault as e:
            raise Error(self._error_message(e)) from e
        LOGGER.info("Received %d VAT rate items from `retrieveVatRates` service", len(received))
        return received

    def _is_usable(self, item: VatRateResult) -> bool:
        # Value of VAT rate is not provided
        if item.rate.value is None:
            return False
        # Regional VAT rate
        if item.type == "REDUCED" and item.category is not None and item.category.identifier == "REGION":
            return False
        # Regional VAT rate for Spanish Canary Islands is provided as standard rate
        if item.memberState == "ES" and item.type == "STANDARD" and "Canary Islands" in (item.comment or ""):
            return False
        return True

    def get_vat_rates(
        self,
        date_from: date,
        date_to: date,
        countries: List[str],
        cpa_codes: Optional[List[str]] = None,
    ) -> List[VatRateResult]:
        """Download VAT rates from SOAP service and filter out unusable items.

        Arguments:
            date_from: start of interval to check for VAT rate changes
            date_to: end of interval
            countries: list of countries (2-letter ISO country codes) to check
            cpa_codes: CPA codes to check (all if `None`)
        """
        vat_rates: List[VatRateResult] = []

        vat_countries = [country_code_to_vat_prefix(c) for c in countries]  # service uses VAT country codes
        params = {"from": date_from.isoformat(), "to": date_to.isoformat(), "memberStates": {"isoCode": vat_countries}}
        if cpa_codes is None:
            result = self._call_soap(params)
        else:
            result = []
            for i in range(0, len(cpa_codes), MAX_CPA_CODES):
                params["cpaCodes"] = {"value": cpa_codes[i : i + MAX_CPA_CODES]}
                result.extend(self._call_soap(params))

        # filter received VAT rates by date and skip regional rates
        vat_rates = [item for item in result if date_from <= item.situationOn <= date_to and self._is_usable(item)]
        LOGGER.info("Returning %d VAT rate items from `retrieveVatRates` service", len(vat_rates))

        return vat_rates
