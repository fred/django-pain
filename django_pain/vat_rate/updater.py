#
# Copyright (C) 2024  CZ.NIC, z. s. p. o.
#
# This file is part of FRED.
#
# FRED is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# FRED is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with FRED.  If not, see <https://www.gnu.org/licenses/>.

"""VAT rates updater."""

from datetime import date
from decimal import Decimal
from logging import getLogger
from typing import Dict, Iterable, List, Optional, Sequence

from django.db import transaction

from django_pain.constants import VerbosityLevel
from django_pain.management.command_utils import Reporter
from django_pain.models.invoices import VatRate
from django_pain.utils import country_code_to_vat_prefix, local_date, local_date_start
from django_pain.vat_rate.tedb_client import VatRateResult

LOGGER = getLogger(__name__)


class VatRateUpdaterError(Exception):
    """Error in VAT rates (broken sequence, ambiguity, etc.)."""


class VatRateUpdater:
    """Update VAT rate changes for reporting and update."""

    def __init__(self, reporter: Reporter) -> None:
        self._reporter = reporter

    @transaction.atomic
    def apply(
        self,
        vat_rates: Sequence[VatRateResult],
        countries: Iterable[str],
        cpa_codes: Iterable[str],
        dry_run: bool = False,
    ) -> None:
        """Resolve changes in VAT rates and apply them to DB.

        Arguments:
            vat_rates: VAT rate changes from TEDB client to process
            countries: countries (2-letter ISO country codes) to process
            cpa_codes: CPA codes to process
            dry_run: do not apply changes to DB if `True`
        """
        LOGGER.info(
            "Called `apply` with %d items for [%s] countries and [%s] CPA codes in mode '%s'",
            len(vat_rates),
            ", ".join(countries),
            ", ".join(cpa_codes),
            "dry-run" if dry_run else "update",
        )
        self._reporter.write(f"VAT rate items to process: {len(vat_rates)}", VerbosityLevel.BASIC)
        # SOAP service provides states (and/or changes) of VAT rate (standard or reduced for CPA code)
        # at significant dates while PAIN database stores intervals of validity for given VAT rate type and value.
        # Current VAT rates in database are not modified (with exception of closing last interval), sequence of
        # VAT records is extented only.
        self._updated_vat_rates: List[VatRate] = []
        self._errors: List[str] = []

        # Process data (all countries, standard rates and all reduced rates)
        for country in countries:
            # Dates for VAT rate changes are collected with standard rate and used for evalution with reduced rates
            self._vat_change_dates: List[date] = []
            # Process standard VAT rate
            self._process_rate_sequence(vat_rates, country, None)
            # Process reduced VAT rates (for each CPA code)
            for cpa_code in cpa_codes:
                self._process_rate_sequence(vat_rates, country, cpa_code)

        self._reporter.write(f"DB records to process: {len(self._updated_vat_rates)}", VerbosityLevel.BASIC)
        for number, item in enumerate(self._updated_vat_rates, 1):
            self._reporter.write(
                f"  {number}. {'INSERT' if item.id is None else 'UPDATE'} [{item}]", VerbosityLevel.BASIC
            )

        if self._errors:  # Updates cannot be applied in case of errors
            message = f"Errors found in VAT rate items: {len(self._errors)}"
            self._reporter.write(message, VerbosityLevel.BASIC)
            for number, error in enumerate(self._errors, 1):
                self._reporter.write(f"  {number}. {error}", VerbosityLevel.EXTENDED)
            raise VatRateUpdaterError(message)

        if dry_run:
            self._reporter.write("DB not updated in dry-run mode", VerbosityLevel.BASIC)
        elif self._updated_vat_rates:
            # Apply accumulated updates
            for item in self._updated_vat_rates:
                item.save()
            LOGGER.info("DB rows afftected: %d", len(self._updated_vat_rates))
            self._reporter.write("DB successfully updated", VerbosityLevel.BASIC)

    def _process_rate_sequence(self, vat_rates: Sequence[VatRateResult], country: str, cpa_code: Optional[str]) -> None:
        # Get last VAT rate record from database
        last_vat_rate = (
            VatRate.objects.filter(country_code=country, cpa_code=cpa_code)
            .order_by("-valid_from")
            .select_for_update()
            .first()
        )
        if last_vat_rate is None:  # no applicable record yet
            last_date = None
            last_value = None
        elif last_vat_rate.valid_to is None:  # VAT rate interval valid from
            last_date = local_date(last_vat_rate.valid_from)
            last_value = last_vat_rate.value
        else:  # VAT rate interval valid to
            last_date = local_date(last_vat_rate.valid_to)
            last_value = None

        # Validate and pre-process VAT rate updates
        vat_rates_dict: Dict[date, Decimal] = {}  # {valid from: VAT rate} to skip duplicates and detect ambiguities
        for item in self._filter_vat_rates(vat_rates, country_code_to_vat_prefix(country), cpa_code, last_date):
            item_date = item.situationOn
            item_value = Decimal(item.rate.value)
            if cpa_code is not None and item_date not in self._vat_change_dates:
                error_msg = "Unexpected VAT rate change for country {} and CPA code {} on {}".format(
                    country, cpa_code, item_date
                )
                self._errors.append(error_msg)
                LOGGER.error(error_msg)
            elif item_date in vat_rates_dict and vat_rates_dict[item_date] != item_value:
                error_msg = "Ambiguous VAT rate for country {} and CPA code {} on {}: {}% vs {}%".format(
                    country, cpa_code, item_date, vat_rates_dict[item_date], item_value
                )
                self._errors.append(error_msg)
                LOGGER.error(error_msg)
            else:
                vat_rates_dict[item_date] = item_value

        if cpa_code is None:
            self._vat_change_dates = sorted(vat_rates_dict.keys())

        # Process VAT rate updates - join neighbours with same VAT rate value and prepare new records
        vat_rate = last_vat_rate
        for item_date in self._vat_change_dates:  # sorted by date
            value = vat_rates_dict.get(item_date, None)
            if value != last_value:
                item_datetime = local_date_start(item_date)
                if value is None:  # close VAT rate interval
                    vat_rate.valid_to = item_datetime
                    if vat_rate == last_vat_rate:  # update last record from DB
                        self._updated_vat_rates.append(vat_rate)
                    LOGGER.debug("Item [%s] updated", str(vat_rate))
                    vat_rate = None
                else:  # close previous (if any) and open new VAT rate interval
                    if vat_rate is not None and vat_rate.valid_to is None:
                        vat_rate.valid_to = item_datetime
                        if vat_rate == last_vat_rate:  # update last record from DB
                            self._updated_vat_rates.append(vat_rate)
                        LOGGER.debug("Item [%s] updated", str(vat_rate))
                    vat_rate = VatRate(
                        country_code=country,
                        cpa_code=cpa_code,
                        valid_from=item_datetime,
                        valid_to=None,
                        value=value,
                    )
                    self._updated_vat_rates.append(vat_rate)
                    LOGGER.debug("Item [%s] created", str(vat_rate))  # without `str()` wrong (lazy) result is logged
                last_value = value

    def _filter_vat_rates(
        self, vat_rates: Sequence[VatRateResult], country: str, cpa_code: Optional[str], date_from: Optional[date]
    ) -> Iterable[VatRateResult]:
        for item in vat_rates:
            if (item.memberState != country) or (date_from is not None and item.situationOn < date_from):
                continue
            item_cpa_codes = item.cpaCodes.code if item.cpaCodes is not None else []
            if (cpa_code is None and item.type == "STANDARD") or (
                cpa_code is not None and any([cpa_code.startswith(i.value) for i in item_cpa_codes])
            ):
                yield item
