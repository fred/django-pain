#
# Copyright (C) 2024  CZ.NIC, z. s. p. o.
#
# This file is part of FRED.
#
# FRED is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# FRED is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with FRED.  If not, see <https://www.gnu.org/licenses/>.

"""Container and items for Helios references."""

from typing import Any, Dict

from lxml import etree


class HeliosRefItem:
    """Base item of Helios container."""

    def __init__(self, tag_name: str, data: Dict[str, Any]) -> None:
        if not data:
            raise ValueError("`data` must not be empty dictionary")
        self._tag_name = tag_name
        self._data = data
        self._index: int = 0

    def __repr__(self) -> str:
        # First value in `data` is used for item placement in container
        key = next(iter(self._data.values()))
        return f"<HeliosRefItem({self._index}) @ [{self._tag_name}][{key}]>"

    def __str__(self) -> str:
        """String representation of item."""
        return f"FK_{self._index + 1}"  # based on index (starting at 1)

    def add_data(self, key: str, value: Any) -> None:
        """Add new key-value pair to item data."""
        old_value = self._data.get(key)
        if old_value is None:
            self._data[key] = value
        elif str(old_value) != str(value):
            raise ValueError("`key` is already present")

    def serialize(self, node: etree._Element) -> None:
        """Serialize item to XML node."""
        item_node = etree.SubElement(node, "Polozka")
        etree.SubElement(item_node, "Klic").text = str(self)  # reference by index
        for key, value in self._data.items():
            if value is not None:
                etree.SubElement(item_node, key).text = str(value)  # item tags


class HeliosRefContainer:
    """Container to hold items in two-layer hierarchy with unique referenceable index."""

    def __init__(self) -> None:
        self._items: Dict[str, Dict] = {}
        self._indexer: int = 0

    def insert(self, tag_name: str, data: Dict[str, Any]) -> HeliosRefItem:
        """Create item and insert it into container if not already present.

        Return item instance, either newly created or stored.
        """
        # First value in `data` is used for item placement in container
        if not data:
            raise ValueError("`data` must not be empty dictionary")
        key = next(iter(data.values()))
        if key not in self._items.setdefault(tag_name, {}):
            # Create and store new item
            item = HeliosRefItem(tag_name, data)
            self._items[tag_name][key] = item
            item._index = self._indexer  # assign unique index
            self._indexer += 1
        else:
            # Use item found in container
            item = self._items[tag_name][key]
        return item

    def serialize(self, node: etree._Element) -> None:
        """Serialize whole container content to XML node."""
        if not self._items:
            return  # nothing to do for empty container
        ref_node = etree.SubElement(node, "Reference")
        for group in self._items:
            group_node = etree.SubElement(ref_node, group)
            for item in self._items[group].values():
                item.serialize(group_node)
