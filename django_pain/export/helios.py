#
# Copyright (C) 2024  CZ.NIC, z. s. p. o.
#
# This file is part of FRED.
#
# FRED is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# FRED is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with FRED.  If not, see <https://www.gnu.org/licenses/>.

"""Invoice export to Helios."""

from abc import ABC, abstractmethod
from datetime import date, datetime
from decimal import Decimal
from itertools import chain
from typing import Any, Dict, Generator, Mapping, Sequence, Tuple, Type

from django_countries import countries
from lxml import etree

from django_pain.constants import BANK_INSTITUTIONS
from django_pain.models import Invoice, InvoiceItem
from django_pain.settings import is_eu_member
from django_pain.utils import country_code_to_vat_prefix, round_money_amount, vat_number_to_country_code

from .helios_ref import HeliosRefContainer, HeliosRefItem

HeliosDataItem = Tuple[str, Any]
HeliosData = Tuple[HeliosDataItem, ...]

HELIOS_XML_TEMPLATE = (
    '<HeliosIQ_1 xmlns="urn:schemas-microsoft-com:xml-data">'
    "  <header>"
    "    <manifest>"
    "      <document>"
    "        <name>PohyboveDoklady</name>"
    "        <description>internal-format-HELIOS:lcs_cz:PohyboveDoklady</description>"
    "        <version>020020140302</version>"
    "      </document>"
    "    </manifest>"
    "  </header>"
    "  <body/>"
    "</HeliosIQ_1>"
)


class BaseInvoiceSerializer(ABC):
    """Common base for invoice serializers - convert invoice data into XML tree.

    Invoice serializers are dedicated to invoice application type. Different invoice item types are handled by methods
    of appropriately formed name. Subclasses should meaningfully implement `_invoice_header` and `_invoice_item_*`
    methods.
    """

    def __init__(self, ref_container: HeliosRefContainer) -> None:
        self.ref_container = ref_container

    @abstractmethod
    def _invoice_header(self, invoice: Invoice) -> HeliosData:
        """Prepare data from invoice header."""

    def _invoice_item(self, invoice: Invoice, item: InvoiceItem) -> HeliosData:
        """Prepare data from invoice item."""
        # Use handler based on item type
        handler_name = f"_invoice_item_{item.item_type}"
        handler = getattr(self, handler_name, None)
        if handler is not None and callable(handler):
            return handler(invoice, item)

        raise ValueError(
            f"Processing of invoice item type `{item.item_type}` not defined in `{self.__class__.__name__}`"
        )

    def _serialize_data(self, data: HeliosData, node: etree._Element) -> None:
        """Serialize data into XML tree."""
        for key, value in data:
            if value is None:
                continue  # skip value
            if isinstance(value, tuple):
                self._serialize_data(value, etree.SubElement(node, key))  # recursive serialization
                continue
            if isinstance(value, datetime):
                value = value.strftime("%Y-%m-%d %H:%M:%S")  # without time zone
            elif isinstance(value, date):
                value = value.isoformat() + " 00:00:00"  # explicitely add time
            elif not isinstance(value, str):
                value = str(value)  # use string representation
            item_node = etree.SubElement(node, key)
            if value != "":
                item_node.text = value

    def serialize(self, invoice: Invoice) -> etree._Element:
        """Add invoice data (i.e. invoice header and all items) to XML tree."""
        node = etree.Element("PohyboveDoklady")
        self._serialize_data(self._invoice_header(invoice), node)
        items = InvoiceItem.objects.filter(invoice=invoice.pk)
        for item in items:
            self._serialize_data(self._invoice_item(invoice, item), etree.SubElement(node, "TabPohybyZbozi"))
        return node


class HeliosExporter:
    """Export batch of invoices as serialized XML."""

    def __init__(self, invoice_serializers: Mapping[str, Type[BaseInvoiceSerializer]]) -> None:
        self.invoice_serializers = invoice_serializers
        self._clear()

    def _clear(self) -> None:
        self.xml = etree.XML(HELIOS_XML_TEMPLATE)
        self.body = self.xml.find("{urn:schemas-microsoft-com:xml-data}body")  # tag to hold invoces and references
        assert self.body is not None
        self.ref_container = HeliosRefContainer()

    def add_invoice(self, invoice: Invoice) -> None:
        """Add invoice data to XML tree."""
        invoice_serializer_cls = self.invoice_serializers.get(invoice.app_id)
        if invoice_serializer_cls is None:
            raise ValueError(f"No invoice serializer defined for `{invoice.app_id}`")
        invoice_serializer = invoice_serializer_cls(self.ref_container)
        self.body.append(invoice_serializer.serialize(invoice))

    def export(self) -> bytes:
        """Export current XML tree."""
        # Add references container to XML tree
        self.ref_container.serialize(self.body)
        # Use readable formatting
        etree.indent(self.xml)
        result = etree.tostring(self.xml, xml_declaration=True, encoding="UTF-8")
        # Clear XML tree and references container
        self._clear()

        return result


# ---------------------------------------------------------------------------------------------
# Application (as of `invoice.app_id`) specific XML serializers

# Note: Sorry for a terrible mix of English and Czech but the circumstances push too hard :-(.


class InvoiceSerializer(BaseInvoiceSerializer):
    """Invoice serializer with some helpers."""

    FA_VYDANA = 13  # DruhPohybuZbo: faktura vydaná
    ZAOKROHLENI_FAKTURY = 0  # ZaokrouhleniFak, ZaokrouhleniFakVal
    ZAOKROUHLENI_DPH = 2  # na 2 desetinná místa
    CIS_ORG_OFFSET = 60_000
    EEA_COUNTRIES = [  # member countries of EEA ouside EU
        "NO",  # Norway
        "IS",  # Iceland
        "LI",  # Liechtenstein
    ]

    def _ref_currency(self, code: str) -> HeliosRefItem:
        data = {
            "Kod": code,
            "Nazev": "Česká koruna" if code == "CZK" else code,
            "MinNasobek": 0,
            "ZaokrouhleniFaktury": self.ZAOKROHLENI_FAKTURY,
        }
        return self.ref_container.insert("TabKodMen", data)

    def _ref_vat_rate(self, rate: Decimal) -> HeliosRefItem:
        data = {
            "Sazba": f"{float(rate):g}",
            "Nazev": f"Sazba {float(rate):g} %",
        }
        return self.ref_container.insert("TabDPH", data)

    def _ref_country(self, country_code: str) -> HeliosRefItem:
        data = {
            "ISOKod": country_code,
            "Nazev": "Česká republika" if country_code == "CZ" else countries.name(country_code),
            "CelniKod": country_code_to_vat_prefix(country_code),
            "ISOKodDIC": country_code,
            "EU": 1 if is_eu_member(country_code) else 0,
            "EEA": 1 if is_eu_member(country_code) or country_code in self.EEA_COUNTRIES else 0,
        }
        return self.ref_container.insert("TabZeme", data)

    def _vat_summary(self, items: Sequence[InvoiceItem]) -> Generator[HeliosDataItem, None, None]:
        # Accumulate data
        vat_summary: Dict[Decimal, Tuple[Decimal, Decimal]] = {}  # vat_rate: (sum_without_vat, sum_with_vat)
        for item in items:
            sum_base, sum_total = vat_summary.setdefault(item.vat_rate, (Decimal(0), Decimal(0)))
            sum_base += item.price_total_base
            sum_total += item.price_total
            vat_summary[item.vat_rate] = (sum_base, sum_total)
        # Yield results
        for i, (vat_rate, (sum_base, sum_total)) in enumerate(vat_summary.items(), 1):
            sum_base = round_money_amount(sum_base)
            sum_total = round_money_amount(sum_total)
            yield (
                "TabOZSumaceCen",
                (
                    ("Poradi", i),  # pořadové číslo od 1 výše
                    ("SazbaDPH", self._ref_vat_rate(vat_rate)),
                    ("CbezDPHOZ", sum_base),  # součet cen bez DPH položek s danou sazbou DPH, v Kč
                    ("CsDPHOZ", sum_total),  # součet cen včetně DPH položek s danou sazbou DPH, v Kč
                    ("CbezDPHValOZ", sum_base),  # součet cen bez DPH položek s danou sazbou DPH v cizí měně
                    ("CsDPHValOZ", sum_total),  # součet cen včetně DPH položek s danou sazbou DPH v cizí měně
                    ("SlevaCastkaBezDPHVal", 0),  # částka slevy bez DPH k dané sazbě DPH v cizí měně
                ),
            )


class AuctionsInvoiceSerializer(InvoiceSerializer):
    """XML serializer for `auctions`."""

    NAZEV = "Správa domény"
    NAZEV1 = "60256-AUKCE"
    REGCIS = "00055K"
    SKP = "Aukce domén .CZ"
    UKOD = 264
    NASTAVENI_SLEV = 4681
    SKUP_ZBO = 900
    SKUP_ZBO_NAZEV = "Služby"
    CISLO_SKLADU = "001"
    NAZEV_SKLADU = "Středisko 001"
    DRUH_SKLADU = 1
    POHYB_SKLADU = 1
    ZAKLAD_SDVSJ = 0
    JIZ_NA_SKLADE = 1
    CISLO_KONTACE = 264
    ORG_SLEVA = 2
    BANK_SPOJENI = {
        "name": "ČSOB - aukce",
        "number": "450044",
        "bank": "0300",
        "internal_id": "22187",
        "internal_name": "Aukce_bankovní účet",
    }
    RADA_DOKLADU_NAZEV = "FV-aukce domén"
    OSS_DATA = {  # country_code: (CisloKontace, CisloOrg) - data provided by VGD
        "AT": (519, 6259),  # Austria
        "BE": (513, 6261),  # Belgium
        "BG": (524, 6307),  # Bulgaria
        "CY": (536, 20536),  # Cyprus
        "DE": (527, 6337),  # Germany
        "DK": (511, 6264),  # Denmark
        "EE": (530, 6469),  # Estonia
        "ES": (517, 6258),  # Spain
        "FI": (526, 6336),  # Finland
        "FR": (518, 6254),  # France
        "GR": (532, 6782),  # Greece
        "HR": (533, 6800),  # Croatia
        "HU": (531, 6528),  # Hungary
        "IE": (525, 6308),  # Ireland
        "IT": (509, 6260),  # Italy
        "LT": (529, 6468),  # Lithuania
        "LU": (528, 6388),  # Luxembourg
        "LV": (534, 20534),  # Latvia
        "MT": (535, 20535),  # Malta
        "NL": (512, 6256),  # Netherlands
        "PL": (522, 6282),  # Poland
        "PT": (510, 6257),  # Portugal
        "RO": (521, 6281),  # Romania
        "SE": (523, 6306),  # Sweden
        "SI": (514, 6255),  # Slovenia
        "SK": (508, 6367),  # Slovakia
    }

    def _oss_client_data(self, country_code: str) -> Dict[str, Any]:
        # In OSS mode fictitious client (one for each involved country) is used instead of real customer
        return {
            "CisloOrg": self.OSS_DATA[country_code][1],
            "Nazev": f"{country_code}, OSS, soukromá osoba",
            "JeOdberatel": 1,
            "PravniForma": 2,
            "IdZeme": self._ref_country(country_code),
            "SlevaSozNa": self.ORG_SLEVA,
            "SlevaSkupZbo": self.ORG_SLEVA,
            "SlevaKmenZbo": self.ORG_SLEVA,
            "SlevaStavSkladu": self.ORG_SLEVA,
            "SlevaZbozi": self.ORG_SLEVA,
            "SlevaOrg": self.ORG_SLEVA,
            "NespolehPlatce": 3,
            "AktZWebuNespolehPlatce": 1,
            "AktZWebuZverejBankUcty": 1,
        }

    def _invoice_tab_doklady(self, invoice: Invoice, item_0: InvoiceItem) -> HeliosData:
        # OSS mode (foreign EU customer without VAT number) has special treatment of som items
        oss_mode = is_eu_member(invoice.country_code) and invoice.country_code != "CZ" and invoice.vat_number is None
        data_zakazka = {
            "CisloZakazky": self.NAZEV,
            "Nazev": self.NAZEV,
        }
        ref_zakazka = self.ref_container.insert("TabZakazka", data_zakazka)
        if oss_mode:
            data_u_kod = {"CisloKontace": self.OSS_DATA[invoice.country_code][0]}
        else:
            data_u_kod = {"CisloKontace": self.CISLO_KONTACE}
        ref_u_kod = self.ref_container.insert("TabUKod", data_u_kod)
        data_rada_dokladu = {
            "RadaDokladu": invoice.number[:3],
            "Nazev": self.RADA_DOKLADU_NAZEV,
            "DruhPohybuZbo": self.FA_VYDANA,
            "PohybSkladu": self.POHYB_SKLADU,
            "DefOKReal": 1,
            "DefOKUcto": 1,
            "UKod": ref_u_kod,
        }
        ref_rada_dokladu = self.ref_container.insert("TabDruhDokZbo", data_rada_dokladu)
        data_nakladovy_okruh = {
            "Cislo": self.NAZEV,
            "Nazev": self.NAZEV,
        }
        ref_nakladovy_okruh = self.ref_container.insert("TabNakladovyOkruh", data_nakladovy_okruh)
        price_total = round_money_amount(invoice.price_total)
        ref_dic = None
        if oss_mode:
            data_cis_org = self._oss_client_data(invoice.country_code)
        else:
            data_cis_org = {
                "CisloOrg": invoice.customer.pk + self.CIS_ORG_OFFSET,
                "Nazev": ", ".join([i for i in (invoice.name, invoice.organization) if i]),
                "Fakturacni": 1,
                "PSC": invoice.postal_code,
                "Ulice": ", ".join([i for i in (invoice.street1, invoice.street2, invoice.street3) if i is not None]),
                "Misto": invoice.city,
                "ICO": invoice.company_registration_number,
                "DIC": ref_dic,
                "TIN": None if is_eu_member(invoice.country_code) else invoice.vat_number,
                "IdZeme": None if invoice.country_code == "CZ" else self._ref_country(invoice.country_code),
                "SlevaSozNa": self.ORG_SLEVA,
                "SlevaSkupZbo": self.ORG_SLEVA,
                "SlevaKmenZbo": self.ORG_SLEVA,
                "SlevaStavSkladu": self.ORG_SLEVA,
                "SlevaZbozi": self.ORG_SLEVA,
                "SlevaOrg": self.ORG_SLEVA,
            }
        ref_cis_org = self.ref_container.insert("TabCisOrg", data_cis_org)
        if invoice.vat_number is not None and is_eu_member(invoice.country_code):
            data_dic = {
                "DIC": invoice.vat_number,
                "CisloOrg": ref_cis_org,
                "ISOZeme": self._ref_country(vat_number_to_country_code(invoice.vat_number)),
                "AktualniDIC": 1,
            }
            ref_dic = self.ref_container.insert("TabDICOrg", data_dic)
            ref_cis_org.add_data("DIC", ref_dic)
        ref_id_ustavu = None
        ref_cilova_zeme = None
        bank_data = BANK_INSTITUTIONS.get(self.BANK_SPOJENI["bank"])
        if bank_data:  # pragma: no branch
            data_id_ustavu = {
                "KodUstavu": self.BANK_SPOJENI["bank"],
                "AlfaKodUstavu": bank_data["alpha"],
                "NazevUstavu": bank_data["name"],
                "SWIFTUstavu": bank_data["swift"],
            }
            ref_id_ustavu = self.ref_container.insert("TabPenezniUstavy", data_id_ustavu)
            ref_cilova_zeme = self._ref_country(bank_data["country"])
        data_id_bank_spoj = {
            "NazevBankSpoj": self.BANK_SPOJENI["name"],
            "CisloUctu": self.BANK_SPOJENI["number"],
            "Prednastaveno": 1,
            "IDOrg": self.ref_container.insert("TabCisOrg", {"CisOrg": None}),
            "Mena": self._ref_currency(invoice.currency),
            "UcetniUcet": self.BANK_SPOJENI["internal_id"],
            "Popis": self.BANK_SPOJENI["internal_name"],
            "IDUstavu": ref_id_ustavu,
            "CilovaZeme": ref_cilova_zeme,
        }
        ref_id_bank_spoj = self.ref_container.insert("TabBankSpojeni", data_id_bank_spoj)
        return (
            ("DatPorizeni", invoice.tax_at),
            ("DUZP", invoice.tax_at),
            ("Splatnost", invoice.due_at),
            ("CastkaZaoValDoKc", 0),  # částka zaokrouhlení valuty přepočtená do hlavní měny
            ("CisloOrg", ref_cis_org),
            ("CisloZakazky", ref_zakazka),
            ("DatPovinnostiFa", invoice.created_at),  # datum vložení faktury do systému
            ("DatRealizace", invoice.paid_at),
            ("DIC", ref_dic),
            ("DodFak", invoice.number),  # číslo daňového dokladu
            ("IDBankSpoj", ref_id_bank_spoj),
            ("KoeficientDPH", 1),  # koeficient DPH, údaj z tabulky TabDokZboDodatek
            ("Mena", self._ref_currency(invoice.currency)),
            ("NavaznaObjednavka", invoice.variable_symbol),  # číslo dodavatelské faktury
            ("NOkruhCislo", ref_nakladovy_okruh),
            ("PoziceZaokrDPH", self.ZAOKROUHLENI_DPH),  # způsob zaokrouhlení DPH Položky
            ("PoziceZaokrDPHHla", self.ZAOKROUHLENI_DPH),
            ("PoziceZaokrDPHTxt", self.ZAOKROUHLENI_DPH),
            ("Poznamka", "Vydražení aukce"),
            ("RadaDokladu", ref_rada_dokladu),
            ("SazbaDPH", self._ref_vat_rate(item_0.vat_rate)),  # weird but required
            ("SumaKc", price_total),  # celková částka v Kč za doklad po odečtení zálohy
            ("SumaVal", price_total),  # celková částka ve valutách za doklad po odečtení zálohy
            ("UKod", ref_u_kod),
            ("ZaokrNaPadesat", 0),  # zaokrouhlení na padesátihaléře
            ("ZaokrouhleniFak", self.ZAOKROHLENI_FAKTURY),
            ("ZaokrouhleniFakVal", self.ZAOKROHLENI_FAKTURY),
            ("RezimMOSS", 2 if oss_mode else None),
            ("ZemeDPH", self._ref_country(invoice.country_code) if oss_mode else None),
        )

    def _invoice_header(self, invoice: Invoice) -> HeliosData:
        items = invoice.items.all()
        assert items, "The invoice needs to have at least one item"
        assert invoice.currency == "CZK", "Only invoices in CZK are supported"
        return (
            ("cisloDokladu", invoice.number),
            ("DruhPohybuZbo", self.FA_VYDANA),
            ("TabDokladyZbozi", tuple(chain(self._invoice_tab_doklady(invoice, items[0]), self._vat_summary(items)))),
        )

    def _invoice_item_domain_registration_right(self, invoice: Invoice, item: InvoiceItem) -> HeliosData:
        price_total_base = round_money_amount(item.price_total_base)
        price_total = round_money_amount(item.price_total)
        price_per_unit_base = round_money_amount(item.price_per_unit)
        price_per_unit = round_money_amount(item.price_total / item.quantity)
        vat_total = round_money_amount(price_total - price_total_base)
        data_skup_zbo = {
            "SkupZbo": self.SKUP_ZBO,
            "Nazev": self.SKUP_ZBO_NAZEV,
        }
        ref_skup_zbo = self.ref_container.insert("TabSkupinyZbozi", data_skup_zbo)
        ref_skup_u_kod = self.ref_container.insert("TabSkupUKod", {"Nazev": self.NAZEV1})
        data_id_zbozi = {
            "RegCis": self.REGCIS,
            "PrepMnozstvi": item.quantity,
            "Nazev1": self.NAZEV1,
            "DruhSkladu": self.DRUH_SKLADU,
            "SkupZbo": ref_skup_zbo,
            "UKod": ref_skup_u_kod,
            "ZakladSDvSJ": self.ZAKLAD_SDVSJ,
        }
        ref_id_zbozi = self.ref_container.insert("TabKmenZbozi", data_id_zbozi)
        data_id_sklad = {
            "Cislo": self.CISLO_SKLADU,
            "Nazev": self.NAZEV_SKLADU,
            "DruhSkladu": self.DRUH_SKLADU,
        }
        ref_id_sklad = self.ref_container.insert("TabStrom", data_id_sklad)
        data_stav_skladu = {
            "JizNaSklade": self.JIZ_NA_SKLADE,
            "IDKmenZbozi": ref_id_zbozi,
            "IDSklad": ref_id_sklad,
        }
        ref_stav_skladu = self.ref_container.insert("TabStavSkladu", data_stav_skladu)
        return (
            ("DruhPohybuZbo", self.FA_VYDANA),
            ("IDZboSklad", ref_stav_skladu),
            ("Nazev1", self.NAZEV1),
            ("RegCis", self.REGCIS),  # registrační číslo
            ("SKP", self.SKP),
            ("SkupZbo", ref_skup_zbo),
            ("CCbezDaniKc", price_total_base),  # celková cena bez daní v HM
            ("CCbezDaniKcPoS", price_total_base),  # celková cena bez daní po odečtení procentních slev v Kč
            ("CCbezDaniVal", price_total_base),  # cena celkem bez SD a DPH v cizí měně
            ("CCbezDaniValPoS", price_total_base),  # celková cena bez daní po odečtení procentních slev v cizí měně
            ("CCevid", 0),  # evidenční (skladová) celková cena, doplní se při realizaci.
            ("CCevidPozadovana", 0),  # požadovaná celková evidenční (skladová) cena (na stornu příjmu, výdeje)
            ("CCsDPHKc", price_total),  # cena celkem včetně SD a DPH v Kč
            ("CCsDPHKcPoS", price_total),  # celková cena se SD a s DPH po odečtení procentních slev v Kč
            ("CCsDPHVal", price_total),  # cena celkem včetně SD a DPH v cizí měně
            ("CCsDPHValPoS", price_total),  # celková cena se SD a s DPH po odečtení procentních slev v cizí měně
            ("CCsSDKc", price_total_base),  # celková cena včetně SD, v Kč
            ("CCsSDKcPoS", price_total_base),  # celková cena se SD po odečtení procentních slev v Kč
            ("CCsSDVal", price_total_base),  # celková cena včetně SD, v cizí měně
            ("CCsSDValPoS", price_total_base),  # celková cena se SD po odečtení procentních slev v cizí měně
            ("DatPorizeni", invoice.tax_at),  # datum případu == DUZP
            ("EvMnozstvi", 0),  # průběžně napočítávaný stav skladu
            ("EvStav", 0),  # průběžně napočítávaný finanční stav skladu
            ("Hmotnost", 0),
            ("JCbezDaniKc", price_per_unit_base),  # jednotková cena bez SD a DPH v Kč
            ("JCbezDaniKcPoS", price_per_unit_base),  # jednotková cena bez daní po odečtení procentních slev v Kč
            ("JCbezDaniVal", price_per_unit_base),  # jednotková cena bez SD a DPH v cizí měně
            (
                "JCbezDaniValPoS",
                price_per_unit_base,
            ),  # jednotková cena bez daní po odečtení procentních slev v cizí měně
            ("JCsDPHKc", price_per_unit),  # jednotková cena včetně SD a DPH v Kč
            ("JCsDPHKcPoS", price_per_unit),  # jednotková cena se SD a s DPH po odečtení procentních slev v Kč
            ("JCsDPHVal", price_per_unit),  # jednotková cena včetně SD a DPH v cizí měně
            ("JCsDPHValPoS", price_per_unit),  # jednotková cena se SD a s DPH po odečtení procentních slev v cizí měně
            ("JCsSDKc", price_per_unit_base),  # jednotková cena včetně SD, v Kč
            ("JCsSDKcPoS", price_per_unit_base),  # jednotková cena se SD po odečtení procentních slev v Kč
            ("JCsSDVal", price_per_unit_base),  # jednotková cena včetně SD, v cizí měně
            ("JCsSDValPoS", price_per_unit_base),  # jednotková cena se SD po odečtení procentních slev v cizí měně
            ("KJKontrolovat", 1),  # 1 = má se provést kontrola (implicitní hodnota 0 se nepřenáší)
            ("KJSkontrolovano", 1),  # 1 = kontrola proběhla (implicitní hodnota 0 se nepřenáší)
            ("Mena", self._ref_currency(invoice.currency)),
            ("Mnozstvi", item.quantity),  # zadané množství
            ("MnozstviOdebrane", 0),  # množství odebrané převodem z objednávky
            ("MnozstviOdebraneReal", 0),  # odebrané množství - vypočtená hodnota při realizaci
            ("MnozstviReal", 0),  # množství - vypočtená hodnota při realizaci
            ("MnozstviStorno", 0),  # množství vystornované (převedením na storno doklad)
            ("MnozstviStornoReal", 0),  # stornované množství - vypočtená hodnota při realizaci
            ("NastaveniSlev", self.NASTAVENI_SLEV),
            ("Poznamka", f"Právo přednostní registrace domény {item.name}"),  # FQDN vydražené domény
            ("PrepMnozstvi", item.quantity),  # přepočet mezi MJ na dokladu a MJ v evidenci stavu skladu
            ("PrepocetMJSD", item.quantity),  # přepočet MJ ke spotřební dani
            ("SamoVyDPHZaklad", price_total_base),  # základ DPH pro samovyměření
            ("SamoVyDPHCastka", vat_total),  # částka DPH pro samovyměření
            ("SamoVyDPHZakladHM", price_total_base),  # základ DPH v hlavní měně pro samovyměření
            ("SamoVyDPHCastkaHM", vat_total),  # částka DPH v hlavní měně pro samovyměření
            ("SazbaDPH", self._ref_vat_rate(item.vat_rate)),
        )
