#
# Copyright (C) 2018-2024  CZ.NIC, z. s. p. o.
#
# This file is part of FRED.
#
# FRED is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# FRED is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with FRED.  If not, see <https://www.gnu.org/licenses/>.

"""Invoice related models."""

from datetime import date, timedelta
from decimal import Decimal
from typing import Any, Dict, Optional, Tuple

from django.core.validators import MaxValueValidator, MinValueValidator
from django.db import models, transaction
from django.db.models import CheckConstraint, Exists, OuterRef, Q, QuerySet, Subquery, UniqueConstraint
from django.utils import timezone
from django.utils.translation import gettext_lazy as _
from django_countries import countries
from django_countries.fields import CountryField
from djmoney.models.fields import CurrencyField
from filed import Storage
from typist import SecretaryClient

from django_pain.constants import (
    BANK_INSTITUTIONS,
    CURRENCY_PRECISION,
    InvoiceType,
    PaymentProcessingError,
    PaymentType,
    PdfTemplate,
)
from django_pain.settings import SETTINGS, is_eu_member
from django_pain.utils import (
    account_to_iban,
    bank_of_account,
    local_date,
    round_money_amount,
    vat_number_to_country_code,
)

from .bank import PAYMENT_TYPE_CHOICES, BankAccount, BankPayment

INVOICE_TYPE_CHOICES = (
    (InvoiceType.ADVANCE, _("advance")),
    (InvoiceType.ACCOUNT, _("account")),
)


class InvoiceRef(models.Model):
    """Invoice reference model."""

    number = models.TextField(unique=True, verbose_name=_("Invoice number"))
    remote_id = models.IntegerField()
    invoice_type = models.TextField(choices=INVOICE_TYPE_CHOICES, verbose_name=_("Invoice type"))
    payments = models.ManyToManyField(BankPayment, related_name="invoice_refs")


# ----------------------------------------------------------------------------------------------------
# Customer model


class Customer(models.Model):
    """Customer model."""

    # Unique customer identifier
    uid = models.TextField(unique=True, verbose_name=_("Customer identifier"))
    # Typically, one of `name` or `organization` is set to easily distinguish physical/legal person
    name = models.TextField(null=True, blank=True, verbose_name=_("Name"))
    organization = models.TextField(null=True, blank=True, verbose_name=_("Organization"))
    # Customer address (to use on invoice)
    street1 = models.TextField(verbose_name=_("Street"))
    street2 = models.TextField(null=True, blank=True, verbose_name=_("Street 2"))
    street3 = models.TextField(null=True, blank=True, verbose_name=_("Street 3"))
    city = models.TextField(verbose_name=_("City"))
    state_or_province = models.TextField(null=True, blank=True, verbose_name=_("State or province"))
    postal_code = models.TextField(verbose_name=_("Postal code"))
    # Country represented by its two-letter ISO code
    country_code = CountryField(verbose_name=_("Country"))
    # Email and phone number
    email = models.EmailField(null=True, blank=True, verbose_name=_("Email"))
    phone_number = models.TextField(null=True, blank=True, verbose_name=_("Phone number"))
    # Company registration number (like IČ in Czechia)
    company_registration_number = models.TextField(null=True, blank=True, verbose_name=_("Company registration number"))
    # VAT registration number (like DIČ in Czechia)
    vat_number = models.TextField(null=True, blank=True, verbose_name=_("VAT number"))
    # VAT was approved by VIES (`null` when not checked yet or not applicable)
    vies_vat_valid = models.BooleanField(null=True, blank=True, verbose_name=_("VIES approved"))
    vies_checked_at = models.DateTimeField(null=True, blank=True, verbose_name=_("Check time"))

    class Meta:
        """Meta class for customer model."""

        verbose_name = _("Customer")
        verbose_name_plural = _("Customers")

        constraints = [
            CheckConstraint(
                check=(
                    ~(Q(name__isnull=True) | Q(name__exact=""))
                    | ~(Q(organization__isnull=True) | Q(organization__exact=""))
                ),
                name="customer_name_or_organization_set",
            )
        ]

    def __str__(self):
        """Return string representation of customer."""
        return f"{self.uid}: {self.name}" if self.name else f"{self.uid}: {self.organization}"

    def update_vies_info(self, vat_valid: Optional[bool]) -> None:
        """Update (atomically) VIES related info."""
        self.vies_vat_valid = vat_valid
        self.vies_checked_at = None if vat_valid is None else timezone.now()
        self.full_clean()
        self.save(update_fields=["vies_vat_valid", "vies_checked_at"])

    @property
    def reverse_charge_mode(self) -> bool:
        """Return flag if invoices for a customer can be issued in reverse charge mode."""
        country_code = self.tax_country_code
        return self.vat_number and is_eu_member(country_code) and self.vies_vat_valid and country_code != "CZ"

    @property
    def tax_country_code(self) -> str:
        """Return country code for VAT evaluation."""
        if self.vat_number and is_eu_member(self.country_code) and self.vies_vat_valid:
            return vat_number_to_country_code(self.vat_number)
        else:
            return self.country_code


# ----------------------------------------------------------------------------------------------------
# Payment requst models


class PaymentRequestManager(models.Manager):
    """Customized manager for payment requests."""

    def find(
        self, app_id: str, payment: BankPayment
    ) -> Tuple[Optional["PaymentRequest"], Optional[PaymentProcessingError]]:
        """Find payment request related to given bank payment.

        Search is carried out on these criteria:
        - match in `app_id`
        - payment request is active, i.e. not closed
        - match in currency
        - match in variable symbol
        - match in bank account for bank transfers / in identifier for card payments

        Return value consists of two parts:
        - payment request on successful search else `None`
        - optional error shows some discrepancy in payment amount (`INSUFFICIENT_AMOUNT` and `EXCESSIVE_AMOUNT`) or
        due time (`OVERDUE`)

        Note: Bank payment should be in `READY_TO_PROCESS` or `DEFERRED` state (ensured by caller).
        """
        payment_request = None
        error = None

        q = PaymentRequest.objects.filter(app_id=app_id, closed_at__isnull=True, currency=payment.amount.currency)
        if payment.payment_type == PaymentType.TRANSFER:
            q = q.filter(variable_symbol=payment.variable_symbol, bank_account=payment.account)
        else:  # PaymentType.CARD_PAYMENT
            q = q.filter(card_payment=payment)
        q.select_for_update()
        if q:  # related payment request found
            payment_request = q.get()

            due_date = local_date(payment_request.due_at) if payment_request.due_at is not None else None
            if due_date is not None and payment.transaction_date > due_date:
                error = PaymentProcessingError.OVERDUE
            elif payment.amount.amount < payment_request.price_to_pay:
                error = PaymentProcessingError.INSUFFICIENT_AMOUNT
            elif payment.amount.amount > payment_request.price_to_pay:
                error = PaymentProcessingError.EXCESSIVE_AMOUNT

        return payment_request, error

    def filter_for_invoice(self) -> QuerySet["PaymentRequest"]:
        """Filter payment requests suitable for invoice creation.

        Criteria:
          - invoice not created yet
          - payment realized
          - no "non-invoiceable" items
          - VAT number of customer is VIES resolved for EU residents
        """
        # "not invoiceable" items in payment request
        not_invoiceable_list = BillingItemType.objects.filter(not_invoiceable=True)
        not_invoiceable_items = PaymentRequestItem.objects.filter(
            payment_request_id=OuterRef("pk"),
            item_type__in=Subquery(not_invoiceable_list.values("handle")),
        )
        # Main query with filters
        q = (
            PaymentRequest.objects.filter(
                ~Exists(not_invoiceable_items),  # no non-invoiceable items
                invoice__isnull=True,  # invoice not created yet
                realized_payment__isnull=False,  # payment is realized
            )
            .select_related("customer")
            .filter(  # VAT number validity resolved where applicable
                Q(customer__vat_number__isnull=True)  # not VAT payer
                | ~Q(customer__country_code__in=SETTINGS.eu_members)  # not EU resident
                | Q(customer__vies_vat_valid__isnull=False),  # VIES check resolved
            )
        )
        return q


class PaymentRequest(models.Model):
    """Payment request model.

    Payment request provides two options to pay:
    - bank transfer: record doesn't appear as `BankPayment` until imported from bank statement
    - card payment: created on payment gateway with immediate `BankPayment` record
    Either one can be realized (in case of card payment `card_payment` and `realized_payment` fields
    refer to the same item).
    """

    uuid = models.UUIDField(unique=True)
    customer = models.ForeignKey(
        Customer, on_delete=models.RESTRICT, editable=False, related_name="+", verbose_name=_("Customer")
    )
    app_id = models.TextField(verbose_name=_("Application ID"))
    # Copy of customer info at the moment of creation
    name = models.TextField(null=True, blank=True, verbose_name=_("Name"))
    organization = models.TextField(null=True, blank=True, verbose_name=_("Organization"))
    street1 = models.TextField(verbose_name=_("Street"))
    street2 = models.TextField(null=True, blank=True, verbose_name=_("Street 2"))
    street3 = models.TextField(null=True, blank=True, verbose_name=_("Street 3"))
    city = models.TextField(verbose_name=_("City"))
    state_or_province = models.TextField(null=True, blank=True, verbose_name=_("State or province"))
    postal_code = models.TextField(verbose_name=_("Postal code"))
    country_code = CountryField(verbose_name=_("Country"))
    company_registration_number = models.TextField(null=True, blank=True, verbose_name=_("Company registration number"))
    vat_number = models.TextField(null=True, blank=True, verbose_name=_("VAT number"))
    # Request lifetime related fields
    created_at = models.DateTimeField(auto_now_add=True, verbose_name=_("Create time"))
    due_at = models.DateTimeField(null=True, blank=True, verbose_name=_("Due time"))
    due_at_displayed = models.DateTimeField(verbose_name=_("Displayed due time"))
    closed_at = models.DateTimeField(null=True, blank=True, verbose_name=_("Close time"))
    # Total price to pay
    price_to_pay = models.DecimalField(max_digits=64, decimal_places=CURRENCY_PRECISION, verbose_name=_("Price to pay"))
    currency = CurrencyField()
    # Variable symbol
    variable_symbol = models.CharField(max_length=10, blank=True, verbose_name=_("Variable symbol"))
    # Bank account for bank transfer payment
    bank_account = models.ForeignKey(
        BankAccount,
        null=True,
        blank=True,
        on_delete=models.RESTRICT,
        related_name="+",
        db_column="ref_bank_account_id",
        verbose_name=_("Bank account"),
    )
    # Bank payment (info about card payment when requested)
    card_payment = models.ForeignKey(
        BankPayment,
        null=True,
        blank=True,
        on_delete=models.RESTRICT,
        related_name="+",
        db_column="ref_card_payment_id",
        verbose_name=_("Card payment"),
    )
    # Bank payment (info when payment was paid and successfully paired)
    realized_payment = models.ForeignKey(
        BankPayment,
        null=True,
        blank=True,
        on_delete=models.RESTRICT,
        related_name="+",
        verbose_name=_("Realized payment"),
    )
    # PDF document in file manager storage
    file_uid = models.TextField(null=True, blank=True, verbose_name="PDF")
    # Related invoice
    invoice = models.OneToOneField(
        "Invoice", on_delete=models.RESTRICT, null=True, blank=True, verbose_name=_("Invoice")
    )

    objects = PaymentRequestManager()

    class Meta:
        """Model Meta class."""

        verbose_name = _("Payment request")
        verbose_name_plural = _("Payment requests")

        constraints = [
            # Variable symbol should be unique for all live (i.e. unclosed) payment requests
            UniqueConstraint(
                fields=["variable_symbol"], condition=Q(closed_at__isnull=True), name="unique_vs_for_unclosed_payments"
            )
        ]

    def __str__(self):
        """Return string representation."""
        return (
            f'{self.uuid}/{self.customer} {round_money_amount(self.price_to_pay)} {self.currency}'
            f' at {self.due_at_displayed.strftime("%Y-%m-%d")}'
        )

    def _get_qr_code(self) -> str:
        """Return SPAYD string to generate QR code for bank transfer."""
        # Format specification at https://cbaonline.cz/upload/1645-standard-qr-v1-2-cerven-2021.pdf
        item_0 = PaymentRequestItem.objects.filter(payment_request=self.pk)[0]
        item_0_name = item_0.name[:40].strip().replace("*", "_")  # limit length and replace asterisk
        data = (
            "SPD",  # header
            "1.0",  # format version
            "ACC:{}".format(account_to_iban(self.bank_account.account_number)),  # bank account
            "AM:{}".format(round_money_amount(self.price_to_pay)),  # amount (always 2 decimal places)
            "CC:{}".format(self.currency),  # currency
            "X-VS:{}".format(self.variable_symbol),  # variable symbol
            "MSG:CZ.NIC-{}".format(item_0_name),  # message
        )
        return "*".join(data)

    def _get_pdf_context(self) -> Dict[str, Any]:
        """Prepare context data for rendering to PDF."""
        bank_data = BANK_INSTITUTIONS.get(bank_of_account(self.bank_account.account_number), {})
        items = PaymentRequestItem.objects.filter(payment_request=self.pk)
        return {
            "name": self.name,
            "organization": self.organization,
            "street": [i for i in (self.street1, self.street2, self.street3) if i is not None],
            "city": self.city,
            "state_or_province": self.state_or_province,
            "postal_code": self.postal_code,
            "country": countries.name(self.country_code),
            "company_registration_number": self.company_registration_number,
            "vat_number": self.vat_number,
            "variable_symbol": self.variable_symbol,
            "created_at": local_date(self.created_at).isoformat(),
            "due_at": local_date(self.due_at_displayed - timedelta(days=1)).isoformat(),
            "bank_account": self.bank_account.account_number,
            "price_paid": "0.00",  # Because of workflow no amount is paid in advance.
            "price_to_pay": str(round_money_amount(self.price_to_pay)),
            "currency": self.currency,
            "iban": account_to_iban(self.bank_account.account_number, human=True),
            "qr_code": self._get_qr_code(),
            "bank_name": bank_data.get("name", ""),
            "bank_swift": bank_data.get("swift", ""),
            "items": [
                {
                    "name": i.name,
                    "item_type": i.item_type,
                    "quantity": i.quantity,
                    "price_total": str(round_money_amount(i.price_total)),
                }
                for i in items
            ],
        }

    def render_pdf(self, secretary_client: SecretaryClient, file_storage: Storage) -> None:
        """Render payment request to PDF and store the document with file manager."""
        if self.file_uid is None:  # nothing to do if already rendered
            if self.country_code == "CZ":
                pdf_template = PdfTemplate.PAYMENT_REQUEST_PDF_TEMPLATE_CZ
            else:
                pdf_template = PdfTemplate.PAYMENT_REQUEST_PDF_TEMPLATE_EN
            pdf = secretary_client.render_pdf(pdf_template, self._get_pdf_context())
            with file_storage.create(f"payment_request_{self.uuid}.pdf", mimetype="application/pdf") as storage:
                storage.write(pdf)
            self.file_uid = str(storage.uuid)
            self.save(update_fields=["file_uid"])


class PaymentRequestItem(models.Model):
    """Model for payment request item."""

    # Parent payment_request
    payment_request = models.ForeignKey(PaymentRequest, on_delete=models.CASCADE, editable=False, related_name="items")
    # Descripton of the item
    name = models.TextField(verbose_name=_("Description"))
    # Item type (to evaluate VAT rate)
    item_type = models.TextField(null=True, blank=True, verbose_name=_("Item type"))
    # Quantity and pricing
    quantity = models.IntegerField(validators=(MinValueValidator(1),), default=1, verbose_name=_("Quantity"))
    price_per_unit = models.DecimalField(
        max_digits=64, decimal_places=CURRENCY_PRECISION, verbose_name=_("Price per unit")
    )
    price_total = models.DecimalField(max_digits=64, decimal_places=CURRENCY_PRECISION, verbose_name=_("Price total"))

    def __str__(self):
        """Return string representation."""
        return f"{self.payment_request.uuid}/{self.name}"


class VatRate(models.Model):
    """VAT rate for type of goods/service."""

    country_code = CountryField(verbose_name=_("Country"))
    # Type of goods/service - `null` for standard VAT rate
    cpa_code = models.TextField(null=True, blank=True, verbose_name=_("CPA code"))
    # Validity range
    valid_from = models.DateTimeField(verbose_name=_("Time from"))
    valid_to = models.DateTimeField(null=True, blank=True, verbose_name=_("Time to"))  # `null` for unlimited
    # VAT rate in per cents
    value = models.DecimalField(max_digits=10, decimal_places=4, verbose_name=_("VAT rate"))

    def __str__(self):
        """Return string representation."""
        return "{} ({}), {}, valid: {} - {}, VAT {:.1f}%".format(
            countries.name(self.country_code),
            self.country_code,
            ("CPA code: " + self.cpa_code) if self.cpa_code else "standard rate",
            self.valid_from.isoformat(),
            self.valid_to.isoformat() if self.valid_to else "...",
            self.value,
        )


class BillingItemType(models.Model):
    """Type of item for billing, VAT rate evaluation, etc."""

    handle = models.TextField(unique=True, verbose_name=_("Name"))
    # Block invoice creation
    not_invoiceable = models.BooleanField(default=False, verbose_name=_("Not invoiceable"))
    # CPA code
    cpa_code = models.TextField(verbose_name=_("CPA code"))

    def __str__(self):
        """Return string representation."""
        return f"{self.handle} - CPA: {self.cpa_code}"

    def vat_rate(self, tax_country_code: str, tax_date: date) -> Decimal:
        """Return VAT rate applicable for the item.

        Raises:
            VatRate.DoesNotExist: If the rate cannot be determined.
        """
        if not is_eu_member(tax_country_code):
            return Decimal("0.0")  # zero rate for non-EU countries
        # VAT exception for specific CPA code
        vat_rate = VatRate.objects.filter(
            country_code=tax_country_code, cpa_code=self.cpa_code, valid_from__date__lte=tax_date
        ).filter(Q(valid_to__date__gt=tax_date) | Q(valid_to__isnull=True))
        if not vat_rate:
            # Standard VAT rate
            vat_rate = VatRate.objects.filter(
                country_code=tax_country_code, cpa_code__isnull=True, valid_from__date__lte=tax_date
            ).filter(Q(valid_to__date__gt=tax_date) | Q(valid_to__isnull=True))
        return vat_rate.get().value


class VariableSymbolSequenceManager(models.Manager):
    """Customized manager for variable symbol sequences."""

    @transaction.atomic
    def next_value(self, app_id: str, year: int) -> str:
        """Get variable symbol from appropriate sequence."""
        # Prevent variable symbol duplicates by locking this into DB transaction
        vs_sequence: VariableSymbolSequence = self.filter(app_id=app_id, year=year).select_for_update().get()
        vs_sequence.current_number += 1
        vs_sequence.save(update_fields=["current_number"])  # ToDo: Handle sequence exhaustion
        return vs_sequence.variable_symbol


class VariableSymbolSequence(models.Model):
    """Variable symbol sequence model."""

    app_id = models.TextField(verbose_name=_("Application ID"))
    year = models.IntegerField(validators=(MinValueValidator(1), MaxValueValidator(9_999)), verbose_name=_("Year"))
    prefix = models.IntegerField(validators=(MinValueValidator(0), MaxValueValidator(9_999)), verbose_name=_("Prefix"))
    current_number = models.IntegerField(editable=False, default=0)

    objects = VariableSymbolSequenceManager()

    class Meta:
        """Meta class for model."""

        unique_together = ["app_id", "year", "prefix"]

        constraints = [
            CheckConstraint(check=Q(year__gte=1) & Q(year__lte=9_999), name="vs_year_range"),
            CheckConstraint(check=Q(prefix__gte=0) & Q(prefix__lte=9_999), name="vs_prefix_4_digits"),
            CheckConstraint(
                check=Q(current_number__gte=0) & Q(current_number__lte=999_999), name="vs_current_number_6_digits"
            ),
        ]

    @property
    def variable_symbol(self) -> str:
        """Format variable symbol as string (up to 10 digits)."""
        variable_symbol = f"{self.prefix:0>4d}{self.current_number:0>6d}"
        if SETTINGS.trim_varsym:
            variable_symbol = variable_symbol.lstrip("0")
        return variable_symbol

    def __str__(self):
        """Return string representation of VS sequence."""
        return f"{self.app_id}: {self.variable_symbol}"


# ----------------------------------------------------------------------------------------------------
# Invoice models


class Invoice(models.Model):
    """Invoice model."""

    uuid = models.UUIDField(unique=True)
    customer = models.ForeignKey(
        Customer, on_delete=models.RESTRICT, editable=False, related_name="+", verbose_name=_("Customer")
    )
    app_id = models.TextField(verbose_name=_("Application ID"))
    # Copy of customer info at the moment of creation
    name = models.TextField(null=True, blank=True, verbose_name=_("Name"))
    organization = models.TextField(null=True, blank=True, verbose_name=_("Organization"))
    street1 = models.TextField(verbose_name=_("Street"))
    street2 = models.TextField(null=True, blank=True, verbose_name=_("Street 2"))
    street3 = models.TextField(null=True, blank=True, verbose_name=_("Street 3"))
    city = models.TextField(verbose_name=_("City"))
    state_or_province = models.TextField(null=True, blank=True, verbose_name=_("State or province"))
    postal_code = models.TextField(verbose_name=_("Postal code"))
    country_code = CountryField(verbose_name=_("Country"))
    company_registration_number = models.TextField(null=True, blank=True, verbose_name=_("Company registration number"))
    vat_number = models.TextField(null=True, blank=True, verbose_name=_("VAT number"))
    # Time related fields
    created_at = models.DateTimeField(auto_now_add=True, verbose_name=_("Create time"))
    due_at = models.DateTimeField(null=True, blank=True, verbose_name=_("Due time"))
    tax_at = models.DateField(verbose_name=_("Tax point"))
    paid_at = models.DateField(null=True, blank=True, verbose_name=_("Paid date"))
    # Identifiers
    number = models.TextField(unique=True, verbose_name=_("Invoice number"))
    variable_symbol = models.TextField(blank=True, verbose_name=_("Variable symbol"))
    # Prices
    price_total_base = models.DecimalField(
        max_digits=64, decimal_places=CURRENCY_PRECISION, verbose_name=_("Total base price")
    )
    price_total_vat = models.DecimalField(max_digits=64, decimal_places=CURRENCY_PRECISION, verbose_name=_("Total VAT"))
    price_total = models.DecimalField(
        max_digits=64, decimal_places=CURRENCY_PRECISION, verbose_name=_("Total price with VAT")
    )
    price_to_pay = models.DecimalField(max_digits=64, decimal_places=CURRENCY_PRECISION, verbose_name=_("Price to pay"))
    currency = CurrencyField()
    # PDF document in file manager storage
    file_uid = models.TextField(null=True, blank=True, verbose_name="PDF")
    # Payment info
    bank_account = models.TextField(verbose_name=_("Bank account"))
    payment_type = models.TextField(choices=PAYMENT_TYPE_CHOICES, verbose_name=_("Payment type"))

    class Meta:
        """Model Meta class."""

        verbose_name = _("Invoice")
        verbose_name_plural = _("Invoices")

    def __str__(self):
        """Return string representation."""
        return (
            f'{self.number}/{self.customer} {round_money_amount(self.price_total)} {self.currency}'
            f' at {self.tax_at.strftime("%Y-%m-%d")}'
        )

    def _get_pdf_context(self) -> Dict[str, Any]:
        """Prepare context data for rendering to PDF."""
        # Invalid VAT number for EU subject is not present in invoice data (see `InvoiceSerializer`)
        bank_data = BANK_INSTITUTIONS.get(bank_of_account(self.bank_account), {})
        items = self.items.all()
        return {
            "name": self.name,
            "organization": self.organization,
            "street": [i for i in (self.street1, self.street2, self.street3) if i is not None],
            "city": self.city,
            "state_or_province": self.state_or_province,
            "postal_code": self.postal_code,
            "country": countries.name(self.country_code),
            "company_registration_number": self.company_registration_number,
            "vat_number": self.vat_number,
            "number": self.number,
            "variable_symbol": self.variable_symbol,
            "created_at": local_date(self.created_at).isoformat(),
            "tax_at": self.tax_at.isoformat(),
            "price_total_base": str(round_money_amount(self.price_total_base)),
            "price_total_vat": str(round_money_amount(self.price_total_vat)),
            "price_total": str(round_money_amount(self.price_total)),
            "price_to_pay": str(round_money_amount(self.price_to_pay)),
            "currency": self.currency,
            "bank_account": self.bank_account,
            "iban": account_to_iban(self.bank_account, human=True),
            "bank_name": bank_data.get("name", ""),
            "bank_swift": bank_data.get("swift", ""),
            "payment_type": self.payment_type,
            "reverse_charge": self.customer.reverse_charge_mode,
            "items": [
                {
                    "name": i.name,
                    "item_type": i.item_type,
                    "vat_rate": str(i.vat_rate),
                    "quantity": i.quantity,
                    "price_per_unit": str(round_money_amount(i.price_per_unit)),
                    "price_total_base": str(round_money_amount(i.price_total_base)),
                    "price_total_vat": str(round_money_amount(i.price_total_vat)),
                    "price_total": str(round_money_amount(i.price_total)),
                }
                for i in items
            ],
        }

    def render_pdf(self, secretary_client: SecretaryClient, file_storage: Storage) -> None:
        """Render invoice to PDF and store the document with file manager."""
        if self.file_uid is None:  # nothing to do if already rendered
            if self.country_code == "CZ":
                pdf_template = PdfTemplate.INVOICE_PDF_TEMPLATE_CZ
            else:
                pdf_template = PdfTemplate.INVOICE_PDF_TEMPLATE_EN
            pdf = secretary_client.render_pdf(pdf_template, self._get_pdf_context())
            with file_storage.create(f"invoice_{self.number}.pdf", mimetype="application/pdf") as storage:
                storage.write(pdf)
            self.file_uid = str(storage.uuid)
            self.save(update_fields=["file_uid"])


class InvoiceItem(models.Model):
    """Invoice item model."""

    # Parent invoice
    invoice = models.ForeignKey(Invoice, on_delete=models.CASCADE, editable=False, related_name="items")
    # Item type
    item_type = models.TextField(null=True, blank=True, verbose_name=_("Item type"))
    # Descripton of the item
    name = models.TextField(verbose_name=_("Description"))
    # VAT rate in per cents
    vat_rate = models.DecimalField(max_digits=10, decimal_places=4, verbose_name=_("VAT rate"))
    # Quantity and prices
    quantity = models.IntegerField(validators=(MinValueValidator(1),), default=1, verbose_name=_("Quantity"))
    price_per_unit = models.DecimalField(
        max_digits=64, decimal_places=CURRENCY_PRECISION, verbose_name=_("Price per unit")
    )
    price_total_base = models.DecimalField(
        max_digits=64, decimal_places=CURRENCY_PRECISION, verbose_name=_("Total base price")
    )
    price_total_vat = models.DecimalField(max_digits=64, decimal_places=CURRENCY_PRECISION, verbose_name=_("Total VAT"))
    price_total = models.DecimalField(
        max_digits=64, decimal_places=CURRENCY_PRECISION, verbose_name=_("Total price with VAT")
    )

    def __str__(self):
        """Return string representation."""
        return f"{self.invoice.number}/{self.name}"


class InvoiceNumberSequenceManager(models.Manager):
    """Customized manager for invoice number sequences."""

    @transaction.atomic
    def next_value(self, app_id: str, year: int) -> str:
        """Get invoice number from appropriate sequence."""
        # Prevent duplicates by locking this into DB transaction
        number_sequence: InvoiceNumberSequence = self.filter(app_id=app_id, year=year).select_for_update().get()
        number_sequence.current_number += 1
        number_sequence.save(update_fields=["current_number"])
        return number_sequence.invoice_number


class InvoiceNumberSequence(models.Model):
    """Invoice number sequence model."""

    app_id = models.TextField(verbose_name=_("Application ID"))
    year = models.IntegerField(validators=(MinValueValidator(1), MaxValueValidator(9_999)), verbose_name=_("Year"))
    prefix = models.TextField(verbose_name=_("Prefix"))
    current_number = models.IntegerField(editable=False, default=0)

    objects = InvoiceNumberSequenceManager()

    class Meta:
        """Meta class for model."""

        unique_together = ["app_id", "year", "prefix"]

        constraints = [CheckConstraint(check=Q(year__gte=1) & Q(year__lte=9_999), name="invoice_year_range")]

    @property
    def invoice_number(self) -> str:
        """Format invoice number as string."""
        return f"{self.prefix}{self.current_number:0>5d}"

    def __str__(self):
        """Return string representation of invoice number sequence."""
        return f"{self.app_id}: {self.invoice_number}"
