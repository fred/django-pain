#
# Copyright (C) 2023-2024  CZ.NIC, z. s. p. o.
#
# This file is part of FRED.
#
# FRED is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# FRED is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with FRED.  If not, see <https://www.gnu.org/licenses/>.

"""Test creating PDF documents."""

from datetime import datetime, timezone
from io import StringIO
from typing import Any, Dict, List
from unittest.mock import patch
from uuid import UUID

from django.core.management import call_command
from django.core.management.base import CommandError
from django.test import TestCase, override_settings
from freezegun import freeze_time
from testfixtures import LogCapture

from django_pain.models import Invoice, PaymentRequest
from django_pain.tests.mixins import CacheResetMixin
from django_pain.tests.utils import (
    create_invoice,
    create_payment_request,
    get_account,
    get_customer,
    get_payment_request,
)


class TestCreatePdf(CacheResetMixin, TestCase):
    """Test `create_pdf` command."""

    def setUp(self):
        super().setUp()
        self.log_handler = LogCapture("django_pain.management.commands.create_pdf", propagate=False)
        self.out = StringIO()

    def tearDown(self):
        self.log_handler.uninstall()

    def test_setting_error(self):
        with self.assertRaisesMessage(
            CommandError,
            "Settings for secretary and file manager are required to create PDF documents",
        ):
            call_command("create_pdf", "--type=payment_request", stdout=self.out)

    @override_settings(PAIN_SECRETARY_URL="http://localhost/", PAIN_FILEMAN_NETLOC="localhost:50001")
    @freeze_time("2023-09-15T12:00:00Z")
    @patch.object(PaymentRequest, "render_pdf")
    def test_payment_requests(self, render_pdf_mock):
        # Prepare test data
        customer = get_customer()
        customer.save()
        account = get_account()
        account.save()
        payment_request_data: List[Dict[str, Any]] = [
            {},  # 1 - with PDF to be created
            # filtered out
            {"closed_at": datetime(2023, 9, 10, tzinfo=timezone.utc)},  # 2 - closed
            {"due_at_displayed": datetime(2023, 9, 10, tzinfo=timezone.utc)},  # 3 - overdue
            {"file_uid": "file-1"},  # 4 - PDF already created
        ]
        for index, data in enumerate(payment_request_data, 1):
            payment_request = get_payment_request(
                bank_account=account, customer=customer, uuid=UUID(int=index), variable_symbol=str(index), **data
            )
            payment_request.save()

        call_command("create_pdf", "--type=payment_request", stdout=self.out)

        render_pdf_mock.assert_called_once()
        self.log_handler.check_present(
            ("django_pain.management.commands.create_pdf", "INFO", "Creating payment request PDFs ..."),
            (
                "django_pain.management.commands.create_pdf",
                "DEBUG",
                "PDF for 00000000-0000-0000-0000-000000000001 created",
            ),
            ("django_pain.management.commands.create_pdf", "INFO", "Payment request PDFs created: 1, failed: 0"),
        )

    @override_settings(PAIN_SECRETARY_URL="http://localhost/", PAIN_FILEMAN_NETLOC="localhost:50001")
    @freeze_time("2023-09-15T12:00:00Z")
    @patch.object(PaymentRequest, "render_pdf")
    def test_payment_requests_failure(self, render_pdf_mock):
        create_payment_request(realized_payment=None)
        render_pdf_mock.side_effect = Exception("It is broken!")

        with self.assertRaisesMessage(CommandError, "Failed to create some PDFs: 1"):
            call_command("create_pdf", "--type=payment_request", stdout=self.out)

        self.log_handler.check_present(
            ("django_pain.management.commands.create_pdf", "INFO", "Creating payment request PDFs ..."),
            (
                "django_pain.management.commands.create_pdf",
                "ERROR",
                "PDF for 00000000-0000-0000-0000-000000000001 not created: Exception('It is broken!')",
            ),
            ("django_pain.management.commands.create_pdf", "INFO", "Payment request PDFs created: 0, failed: 1"),
        )

    @override_settings(PAIN_SECRETARY_URL="http://localhost/", PAIN_FILEMAN_NETLOC="localhost:50001")
    @freeze_time("2024-01-25T12:00:00Z")
    @patch.object(Invoice, "render_pdf")
    def test_invoice(self, render_pdf_mock):
        create_invoice()

        call_command("create_pdf", "--type=invoice", stdout=self.out)

        render_pdf_mock.assert_called_once()
        self.log_handler.check_present(
            ("django_pain.management.commands.create_pdf", "INFO", "Creating invoice PDFs ..."),
            ("django_pain.management.commands.create_pdf", "DEBUG", "PDF for 902400001 created"),
            ("django_pain.management.commands.create_pdf", "INFO", "Invoice PDFs created: 1, failed: 0"),
        )

    @override_settings(PAIN_SECRETARY_URL="http://localhost/", PAIN_FILEMAN_NETLOC="localhost:50001")
    @freeze_time("2024-01-25T12:00:00Z")
    @patch.object(Invoice, "render_pdf")
    def test_invoice_failure(self, render_pdf_mock):
        create_invoice()
        render_pdf_mock.side_effect = Exception("It is broken!")

        with self.assertRaisesMessage(CommandError, "Failed to create some PDFs: 1"):
            call_command("create_pdf", "--type=invoice", stdout=self.out)

        self.log_handler.check_present(
            ("django_pain.management.commands.create_pdf", "INFO", "Creating invoice PDFs ..."),
            (
                "django_pain.management.commands.create_pdf",
                "ERROR",
                "PDF for 902400001 not created: Exception('It is broken!')",
            ),
            ("django_pain.management.commands.create_pdf", "INFO", "Invoice PDFs created: 0, failed: 1"),
        )
