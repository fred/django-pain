#
# Copyright (C) 2024  CZ.NIC, z. s. p. o.
#
# This file is part of FRED.
#
# FRED is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# FRED is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with FRED.  If not, see <https://www.gnu.org/licenses/>.

"""Test creating invoices from payment requests."""

from datetime import date, datetime, timezone
from decimal import Decimal
from io import StringIO
from unittest.mock import patch
from uuid import UUID

from django.core.management import call_command
from django.core.management.base import CommandError
from django.test import TestCase, override_settings
from freezegun import freeze_time
from testfixtures import LogCapture

from django_pain.models import BankPayment, Invoice, PaymentRequest
from django_pain.models.invoices import BillingItemType, InvoiceNumberSequence, VatRate
from django_pain.tests.mixins import CacheResetMixin
from django_pain.tests.utils import create_payment_request, get_account, get_customer, get_payment


class TestCreateInvoice(CacheResetMixin, TestCase):
    """Test `create_invoice` command."""

    def setUp(self):
        super().setUp()
        self.log_handler = LogCapture("django_pain.management.commands.create_invoice", propagate=False)
        self.out = StringIO()
        BillingItemType.objects.create(handle="basic_rate")
        VatRate.objects.create(
            country_code="CZ",
            cpa_code=None,
            valid_from=datetime(2023, 1, 1, tzinfo=timezone.utc),
            valid_to=None,
            value=Decimal("21.0"),
        )
        customer = get_customer(
            vies_vat_valid=True, vies_checked_at=datetime(2024, 1, 15, 5, 0, 0, tzinfo=timezone.utc)
        )
        account = get_account()
        payment = get_payment(uuid=UUID(int=21), account=account, transaction_date=date(2024, 1, 19))
        create_payment_request(
            uuid=UUID(int=1), customer=customer, bank_account=account, realized_payment=payment, variable_symbol="1"
        )
        InvoiceNumberSequence.objects.create(app_id="auctions", year=2024, prefix=9024, current_number=0)

    def tearDown(self):
        self.log_handler.uninstall()

    @freeze_time("2024-01-20T10:00:00Z")
    def test_create_invoice_ok(self):
        call_command("create_invoice", stdout=self.out)

        # Check updated payment request
        payment_request = PaymentRequest.objects.get()
        invoice = Invoice.objects.get()
        self.assertEqual(payment_request.invoice, invoice)
        # Check log
        self.log_handler.check_present(
            ("django_pain.management.commands.create_invoice", "INFO", "Command `create_invoice` started"),
            (
                "django_pain.management.commands.create_invoice",
                "DEBUG",
                "Invoice 902400001 created from payment request 00000000-0000-0000-0000-000000000001",
            ),
            ("django_pain.management.commands.create_invoice", "INFO", "Command `create_invoice` finished"),
        )

    @freeze_time("2024-01-20T10:00:00Z")
    @override_settings(PAIN_SECRETARY_URL="http://localhost/api/", PAIN_FILEMAN_NETLOC="localhost:50053")
    def test_create_invoice_ok_with_pdf(self):
        def fake_render_pdf(invoice, secretary_client, file_storage):
            # Simulate storing a rendered PDF
            invoice.file_uid = "pdf-1"
            invoice.save(update_fields=["file_uid"])

        with patch.object(Invoice, "render_pdf", new=fake_render_pdf):
            call_command("create_invoice", stdout=self.out)

            invoice = Invoice.objects.get()
            self.assertEqual(invoice.file_uid, "pdf-1")
            self.log_handler.check_present(
                ("django_pain.management.commands.create_invoice", "DEBUG", "PDF for 902400001 created"),
            )

    @freeze_time("2024-01-20T10:00:00Z")
    @override_settings(PAIN_SECRETARY_URL="http://localhost/api/", PAIN_FILEMAN_NETLOC="localhost:50053")
    def test_create_invoice_ok_without_pdf(self):
        with patch.object(Invoice, "render_pdf") as render_mock:
            render_mock.side_effect = Exception()

            call_command("create_invoice", stdout=self.out)

            invoice = Invoice.objects.get()
            self.assertIsNone(invoice.file_uid)
            render_mock.assert_called_once()

    @freeze_time("2024-01-20T10:00:00Z")
    def test_create_invoice_validation_error(self):
        # Prepare data - no invoice number sequence
        invoice_sequence = InvoiceNumberSequence.objects.get()
        invoice_sequence.delete()

        with self.assertRaisesMessage(CommandError, "Failed to create 1 invoice(s)"):
            call_command("create_invoice", stdout=self.out)

        # Check log
        self.log_handler.check_present(
            ("django_pain.management.commands.create_invoice", "INFO", "Command `create_invoice` started"),
            (
                "django_pain.management.commands.create_invoice",
                "ERROR",
                "Invoice not created from payment request 00000000-0000-0000-0000-000000000001: "
                "{'detail': ErrorDetail(string='No invoice number sequence available for `auctions` and `2024`',"
                " code='invalid')}",
            ),
            ("django_pain.management.commands.create_invoice", "INFO", "Command `create_invoice` finished"),
        )

    @freeze_time("2024-01-20T10:00:00Z")
    def test_create_invoice_payment_too_old(self):
        # Prepare data - outdated payment
        payment = BankPayment.objects.get(uuid=UUID(int=21))
        payment.transaction_date = date(2023, 10, 10)
        payment.save(update_fields=["transaction_date"])

        with self.assertRaisesMessage(CommandError, "Failed to create 1 invoice(s)"):
            call_command("create_invoice", stdout=self.out)

        # Check log
        self.log_handler.check_present(
            ("django_pain.management.commands.create_invoice", "INFO", "Command `create_invoice` started"),
            (
                "django_pain.management.commands.create_invoice",
                "ERROR",
                "Invoice not created from payment request 00000000-0000-0000-0000-000000000001: "
                "Payment before start of previous month",
            ),
            ("django_pain.management.commands.create_invoice", "INFO", "Command `create_invoice` finished"),
        )
