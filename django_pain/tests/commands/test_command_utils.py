#
# Copyright (C) 2024  CZ.NIC, z. s. p. o.
#
# This file is part of FRED.
#
# FRED is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# FRED is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with FRED.  If not, see <https://www.gnu.org/licenses/>.

"""Tests for command utilities."""

import sys
from datetime import date
from io import StringIO
from unittest import skipUnless

from django.test import SimpleTestCase, TestCase
from djmoney.money import Money

from django_pain.constants import VerbosityLevel
from django_pain.management.command_utils import Reporter, TellerBankPaymentParser
from django_pain.models import BankAccount

try:
    from teller.statement import Payment
except ImportError:
    Payment = object  # type: ignore


class DummyStatementParser:
    """Dummy parser."""


@skipUnless("teller" in sys.modules, "Can not run without teller library.")
class TellerBankPaymentParserTest(TestCase):
    """Tests for `TellerBankPaymentParser`."""

    def test_from_payment_data_class(self):
        payment_data = Payment()
        payment_data.identifier = "abc123"
        payment_data.transaction_date = date(2020, 9, 1)
        payment_data.counter_account = "12345/678"
        payment_data.name = "John Doe"
        payment_data.amount = Money(1, "CZK")
        payment_data.description = "Hello!"
        payment_data.constant_symbol = "123"
        payment_data.variable_symbol = "456"
        payment_data.specific_symbol = "789"

        account = BankAccount(account_number="1234567890/2010")
        model = TellerBankPaymentParser(DummyStatementParser())._payment_from_data_class(account, payment_data)

        self.assertEqual(model.identifier, payment_data.identifier)
        self.assertEqual(model.account, account)
        self.assertEqual(model.transaction_date, payment_data.transaction_date)
        self.assertEqual(model.counter_account_number, payment_data.counter_account)
        self.assertEqual(model.counter_account_name, payment_data.name)
        self.assertEqual(model.amount, payment_data.amount)
        self.assertEqual(model.description, payment_data.description)
        self.assertEqual(model.constant_symbol, payment_data.constant_symbol)
        self.assertEqual(model.variable_symbol, payment_data.variable_symbol)
        self.assertEqual(model.specific_symbol, payment_data.specific_symbol)

    def test_from_payment_data_class_blank_values(self):
        payment_data = Payment()
        payment_data.identifier = "abc123"
        payment_data.transaction_date = date(2020, 9, 1)
        payment_data.counter_account = None
        payment_data.name = None
        payment_data.amount = Money(1, "CZK")
        payment_data.description = None
        payment_data.constant_symbol = None
        payment_data.variable_symbol = None
        payment_data.specific_symbol = None

        account = BankAccount(account_number="1234567890/2010")
        model = TellerBankPaymentParser(DummyStatementParser())._payment_from_data_class(account, payment_data)

        self.assertEqual(model.identifier, payment_data.identifier)
        self.assertEqual(model.account, account)
        self.assertEqual(model.transaction_date, payment_data.transaction_date)
        self.assertEqual(model.counter_account_number, "")
        self.assertEqual(model.counter_account_name, "")
        self.assertEqual(model.amount, payment_data.amount)
        self.assertEqual(model.description, "")
        self.assertEqual(model.constant_symbol, "")
        self.assertEqual(model.variable_symbol, "")
        self.assertEqual(model.specific_symbol, "")


class TestReporter(SimpleTestCase):
    """Test `Reporter` class."""

    def test_reporter(self):
        data = [
            (VerbosityLevel.NONE, ""),
            (VerbosityLevel.BASIC, "defaultbasic1"),
            (VerbosityLevel.EXTENDED, "defaultbasicextended1"),
            (VerbosityLevel.FULL, "defaultbasicextendedfull1"),
        ]
        for level, expected in data:
            with self.subTest(level=level):
                out = StringIO()
                reporter = Reporter(out, level)

                reporter.write("default")
                reporter.write("basic", VerbosityLevel.BASIC)
                reporter.write("extended", VerbosityLevel.EXTENDED)
                reporter.write("full", VerbosityLevel.FULL)
                reporter.write("1", 1)

                self.assertEqual(out.getvalue(), expected)
