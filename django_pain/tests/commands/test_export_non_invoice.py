#
# Copyright (C) 2024  CZ.NIC, z. s. p. o.
#
# This file is part of FRED.
#
# FRED is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# FRED is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with FRED.  If not, see <https://www.gnu.org/licenses/>.

"""Test export_non_invoice command."""

from csv import DictReader
from datetime import datetime, timezone
from io import SEEK_SET, StringIO

from django.core.management import call_command
from django.test import TransactionTestCase
from freezegun import freeze_time
from testfixtures import LogCapture

from django_pain.models.invoices import BillingItemType
from django_pain.tests.utils import create_payment_request


class TestExportNonInvoice(TransactionTestCase):
    """Test export_non_invoice command."""

    reset_sequences = True

    def setUp(self):
        super().setUp()
        self.log_handler = LogCapture("django_pain.management.commands.export_non_invoice", propagate=False)
        self.out = StringIO()

    def tearDown(self):
        self.log_handler.uninstall()

    def test_export_empty(self):
        call_command(
            "export_non_invoice",
            "--application=auctions",
            "--from=2024-01-01T12:00Z",
            "--to=2024-06-30T12:00Z",
            stdout=self.out,
        )

        # Check results
        self.assertEqual(self.out.getvalue(), "")
        self.log_handler.check(
            (
                "django_pain.management.commands.export_non_invoice",
                "INFO",
                "Command `export_non_invoice` started for application `auctions` with interval 2024-01-01 12:00:00+00:00 to 2024-06-30 12:00:00+00:00",
            ),
            ("django_pain.management.commands.export_non_invoice", "INFO", "Payments exported: 0"),
            ("django_pain.management.commands.export_non_invoice", "INFO", "Command `export_non_invoice` finished"),
        )

    @freeze_time("2023-09-20T12:00:00Z")
    def test_export(self):
        # Prepare data and save all objects to database
        create_payment_request(closed_at=datetime(2023, 9, 20, tzinfo=timezone.utc))
        BillingItemType.objects.create(handle="basic_rate", not_invoiceable=True)

        call_command(
            "export_non_invoice",
            "--application=auctions",
            "--from=2023-09-01T12:00Z",
            "--to=2023-10-01T12:00Z",
            stdout=self.out,
        )

        # Check results
        expected = [
            {
                "identifier": "PAYMENT1",
                "transaction date": "2023-09-15",
                "variable symbol": "603",
                "amount": "42.00",
                "currency": "CZK",
                "customer": "Test, s.r.o.",
                "description": "Item 1, Item 2, Item 3",
                "payment type": "transfer",
                "counter account": "098765/4321",
            }
        ]
        self.out.seek(SEEK_SET)  # rewind "file"
        csv_reader = DictReader(self.out)
        self.assertEqual([row for row in csv_reader], expected)
        self.log_handler.check_present(
            ("django_pain.management.commands.export_non_invoice", "INFO", "Payments exported: 1")
        )

    @freeze_time("2023-09-20T12:00:00Z")
    def test_export_not_closed(self):
        # Prepare data and save all objects to database
        create_payment_request()
        BillingItemType.objects.create(handle="basic_rate", not_invoiceable=True)

        call_command(
            "export_non_invoice",
            "--application=auctions",
            "--from=2023-09-01T12:00Z",
            "--to=2023-10-01T12:00Z",
            stdout=self.out,
        )

        self.assertEqual(self.out.getvalue(), "")

    @freeze_time("2023-09-20T12:00:00Z")
    def test_export_invoiceable(self):
        # Prepare data and save all objects to database
        create_payment_request(closed_at=datetime(2023, 9, 20, tzinfo=timezone.utc))
        BillingItemType.objects.create(handle="basic_rate", not_invoiceable=False)

        call_command(
            "export_non_invoice",
            "--application=auctions",
            "--from=2023-09-01T12:00Z",
            "--to=2023-10-01T12:00Z",
            stdout=self.out,
        )

        self.assertEqual(self.out.getvalue(), "")

    @freeze_time("2023-09-20T12:00:00Z")
    def test_export_bad_app(self):
        # Prepare data and save all objects to database
        create_payment_request(closed_at=datetime(2023, 9, 20, tzinfo=timezone.utc), app_id="other")
        BillingItemType.objects.create(handle="basic_rate", not_invoiceable=True)

        call_command(
            "export_non_invoice",
            "--application=auctions",
            "--from=2023-09-01T12:00Z",
            "--to=2023-10-01T12:00Z",
            stdout=self.out,
        )

        self.assertEqual(self.out.getvalue(), "")

    @freeze_time("2023-09-20T12:00:00Z")
    def test_export_bad_interval(self):
        # Prepare data and save all objects to database
        create_payment_request(closed_at=datetime(2023, 10, 2, tzinfo=timezone.utc))
        BillingItemType.objects.create(handle="basic_rate", not_invoiceable=True)

        call_command(
            "export_non_invoice",
            "--application=auctions",
            "--from=2023-09-01T12:00Z",
            "--to=2023-10-01T12:00Z",
            stdout=self.out,
        )

        self.assertEqual(self.out.getvalue(), "")
