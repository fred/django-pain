#
# Copyright (C) 2024  CZ.NIC, z. s. p. o.
#
# This file is part of FRED.
#
# FRED is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# FRED is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with FRED.  If not, see <https://www.gnu.org/licenses/>.

"""Test checking VAT number."""

from datetime import datetime, timezone
from io import StringIO
from typing import Any, Dict, List
from unittest.mock import call, patch

import requests
import requests_mock
from django.core.management import call_command
from django.test import SimpleTestCase, TestCase
from freezegun import freeze_time
from testfixtures import LogCapture

from django_pain.management.commands.check_vat_number import ViesUnresolved, validate_eu_vat_number
from django_pain.tests.utils import get_customer


class TestValidateEuVatNumber(SimpleTestCase):
    """Test `validate_eu_vat_number` function."""

    VIES_URL = "https://ec.europa.eu/taxation_customs/vies/rest-api/check-vat-number"

    def test_no_vat_number(self):
        self.assertIsNone(validate_eu_vat_number(None, timeout=3.0))

    def test_non_eu_resident(self):
        self.assertIsNone(validate_eu_vat_number("CH0001", timeout=3.0))

    @requests_mock.mock()
    def test_resolved_true(self, mock):
        mock.post(self.VIES_URL, json={"valid": True})  # VAT subject found in VIES

        self.assertTrue(validate_eu_vat_number("DE0001", timeout=3.0))

        # Check HTTP call
        self.assertEqual(mock.call_count, 1)
        request = mock.last_request
        self.assertEqual(request.headers["Content-Type"], "application/json")  # check data encoding
        expected_request_data = {
            "countryCode": "DE",
            "vatNumber": "0001",
            "requesterMemberStateCode": "CZ",
            "requesterNumber": "67985726",
        }
        self.assertEqual(request.json(), expected_request_data)  # check request data

    @requests_mock.mock()
    def test_resolved_false(self, mock):
        mock.post(self.VIES_URL, json={"valid": False})  # VAT subject not found in VIES

        self.assertFalse(validate_eu_vat_number("DE0001", timeout=3.0))

    @requests_mock.mock()
    def test_unresolved(self, mock):
        # VIES check was not resolved for different reasons
        response_data = [
            {"exc": requests.exceptions.ConnectTimeout},  # exception
            {"status_code": 500},  # bad status code
            {"text": "malformed"},  # not a JSON response
            {"json": {"actionSucceed": False}},  # `valid` field not present
        ]

        for data in response_data:
            with self.subTest(data=data):
                mock.post(self.VIES_URL, **data)

                self.assertRaises(ViesUnresolved, validate_eu_vat_number, "DE0001", timeout=3.0)


class TestCheckVatNumber(TestCase):
    """Test `check_vat_number` command."""

    def setUp(self):
        self.log_handler = LogCapture("django_pain.management.commands.check_vat_number", propagate=False)
        self.out = StringIO()

    def tearDown(self):
        self.log_handler.uninstall()

    @freeze_time("2024-01-10T12:00:00Z")
    @patch("django_pain.management.commands.check_vat_number.validate_eu_vat_number")
    def test_check(self, validate_mock):
        customer_data: List[Dict[str, Any]] = [
            {"uid": "cid-1", "vat_number": None},  # not a VAT subject
            {"uid": "cid-2", "vat_number": "CZ0002"},  # new check
            {"uid": "cid-3", "vat_number": "CZ0003"},  # new check
            {"uid": "cid-4", "country_code": "CH", "vat_number": "CH0004"},  # not an EU resident
            {
                "uid": "cid-5",
                "vat_number": "CZ0005",
                "vies_checked_at": datetime(2023, 12, 1, tzinfo=timezone.utc),
            },  # periodic check
            {
                "uid": "cid-6",
                "vat_number": "CZ0006",
                "vies_checked_at": datetime(2023, 12, 15, tzinfo=timezone.utc),
            },  # periodic check
            {
                "uid": "cid-7",
                "vat_number": "CZ0007",
                "vies_checked_at": datetime(2024, 1, 5, tzinfo=timezone.utc),
            },  # recent enough
        ]
        for item in customer_data:
            customer = get_customer(**item)
            customer.save()
        validate_mock.side_effect = [True, False, None, ViesUnresolved("Malice in Wonderland!")]

        call_command("check_vat_number", ["--interval=10", "--timeout=3.0"], stdout=self.out)

        self.assertEqual(
            validate_mock.mock_calls,
            [
                call("CZ0002", timeout=3.0),
                call("CZ0003", timeout=3.0),
                call("CZ0005", timeout=3.0),
                call("CZ0006", timeout=3.0),
            ],
        )
        self.log_handler.check_present(
            ("django_pain.management.commands.check_vat_number", "INFO", "Command `check_vat_number` started"),
            (
                "django_pain.management.commands.check_vat_number",
                "DEBUG",
                "Customer cid-2 - VIES info resolved, valid: True",
            ),
            (
                "django_pain.management.commands.check_vat_number",
                "DEBUG",
                "Customer cid-3 - VIES info resolved, valid: False",
            ),
            ("django_pain.management.commands.check_vat_number", "DEBUG", "Customer cid-5 - VIES info cleared"),
            (
                "django_pain.management.commands.check_vat_number",
                "DEBUG",
                "Customer cid-6 - VIES info not resolved: Malice in Wonderland!",
            ),
            ("django_pain.management.commands.check_vat_number", "INFO", "Customers processed: 4"),
            ("django_pain.management.commands.check_vat_number", "INFO", "Command `check_vat_number` finished"),
        )
