#
# Copyright (C) 2024  CZ.NIC, z. s. p. o.
#
# This file is part of FRED.
#
# FRED is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# FRED is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with FRED.  If not, see <https://www.gnu.org/licenses/>.

"""Test export_invoice command."""

from datetime import date, datetime, timedelta, timezone
from decimal import Decimal
from io import StringIO
from pathlib import Path

from django.core.management import CommandError, call_command
from django.test import TransactionTestCase
from testfixtures import LogCapture

from django_pain.models import Customer, Invoice
from django_pain.tests.utils import create_invoice, get_customer
from django_pain.utils import round_money_amount


class TestExportInvoice(TransactionTestCase):
    """Test export_invoice command."""

    reset_sequences = True

    def setUp(self):
        super().setUp()
        self.log_handler = LogCapture("django_pain.management.commands.export_invoice", propagate=False)
        self.out = StringIO()

    def tearDown(self):
        self.log_handler.uninstall()

    def test_export_empty(self):
        expected_lines = (
            "<?xml version='1.0' encoding='UTF-8'?>",
            '<HeliosIQ_1 xmlns="urn:schemas-microsoft-com:xml-data">',
            "  <header>",
            "    <manifest>",
            "      <document>",
            "        <name>PohyboveDoklady</name>",
            "        <description>internal-format-HELIOS:lcs_cz:PohyboveDoklady</description>",
            "        <version>020020140302</version>",
            "      </document>",
            "    </manifest>",
            "  </header>",
            "  <body/>",
            "</HeliosIQ_1>",
        )

        call_command("export_invoice", "--from=2024-01-01T12:00Z", "--to=2024-06-30T12:00Z", stdout=self.out)

        # Check results
        self.assertEqual(self.out.getvalue(), "\n".join(expected_lines))

    def _create_invoice(self, index: int, customer: Customer) -> Invoice:
        price_total = round_money_amount(Decimal(1000 + index * 75))
        vat_rate = Decimal("21.0") if customer.country_code == "CZ" else Decimal("0.0")
        price_base = round_money_amount(price_total / (Decimal(1) + Decimal("0.01") * vat_rate))
        kwargs = {
            "customer": customer,
            "variable_symbol": str(4024000025 + index),
            "due_at": datetime(2024, 4, 30, 12, 0, 0, tzinfo=timezone.utc),
            "tax_at": date(2024, 4, 23),
            "paid_at": date(2024, 4, 23),
            "bank_account": "450044/0300",
            "items": [
                {
                    "name": f"example-{index + 1}.org",
                    "item_type": "domain_registration_right",
                    "vat_rate": vat_rate,
                    "quantity": 1,
                    "price_per_unit": price_base,
                    "price_total_base": price_base,
                    "price_total_vat": price_total - price_base,
                    "price_total": price_total,
                },
            ],
        }
        invoice = create_invoice(**kwargs)
        invoice.created_at = datetime(2024, 4, 20, 12, 0, 0, tzinfo=timezone.utc) + timedelta(hours=index)
        invoice.save(update_fields=["created_at"])
        return invoice

    def _read_xml(self, file_name: str) -> bytes:
        xml_file_name = Path(__file__).parent.parent / "export" / "data" / file_name
        with open(xml_file_name, "rb") as file:
            return file.read()

    def test_export_ok(self):
        # Prepare data - multiple invoices for two different customers with different VAT rates
        customer_1 = get_customer()
        data_2 = {
            "uid": "CID-2",
            "name": None,
            "organization": "Custom Chrome Europe GmbH",
            "street1": "Carl-von-Ossietzky-Str. 8",
            "city": "Grolsheim",
            "postal_code": "D-55459",
            "country_code": "DE",
            "company_registration_number": "811309246",
            "vat_number": "DE811309246",
        }
        customer_2 = get_customer(**data_2)
        for i in range(5):
            self._create_invoice(i, customer_1 if i % 2 else customer_2)

        # Carry out export - first and last invoice are out of processed interval
        call_command("export_invoice", "--from=2024-04-20T13:00Z", "--to=2024-04-20T16:00Z", stdout=self.out)

        # Check results
        self.assertEqual(self.out.getvalue(), self._read_xml("export-1.xml").decode())
        self.log_handler.check_present(
            (
                "django_pain.management.commands.export_invoice",
                "INFO",
                "Command `export_invoice` started with interval 2024-04-20 13:00:00+00:00 to 2024-04-20 16:00:00+00:00",
            ),
            (
                "django_pain.management.commands.export_invoice",
                "DEBUG",
                "Invoice 902400002 (1075.00 CZK, VS: 4024000026) processed",
            ),
            (
                "django_pain.management.commands.export_invoice",
                "DEBUG",
                "Invoice 902400003 (1150.00 CZK, VS: 4024000027) processed",
            ),
            (
                "django_pain.management.commands.export_invoice",
                "DEBUG",
                "Invoice 902400004 (1225.00 CZK, VS: 4024000028) processed",
            ),
            ("django_pain.management.commands.export_invoice", "INFO", "Invoices exported: 3, failed: 0"),
            ("django_pain.management.commands.export_invoice", "INFO", "Command `export_invoice` finished"),
        )

    def test_export_error(self):
        invoice = self._create_invoice(0, get_customer())
        invoice.app_id = "not_supported"  # there is no serializer for this application
        invoice.save(update_fields=["app_id"])

        with self.assertRaisesMessage(CommandError, "Failed to export some invoices: 1"):
            call_command("export_invoice", "--from=2024-01-01T00:00Z", "--to=2024-06-30T00:00Z", stdout=self.out)

        # Check results
        self.log_handler.check_present(
            (
                "django_pain.management.commands.export_invoice",
                "ERROR",
                "Invoice 902400001 not exported: ValueError('No invoice serializer defined for `not_supported`')",
            ),
        )
