#
# Copyright (C) 2024  CZ.NIC, z. s. p. o.
#
# This file is part of FRED.
#
# FRED is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# FRED is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with FRED.  If not, see <https://www.gnu.org/licenses/>.

"""Test for VAT rate monitoring/update."""

from datetime import date, datetime, timezone
from decimal import Decimal
from io import StringIO
from pathlib import Path
from unittest.mock import patch

from django.core.management import call_command
from django.core.management.base import CommandError
from django.test import TestCase
from freezegun import freeze_time
from testfixtures import LogCapture

from django_pain.models.invoices import BillingItemType, VatRate
from django_pain.settings import SETTINGS
from django_pain.vat_rate.tedb_client import TedbClient

VAT_RATE_WSDL_TEST = Path(__file__).parent.parent / "vat_rate" / "data" / "VatRetrievalService.wsdl"


@patch("django_pain.management.commands.monitor_vat_rates.VAT_RATES_WSDL", VAT_RATE_WSDL_TEST)
class TestMonitorVatRates(TestCase):
    """Tests for `monitor_vat_rates` command."""

    @classmethod
    def setUpClass(cls) -> None:
        tedb_client = TedbClient(VAT_RATE_WSDL_TEST)
        cls.response_type = tedb_client._client.get_type(
            "{urn:ec.europa.eu:taxud:tedb:services:v1:IVatRetrievalService:types}retrieveVatRatesRespType"
        )
        return super().setUpClass()

    def setUp(self) -> None:
        self.out = StringIO()

    @patch("django_pain.management.commands.monitor_vat_rates.VatRateUpdater.apply")
    @patch("django_pain.management.commands.monitor_vat_rates.TedbClient.get_vat_rates")
    def test_default_param_lists(self, get_vat_rates_mock, apply_mock):
        BillingItemType.objects.create(handle="test_item_1", cpa_code="62.0")
        BillingItemType.objects.create(handle="test_item_2", cpa_code="62.0")
        BillingItemType.objects.create(handle="test_item_3", cpa_code="-", not_invoiceable=True)
        BillingItemType.objects.create(handle="test_item_4", cpa_code="37")
        get_vat_rates_mock.return_value = []

        call_command("monitor_vat_rates", ["-f=2024-01-01", "-t=2024-07-01"], stdout=self.out)

        get_vat_rates_mock.assert_called_once_with(
            date(2024, 1, 1),
            date(2024, 7, 1),
            SETTINGS.eu_members,
            ["37", "62.0"],
        )
        apply_mock.assert_called_once_with(
            [],
            SETTINGS.eu_members,
            ["37", "62.0"],
            dry_run=True,
        )

    @patch("django_pain.management.commands.monitor_vat_rates.TedbClient.get_vat_rates")
    def test_exclude_param_lists(self, get_vat_rates_mock):
        BillingItemType.objects.create(handle="test_item_1", cpa_code="62.0")
        BillingItemType.objects.create(handle="test_item_2", cpa_code="37")
        get_vat_rates_mock.return_value = []

        call_command(
            "monitor_vat_rates",
            [
                "-f=2024-01-01",
                "-t=2024-07-01",
                "--exclude-countries=CY,CZ,DE,DK,EE,ES,FI,FR,GR,HR,HU,IE,IT,LT,LU,LV,MT,NL,PL,PT,RO,SE,SI,SK",
                "--exclude-cpa_codes=37",
            ],
            stdout=self.out,
        )

        get_vat_rates_mock.assert_called_once_with(
            date(2024, 1, 1),
            date(2024, 7, 1),
            ["AT", "BE", "BG"],
            ["62.0"],
        )

    @patch("django_pain.management.commands.monitor_vat_rates.TedbClient.get_vat_rates")
    def test_include_param_lists(self, get_vat_rates_mock):
        get_vat_rates_mock.return_value = []

        call_command(
            "monitor_vat_rates",
            [
                "-f=2024-01-01",
                "-t=2024-07-01",
                "--include-countries=AT,BE,BG",
                "--include-cpa_codes=01.1,24,38.1.5",
            ],
            stdout=self.out,
        )

        get_vat_rates_mock.assert_called_once_with(
            date(2024, 1, 1), date(2024, 7, 1), ["AT", "BE", "BG"], ["01.1", "24", "38.1.5"]
        )

    @patch("django_pain.management.commands.monitor_vat_rates.VatRateUpdater.apply")
    @patch("django_pain.management.commands.monitor_vat_rates.TedbClient.get_vat_rates")
    def test_param_update(self, get_vat_rates_mock, apply_mock):
        get_vat_rates_mock.return_value = []

        call_command(
            "monitor_vat_rates",
            ["-f=2024-01-01", "-t=2024-07-01", "--include-countries=AT", "--include-cpa_codes=01.1", "--update"],
            stdout=self.out,
        )

        apply_mock.assert_called_once_with([], ["AT"], ["01.1"], dry_run=False)

    @patch("django_pain.management.commands.monitor_vat_rates.TedbClient.get_vat_rates")
    def test_command_error(self, get_vat_rates_mock):
        get_vat_rates_mock.side_effect = ValueError("Broken!")

        with self.assertRaisesMessage(CommandError, "ValueError('Broken!')"):
            call_command(
                "monitor_vat_rates",
                ["-f=2024-01-01", "-t=2024-07-01", "--include-countries=AT", "--include-cpa_codes=01.1"],
                stdout=self.out,
            )

    @freeze_time("2024-06-25T12:00:00Z")
    @patch("django_pain.management.commands.monitor_vat_rates.TedbClient.get_vat_rates")
    def test_report_basic_dry_run(self, get_vat_rates_mock):
        data = [
            {
                "memberState": "SI",
                "type": "STANDARD",
                "rate": {"type": "DEFAULT", "value": 21.0},
                "situationOn": date(2024, 1, 1),
            },
            {
                "memberState": "SI",
                "type": "REDUCED",
                "rate": {"type": "REDUCED_RATE", "value": 10.0},
                "situationOn": date(2024, 1, 1),
                "cpaCodes": {"code": [{"value": "01.1"}]},
            },
        ]
        get_vat_rates_mock.return_value = self.response_type(vatRateResults=data).vatRateResults

        call_command(
            "monitor_vat_rates",
            [
                "-f=2024-01-01",
                "-t=2024-07-01",
                "--include-countries=SI",
                "--include-cpa_codes=01.1",
                "--verbosity=1",
            ],
            stdout=self.out,
        )

        with open(Path(__file__).parent / "data" / "monitor_vat_rates_basic.txt") as expected:
            self.assertEqual(self.out.getvalue(), expected.read())

    @freeze_time("2024-06-25T12:00:00Z")
    @patch("django_pain.management.commands.monitor_vat_rates.TedbClient.get_vat_rates")
    def test_report_extended_update(self, get_vat_rates_mock):
        VatRate.objects.create(
            country_code="SI",
            cpa_code=None,
            valid_from=datetime(2023, 1, 1, tzinfo=timezone.utc),
            valid_to=None,
            value=Decimal(22.0),
        )
        VatRate.objects.create(
            country_code="SI",
            cpa_code="01.1",
            valid_from=datetime(2023, 1, 1, tzinfo=timezone.utc),
            valid_to=None,
            value=Decimal(10.0),
        )
        data = [
            {
                "memberState": "SI",
                "type": "STANDARD",
                "rate": {"type": "DEFAULT", "value": 21.0},
                "situationOn": date(2024, 1, 1),
            },
            {
                "memberState": "SI",
                "type": "STANDARD",
                "rate": {"type": "DEFAULT", "value": 22.0},
                "situationOn": date(2024, 4, 1),
            },
            {
                "memberState": "SI",
                "type": "STANDARD",
                "rate": {"type": "DEFAULT", "value": 22.0},
                "situationOn": date(2024, 7, 1),
            },
            {
                "memberState": "SI",
                "type": "REDUCED",
                "rate": {"type": "REDUCED_RATE", "value": 10.0},
                "situationOn": date(2024, 4, 1),
                "cpaCodes": {"code": [{"value": "01.1"}]},
            },
        ]
        get_vat_rates_mock.return_value = self.response_type(vatRateResults=data).vatRateResults
        log_targets = ("django_pain.management.commands.monitor_vat_rates", "django_pain.vat_rate.updater")
        with LogCapture(log_targets, propagate=False) as log_handler:
            call_command(
                "monitor_vat_rates",
                [
                    "-f=2024-01-01",
                    "-t=2024-07-01",
                    "--exclude-countries=AT",
                    "--include-cpa_codes=01.1",
                    "--update",
                    "--verbosity=2",
                ],
                stdout=self.out,
            )

            with open(Path(__file__).parent / "data" / "monitor_vat_rates_extended.txt") as expected:
                self.assertEqual(self.out.getvalue(), expected.read())

            expected_logs = [
                ("django_pain.management.commands.monitor_vat_rates", "INFO", "Command `monitor_vat_rates` started"),
                (
                    "django_pain.vat_rate.updater",
                    "INFO",
                    "Called `apply` with 4 items for [BE, BG, CY, CZ, DE, DK, EE, ES, FI, FR, GR, HR, HU, IE, IT, LT, LU, LV, MT, NL, PL, PT, RO, SE, SI, SK] countries and [01.1] CPA codes in mode 'update'",
                ),
                (
                    "django_pain.vat_rate.updater",
                    "DEBUG",
                    "Item [Slovenia (SI), standard rate, valid: 2023-01-01T00:00:00+00:00 - 2024-01-01T00:00:00+00:00, VAT 22.0%] updated",
                ),
                (
                    "django_pain.vat_rate.updater",
                    "DEBUG",
                    "Item [Slovenia (SI), standard rate, valid: 2024-01-01T00:00:00+00:00 - ..., VAT 21.0%] created",
                ),
                (
                    "django_pain.vat_rate.updater",
                    "DEBUG",
                    "Item [Slovenia (SI), standard rate, valid: 2024-01-01T00:00:00+00:00 - 2024-04-01T00:00:00+00:00, VAT 21.0%] updated",
                ),
                (
                    "django_pain.vat_rate.updater",
                    "DEBUG",
                    "Item [Slovenia (SI), standard rate, valid: 2024-04-01T00:00:00+00:00 - ..., VAT 22.0%] created",
                ),
                (
                    "django_pain.vat_rate.updater",
                    "DEBUG",
                    "Item [Slovenia (SI), CPA code: 01.1, valid: 2023-01-01T00:00:00+00:00 - 2024-01-01T00:00:00+00:00, VAT 10.0%] updated",
                ),
                (
                    "django_pain.vat_rate.updater",
                    "DEBUG",
                    "Item [Slovenia (SI), CPA code: 01.1, valid: 2024-04-01T00:00:00+00:00 - ..., VAT 10.0%] created",
                ),
                (
                    "django_pain.vat_rate.updater",
                    "DEBUG",
                    "Item [Slovenia (SI), CPA code: 01.1, valid: 2024-04-01T00:00:00+00:00 - 2024-07-01T00:00:00+00:00, VAT 10.0%] updated",
                ),
                ("django_pain.vat_rate.updater", "INFO", "DB rows afftected: 5"),
                ("django_pain.management.commands.monitor_vat_rates", "INFO", "Command `monitor_vat_rates` finished"),
            ]
            log_handler.check_present(*expected_logs)
