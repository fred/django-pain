#
# Copyright (C) 2018-2024  CZ.NIC, z. s. p. o.
#
# This file is part of FRED.
#
# FRED is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# FRED is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with FRED.  If not, see <https://www.gnu.org/licenses/>.

"""Settings for tests."""

from pathlib import Path

USE_TZ = True
TIME_ZONE = "UTC"

SECRET_KEY = "Qapla'!"

INSTALLED_APPS = [
    "django.contrib.contenttypes",
    "django.contrib.auth",
    "django.contrib.sessions",
    "django.contrib.messages",
    "djmoney",
    "django_countries",
    "django_pain.apps.DjangoPainConfig",
    "django_lang_switch.apps.DjangoLangSwitchConfig",
    "django_pain.apps.DjangoPainAdminConfig",
]

MIDDLEWARE = [
    "django.contrib.sessions.middleware.SessionMiddleware",
    "django.contrib.auth.middleware.AuthenticationMiddleware",
    "django.contrib.messages.middleware.MessageMiddleware",
    "django_pain.middleware.TrafficLoggingMiddleware",
]

DATABASES = {"default": {"ENGINE": "django.db.backends.sqlite3", "NAME": ":memory:"}}

DEFAULT_AUTO_FIELD = "django.db.models.AutoField"

TEMPLATES = [
    {
        "BACKEND": "django.template.backends.django.DjangoTemplates",
        "DIRS": [],
        "APP_DIRS": True,
        "OPTIONS": {
            "context_processors": [
                "django.template.context_processors.debug",
                "django.template.context_processors.request",
                "django.contrib.auth.context_processors.auth",
                "django.contrib.messages.context_processors.messages",
            ],
        },
    },
]

PAIN_PROCESSORS = {}  # type: dict
PAIN_CSOB_GATEWAYS = {
    "default": {
        "API_URL": "https://api.example.org/",
        "API_PUBLIC_KEY": Path(__file__).parent / "empty_key.txt",
        "MERCHANT_ID": "abc123",
        "MERCHANT_PRIVATE_KEY": Path(__file__).parent / "empty_key.txt",
        "ACCOUNT_NUMBERS": {"CZK": "123456", "EUR": "234567"},
    },
}

PAIN_VIES_REQUESTER = "CZ67985726"

DEFAULT_CURRENCY = "CZK"
CURRENCIES = ["CZK", "EUR"]

PAIN_DOWNLOADERS = {}  # type: dict
