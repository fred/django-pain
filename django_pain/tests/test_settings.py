#
# Copyright (C) 2018-2024  CZ.NIC, z. s. p. o.
#
# This file is part of FRED.
#
# FRED is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# FRED is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with FRED.  If not, see <https://www.gnu.org/licenses/>.

"""Test processor utils."""

from datetime import timedelta
from typing import cast
from unittest.mock import sentinel
from warnings import warn

from django.conf import settings
from django.core.exceptions import ImproperlyConfigured, ValidationError
from django.test import SimpleTestCase, override_settings
from filed import Storage
from typist import SecretaryClient

from django_pain.settings import (
    SETTINGS,
    card_handler_validator,
    get_card_payment_handler_class,
    get_card_payment_handler_instance,
    get_file_storage_instance,
    get_processor_class,
    get_processor_instance,
    get_processor_objective,
    get_secretary_client_instance,
    is_eu_member,
    processor_validator,
    timeout_validator,
)

from .mixins import CacheResetMixin
from .utils import DummyCardPaymentHandler, DummyPaymentProcessor

try:
    from teller.downloaders import BankStatementDownloader
    from teller.parsers import BankStatementParser
except ImportError:
    warn("Failed to import teller library.")
    BankStatementDownloader = object  # type: ignore
    BankStatementParser = object  # type: ignore


class TestProcessorsSetting(SimpleTestCase):
    """Test ProcessorsSetting checker."""

    @override_settings(PAIN_PROCESSORS={"dummy": "django_pain.tests.utils.DummyPaymentProcessor"})
    def test_ok(self):
        """Test ok setting."""
        SETTINGS.check()
        self.assertEqual(SETTINGS.processors, {"dummy": DummyPaymentProcessor})

    @override_settings(PAIN_PROCESSORS=[])
    def test_not_dict(self):
        """Test not dictionary setting."""
        with self.assertRaisesRegex(ImproperlyConfigured, "PAIN_PROCESSORS must be {}, not {}".format(dict, list)):
            SETTINGS.check()

    @override_settings(PAIN_PROCESSORS={0: 1})
    def test_not_str_key(self):
        """Test dictionary with not str keys."""
        with self.assertRaisesRegex(ImproperlyConfigured, "All keys of PAIN_PROCESSORS must be {}".format(str)):
            SETTINGS.check()

    @override_settings(PAIN_PROCESSORS={"test_class": "django_pain.tests.test_settings.TestProcessorsSetting"})
    def test_not_correct_subclass(self):
        """Test not subclass of AbstractPaymentProcessor."""
        with self.assertRaisesRegex(
            ImproperlyConfigured,
            "{} is not subclass of AbstractPaymentProcessor".format(
                "django_pain.tests.test_settings.TestProcessorsSetting"
            ),
        ):
            SETTINGS.check()


class DummyDownloader(BankStatementDownloader):
    """Dummy class which does not do anything. It is used in TestDownloadersSetting."""


class DummyParser(BankStatementParser):
    """Dummy class which does not do anything. It is used in TestDownloadersSetting."""


class TestDownloadersSetting(SimpleTestCase):
    """Test DownloadersSetting."""

    test_settings = {
        "DOWNLOADER": "django_pain.tests.test_settings.DummyDownloader",
        "PARSER": "django_pain.tests.test_settings.DummyParser",
        "DOWNLOADER_PARAMS": {"param_name": "param_val"},
    }

    extra_key_settings = {
        "DOWNLOADER": "django_pain.tests.test_settings.DummyDownloader",
        "PARSER": "django_pain.tests.test_settings.DummyParser",
        "DOWNLOADER_PARAMS": {"param_name": "param_val"},
        "EXTRA": True,
    }

    missing_key_settings = {
        "DOWNLOADER": "django_pain.tests.test_settings.DummyDownloader",
        "DOWNLOADER_PARAMS": {"param_name": "param_val"},
    }

    invalid_sub_settings = {
        "DOWNLOADER": "django_pain.tests.test_settings.DummyDownloader",
        "PARSER": "django_pain.tests.test_settings.DummyParser",
        "DOWNLOADER_PARAMS": {1: 0},
    }

    invalid_downloader = {
        "DOWNLOADER": "django_pain.tests.test_settings.DummyParser",
        "PARSER": "django_pain.tests.test_settings.DummyParser",
        "DOWNLOADER_PARAMS": {"param_name": "param_val"},
    }

    invalid_parser = {
        "DOWNLOADER": "django_pain.tests.test_settings.DummyDownloader",
        "PARSER": "django_pain.tests.test_settings.DummyDownloader",
        "DOWNLOADER_PARAMS": {"param_name": "param_val"},
    }

    @override_settings(PAIN_DOWNLOADERS={"dummy": test_settings})
    def test_ok(self):
        """Test ok setting."""
        SETTINGS.check()
        expected = {
            "DOWNLOADER": DummyDownloader,
            "PARSER": DummyParser,
            "DOWNLOADER_PARAMS": {"param_name": "param_val"},
        }
        self.assertEqual(SETTINGS.downloaders, {"dummy": expected})

    @override_settings(PAIN_DOWNLOADERS=[])
    def test_not_dict(self):
        """Test not dictionary setting."""
        with self.assertRaisesRegex(ImproperlyConfigured, "PAIN_DOWNLOADERS must be {}, not {}".format(dict, list)):
            SETTINGS.check()

    @override_settings(PAIN_DOWNLOADERS={0: {}})
    def test_not_str_key(self):
        """Test dictionary with not str keys."""
        expected = "The key 0 is not of type str."
        with self.assertRaisesRegex(ImproperlyConfigured, expected):
            SETTINGS.check()

    @override_settings(PAIN_DOWNLOADERS={"dummy": 1})
    def test_not_dict_value(self):
        """Test dictionary with not dict values."""
        expected = "Item dummy's value 1 is not of type dict."
        with self.assertRaisesRegex(ImproperlyConfigured, expected):
            SETTINGS.check()

    @override_settings(PAIN_DOWNLOADERS={"dummy": missing_key_settings})
    def test_wrong_keys(self):
        """Test dictionary with wrong keys."""
        expected = "Invalid keys."
        with self.assertRaisesRegex(ImproperlyConfigured, expected):
            SETTINGS.check()

    @override_settings(PAIN_DOWNLOADERS={"dummy": extra_key_settings})
    def test_extra_keys(self):
        """Test dictionary with extra keys."""
        expected = "Invalid keys."
        with self.assertRaisesRegex(ImproperlyConfigured, expected):
            SETTINGS.check()

    @override_settings(PAIN_DOWNLOADERS={"dummy": invalid_sub_settings})
    def test_invalid_subsettings(self):
        """Test invalid subsettings."""
        expected = "The key 1 is not of type str."
        with self.assertRaisesRegex(ImproperlyConfigured, expected):
            SETTINGS.check()

    @override_settings()
    def test_not_required(self):
        """Test there is no exception when the setting is missing."""
        del settings.PAIN_DOWNLOADERS
        SETTINGS.check()


class TestCallableListSetting(SimpleTestCase):
    """Test CallableListSetting."""

    @override_settings(PAIN_IMPORT_CALLBACKS=["django_pain.settings.PainSettings"])
    def test_ok(self):
        SETTINGS.check()

    @override_settings(PAIN_IMPORT_CALLBACKS=["django_pain.tests.test_settings"])
    def test_not_callable(self):
        with self.assertRaisesMessage(ImproperlyConfigured, "CALLBACKS must be a list of dotted paths to callables"):
            SETTINGS.check()

    @override_settings(PAIN_IMPORT_CALLBACKS=[1, 2, 3])
    def test_not_a_string(self):
        with self.assertRaisesMessage(ImproperlyConfigured, "CALLBACKS must be a list of dotted paths to callables"):
            SETTINGS.check()

    @override_settings(PAIN_IMPORT_CALLBACKS=["django_pain._non._existing._module"])
    def test_non_existing_import(self):
        with self.assertRaises(ImportError):
            SETTINGS.check()


class TestEUMembersSetting(SimpleTestCase):
    """Test list of EU countries."""

    @override_settings(PAIN_EU_MEMBERS=["AT", "CZ", "XX"])
    def test_invalid_country(self):
        with self.assertRaisesMessage(ImproperlyConfigured, "Setting  has an invalid value: ['Invalid country code']"):
            SETTINGS.check()


@override_settings(PAIN_PROCESSORS={"dummy": "django_pain.tests.utils.DummyPaymentProcessor"})
class TestGetProcessorClass(CacheResetMixin, SimpleTestCase):
    """Test get_processor_class."""

    def test_success(self):
        """Test successful import."""
        self.assertEqual(get_processor_class("dummy"), DummyPaymentProcessor)

    def test_invalid(self):
        """Test not defined processor."""
        processor = "invalid.package.name"
        with self.assertRaisesRegex(ValueError, "{} is not present in PAIN_PROCESSORS setting".format(processor)):
            get_processor_class(processor)


@override_settings(PAIN_PROCESSORS={"dummy": "django_pain.tests.utils.DummyPaymentProcessor"})
class TestGetProcessorInstance(CacheResetMixin, SimpleTestCase):
    """Test get_processor_instance."""

    def test_success(self):
        """Test success."""
        self.assertIsInstance(get_processor_instance("dummy"), DummyPaymentProcessor)


@override_settings(PAIN_PROCESSORS={"dummy": "django_pain.tests.utils.DummyPaymentProcessor"})
class TestGetProcessorObjective(CacheResetMixin, SimpleTestCase):
    """Test get_processor_objective."""

    def test_success(self):
        """Test success."""
        self.assertEqual(get_processor_objective("dummy"), "Dummy objective")


@override_settings(
    PAIN_CARD_PAYMENT_HANDLERS={
        "dummy": {"HANDLER": "django_pain.tests.utils.DummyCardPaymentHandler", "GATEWAY": "default"}
    }
)
class TestGetCardPaymentHandlerClass(CacheResetMixin, SimpleTestCase):
    """Test get_payment_handler_class."""

    def test_success(self):
        """Test successful import."""
        self.assertEqual(get_card_payment_handler_class("dummy"), DummyCardPaymentHandler)

    def test_invalid(self):
        """Test not defined card payment handler."""
        handler = "invalid.package.name"
        with self.assertRaisesRegex(
            ValueError, "{} is not present in PAIN_CARD_PAYMENT_HANDLERS setting".format(handler)
        ):
            get_card_payment_handler_class(handler)


@override_settings(
    PAIN_CARD_PAYMENT_HANDLERS={
        "dummy": {"HANDLER": "django_pain.tests.utils.DummyCardPaymentHandler", "GATEWAY": "default"}
    }
)
class TestGetCardPaymentHandlerInstance(CacheResetMixin, SimpleTestCase):
    """Test get_card_payment_handler_instance."""

    def test_success(self):
        """Test success."""
        self.assertIsInstance(get_card_payment_handler_instance("dummy"), DummyCardPaymentHandler)


class TestResendEmailSetting(SimpleTestCase):
    """Test resend email settings."""

    def test_get_resend_email_settings(self):
        data = [
            ("empty", {}, None),
            (
                "default",
                {"RESEND_EMAIL": {"TIME_DELTAS": [1]}},
                {
                    "processors": None,
                    "time_deltas": (timedelta(days=1),),
                    "min_delta": timedelta(days=3),
                },
            ),
            (
                "full",
                {"RESEND_EMAIL": {"PROCESSORS": ["proc1", "proc2"], "TIME_DELTAS": [1, -3], "MIN_DELTA": 2}},
                {
                    "processors": ["proc1", "proc2"],
                    "time_deltas": (timedelta(days=1), timedelta(days=-3)),
                    "min_delta": timedelta(days=2),
                },
            ),
        ]
        for name, handler_settings, result in data:
            with self.subTest(name=name):
                with override_settings(PAIN_CSOB_CUSTOM_HANDLER=handler_settings):
                    self.assertEqual(SETTINGS.csob_custom_handler["resend_email"], result)


class TestHttpWithSlashValidator(SimpleTestCase):
    """Test web link validator."""

    def test_ok(self):
        # Test with valid values
        data = [
            (1, {}, "https://platebnibrana.csob.cz/zaplat/"),
            (2, {"CUSTOM_PAYMENT_URL": "http://www.example.org/path/"}, "http://www.example.org/path/"),
            (3, {"CUSTOM_PAYMENT_URL": "https://www.example.org/path/"}, "https://www.example.org/path/"),
        ]
        for number, url_settings, result in data:
            with self.subTest(number=number):
                with override_settings(PAIN_CSOB_CUSTOM_HANDLER=url_settings):
                    SETTINGS.check()
                    self.assertEqual(SETTINGS.csob_custom_handler["custom_payment_url"], result)

    def test_validation_fail(self):
        # Test with invalid values
        data = [
            (1, {"CUSTOM_PAYMENT_URL": "ftp://www.example.org/path"}),  # bad scheme
            (2, {"CUSTOM_PAYMENT_URL": "www.example.org/path/"}),  # no scheme
            (3, {"CUSTOM_PAYMENT_URL": "http://"}),  # only scheme
            (4, {"CUSTOM_PAYMENT_URL": "http://www.example.org/path"}),  # missing trailing slash
        ]
        for number, url_settings in data:
            with self.subTest(number=number):
                with override_settings(PAIN_CSOB_CUSTOM_HANDLER=url_settings):
                    with self.assertRaisesRegex(ImproperlyConfigured, "CUSTOM_PAYMENT_URL"):
                        SETTINGS.check()


class TestCardHandlersSetting(SimpleTestCase):
    """Test settings of card payment handlers."""

    def test_ok(self):
        handler_settings = {
            "dummy_1": {"HANDLER": "django_pain.tests.utils.DummyCardPaymentHandler", "GATEWAY": "dummy_cfg_1"},
            "dummy_2": {"HANDLER": "django_pain.tests.utils.DummyCardPaymentHandler", "GATEWAY": "dummy_cfg_2"},
        }
        with override_settings(PAIN_CARD_PAYMENT_HANDLERS=handler_settings):
            SETTINGS.check()
            self.assertEqual(
                SETTINGS.card_payment_handlers,
                {
                    "dummy_1": {"HANDLER": DummyCardPaymentHandler, "GATEWAY": "dummy_cfg_1"},
                    "dummy_2": {"HANDLER": DummyCardPaymentHandler, "GATEWAY": "dummy_cfg_2"},
                },
            )

    def test_validation_failure(self):
        handler_settings = {
            "dummy": {"HANDLER": "django_pain.tests.utils.DummyPaymentProcessor", "GATEWAY": "dummy_cfg"},
        }
        with override_settings(PAIN_CARD_PAYMENT_HANDLERS=handler_settings):
            with self.assertRaisesRegex(
                ImproperlyConfigured,
                "django_pain.tests.utils.DummyPaymentProcessor is not subclass of AbstractCardPaymentHandler",
            ):
                SETTINGS.check()


class TestEUMembership(SimpleTestCase):
    """Test `is_eu_member` function."""

    def test_true(self):
        self.assertTrue(is_eu_member("CZ"))

    def test_false(self):
        self.assertFalse(is_eu_member("GB"))


@override_settings(
    PAIN_CARD_PAYMENT_HANDLERS={
        "test_handler": {"HANDLER": "django_pain.tests.utils.DummyCardPaymentHandler", "GATEWAY": "default"}
    },
    PAIN_PROCESSORS={"test_processor": "django_pain.tests.utils.DummyPaymentProcessor"},
)
class TestPaymentRequestsValidators(SimpleTestCase):
    """Test validators for for payment requests setting."""

    def test_card_handler_ok(self):
        card_handler_validator("test_handler")

    def test_bad_card_handler(self):
        self.assertRaisesMessage(
            ValidationError,
            "Card handler `bad_handler` not found in `PAIN_CARD_PAYMENT_HANDLERS`",
            card_handler_validator,
            "bad_handler",
        )

    def test_processor_ok(self):
        processor_validator("test_processor")

    def test_bad_processor(self):
        self.assertRaisesMessage(
            ValidationError,
            "Processor `bad_processor` not found in `PAIN_PROCESSORS`",
            processor_validator,
            "bad_processor",
        )


class TestTimeoutValidator(SimpleTestCase):
    """Test timeout validator for `requests`."""

    def test_ok(self):
        # Test with valid values
        data = [
            10,  # int
            10.0,  # float
            (1, 5),  # int tuple
            (1.0, 5.0),  # float tuple
            (1, 5.0),  # mixed int/float tuple
        ]
        for timeout in data:
            with self.subTest(timeout=timeout):
                timeout_validator(timeout)

    def test_validation_fail(self):
        # Test with invalid values
        data = [
            None,
            "string",
            (1, "string"),
            (1, 2, 3),
        ]
        for timeout in data:
            with self.subTest(timeout=timeout):
                self.assertRaises(ValidationError, timeout_validator, timeout)


class TestGetSecretaryClientInstance(CacheResetMixin, SimpleTestCase):
    """Test retrieving of secretary client."""

    def test_none(self):
        self.assertIsNone(get_secretary_client_instance())

    @override_settings(PAIN_SECRETARY_URL="http://example.org/")
    def test(self):
        client = cast(SecretaryClient, get_secretary_client_instance())

        self.assertEqual(client.server_url, "http://example.org/")
        self.assertIsNone(client.auth)

    @override_settings(PAIN_SECRETARY_URL="http://example.org/", PAIN_SECRETARY_TOKEN="Gazpacho!")
    def test_token(self):
        client = cast(SecretaryClient, get_secretary_client_instance())

        self.assertEqual(client.server_url, "http://example.org/")
        self.assertEqual(client.auth.token, "Gazpacho!")  # type: ignore[union-attr]

    @override_settings(PAIN_SECRETARY_URL="http://example.org/", PAIN_SECRETARY_TIMEOUT=sentinel.timeout)
    def test_timeout(self):
        client = cast(SecretaryClient, get_secretary_client_instance())

        self.assertEqual(client.server_url, "http://example.org/")
        self.assertEqual(client.timeout, sentinel.timeout)


class TestGetFileStorageInstance(CacheResetMixin, SimpleTestCase):
    """Test retrieving of file storage."""

    def test_none(self):
        self.assertIsNone(get_file_storage_instance())

    @override_settings(PAIN_FILEMAN_NETLOC="localhost:50000")
    def test_instance(self):
        self.assertTrue(isinstance(get_file_storage_instance(), Storage))
