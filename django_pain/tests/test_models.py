#
# Copyright (C) 2018-2024  CZ.NIC, z. s. p. o.
#
# This file is part of FRED.
#
# FRED is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# FRED is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with FRED.  If not, see <https://www.gnu.org/licenses/>.

"""Test models."""

from datetime import date, datetime, timezone
from decimal import Decimal
from typing import Any, Dict, List, Tuple
from unittest.mock import ANY, MagicMock, Mock, call
from uuid import UUID

from django.core.exceptions import ValidationError
from django.db import IntegrityError
from django.db.models import BLANK_CHOICE_DASH
from django.test import SimpleTestCase, TestCase, override_settings
from djmoney.money import Money
from filed import Storage
from freezegun import freeze_time
from typist import SecretaryClient

from django_pain.constants import InvoiceType, PaymentProcessingError, PaymentState, PaymentType
from django_pain.models import (
    BankPayment,
    Customer,
    Invoice,
    InvoiceNumberSequence,
    PaymentImportHistory,
    PaymentRequest,
    ProcessPaymentResultType,
    VariableSymbolSequence,
)
from django_pain.models.invoices import BillingItemType, InvoiceItem, PaymentRequestItem, VatRate
from django_pain.utils import ProcessPaymentResult, local_date_start

from .mixins import CacheResetMixin
from .utils import (
    create_invoice,
    create_payment_request,
    get_account,
    get_customer,
    get_invoice,
    get_invoice_context_data,
    get_invoice_ref,
    get_payment,
    get_payment_request,
    get_payment_request_context_data,
)


class TestBankAccount(SimpleTestCase):
    """Test BankAccount model."""

    def test_str(self):
        """Test string representation."""
        account = get_account(account_name="Account", account_number="123")
        self.assertEqual(str(account), "Account 123")


class TestBankPayment(CacheResetMixin, TestCase):
    """Test BankPayment model."""

    def test_str(self):
        """Test string representation."""
        payment = get_payment(identifier="PID")
        self.assertEqual(str(payment), "PID")

    def test_currency_constraint_success(self):
        """Test clean method with not violated constraint."""
        account = get_account(currency="USD")
        payment = get_payment(account=account, amount=Money("999.00", "USD"))
        payment.clean()

    def test_currency_constraint_error(self):
        """Test clean method with violated constraint."""
        account = get_account(account_name="ACCOUNT", account_number="123", currency="USD")
        payment = get_payment(identifier="PAYMENT", account=account, amount=Money("999.00", "CZK"))
        with self.assertRaisesMessage(
            ValidationError,
            "Bank payment PAYMENT is in different currency (CZK) " "than bank account ACCOUNT 123 (USD).",
        ):
            payment.clean()

    @override_settings(PAIN_PROCESSORS={"dummy": "django_pain.tests.utils.DummyPaymentProcessor"})
    def test_objective_choices(self):
        self.assertEqual(
            BankPayment.objective_choices(),
            BLANK_CHOICE_DASH
            + [
                ("dummy", "Dummy objective"),
            ],
        )

    def test_advance_invoice(self):
        account = get_account()
        account.save()
        payment = get_payment(account=account)
        payment.save()

        account_invoice = get_invoice_ref(number="1", invoice_type=InvoiceType.ACCOUNT)
        account_invoice.save()
        account_invoice.payments.add(payment)
        self.assertEqual(payment.advance_invoice, None)

        advance_invoice = get_invoice_ref(number="2", invoice_type=InvoiceType.ADVANCE)
        advance_invoice.save()
        advance_invoice.payments.add(payment)
        self.assertEqual(payment.advance_invoice, advance_invoice)

    @override_settings(PAIN_PROCESSORS={"dummy": "django_pain.tests.utils.DummyPaymentProcessor"})
    def test_objective(self):
        payment = get_payment()
        self.assertEqual(payment.objective, "")

        payment = get_payment(processor="dummy")
        self.assertEqual(payment.objective, "Dummy objective")

    def test_transfer_may_not_have_counter_account(self):
        account = get_account()
        account.save()

        payment = get_payment(account=account, payment_type=PaymentType.TRANSFER, counter_account_number="")
        payment.save()

        self.assertEqual(BankPayment.objects.get().counter_account_number, "")

    def test_card_payment_must_not_have_counter_account(self):
        account = get_account()
        account.save()

        payment = get_payment(account=account, payment_type=PaymentType.CARD_PAYMENT, counter_account_number="123")
        self.assertRaises(IntegrityError, payment.save)

    def test_custom_payment(self):
        account = get_account()
        account.save()
        payment = get_payment(
            account=account,
            payment_type=PaymentType.CARD_PAYMENT,
            counter_account_number="",
            email="user@example.org",
            custom_expiry=datetime(2023, 7, 31, 12, 0, 0, tzinfo=timezone.utc),
            customer_code="ABC123",
            email_sent=datetime(2023, 8, 1, 9, 0, 0, tzinfo=timezone.utc),
        )
        payment.save()

        payment = BankPayment.objects.get()
        self.assertEqual(payment.email, "user@example.org")
        self.assertEqual(payment.custom_expiry, datetime(2023, 7, 31, 12, 0, 0, tzinfo=timezone.utc))
        self.assertEqual(payment.customer_code, "ABC123")
        self.assertEqual(payment.email_sent, datetime(2023, 8, 1, 9, 0, 0, tzinfo=timezone.utc))

    def test_custom_payment_defaults(self):
        account = get_account()
        account.save()
        payment = get_payment(
            account=account,
            payment_type=PaymentType.CARD_PAYMENT,
            counter_account_number="",
        )
        payment.save()

        payment = BankPayment.objects.get()
        self.assertIsNone(payment.email)
        self.assertIsNone(payment.custom_expiry)
        self.assertEqual(payment.customer_code, "")
        self.assertIsNone(payment.email_sent)

    def test_save_processing_result(self):
        data = [
            ("id-1", ProcessPaymentResultType.PROCESSED, None, PaymentState.PROCESSED),
            ("id-2", ProcessPaymentResultType.TO_REFUND, None, PaymentState.TO_REFUND),
            ("id-3", ProcessPaymentResultType.NOT_PROCESSED, PaymentProcessingError.OVERDUE, PaymentState.DEFERRED),
        ]
        account = get_account()
        account.save()
        for id, result, error, state in data:
            with self.subTest(id=id):
                payment = get_payment(identifier=id, account=account)

                payment.save_processing_result(ProcessPaymentResult(result=result, error=error))

                payment = BankPayment.objects.get(identifier=id)
                self.assertEqual(payment.state, state)
                self.assertEqual(payment.processing_error, error)


class TestPaymentImportHistory(CacheResetMixin, TestCase):
    """Test PaymentImportHistory model."""

    @freeze_time("2021-02-01T10:15Z")
    def test_str(self):
        """Test string representation."""
        import_history = PaymentImportHistory(origin="test")
        import_history.save()
        self.assertEqual(str(import_history), "test 2021-02-01 10:15:00+00:00")

    @freeze_time("2021-02-01T10:15Z")
    def test_can_not_override_start_datetime(self):
        """Test string representation."""
        import_history = PaymentImportHistory(origin="test", start_datetime=datetime(2021, 4, 1, tzinfo=timezone.utc))
        import_history.save()
        self.assertEqual(str(import_history), "test 2021-02-01 10:15:00+00:00")

    def test_filenames(self):
        import_history = PaymentImportHistory(origin="test")
        self.assertEqual(list(import_history.filenames), [])

        import_history.add_filename("file_1.txt")
        self.assertEqual(list(import_history.filenames), ["file_1.txt"])

        import_history.add_filename("file_2.txt")
        self.assertEqual(list(import_history.filenames), ["file_1.txt", "file_2.txt"])

    def test_filename_can_not_contain_separator(self):
        import_history = PaymentImportHistory(origin="test")
        with self.assertRaisesRegex(ValueError, "Character ; not allowed in filenames"):
            import_history.add_filename("file;name.txt")

    def test_success(self):
        """Test success property."""
        self.assertTrue(PaymentImportHistory(origin="test", errors=0, finished=True).success)
        self.assertFalse(PaymentImportHistory(origin="test", errors=1, finished=True).success)
        self.assertFalse(PaymentImportHistory(origin="test", errors=None, finished=True).success)
        self.assertFalse(PaymentImportHistory(origin="test", errors=0, finished=False).success)


class TestCustomer(TestCase):
    """Test `Customer` model."""

    def test_str(self):
        """Test string representation."""
        data = [
            ("John Doe", None, "CID-1: John Doe"),
            ("John Doe", "The Company, Ltd.", "CID-1: John Doe"),
            (None, "The Company, Ltd.", "CID-1: The Company, Ltd."),
            ("", "The Company, Ltd.", "CID-1: The Company, Ltd."),
        ]
        for name, organization, expected in data:
            with self.subTest(name=name, organization=organization):
                customer = get_customer(name=name, organization=organization)
                self.assertEqual(str(customer), expected)

    def test_update_vies_none(self):
        customer = get_customer(vies_vat_valid=True, vies_checked_at=datetime(2024, 1, 1, tzinfo=timezone.utc))
        customer.save()

        customer.update_vies_info(None)

        customer = Customer.objects.get()
        self.assertIsNone(customer.vies_vat_valid)
        self.assertIsNone(customer.vies_checked_at)

    @freeze_time("2024-01-01T12:00:00Z")
    def test_update_vies_false(self):
        customer = get_customer()
        customer.save()

        customer.update_vies_info(False)

        customer = Customer.objects.get()
        self.assertFalse(customer.vies_vat_valid)
        self.assertEqual(customer.vies_checked_at, datetime(2024, 1, 1, 12, 0, 0, tzinfo=timezone.utc))

    @freeze_time("2024-01-01T12:00:00Z")
    def test_update_vies_true(self):
        customer = get_customer()
        customer.save()

        customer.update_vies_info(True)

        customer = Customer.objects.get()
        self.assertTrue(customer.vies_vat_valid)
        self.assertEqual(customer.vies_checked_at, datetime(2024, 1, 1, 12, 0, 0, tzinfo=timezone.utc))

    def test_reverse_charge_mode_true(self):
        customer = get_customer(vat_number="DE0001", country_code="DE", vies_vat_valid=True)
        self.assertTrue(customer.reverse_charge_mode)

    def test_get_reverse_charge_mode_false(self):
        data: List[Dict[str, Any]] = [
            {"vat_number": "DE0001", "country_code": "DE", "vies_vat_valid": False},  # invalid VAT number
            {"vat_number": None, "country_code": "DE", "vies_vat_valid": False},  # no VAT number
            {"vat_number": "CZ0001", "country_code": "CZ", "vies_vat_valid": True},  # Czech resident
            {"vat_number": "CZ0001", "country_code": "DE", "vies_vat_valid": True},  # Czech VAT number
            {"vat_number": "CH0001", "country_code": "CH"},  # non-EU resident
            {"vat_number": "DE0001", "country_code": "DE", "vies_vat_valid": None},  # VAT number not checked yet
        ]
        for kwargs in data:
            with self.subTest(data=kwargs):
                customer = get_customer(**kwargs)
                self.assertFalse(customer.reverse_charge_mode)

    def test_tax_country_code(self):
        data: List[Tuple[Dict[str, Any], str]] = [
            # tax country code from VAT number
            ({"vat_number": "DE0001", "country_code": "CZ", "vies_vat_valid": True}, "DE"),
            # tax country code from address
            ({"vat_number": "DE0001", "country_code": "CZ", "vies_vat_valid": False}, "CZ"),  # invalid VAT number
            ({"vat_number": None, "country_code": "CZ"}, "CZ"),  # no VAT number
            ({"vat_number": "XX0001", "country_code": "CH"}, "CH"),  # non-EU resident
            ({"vat_number": "DE0001", "country_code": "CZ", "vies_vat_valid": None}, "CZ"),  # not VIES checked yet
        ]
        for kwargs, expected in data:
            with self.subTest(data=kwargs):
                customer = get_customer(**kwargs)
                self.assertEqual(customer.tax_country_code, expected)


class TestVariableSymbolSequence(SimpleTestCase):
    """Test model for variable symbol sequence."""

    def test_str(self):
        """Test string representation."""
        data: List[Tuple[Dict[str, Any], str]] = [
            ({"prefix": 0}, "0000000000"),
            ({"prefix": 1, "current_number": 1234}, "0001001234"),
            ({"prefix": 1234}, "1234000000"),
        ]
        for num_items, expected in data:
            with self.subTest(num_items=num_items):
                items = {"app_id": "test"}
                items.update(num_items)
                vs_sequence = VariableSymbolSequence(**items)
                self.assertEqual(str(vs_sequence), f"test: {expected}")

    @override_settings(PAIN_TRIM_VARSYM=True)
    def test_str_trimmed(self):
        """Test string representation."""
        data: List[Tuple[Dict[str, Any], str]] = [
            ({"prefix": 0}, ""),
            ({"prefix": 1, "current_number": 1234}, "1001234"),
            ({"prefix": 1234}, "1234000000"),
        ]
        for num_items, expected in data:
            with self.subTest(num_items=num_items):
                items = {"app_id": "test"}
                items.update(num_items)
                vs_sequence = VariableSymbolSequence(**items)
                self.assertEqual(str(vs_sequence), f"test: {expected}")


class TestVariableSymbolSequenceManager(TestCase):
    """Test of retrieving variable_symbol from sequence."""

    def setUp(self) -> None:
        vs_item = VariableSymbolSequence(app_id="test", year=2023, prefix=823, current_number=205)
        vs_item.save()

    def test_get_ok(self):
        self.assertEqual(VariableSymbolSequence.objects.next_value("test", 2023), "0823000206")
        vs_sequence = VariableSymbolSequence.objects.get(app_id="test", year=2023)
        self.assertEqual(vs_sequence.current_number, 206)

    def test_sequence_not_found(self):
        with self.assertRaises(VariableSymbolSequence.DoesNotExist):
            VariableSymbolSequence.objects.next_value("other_test", 2023)

    @override_settings(PAIN_TRIM_VARSYM=True)
    def test_get_ok_trimmed(self):
        self.assertEqual(VariableSymbolSequence.objects.next_value("test", 2023), "823000206")


class TestPaymentRequest(TestCase):
    """Test `PaymentRequest` model."""

    def test_str(self):
        """Test string representation."""
        self.assertEqual(
            str(get_payment_request()),
            "00000000-0000-0000-0000-000000000001/CID-1: Test, s.r.o. 25.60 CZK at 2023-10-01",
        )

    def test_get_context(self):
        create_payment_request()

        self.assertEqual(PaymentRequest.objects.get()._get_pdf_context(), get_payment_request_context_data())


class TestPaymentRequestRenderPdf(TestCase):
    """Test `PaymentRequest` rendering to PDF."""

    def setUp(self):
        create_payment_request(realized_payment=None)
        self.payment_request = PaymentRequest.objects.get()
        self.secretary_mock = Mock(spec=SecretaryClient)
        self.storage_mock = MagicMock(spec=Storage)

    def test_render_ok(self):
        self.secretary_mock.render_pdf.return_value = b"PDF binary contents"
        self.storage_mock.create.return_value.__enter__.return_value.uuid = UUID(int=10)

        self.payment_request.render_pdf(self.secretary_mock, self.storage_mock)

        payment_request = PaymentRequest.objects.get()
        self.assertEqual(payment_request.file_uid, "00000000-0000-0000-0000-00000000000a")
        self.assertEqual(
            self.secretary_mock.mock_calls,
            [call.render_pdf("pain-payment-request-cs.html", get_payment_request_context_data())],
        )
        expected_calls = [
            call.create("payment_request_00000000-0000-0000-0000-000000000001.pdf", mimetype="application/pdf"),
            call.create().__enter__(),
            call.create().__enter__().write(b"PDF binary contents"),
            call.create().__exit__(None, None, None),
        ]
        self.assertEqual(self.storage_mock.mock_calls, expected_calls)

    def test_render_ok_template_en(self):
        self.secretary_mock.render_pdf.return_value = b"PDF binary contents"
        self.storage_mock.create.return_value.__enter__.return_value.uuid = UUID(int=10)
        self.payment_request.country_code = "DE"  # simulate German customer

        self.payment_request.render_pdf(self.secretary_mock, self.storage_mock)

        payment_request = PaymentRequest.objects.get()
        self.assertEqual(payment_request.file_uid, "00000000-0000-0000-0000-00000000000a")
        self.assertEqual(
            self.secretary_mock.mock_calls,
            [call.render_pdf("pain-payment-request-en.html", ANY)],  # English template in use
        )

    def test_already_rendered(self):
        self.payment_request.file_uid = "file-1"
        self.payment_request.save(update_fields=["file_uid"])

        self.payment_request.render_pdf(self.secretary_mock, self.storage_mock)

        payment_request = PaymentRequest.objects.get()
        self.assertEqual(str(payment_request.file_uid), "file-1")
        self.secretary_mock.assert_not_called()
        self.storage_mock.assert_not_called()


class TestPaymentRequestManager(TestCase):
    """Test customized manager for `PaymentRequest` model."""

    def test_found_ok(self):
        payment_request = create_payment_request(realized_payment=None)
        payment = get_payment(
            identifier="pid-1",
            account=payment_request.bank_account,
            variable_symbol=payment_request.variable_symbol,
            amount=Money(payment_request.price_to_pay, payment_request.currency),
            transaction_date=date(2023, 10, 5),
        )
        payment.save()
        payment = BankPayment.objects.get(identifier="pid-1")

        found, error = PaymentRequest.objects.find("auctions", payment)
        self.assertEqual(payment_request, found)
        self.assertIsNone(error)

    def test_not_found_closed(self):
        payment_request = create_payment_request(
            realized_payment=None, closed_at=datetime(2023, 9, 15, tzinfo=timezone.utc)
        )
        payment = get_payment(
            identifier="pid-1",
            account=payment_request.bank_account,
            variable_symbol=payment_request.variable_symbol,
            amount=Money(payment_request.price_to_pay, payment_request.currency),
            transaction_date=date(2023, 10, 5),
        )
        payment.save()

        found, _ = PaymentRequest.objects.find("auctions", payment)
        self.assertIsNone(found)

    def test_not_found_vs(self):
        payment_request = create_payment_request(realized_payment=None)
        payment = get_payment(
            identifier="pid-1",
            account=payment_request.bank_account,
            variable_symbol="1234567890",
            amount=Money(payment_request.price_to_pay, payment_request.currency),
            transaction_date=date(2023, 10, 5),
        )
        payment.save()

        found, _ = PaymentRequest.objects.find("auctions", payment)
        self.assertIsNone(found)

    def test_not_found_currency(self):
        payment_request = create_payment_request(realized_payment=None)
        payment = get_payment(
            identifier="pid-1",
            account=payment_request.bank_account,
            variable_symbol=payment_request.variable_symbol,
            amount=Money(payment_request.price_to_pay, "EUR"),
            transaction_date=date(2023, 10, 5),
        )
        payment.save()
        payment = BankPayment.objects.get(identifier="pid-1")

        found, _ = PaymentRequest.objects.find("auctions", payment)
        self.assertIsNone(found)

    def test_found_overdue(self):
        payment_request = create_payment_request(
            realized_payment=None, due_at=datetime(2023, 10, 1, tzinfo=timezone.utc)
        )
        payment = get_payment(
            identifier="pid-1",
            account=payment_request.bank_account,
            variable_symbol=payment_request.variable_symbol,
            amount=Money(payment_request.price_to_pay, payment_request.currency),
            transaction_date=date(2023, 10, 5),
        )
        payment.save()

        found, error = PaymentRequest.objects.find("auctions", payment)
        self.assertEqual(payment_request, found)
        self.assertIs(error, PaymentProcessingError.OVERDUE)

    def test_found_insufficient_amount(self):
        payment_request = create_payment_request(realized_payment=None)
        payment = get_payment(
            identifier="pid-1",
            account=payment_request.bank_account,
            variable_symbol=payment_request.variable_symbol,
            amount=Money(payment_request.price_to_pay - 1, payment_request.currency),
            transaction_date=date(2023, 10, 5),
        )
        payment.save()

        found, error = PaymentRequest.objects.find("auctions", payment)
        self.assertEqual(payment_request, found)
        self.assertIs(error, PaymentProcessingError.INSUFFICIENT_AMOUNT)

    def test_found_excessive_amount(self):
        payment_request = create_payment_request(realized_payment=None)
        payment = get_payment(
            identifier="pid-1",
            account=payment_request.bank_account,
            variable_symbol=payment_request.variable_symbol,
            amount=Money(payment_request.price_to_pay + 1, payment_request.currency),
            transaction_date=date(2023, 10, 5),
        )
        payment.save()

        found, error = PaymentRequest.objects.find("auctions", payment)
        self.assertEqual(payment_request, found)
        self.assertIs(error, PaymentProcessingError.EXCESSIVE_AMOUNT)

    def test_found_card_payment(self):
        payment_request = create_payment_request(realized_payment=None)
        payment = get_payment(
            identifier="pid-1",
            payment_type=PaymentType.CARD_PAYMENT,
            account=payment_request.bank_account,
            counter_account_number="",
            counter_account_name="",
            variable_symbol=payment_request.variable_symbol,
            amount=Money(payment_request.price_to_pay, payment_request.currency),
            transaction_date=date(2023, 10, 5),
        )
        payment.save()
        payment_request.card_payment = payment
        payment_request.save(update_fields=["card_payment"])

        found, error = PaymentRequest.objects.find("auctions", payment)
        self.assertEqual(payment_request, found)
        self.assertIsNone(error)

    @freeze_time("2024-01-20T10:00:00Z")
    def test_filter_for_invoice(self):
        # Prepare data
        BillingItemType.objects.create(handle="basic_rate", not_invoiceable=False)
        BillingItemType.objects.create(handle="penalty", not_invoiceable=True)
        customer_1 = get_customer(
            uid="CID-1", vies_vat_valid=True, vies_checked_at=datetime(2024, 1, 15, 5, 0, 0, tzinfo=timezone.utc)
        )
        customer_2 = get_customer(uid="CID-2")  # not VIES validated
        account = get_account()
        payment_1 = get_payment(uuid=UUID(int=21), identifier="pid-1", account=account)
        payment_2 = get_payment(uuid=UUID(int=22), identifier="pid-2", account=account)
        payment_3 = get_payment(uuid=UUID(int=23), identifier="pid-3", account=account)
        payment_4 = get_payment(uuid=UUID(int=24), identifier="pid-4", account=account)
        # uuid: 1 - ready for invoice
        create_payment_request(
            uuid=UUID(int=1), customer=customer_1, bank_account=account, realized_payment=payment_1, variable_symbol="1"
        )
        # uuid: 2 - payment not realized
        create_payment_request(
            uuid=UUID(int=2), customer=customer_1, bank_account=account, realized_payment=None, variable_symbol="2"
        )
        # uuid: 3 - customer not VIES validated
        create_payment_request(
            uuid=UUID(int=3), customer=customer_2, bank_account=account, realized_payment=payment_2, variable_symbol="3"
        )
        # uuid: 4 - with non-invoiceable item
        pr = create_payment_request(
            uuid=UUID(int=4), customer=customer_1, bank_account=account, realized_payment=payment_3, variable_symbol="4"
        )
        PaymentRequestItem.objects.create(
            payment_request_id=pr.pk,
            name="Overdue penalty",
            item_type="penalty",
            quantity=1,
            price_per_unit=Decimal(150.0),
            price_total=Decimal(150.0),
        )
        # uuid: 5 - invoice already exists
        pr = create_payment_request(
            uuid=UUID(int=5), customer=customer_1, bank_account=account, realized_payment=payment_4, variable_symbol="5"
        )
        pr.invoice = Invoice.objects.create(
            uuid=UUID(int=10),
            customer=customer_1,
            tax_at=date(2024, 1, 20),
            price_total_base=0.0,
            price_total_vat=0.0,
            price_total=0.0,
            price_to_pay=0.0,
        )
        pr.save()

        q = PaymentRequest.objects.filter_for_invoice()

        # Check filtered result
        self.assertEqual(len(q), 1)
        filtered = q.get()
        self.assertEqual(filtered.uuid, UUID(int=1))


class TestPaymentRequestItem(SimpleTestCase):
    """Test `PaymentRequestItem` model."""

    def test_str(self):
        """Test string representation."""
        payment_request_item = PaymentRequestItem(
            payment_request=get_payment_request(),
            name="Test item",
            item_type="basic_rate",
            quantity=2,
            price_per_unit=Decimal("8.05"),
            price_total=Decimal("16.10"),
        )
        self.assertEqual(str(payment_request_item), "00000000-0000-0000-0000-000000000001/Test item")


class TestVatRate(TestCase):
    """Test `VatRate` model."""

    def test_str(self):
        """Test string representation."""
        data = [
            (
                ("CZ", None, date(2023, 1, 1), date(2024, 1, 1), 22.0),
                "Czechia (CZ), standard rate, valid: 2023-01-01T00:00:00+00:00 - 2024-01-01T00:00:00+00:00, VAT 22.0%",
            ),
            (
                ("DE", "62.0", date(2024, 1, 1), None, 12.0),
                "Germany (DE), CPA code: 62.0, valid: 2024-01-01T00:00:00+00:00 - ..., VAT 12.0%",
            ),
        ]
        for item, expected in data:
            with self.subTest(item=item):
                vat_rate = VatRate(
                    country_code=item[0],
                    cpa_code=item[1],
                    valid_from=local_date_start(item[2]),
                    valid_to=local_date_start(item[3]) if item[3] else None,
                    value=Decimal(item[4]),
                )
                self.assertEqual(str(vat_rate), expected)


class TestBillingItemType(TestCase):
    """Test `BillingItemType` model."""

    def setUp(self) -> None:
        vat_rate_data = [
            # Standard VAT rate with evolution in time
            {
                "country_code": "CZ",
                "cpa_code": None,
                "valid_from": datetime(2023, 1, 1, tzinfo=timezone.utc),
                "valid_to": datetime(2024, 1, 1, tzinfo=timezone.utc),
                "value": Decimal("22.0"),
            },
            {
                "country_code": "CZ",
                "cpa_code": None,
                "valid_from": datetime(2024, 1, 1, tzinfo=timezone.utc),
                "valid_to": None,
                "value": Decimal("21.0"),
            },
            {
                "country_code": "DE",
                "cpa_code": None,
                "valid_from": datetime(2023, 1, 1, tzinfo=timezone.utc),
                "valid_to": None,
                "value": Decimal("20.0"),
            },
            # Exception - reduced rate
            {
                "country_code": "CZ",
                "cpa_code": "62.0",
                "valid_from": datetime(2023, 1, 1, tzinfo=timezone.utc),
                "valid_to": datetime(2024, 1, 1, tzinfo=timezone.utc),
                "value": Decimal("12.0"),
            },
        ]
        for item in vat_rate_data:
            VatRate.objects.create(**item)

    def test_str(self):
        """Test string representation."""
        billing_item_type = BillingItemType(handle="test_item", cpa_code="62.0")
        self.assertEqual(str(billing_item_type), "test_item - CPA: 62.0")

    def test_vat_rate(self):
        test_data = [
            ("CZ", None, date(2023, 6, 1), 22.0),  # standard rate in 2023
            ("CZ", None, date(2024, 6, 1), 21.0),  # standard rate in 2024
            ("CZ", "62.0", date(2023, 6, 1), 12.0),  # reduced rate in 2023
            ("CZ", "62.0", date(2024, 6, 1), 21.0),  # standard rate in 2024, reduced rate no more applicable
            ("DE", "62.0", date(2024, 6, 1), 20.0),  # standard DE rate, reduced rate in CZ only
            ("CH", None, date(2023, 6, 1), 0.0),  # zero rate, non-EU resident
            ("CH", "62.0", date(2023, 6, 1), 0.0),  # zero rate, non-EU resident
        ]
        for country, cpa_code, tax_date, expected_rate in test_data:
            with self.subTest(country=country, cpa_code=cpa_code, tax_date=tax_date):
                billing_item_type = BillingItemType(handle="test_item", cpa_code=cpa_code)
                self.assertEqual(billing_item_type.vat_rate(country, tax_date), expected_rate)

    def test_vat_rate_not_found(self):
        test_data = [
            ("CZ", None, date(2022, 6, 1)),  # not found because of date
            ("AT", None, date(2023, 6, 1)),  # not found because of country
        ]
        for country, cpa_code, tax_date in test_data:
            with self.subTest(country=country, cpa_code=cpa_code, tax_date=tax_date):
                billing_item_type = BillingItemType(handle="test_item", cpa_code=cpa_code)
                self.assertRaises(VatRate.DoesNotExist, billing_item_type.vat_rate, country, tax_date)


class TestInvoiceNumberSequence(SimpleTestCase):
    """Test model for invoice number sequence."""

    def test_str(self):
        """Test string representation."""
        data: List[Tuple[Dict[str, Any], str]] = [
            ({"prefix": "AUC23"}, "AUC2300000"),
            ({"prefix": "AUC23", "current_number": 12}, "AUC2300012"),
            ({"prefix": "AUC23", "current_number": 123456789}, "AUC23123456789"),
        ]
        for num_items, expected in data:
            with self.subTest(num_items=num_items):
                items = {"app_id": "test"}
                items.update(num_items)
                invoice_sequence = InvoiceNumberSequence(**items)
                self.assertEqual(str(invoice_sequence), f"test: {expected}")


class TestInvoiceNumberSequenceManager(TestCase):
    """Test of retrieving variable_symbol from sequence."""

    def setUp(self) -> None:
        vs_item = InvoiceNumberSequence(app_id="test", year=2023, prefix="AUC23", current_number=205)
        vs_item.save()

    def test_get_ok(self):
        self.assertEqual(InvoiceNumberSequence.objects.next_value("test", 2023), "AUC2300206")
        invoice_sequence = InvoiceNumberSequence.objects.get(app_id="test", year=2023)
        self.assertEqual(invoice_sequence.current_number, 206)

    def test_sequence_not_found(self):
        with self.assertRaises(InvoiceNumberSequence.DoesNotExist):
            InvoiceNumberSequence.objects.next_value("other_test", 2023)


class TestInvoice(TestCase):
    """Test `Invoice` model."""

    def test_str(self):
        """Test string representation."""
        self.assertEqual(
            str(get_invoice()),
            "902300008/CID-1: Test, s.r.o. 1200.00 CZK at 2023-09-15",
        )

    def test_get_context(self):
        create_invoice()

        self.assertEqual(Invoice.objects.get()._get_pdf_context(), get_invoice_context_data())


class TestInvoiceRenderPdf(TestCase):
    """Test `Invoice` rendering to PDF."""

    def setUp(self):
        self.secretary_mock = Mock(spec=SecretaryClient)
        self.storage_mock = MagicMock(spec=Storage)
        create_invoice()

    @freeze_time("2024-01-31T15:00:00Z")
    def test_render_ok(self):
        self.secretary_mock.render_pdf.return_value = b"PDF binary contents"
        self.storage_mock.create.return_value.__enter__.return_value.uuid = UUID(int=10)
        invoice = Invoice.objects.get()

        invoice.render_pdf(self.secretary_mock, self.storage_mock)

        invoice = Invoice.objects.get()
        self.assertEqual(invoice.file_uid, "00000000-0000-0000-0000-00000000000a")
        self.assertEqual(
            self.secretary_mock.mock_calls,
            [call.render_pdf("pain-invoice-cs.html", get_invoice_context_data())],
        )
        expected_calls = [
            call.create("invoice_902400001.pdf", mimetype="application/pdf"),
            call.create().__enter__(),
            call.create().__enter__().write(b"PDF binary contents"),
            call.create().__exit__(None, None, None),
        ]
        self.assertEqual(self.storage_mock.mock_calls, expected_calls)

    @freeze_time("2024-01-31T15:00:00Z")
    def test_render_ok_template_en(self):
        self.secretary_mock.render_pdf.return_value = b"PDF binary contents"
        self.storage_mock.create.return_value.__enter__.return_value.uuid = UUID(int=10)
        invoice = Invoice.objects.get()
        invoice.country_code = "DE"  # simulate German customer
        invoice.save()

        invoice.render_pdf(self.secretary_mock, self.storage_mock)

        invoice = Invoice.objects.get()
        self.assertEqual(invoice.file_uid, "00000000-0000-0000-0000-00000000000a")
        self.assertEqual(
            self.secretary_mock.mock_calls,
            [call.render_pdf("pain-invoice-en.html", ANY)],  # English template in use
        )

    def test_already_rendered(self):
        invoice = Invoice.objects.get()
        invoice.file_uid = "file-1"  # simulate rendered PDF
        invoice.save()

        invoice.render_pdf(self.secretary_mock, self.storage_mock)

        invoice = Invoice.objects.get()
        self.assertEqual(str(invoice.file_uid), "file-1")
        self.secretary_mock.assert_not_called()
        self.storage_mock.assert_not_called()


class TestInvoiceItem(SimpleTestCase):
    """Test `InvoiceItem` model."""

    def test_str(self):
        """Test string representation."""
        invoice_item = InvoiceItem(
            invoice=get_invoice(),
            name="Test item",
            vat_rate=Decimal("20.0"),
            quantity=2,
            price_per_unit=Decimal("500.00"),
            price_total_base=Decimal("1000.00"),
            price_total_vat=Decimal("200.00"),
            price_total=Decimal("1200.00"),
        )
        self.assertEqual(str(invoice_item), "902300008/Test item")
