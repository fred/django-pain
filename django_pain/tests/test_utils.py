#
# Copyright (C) 2018-2024  CZ.NIC, z. s. p. o.
#
# This file is part of FRED.
#
# FRED is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# FRED is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with FRED.  If not, see <https://www.gnu.org/licenses/>.

"""Test utils."""

from datetime import date, datetime, timezone
from decimal import ROUND_DOWN, Decimal
from typing import Dict, List, Tuple

from django.test import SimpleTestCase, override_settings

from django_pain.constants import PaymentProcessingError, ProcessPaymentResultType
from django_pain.models.bank import BankAccount
from django_pain.utils import (
    ProcessPaymentResult,
    account_to_iban,
    bank_of_account,
    country_code_to_vat_prefix,
    full_class_name,
    local_date,
    local_date_start,
    parse_date_safe,
    parse_datetime_safe,
    round_money_amount,
    vat_number_to_country_code,
)


class FullClassNameTest(SimpleTestCase):
    def test_str(self):
        """Test full_class_name."""
        self.assertEqual(full_class_name(BankAccount), "django_pain.models.bank.BankAccount")


class ParseDateSafeTest(SimpleTestCase):
    def test_parse_date_safe(self):
        self.assertEqual(parse_date_safe("2017-01-31"), date(2017, 1, 31))

    def test_parse_date_safe_fails_on_invalid(self):
        with self.assertRaises(ValueError):
            parse_date_safe("2017-01-32")
        with self.assertRaises(ValueError):
            parse_date_safe("not a date")


class ParseDatetimeSafeTest(SimpleTestCase):
    def test_parse_datetime_safe(self):
        self.assertEqual(parse_datetime_safe("2017-01-31 00:00"), datetime(2017, 1, 31, 0, 0))

    def test_parse_datetime_safe_fails_on_invalid(self):
        with self.assertRaises(ValueError):
            parse_datetime_safe("2017-01-32 00:00")
        with self.assertRaises(ValueError):
            parse_datetime_safe("not a date")


class RoundMoneyAmountTest(SimpleTestCase):
    def test_rounding(self):
        data: List[Tuple[Dict, str]] = [
            ({"amount": Decimal("1.1")}, "1.10"),
            ({"amount": Decimal("1.124")}, "1.12"),
            ({"amount": Decimal("1.125")}, "1.13"),
            ({"amount": Decimal("1.126")}, "1.13"),
            ({"amount": Decimal("1.124"), "places": 1}, "1.1"),
            ({"amount": Decimal("1.119"), "rounding": ROUND_DOWN}, "1.11"),
        ]
        for param, expected in data:
            with self.subTest(param=param):
                self.assertEqual(str(round_money_amount(**param)), expected)


class LocalDateTest(SimpleTestCase):
    @override_settings(TIME_ZONE="Europe/Prague")
    def test_local_date(self):
        data = [
            # naive datetimes
            (datetime(2023, 10, 1, 0, 0, 0), date(2023, 10, 1)),
            (datetime(2023, 10, 1, 23, 59, 59), date(2023, 10, 1)),
            # aware datetimes
            (datetime(2023, 10, 1, 0, 0, 0, tzinfo=timezone.utc), date(2023, 10, 1)),
            (datetime(2023, 10, 1, 23, 59, 59, tzinfo=timezone.utc), date(2023, 10, 2)),  # timezone makes difference!
        ]
        for dt, expected in data:
            with self.subTest(datetime=dt):
                self.assertEqual(local_date(dt), expected)


class LocalDateStartTest(SimpleTestCase):
    @override_settings(TIME_ZONE="Europe/Prague")
    def test_local_date_start(self):
        data = [
            (date(2024, 1, 1), datetime.fromisoformat("2024-01-01T00:00:00+01:00")),  # CET
            (date(2024, 7, 1), datetime.fromisoformat("2024-07-01T00:00:00+02:00")),  # CEST
        ]
        for d, expected in data:
            with self.subTest(date=d):
                self.assertEqual(local_date_start(d), expected)


class TestProcessPaymentResult(SimpleTestCase):
    """Test ProcessPaymentResult."""

    def test_process_payment_result_init(self):
        """Test ProcessPaymentResult __init__."""
        self.assertEqual(
            ProcessPaymentResult(ProcessPaymentResultType.PROCESSED).result, ProcessPaymentResultType.PROCESSED
        )
        self.assertEqual(
            ProcessPaymentResult(ProcessPaymentResultType.TO_REFUND).result, ProcessPaymentResultType.TO_REFUND
        )
        self.assertEqual(
            ProcessPaymentResult(ProcessPaymentResultType.NOT_PROCESSED).result, ProcessPaymentResultType.NOT_PROCESSED
        )
        self.assertEqual(ProcessPaymentResult(ProcessPaymentResultType.PROCESSED).error, None)
        self.assertEqual(
            ProcessPaymentResult(ProcessPaymentResultType.PROCESSED, PaymentProcessingError.DUPLICITY).error,
            PaymentProcessingError.DUPLICITY,
        )

    def test_process_payment_result_eq(self):
        """Test ProcessPaymentResult __eq__."""
        self.assertEqual(
            ProcessPaymentResult(ProcessPaymentResultType.PROCESSED),
            ProcessPaymentResult(ProcessPaymentResultType.PROCESSED),
        )
        self.assertNotEqual(
            ProcessPaymentResult(ProcessPaymentResultType.PROCESSED),
            ProcessPaymentResult(ProcessPaymentResultType.NOT_PROCESSED),
        )
        self.assertNotEqual(ProcessPaymentResult(ProcessPaymentResultType.NOT_PROCESSED), 0)
        self.assertNotEqual(
            ProcessPaymentResult(ProcessPaymentResultType.NOT_PROCESSED),
            ProcessPaymentResult(ProcessPaymentResultType.NOT_PROCESSED, PaymentProcessingError.DUPLICITY),
        )

    def test_processed(self):
        """Test detecting uprocessed state."""
        data = [
            (ProcessPaymentResultType.PROCESSED, None, True),
            (ProcessPaymentResultType.TO_REFUND, None, True),
            (ProcessPaymentResultType.NOT_PROCESSED, PaymentProcessingError.DUPLICITY, True),
            (ProcessPaymentResultType.NOT_PROCESSED, None, False),
        ]
        for result, error, expected in data:
            with self.subTest(result=result, error=error):
                instance = ProcessPaymentResult(result=result, error=error)
                self.assertEqual(instance.processed, expected)
                self.assertEqual(instance.unprocessed, not expected)


class TestBankOfAccount(SimpleTestCase):
    """Test retriving bank code from accountnumber."""

    def test_bank_ok(self):
        data = [
            ("276463778/0300", "0300"),
            ("625/5500", "5500"),
            ("000000-0000000625/6210", "6210"),
        ]
        for account, bank in data:
            with self.subTest(account=account):
                self.assertEqual(bank_of_account(account), bank)

    def test_iban_exception(self):
        data = [
            "",
            "A10/0100",
            "123//0100",
            "1/0100",
            "98765432109/0100",
            "9*123/0100",
            "1-123/0100",
            "1234567-123/0100",
            "123/105",
        ]
        for account in data:
            with self.subTest(account=account):
                self.assertRaises(ValueError, bank_of_account, account)


class TestAccountToIban(SimpleTestCase):
    """Test account number to IBAN conversion."""

    def test_iban_ok(self):
        data = [
            ("276463778/0300", "CZ3703000000000276463778"),
            ("625/5500", "CZ3155000000000000000625"),
            ("000000-0000000625/5500", "CZ3155000000000000000625"),
            ("268518759/ 0300", "CZ5003000000000268518759"),
            ("268518759 /0300", "CZ5003000000000268518759"),
            ("268518759 / 0300", "CZ5003000000000268518759"),
            ("288234710/0300", "CZ3803000000000288234710"),
            ("19-288234710/0300", "CZ2403000000190288234710"),
            ("20001- 288234710/0300", "CZ2403000200010288234710"),
            ("20001 -288234710/0300", "CZ2403000200010288234710"),
            ("20001 - 288234710/0300", "CZ2403000200010288234710"),
        ]
        for account, iban in data:
            with self.subTest(account=account):
                self.assertEqual(account_to_iban(account), iban)

    def test_iban_exception(self):
        data = [
            "",
            "A10/0100",
            "123//0100",
            "1/0100",
            "98765432109/0100",
            "9*123/0100",
            "1-123/0100",
            "1234567-123/0100",
            "123/105",
        ]
        for account in data:
            with self.subTest(account=account):
                self.assertRaises(ValueError, account_to_iban, account)

    def test_iban_human(self):
        self.assertEqual(account_to_iban("276463778/0300", human=True), "CZ37 0300 0000 0002 7646 3778")


class TestVatNumberToCountryCode(SimpleTestCase):
    """Test `vat_number_to_country_code` function."""

    def test_function(self):
        data = [
            ("CZ0123456", "CZ"),
            ("EL0123456", "GR"),  # Greece
        ]
        for vat_number, country_code in data:
            with self.subTest(vat_number=vat_number):
                self.assertEqual(vat_number_to_country_code(vat_number), country_code)


class TestCountryCodeToVatPrefix(SimpleTestCase):
    """Test `country_code_to_vat_prefix` function."""

    def test_function(self):
        data = [
            ("CZ", "CZ"),
            ("GR", "EL"),  # Greece
        ]
        for country_code, vat_prefix in data:
            with self.subTest(country_code=country_code):
                self.assertEqual(country_code_to_vat_prefix(country_code), vat_prefix)
