#
# Copyright (C) 2020-2024  CZ.NIC, z. s. p. o.
#
# This file is part of FRED.
#
# FRED is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# FRED is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with FRED.  If not, see <https://www.gnu.org/licenses/>.

"""Tests of serializers."""

from collections import OrderedDict
from datetime import date, datetime, timezone
from decimal import Decimal
from typing import Any, Dict, List
from unittest.mock import patch
from uuid import UUID

from django.test import SimpleTestCase, TestCase, override_settings
from djmoney.money import Money
from freezegun import freeze_time
from rest_framework.serializers import ErrorDetail, ValidationError
from testfixtures import LogCapture

from django_pain.card_payment_handlers import AbstractCardPaymentHandler, CartItem, PaymentHandlerLimitError
from django_pain.constants import PaymentState, PaymentType
from django_pain.models import BankAccount, BankPayment, Invoice, PaymentRequest
from django_pain.models.invoices import BillingItemType, InvoiceNumberSequence, VariableSymbolSequence, VatRate
from django_pain.serializers import (
    BankPaymentSerializer,
    CustomerSerializer,
    ExternalPaymentState,
    InvoiceSerializer,
    PaymentRequestForInvoiceSerializer,
    PaymentRequestItemSerializer,
    PaymentRequestSerializer,
    PaymentTooOldException,
    custom_expiry_validator,
    not_in_past_validator,
    vs_format_validator,
)
from django_pain.tests.utils import (
    create_payment_request,
    get_account,
    get_customer,
    get_invoice,
    get_payment,
    get_payment_request,
    get_payment_request_data,
)


class TestBankPaymentSerializer(SimpleTestCase):
    """Test BankPayment serializer."""

    def setUp(self):
        self.log_handler = LogCapture(("django_pain.serializers",), propagate=False)

    def tearDown(self):
        self.log_handler.uninstall()

    def test_get_state(self):
        account = get_account(account_number="123456", currency="CZK")
        payment = get_payment(
            identifier="1", account=account, counter_account_name="Account one", state=PaymentState.PROCESSED
        )

        serializer = BankPaymentSerializer()
        self.assertEqual(serializer.get_state(payment), ExternalPaymentState.PAID)

    def test_validate_cart(self):
        serializer = BankPaymentSerializer()

        self.assertRaisesRegex(ValidationError, "one or two item", serializer.validate_cart, [])
        self.assertRaisesRegex(ValidationError, "one or two item", serializer.validate_cart, [{}, {}, {}])

        self.assertRaisesRegex(
            ValidationError,
            "must not exceed 20",
            serializer.validate_cart,
            [{"name": "Dar too long to fit max length", "amount": 20, "description": "Dar datovce", "quantity": 1}],
        )

        result = serializer.validate_cart([{"name": "Dar", "amount": 20, "description": "Dar datovce", "quantity": 1}])
        self.assertEqual(result, [CartItem(name="Dar", amount=Decimal(20), description="Dar datovce", quantity=1)])

    @override_settings(DEFAULT_CURRENCY="EUR")
    def test_validate_amount(self):
        data: OrderedDict[str, Any] = OrderedDict(
            cart=[CartItem(name="Dar", amount=Decimal(10), description="Dar datovce", quantity=1)]
        )
        serializer = BankPaymentSerializer()

        data["amount"] = Money(10, "CZK")
        validated = serializer.validate(data)
        self.assertEqual(validated["amount"], Money(10, "CZK"))

        data["amount"] = Decimal(10)
        validated = serializer.validate(data)
        self.assertEqual(validated["amount"], Money(10, "EUR"))

        data["amount"] = 10
        data["processor"] = "processor"
        validated = serializer.validate(data)
        self.assertEqual(validated["amount"], Money(10, "EUR"))

        message = 'Parameter "amount_currency" not set. Using default currency. Processor for this payment: "{}"'
        self.log_handler.check(
            ("django_pain.serializers", "WARNING", message.format("")),
            ("django_pain.serializers", "WARNING", message.format("processor")),
        )

    def test_validate_amount_and_cart(self):
        data: OrderedDict[str, Any] = OrderedDict(
            amount=Money(40, "CZK"),
            cart=[CartItem(name="Dar", amount=Decimal(20), description="Dar datovce", quantity=2)],
        )
        serializer = BankPaymentSerializer()
        self.assertEqual(data, serializer.validate(data))

        data["amount"] = Money(100, "CZK")
        self.assertRaisesRegex(ValidationError, "amounts must be equal", serializer.validate, data)


class TestStreetFieldSerializer(TestCase):
    """`StreetField` related tests."""

    def setUp(self) -> None:
        self.items = {
            "uid": "CID-1",
            "street": ["Na Okraji 1"],
            "city": "Horní Dolní",
            "postal_code": "050 10",
            "country_code": "CZ",
            "company_registration_number": "001234",
            "vat_number": "CZ001234",
        }

    def test_street_validation_ok(self):
        data: List[Any] = [
            ["Street 1"],
            ["Street 1", "Street 2"],
            ["Street 1", "Street 2", "Street 3"],
        ]
        for street in data:
            with self.subTest(street=street):
                self.items.update({"name": "Jan Novák", "street": street})
                serializer = CustomerSerializer(data=self.items)
                self.assertTrue(serializer.is_valid())

    def test_street_validation_bad_list(self):
        data: List[Any] = [
            123,  # not a list
            [],  # list too short
            ["Street 1", "Street 2", "Street 3", "Street 4"],  # list too long
        ]
        for street in data:
            with self.subTest(street=street):
                self.items.update({"name": "Jan Novák", "street": street})
                serializer = CustomerSerializer(data=self.items)
                self.assertFalse(serializer.is_valid())
                self.assertIn(
                    ErrorDetail(string="List of one to three strings is required", code="invalid"),
                    serializer.errors["street"],
                )

    def test_street_validation_bad_list_items(self):
        data: List[Any] = [
            ["Street 1", "Street 2", ""],  # empty string item
            ["Street 1", True],  # non-string item
        ]
        for street in data:
            with self.subTest(street=street):
                self.items.update({"name": "Jan Novák", "street": street})
                serializer = CustomerSerializer(data=self.items)
                self.assertFalse(serializer.is_valid())
                self.assertIn(
                    ErrorDetail(string="List items should be non-empty strings", code="invalid"),
                    serializer.errors["street"],
                )

    def test_street_serialization(self):
        data = [
            ("Street 1", "Street 2", "Street 3", ["Street 1", "Street 2", "Street 3"]),
            ("Street 1", "Street 2", None, ["Street 1", "Street 2"]),
            ("Street 1", None, None, ["Street 1"]),
        ]
        for street1, street2, street3, expected in data:
            with self.subTest(street1=street1, street2=street2, street3=street3):
                customer = get_customer(street1=street1, street2=street2, street3=street3)
                serializer = CustomerSerializer(customer)
                self.assertEqual(serializer.data["street"], expected)


class TestCustomerSerializer(TestCase):
    """`CustomerSerializer` tests."""

    def _get_item_defaults(self) -> Dict[str, Any]:
        return {
            "uid": "CID-1",
            "street": ["Na Okraji 1"],
            "city": "Horní Dolní",
            "postal_code": "050 10",
            "country_code": "CZ",
            "company_registration_number": "001234",
            "vat_number": "CZ001234",
        }

    def test_validation_ok(self):
        data: List[Dict[str, Any]] = [
            {"name": "Jan Novák"},  # name only
            {"organization": "Firma, s.r.o."},  # organization only
            {"name": "Jan Novák", "organization": "Firma, s.r.o."},  # both name and organization
            {"name": "Jan Novák", "vat_number": None},  # no VAT number
            {"name": "Jan Novák", "country_code": "CH"},  # non-EU resident
            {"name": "Jan Novák", "vat_number": "DE0001"},  # VAT in other EU country
            {"name": "Stavros Niarchos", "vat_number": "EL0001", "country_code": "GR"},  # Greek exception
        ]
        for new_items in data:
            with self.subTest(new_item=new_items):
                items = self._get_item_defaults()
                items.update(new_items)
                serializer = CustomerSerializer(data=items)
                self.assertTrue(serializer.is_valid())

    def test_validation_neither_name_nor_organization(self):
        data: List[Dict[str, Any]] = [
            {},
            {"name": ""},
            {"name": None},
            {"organization": ""},
            {"organization": None},
            {"name": "", "organization": ""},
            {"name": None, "organization": None},
        ]
        for new_items in data:
            with self.subTest(new_items=new_items):
                items = self._get_item_defaults()
                items.update(new_items)
                serializer = CustomerSerializer(data=items)
                self.assertFalse(serializer.is_valid())
                self.assertIn(
                    ErrorDetail(string="Neither `name` nor `organization` is set", code="invalid"),
                    serializer.errors["non_field_errors"],
                )

    def test_phone_number_pass(self):
        data: List[str] = [
            "420 123 456 789",
            "+420 123 456 789",
            "0420 123 456 789",
            "00420 123 456 789",
            "+420 123456789",
            "+420.123456789",
            "+420 123.456.789",
            "+420.123.456.789",
            "+420 123-456-789",
        ]
        for phone_number in data:
            with self.subTest(phone_number=phone_number):
                items = self._get_item_defaults()
                items.update({"name": "Jan Novák", "phone_number": phone_number})
                serializer = CustomerSerializer(data=items)
                self.assertTrue(serializer.is_valid())

    def test_phone_number_fail(self):
        data: List[str] = [
            "A420 123 456 789",
            "420123",
            "+420123",
            "+420-123",
            "+420 123*456",
            "+420 123-",
            "+420 123--456",
            "+420 A",
        ]
        for phone_number in data:
            with self.subTest(phone_number=phone_number):
                items = self._get_item_defaults()
                items.update({"name": "Jan Novák", "phone_number": phone_number})
                serializer = CustomerSerializer(data=items)
                self.assertFalse(serializer.is_valid())

    def test_validation_malformed_vat_number(self):
        data: List[Dict[str, Any]] = [
            {"name": "Jan Novák", "vat_number": "0001"},  # no prefix
            {"name": "Jan Novák", "vat_number": "C0001"},  # bad prefix
            {"name": "Jan Novák", "vat_number": "CZ-0001"},  # invalid character
            {"name": "Stavros Niarchos", "vat_number": "GR0001", "country_code": "GR"},  # Greek exception
        ]
        for new_items in data:
            with self.subTest(new_items=new_items):
                items = self._get_item_defaults()
                items.update(new_items)
                serializer = CustomerSerializer(data=items)
                self.assertFalse(serializer.is_valid())
                self.assertIn(
                    ErrorDetail(string="Malformed VAT number for EU country", code="invalid"),
                    serializer.errors["non_field_errors"],
                )

    def test_empty_vat_number_to_none(self):
        items = self._get_item_defaults()
        items.update({"name": "Jan Novák", "vat_number": ""})
        serializer = CustomerSerializer(data=items)
        self.assertTrue(serializer.is_valid())
        self.assertIsNone(serializer.validated_data["vat_number"])


class TestPaymentRequestItemSerializer(TestCase):
    """Tests for validation, VAT processing and price calculation in `PaymentRequestItemSerializer`."""

    def setUp(self) -> None:
        # Initialize code list
        BillingItemType.objects.create(handle="test_type")
        # Create related objects
        customer = get_customer()
        customer.save()
        account = get_account()
        account.save()
        payment_request = get_payment_request(customer=customer, bank_account=account)
        payment_request.save()

        self.data = {
            "payment_request": payment_request,
            "name": "Item 1",
            "item_type": "test_type",
            "quantity": 2,
            "price_per_unit": "1.0000000000",
            "price_with_vat": True,
        }

    def test_validation_ok(self):
        serializer = PaymentRequestItemSerializer(data=self.data)
        self.assertTrue(serializer.is_valid())
        self.assertEqual(serializer.validated_data["price_total"], Decimal("2.00"))

    def test_validation_failure_item_type(self):
        data = self.data
        data.update(item_type="another_type")
        serializer = PaymentRequestItemSerializer(data=data)
        self.assertFalse(serializer.is_valid())
        self.assertIn(
            ErrorDetail(string="Unknown item type", code="invalid"),
            serializer.errors["non_field_errors"],
        )

    def test_validation_failure_without_vat(self):
        data = self.data
        data.update(price_with_vat=False)
        serializer = PaymentRequestItemSerializer(data=data)
        self.assertFalse(serializer.is_valid())
        self.assertIn(
            ErrorDetail(string="Items without VAT are not supported", code="invalid"),
            serializer.errors["non_field_errors"],
        )


class TestNotInPastValidator(SimpleTestCase):
    """Tests of timestamp validation (not to be in past)."""

    def test_validation_none(self):
        not_in_past_validator(None)

    @freeze_time("2024-02-05T17:00:00Z")
    def test_validation_ok(self):
        not_in_past_validator(datetime(2024, 2, 15, tzinfo=timezone.utc))

    @freeze_time("2024-02-05T17:00:00Z")
    def test_validation_raises(self):
        with self.assertRaisesMessage(ValidationError, "Time point must not be in the past"):
            not_in_past_validator(datetime(2024, 1, 15, tzinfo=timezone.utc))


class TestVariableSymbolFormatValidator(TestCase):
    """Tests of variable symbol format validation."""

    def test_ok(self):
        data = [
            "",  # empty
            "1234",  # short
            "0123456789",  # full
        ]
        for vs in data:
            with self.subTest(vs=vs):
                vs_format_validator(vs)

    def test_invalid(self):
        data = [
            "0123456789012",  # too long
            " 1234",  # invalid character
        ]
        for vs in data:
            with self.subTest(vs=vs):
                self.assertRaises(ValidationError, vs_format_validator, vs)


@override_settings(PAIN_CSOB_CUSTOM_HANDLER={"MIN_VALIDITY": 3600, "MAX_VALIDITY": 86400})
@freeze_time("2023-01-01T00:00:00Z")
class TestCustomExpiryValidator(SimpleTestCase):
    """Tests of validator for custom expiry."""

    def test_custom_expiry_ok(self):
        custom_expiry_validator(datetime(2023, 1, 1, 10, 0, 0, tzinfo=timezone.utc))

    def test_custom_expiry_invalid(self):
        data = [
            datetime(2023, 1, 1, 0, 59, 59, tzinfo=timezone.utc),  # too early
            datetime(2023, 1, 2, 0, 0, 1, tzinfo=timezone.utc),  # too late
        ]

        for custom_expiry in data:
            with self.subTest(custom_expiry=custom_expiry):
                with self.assertRaisesMessage(ValidationError, "Time point outside allowed interval"):
                    custom_expiry_validator(custom_expiry)


class TestCardPaymentHandler(AbstractCardPaymentHandler):
    """Card payment handler for tests of payment requests."""

    def init_payment(self, amount, variable_symbol, processor, return_url, return_method, cart, language, **kwargs):
        """Create and save test payment (with some data based on variable symbol)."""
        kwargs.pop("customer", None)
        payment = BankPayment.objects.create(
            identifier="pid-" + variable_symbol,
            uuid=UUID(int=int(variable_symbol)),
            payment_type=PaymentType.CARD_PAYMENT,
            account=BankAccount.objects.get(account_number="123456/0300"),
            amount=amount,
            description="Test payment",
            state=PaymentState.INITIALIZED,
            card_payment_state="",
            variable_symbol=variable_symbol,
            processor=processor,
            card_handler=self.name,
            **kwargs,
        )
        return payment, "https://gateway.example.org/" + variable_symbol

    def update_payments_state(self, payment):
        """Do nothing."""

    def get_payment_url(self, payment):
        """Return URL."""
        return "https://gateway.example.org/" + payment.variable_symbol


class TestPaymentRequestSerializer(TestCase):
    """`PaymentRequestSerializer` tests."""

    # --- Serialization tests ---

    def test_serialization(self):
        # Prepare data and save all objects to database
        pk = create_payment_request().pk

        # Get object from database and serialize it
        serializer = PaymentRequestSerializer(PaymentRequest.objects.get(pk=pk))
        self.assertEqual(serializer.data, get_payment_request_data())

    def test_serialization_invoice_number(self):
        # Prepare data and save all objects to database
        payment_request = create_payment_request()
        invoice = get_invoice(customer=payment_request.customer)
        invoice.save()
        payment_request.invoice = invoice
        payment_request.save()

        # Get object from database and serialize it
        serializer = PaymentRequestSerializer(PaymentRequest.objects.get(pk=payment_request.pk))
        self.assertEqual(serializer.data, get_payment_request_data(invoice_number="902300008"))

    def _prepare_get_card_gateway_url(self):
        account = get_account(account_number="1000/1000")
        account.save()
        data = {
            "identifier": "PAYMENT1",
            "payment_type": PaymentType.CARD_PAYMENT,
            "account": account,
            "amount": Money("42.00", "CZK"),
            "description": "Gift for FRED",
            "state": PaymentState.INITIALIZED,
            "variable_symbol": "2023000001",
            "card_handler": "custom",
            "email": "user@example.org",
            "custom_expiry": datetime(2023, 10, 15, 15, 30, 0, tzinfo=timezone.utc),
            "customer_code": "XYZ789",
        }
        card_payment = BankPayment(**data)
        card_payment.save()
        return create_payment_request(card_payment=card_payment)

    @override_settings(
        PAIN_CSOB_CUSTOM_HANDLER={},
        PAIN_CARD_PAYMENT_HANDLERS={
            "custom": {
                "HANDLER": "django_pain.card_payment_handlers.csob.CSOBCustomPaymentHandler",
                "GATEWAY": "default",
            },
        },
    )
    @freeze_time("2023-09-20T12:00Z")
    def test_get_card_gateway_url(self):
        payment_request = self._prepare_get_card_gateway_url()
        serializer = PaymentRequestSerializer(payment_request)
        self.assertEqual(serializer.data["card_gateway_url"], "https://platebnibrana.csob.cz/zaplat/XYZ789")

    @override_settings(
        PAIN_CSOB_CUSTOM_HANDLER={},
        PAIN_CARD_PAYMENT_HANDLERS={
            "dummy": {"HANDLER": "django_pain.tests.utils.DummyCardPaymentHandler", "GATEWAY": "dummy"},
        },
    )
    def test_get_card_gateway_url_default(self):
        payment_request = self._prepare_get_card_gateway_url()
        payment_request.card_payment.card_handler = "dummy"
        serializer = PaymentRequestSerializer(payment_request)
        self.assertIsNone(serializer.data["card_gateway_url"])

    # --- Deserialization and validation tests ---

    @freeze_time("2023-09-01T12:00:00Z")
    def test_create_ok(self):
        create_payment_request(
            uuid="00000000-0000-0000-0000-000000000002",
            closed_at="2023-09-20T12:00:00Z",  # stored payment request is closed
        )
        BillingItemType.objects.create(handle="basic_rate")

        data: Dict[str, Any] = {
            "uuid": "00000000-0000-0000-0000-000000000001",
            "app_id": "auctions",
            "customer": "CID-1",
            "due_at_displayed": "2023-10-01T12:00:00",
            "currency": "CZK",
            "variable_symbol": "0000009876",
            "bank_account": "123456/0300",
            "items": [
                {
                    "name": "Item 1",
                    "item_type": "basic_rate",
                    "quantity": 1,
                    "price_per_unit": "11.0000000000",
                    "price_with_vat": True,
                },
                {
                    "name": "Item 2",
                    "item_type": "basic_rate",
                    "quantity": 2,
                    "price_per_unit": "12.0000000000",
                    "price_with_vat": True,
                },
                {
                    "name": "Item 3",
                    "item_type": "basic_rate",
                    "quantity": 3,
                    "price_per_unit": "13.0000000000",
                    "price_with_vat": True,
                },
            ],
        }

        # Create and save objects to database
        serializer = PaymentRequestSerializer(data=data)
        self.assertTrue(serializer.is_valid())
        serializer.save()

        # Serialize stored objects for verificaton
        serializer = PaymentRequestSerializer(PaymentRequest.objects.get(uuid=UUID(int=1)))
        self.assertEqual(serializer.data, get_payment_request_data(realized_payment=None))

    @freeze_time("2023-09-01T12:00:00Z")
    def test_create_ok_with_created_vs(self):
        customer = get_customer()
        customer.save()
        account = get_account()
        account.save()
        vs_item = VariableSymbolSequence(app_id="auctions", year=2023, prefix=823, current_number=205)
        vs_item.save()
        BillingItemType.objects.create(handle="basic_rate")

        data: Dict[str, Any] = {
            "uuid": "00000000-0000-0000-0000-000000000001",
            "app_id": "auctions",
            "customer": "CID-1",
            "due_at_displayed": "2023-10-01T12:00:00",
            "currency": "CZK",
            "bank_account": "123456/0300",
            "items": [
                {
                    "name": "Item 1",
                    "item_type": "basic_rate",
                    "quantity": 1,
                    "price_per_unit": "11.0000000000",
                    "price_with_vat": True,
                },
            ],
        }

        serializer = PaymentRequestSerializer(data=data)
        self.assertTrue(serializer.is_valid())
        self.assertIsNone(serializer.validated_data["variable_symbol"])
        payment_request = serializer.save()  # VS generated just before saving to DB
        self.assertEqual(payment_request.variable_symbol, "0823000206")

    @freeze_time("2023-09-01T12:00:00Z")
    def test_bad_field(self):
        customer = get_customer()
        customer.save()
        account = get_account()
        account.save()

        data: Dict[str, Any] = {
            "uuid": "00000000-0000-0000-0000-000000000001",
            "app_id": None,
            "customer": "CID-1",
            "due_at_displayed": "2023-10-01T12:00:00",
            "currency": "CZK",
            "variable_symbol": "0000009876",
            "bank_account": "123456/0300",
            "items": [
                {
                    "name": "Item 1",
                    "item_type": "basic_rate",
                    "quantity": 1,
                    "price_per_unit": "11.0000000000",
                    "price_with_vat": True,
                },
            ],
        }

        serializer = PaymentRequestSerializer(data=data)
        self.assertFalse(serializer.is_valid())
        self.assertIn(
            ErrorDetail(string="This field may not be null.", code="null"),
            serializer.errors["app_id"],
        )

    @freeze_time("2023-09-01T12:00:00Z")
    def test_customer_not_found(self):
        account = get_account()
        account.save()

        data: Dict[str, Any] = {
            "uuid": "00000000-0000-0000-0000-000000000001",
            "app_id": "auctions",
            "customer": "CID-1",
            "due_at_displayed": "2023-10-01T12:00:00",
            "currency": "CZK",
            "variable_symbol": "0000009876",
            "bank_account": "123456/0300",
            "items": [
                {
                    "name": "Item 1",
                    "item_type": "basic_rate",
                    "quantity": 1,
                    "price_per_unit": "11.0000000000",
                    "price_with_vat": True,
                },
            ],
        }

        serializer = PaymentRequestSerializer(data=data)
        self.assertFalse(serializer.is_valid())
        self.assertIn(
            ErrorDetail(string="Object with uid=CID-1 does not exist.", code="does_not_exist"),
            serializer.errors["customer"],
        )

    @freeze_time("2023-09-01T12:00:00Z")
    def test_account_not_found(self):
        customer = get_customer()
        customer.save()

        data: Dict[str, Any] = {
            "uuid": "00000000-0000-0000-0000-000000000001",
            "app_id": "auctions",
            "customer": "CID-1",
            "due_at_displayed": "2023-10-01T12:00:00",
            "currency": "CZK",
            "variable_symbol": "0000009876",
            "bank_account": "123456/0300",
            "items": [
                {
                    "name": "Item 1",
                    "item_type": "basic_rate",
                    "quantity": 1,
                    "price_per_unit": "11.0000000000",
                    "price_with_vat": True,
                },
            ],
        }

        serializer = PaymentRequestSerializer(data=data)
        self.assertFalse(serializer.is_valid())
        self.assertIn(
            ErrorDetail(string="Object with account_number=123456/0300 does not exist.", code="does_not_exist"),
            serializer.errors["bank_account"],
        )

    @freeze_time("2023-09-01T12:00:00Z")
    def test_account_not_compatible(self):
        create_payment_request(
            uuid="00000000-0000-0000-0000-000000000002",
            closed_at="2023-09-20T12:00:00Z",  # stored payment request is closed
        )
        BillingItemType.objects.create(handle="basic_rate")

        data: Dict[str, Any] = {
            "uuid": "00000000-0000-0000-0000-000000000001",
            "app_id": "auctions",
            "customer": "CID-1",
            "due_at_displayed": "2023-10-01T12:00:00",
            "currency": "USD",
            "variable_symbol": "0000009876",
            "bank_account": "123456/0300",
            "items": [
                {
                    "name": "Item 1",
                    "item_type": "basic_rate",
                    "quantity": 1,
                    "price_per_unit": "11.0000000000",
                    "price_with_vat": True,
                },
            ],
        }

        serializer = PaymentRequestSerializer(data=data)
        self.assertFalse(serializer.is_valid())
        self.assertIn(
            ErrorDetail(string="Selected currency is not compatible with the bank account", code="invalid"),
            serializer.errors["non_field_errors"],
        )

    @freeze_time("2024-01-20T12:00:00Z")
    def test_mix_of_item_types(self):
        create_payment_request(
            uuid="00000000-0000-0000-0000-000000000002",
            closed_at="2023-09-20T12:00:00Z",  # stored payment request is closed
        )
        BillingItemType.objects.create(handle="basic_rate", not_invoiceable=False)
        BillingItemType.objects.create(handle="penalty", not_invoiceable=True)

        data: Dict[str, Any] = {
            "uuid": "00000000-0000-0000-0000-000000000001",
            "app_id": "auctions",
            "customer": "CID-1",
            "due_at_displayed": "2024-01-30T12:00:00",
            "currency": "CZK",
            "variable_symbol": "0000009876",
            "bank_account": "123456/0300",
            "items": [
                {
                    "name": "Item 1",
                    "item_type": "basic_rate",
                    "quantity": 1,
                    "price_per_unit": "11.0000000000",
                    "price_with_vat": True,
                },
                {
                    "name": "Item 2",
                    "item_type": "penalty",
                    "quantity": 1,
                    "price_per_unit": "10.0000000000",
                    "price_with_vat": True,
                },
            ],
        }

        serializer = PaymentRequestSerializer(data=data)
        self.assertFalse(serializer.is_valid())
        self.assertIn(
            ErrorDetail(string="Mix of invoiceable and non-invoiceable items is not allowed", code="invalid"),
            serializer.errors["non_field_errors"],
        )

    @freeze_time("2023-09-01T12:00:00Z")
    def test_no_items(self):
        customer = get_customer()
        customer.save()
        account = get_account()
        account.save()

        data: Dict[str, Any] = {
            "uuid": "00000000-0000-0000-0000-000000000001",
            "app_id": "auctions",
            "customer": "CID-1",
            "due_at_displayed": "2023-10-01T12:00:00",
            "currency": "CZK",
            "variable_symbol": "0000009876",
            "bank_account": "123456/0300",
            "items": [],
        }

        serializer = PaymentRequestSerializer(data=data)
        self.assertFalse(serializer.is_valid())
        self.assertIn(
            ErrorDetail(string="Ensure this field has at least 1 elements.", code="min_length"),
            serializer.errors["items"]["non_field_errors"],
        )

    @freeze_time("2023-09-01T12:00:00Z")
    @override_settings(
        PAIN_PROCESSORS={"dummy": "django_pain.tests.utils.DummyPaymentProcessor"},
        PAIN_CARD_PAYMENT_HANDLERS={
            "test": {"HANDLER": "django_pain.tests.test_serializers.TestCardPaymentHandler", "GATEWAY": "default"},
        },
        PAIN_PAYMENT_REQUESTS={"auctions": {"CARD_HANDLER": "test", "PROCESSOR": "dummy"}},
        PAIN_CSOB_CUSTOM_HANDLER={},
    )
    def test_create_card_payment_ok(self):
        create_payment_request(
            uuid="00000000-0000-0000-0000-000000000002",
            closed_at="2023-09-20T12:00:00Z",  # stored payment request is closed
        )
        BillingItemType.objects.create(handle="basic_rate")

        data: Dict[str, Any] = {
            "uuid": "00000000-0000-0000-0000-000000000001",
            "app_id": "auctions",
            "customer": "CID-1",
            "due_at_displayed": "2023-10-01T12:00:00",
            "currency": "CZK",
            "variable_symbol": "0000009876",
            "bank_account": "123456/0300",
            "items": [
                {
                    "name": "Item 1",
                    "item_type": "basic_rate",
                    "quantity": 1,
                    "price_per_unit": "11.0000000000",
                    "price_with_vat": True,
                },
                {
                    "name": "Item 2",
                    "item_type": "basic_rate",
                    "quantity": 2,
                    "price_per_unit": "12.0000000000",
                    "price_with_vat": True,
                },
                {
                    "name": "Item 3",
                    "item_type": "basic_rate",
                    "quantity": 3,
                    "price_per_unit": "13.0000000000",
                    "price_with_vat": True,
                },
            ],
            "create_card_payment": {
                "expiry": "2023-10-01T12:00:00",
                "redirect_url": "https://callback.example.org/",
                "redirect_method": "GET",
            },
        }

        serializer = PaymentRequestSerializer(data=data)
        self.assertTrue(serializer.is_valid())
        serializer.save()
        expected = get_payment_request_data(
            card_gateway_url="https://gateway.example.org/0000009876",
            card_payment=str(UUID(int=9876)),
            realized_payment=None,
        )
        self.assertEqual(serializer.data, expected)

    @freeze_time("2023-09-01T12:00:00Z")
    @override_settings(
        PAIN_PROCESSORS={"dummy": "django_pain.tests.utils.DummyPaymentProcessor"},
        PAIN_CARD_PAYMENT_HANDLERS={
            "test": {"HANDLER": "django_pain.tests.test_serializers.TestCardPaymentHandler", "GATEWAY": "default"},
        },
        PAIN_PAYMENT_REQUESTS={"auctions": {"CARD_HANDLER": "test", "PROCESSOR": "dummy"}},
        PAIN_CSOB_CUSTOM_HANDLER={},
    )
    def test_create_card_payment_limit_exceeded(self):
        create_payment_request(
            uuid="00000000-0000-0000-0000-000000000002",
            closed_at="2023-09-20T12:00:00Z",  # stored payment request is closed
        )
        BillingItemType.objects.create(handle="basic_rate")

        data: Dict[str, Any] = {
            "uuid": "00000000-0000-0000-0000-000000000001",
            "app_id": "auctions",
            "customer": "CID-1",
            "due_at_displayed": "2023-10-01T12:00:00",
            "currency": "CZK",
            "variable_symbol": "0000009876",
            "bank_account": "123456/0300",
            "items": [
                {
                    "name": "Item 1",
                    "item_type": "basic_rate",
                    "quantity": 1,
                    "price_per_unit": "11.0000000000",
                    "price_with_vat": True,
                },
                {
                    "name": "Item 2",
                    "item_type": "basic_rate",
                    "quantity": 2,
                    "price_per_unit": "12.0000000000",
                    "price_with_vat": True,
                },
                {
                    "name": "Item 3",
                    "item_type": "basic_rate",
                    "quantity": 3,
                    "price_per_unit": "13.0000000000",
                    "price_with_vat": True,
                },
            ],
            "create_card_payment": {
                "expiry": "2023-10-01T12:00:00",
                "redirect_url": "https://callback.example.org/",
                "redirect_method": "GET",
            },
        }

        with patch.object(TestCardPaymentHandler, "init_payment") as card_mock:
            card_mock.side_effect = PaymentHandlerLimitError

            serializer = PaymentRequestSerializer(data=data)
            self.assertTrue(serializer.is_valid())
            serializer.save()

            self.assertIsNone(serializer.instance.card_payment)  # card payment not created, no error reported

    # --- Making card payment cart ---

    def test_make_cart_one_item(self):
        items = [
            OrderedDict(
                {
                    "name": "Item 1 with loooooong description",
                    "item_type": "basic",
                    "quantity": 3,
                    "price_per_unit": Decimal("11.00"),
                    "price_total": Decimal("33.00"),
                }
            ),
        ]
        expected = [
            CartItem(name="Item 1 with loooooon", quantity=3, amount=Decimal("33.00"), description=""),
        ]

        self.assertEqual(PaymentRequestSerializer.make_cart(items), expected)

    def test_make_cart_two_items(self):
        items = [
            OrderedDict(
                {
                    "name": "Item 1",
                    "item_type": "basic",
                    "quantity": 3,
                    "price_per_unit": Decimal("11.00"),
                    "price_total": Decimal("33.00"),
                }
            ),
            OrderedDict(
                {
                    "name": "Item 2",
                    "item_type": "basic",
                    "quantity": 2,
                    "price_per_unit": Decimal("20.00"),
                    "price_total": Decimal("40.00"),
                }
            ),
        ]
        expected = [
            CartItem(name="Item 1", quantity=3, amount=Decimal("33.0"), description=""),
            CartItem(name="Item 2", quantity=2, amount=Decimal("40.0"), description=""),
        ]

        self.assertEqual(PaymentRequestSerializer.make_cart(items), expected)

    def test_make_cart_more_items(self):
        items = [
            OrderedDict(
                {
                    "name": "Item 1",
                    "item_type": "basic",
                    "quantity": 3,
                    "price_per_unit": Decimal("11.00"),
                    "price_total": Decimal("33.00"),
                }
            ),
            OrderedDict(
                {
                    "name": "Item 2",
                    "item_type": "basic",
                    "quantity": 1,
                    "price_per_unit": Decimal("20.00"),
                    "price_total": Decimal("20.00"),
                }
            ),
            OrderedDict(
                {
                    "name": "Item 3",
                    "item_type": "basic",
                    "quantity": 1,
                    "price_per_unit": Decimal("3.00"),
                    "price_total": Decimal("3.00"),
                }
            ),
            OrderedDict(
                {
                    "name": "Item 4",
                    "item_type": "basic",
                    "quantity": 1,
                    "price_per_unit": Decimal("4.00"),
                    "price_total": Decimal("4.00"),
                }
            ),
        ]
        expected = [
            CartItem(name="Item 1", quantity=3, amount=Decimal("33.0"), description=""),
            CartItem(name="+3 ...", quantity=1, amount=Decimal("27.0"), description=""),
        ]

        self.assertEqual(PaymentRequestSerializer.make_cart(items), expected)


class TestPaymentRequestForInvoiceSerializer(TestCase):
    """`PaymentRequestForInvoiceSerializer` tests."""

    def test_tax_point_ok(self):
        data = [
            # Tax supply date at tranaction date
            (date(2023, 12, 15), date(2023, 12, 30), date(2023, 12, 15)),  # max 15 days old, same month
            (date(2023, 12, 20), date(2024, 1, 4), date(2023, 12, 20)),  # max 15 days old, previous month
            # Tax supply date at today
            (date(2023, 12, 1), date(2023, 12, 30), date(2023, 12, 30)),  # more than 15 days old, same month
            # Tax supply date at last day of previous month
            (date(2023, 12, 20), date(2024, 1, 14), date(2023, 12, 31)),  # more than 15 days old, previous month
        ]
        for transaction_date, today, tax_at in data:
            with self.subTest(transaction_date=transaction_date, today=today):
                self.assertEqual(
                    PaymentRequestForInvoiceSerializer._get_tax_point(transaction_date, today),
                    tax_at,
                )

    def test_tax_point_exception(self):
        data = [
            # Payment realized in future
            (date(2024, 1, 1), date(2023, 12, 30), ValueError, "Payment realized in future"),
            # Payment older than previous month
            (date(2023, 12, 30), date(2024, 2, 6), PaymentTooOldException, "Payment before start of previous month"),
            # Payment in previous month but today after 15th
            (
                date(2023, 12, 30),
                date(2024, 1, 16),
                PaymentTooOldException,
                "Payment during previous month, more than 15 days old",
            ),
        ]
        for transaction_date, today, exception, message in data:
            with self.subTest(transaction_date=transaction_date, today=today):
                self.assertRaisesMessage(
                    exception,
                    message,
                    PaymentRequestForInvoiceSerializer._get_tax_point,
                    transaction_date,
                    today,
                )

    @freeze_time("2023-09-20T12:00:00Z")
    def test_serialization(self):
        # Prepare data and save all objects to database
        payment_request = create_payment_request()
        customer = payment_request.customer
        customer.vies_vat_valid = True
        customer.save()
        BillingItemType.objects.create(handle="basic_rate")
        VatRate.objects.create(
            country_code="CZ",
            cpa_code=None,
            valid_from=datetime(2020, 1, 1, tzinfo=timezone.utc),
            valid_to=None,
            value=Decimal("20.0"),
        )

        # Get object from database and serialize it
        serializer = PaymentRequestForInvoiceSerializer(PaymentRequest.objects.get(pk=payment_request.pk))

        expected = {
            "customer": "CID-1",
            "app_id": "auctions",
            "due_at": datetime(2023, 10, 1, 12, 0, 0, tzinfo=timezone.utc),
            "tax_at": date(2023, 9, 15),
            "paid_at": date(2023, 9, 15),
            "price_to_pay": Decimal("0.00"),
            "currency": "CZK",
            "variable_symbol": "0000009876",
            "bank_account": "123456/0300",
            "payment_type": PaymentType.TRANSFER,
            "items": [
                {
                    "name": "Item 1",
                    "item_type": "basic_rate",
                    "vat_rate": Decimal("20.0"),
                    "price_per_unit": Decimal("9.17"),
                    "quantity": 1,
                    "price_total_base": Decimal("9.17"),
                    "price_total_vat": Decimal("1.83"),
                    "price_total": Decimal("11.00"),
                },
                {
                    "name": "Item 2",
                    "item_type": "basic_rate",
                    "vat_rate": Decimal("20.0"),
                    "price_per_unit": Decimal("10.00"),
                    "quantity": 2,
                    "price_total_base": Decimal("20.00"),
                    "price_total_vat": Decimal("4.00"),
                    "price_total": Decimal("24.00"),
                },
                {
                    "name": "Item 3",
                    "item_type": "basic_rate",
                    "vat_rate": Decimal("20.0"),
                    "price_per_unit": Decimal("10.83"),
                    "quantity": 3,
                    "price_total_base": Decimal("32.50"),
                    "price_total_vat": Decimal("6.50"),
                    "price_total": Decimal("39.00"),
                },
            ],
        }
        self.assertEqual(serializer.data, expected)

    @freeze_time("2023-09-20T12:00:00Z")
    def test_serialization_reverse_charge(self):
        # Prepare data and save all objects to database
        payment_request = create_payment_request()
        customer = payment_request.customer
        customer.vat_number = "DE0001"
        customer.country_code = "DE"
        customer.vies_vat_valid = True
        customer.save()
        BillingItemType.objects.create(handle="basic_rate")

        # Get object from database and serialize it
        serializer = PaymentRequestForInvoiceSerializer(PaymentRequest.objects.get(pk=payment_request.pk))

        expected = {
            "customer": "CID-1",
            "app_id": "auctions",
            "due_at": datetime(2023, 10, 1, 12, 0, 0, tzinfo=timezone.utc),
            "tax_at": date(2023, 9, 15),
            "paid_at": date(2023, 9, 15),
            "price_to_pay": Decimal("0.00"),
            "currency": "CZK",
            "variable_symbol": "0000009876",
            "bank_account": "123456/0300",
            "payment_type": PaymentType.TRANSFER,
            "items": [
                {
                    "name": "Item 1",
                    "item_type": "basic_rate",
                    "vat_rate": Decimal("0.0"),
                    "price_per_unit": Decimal("11.00"),
                    "quantity": 1,
                    "price_total_base": Decimal("11.00"),
                    "price_total_vat": Decimal("0.00"),
                    "price_total": Decimal("11.00"),
                },
                {
                    "name": "Item 2",
                    "item_type": "basic_rate",
                    "vat_rate": Decimal("0.0"),
                    "price_per_unit": Decimal("12.00"),
                    "quantity": 2,
                    "price_total_base": Decimal("24.00"),
                    "price_total_vat": Decimal("0.00"),
                    "price_total": Decimal("24.00"),
                },
                {
                    "name": "Item 3",
                    "item_type": "basic_rate",
                    "vat_rate": Decimal("0.0"),
                    "price_per_unit": Decimal("13.00"),
                    "quantity": 3,
                    "price_total_base": Decimal("39.00"),
                    "price_total_vat": Decimal("0.00"),
                    "price_total": Decimal("39.00"),
                },
            ],
        }
        self.assertEqual(serializer.data, expected)

    @freeze_time("2023-09-20T12:00:00Z")
    def test_serialization_not_invoiceable(self):
        # Prepare data and save all objects to database
        payment_request = create_payment_request()
        customer = payment_request.customer
        customer.vies_vat_valid = True
        customer.save()
        BillingItemType.objects.create(handle="basic_rate", not_invoiceable=True)

        # Get object from database and serialize it
        serializer = PaymentRequestForInvoiceSerializer(PaymentRequest.objects.get(pk=payment_request.pk))
        with self.assertRaisesMessage(ValidationError, "`basic_rate` is non-invoiceable"):
            serializer.data

    @freeze_time("2023-09-20T12:00:00Z")
    def test_serialization_no_vies(self):
        # Prepare data and save all objects to database
        payment_request = create_payment_request()
        BillingItemType.objects.create(handle="basic_rate")

        # Get object from database and serialize it
        serializer = PaymentRequestForInvoiceSerializer(PaymentRequest.objects.get(pk=payment_request.pk))
        with self.assertRaisesMessage(ValidationError, "VAT number for EU resident is not yet validated"):
            serializer.data

    @freeze_time("2023-09-20T12:00:00Z")
    def test_serialization_no_vat_rate(self):
        # Prepare data and save all objects to database
        payment_request = create_payment_request()
        customer = payment_request.customer
        customer.vies_vat_valid = True
        customer.save()
        BillingItemType.objects.create(handle="basic_rate")

        # Get object from database and serialize it
        serializer = PaymentRequestForInvoiceSerializer(PaymentRequest.objects.get(pk=payment_request.pk))
        with self.assertRaisesMessage(ValidationError, "No applicable VAT rate found for `basic_rate` at 2023-09-15"):
            serializer.data

    @freeze_time("2023-09-20T12:00:00Z")
    def test_serialization_payment_not_realized(self):
        # Prepare data and save all objects to database
        payment_request = create_payment_request(realized_payment=None)

        # Get object from database and serialize it
        with self.assertRaisesMessage(ValidationError, "Payment is not realized yet"):
            PaymentRequestForInvoiceSerializer(PaymentRequest.objects.get(pk=payment_request.pk))


class TestInvoiceSerializer(TestCase):
    """`InvoiceSerializer` tests."""

    def setUp(self):
        self.data: Dict[str, Any] = {
            "app_id": "auctions",
            "customer": "CID-1",
            "due_at": datetime(2024, 1, 25, 12, 0, 0, tzinfo=timezone.utc),
            "tax_at": date(2024, 1, 20),
            "paid_at": date(2024, 1, 20),
            "price_to_pay": Decimal("0.00"),
            "currency": "CZK",
            "variable_symbol": "0902400012",
            "bank_account": "123456/0300",
            "payment_type": PaymentType.TRANSFER,
            "items": [
                {
                    "name": "Item 1",
                    "item_type": "basic_rate",
                    "vat_rate": Decimal("10.0"),
                    "quantity": 1,
                    "price_per_unit": Decimal("11.00"),
                    "price_total_base": Decimal("11.00"),
                    "price_total_vat": Decimal("1.10"),
                    "price_total": Decimal("12.10"),
                },
                {
                    "name": "Item 2",
                    "item_type": "basic_rate",
                    "vat_rate": Decimal("20.0"),
                    "quantity": 2,
                    "price_per_unit": Decimal("15.00"),
                    "price_total_base": Decimal("30.00"),
                    "price_total_vat": Decimal("6.00"),
                    "price_total": Decimal("36.00"),
                },
            ],
        }
        InvoiceNumberSequence.objects.create(app_id="auctions", year=2024, prefix=9024, current_number=0)

    def test_validate_ok(self):
        customer = get_customer(vies_vat_valid=True, vies_checked_at=datetime(2024, 1, 1, tzinfo=timezone.utc))
        customer.save()
        serializer = InvoiceSerializer(data=self.data)
        self.assertTrue(serializer.is_valid())
        # Check copied VAT number
        self.assertEqual(serializer.validated_data["vat_number"], "CZ001234")
        # Check calculated total prices
        self.assertEqual(serializer.validated_data["price_total_base"], Decimal("41.00"))
        self.assertEqual(serializer.validated_data["price_total_vat"], Decimal("7.10"))
        self.assertEqual(serializer.validated_data["price_total"], Decimal("48.10"))

    def test_validate_ok_vies_false(self):
        customer = get_customer(vies_vat_valid=False, vies_checked_at=datetime(2024, 1, 1, tzinfo=timezone.utc))
        customer.save()
        serializer = InvoiceSerializer(data=self.data)
        self.assertTrue(serializer.is_valid())
        # Check VAT number
        self.assertIsNone(serializer.validated_data["vat_number"])  # VAT cleared

    def test_validate_ok_non_eu(self):
        customer = get_customer(vat_number="CH0001", country_code="CH")
        customer.save()
        serializer = InvoiceSerializer(data=self.data)
        self.assertTrue(serializer.is_valid())
        # Check copied VAT number
        self.assertEqual(serializer.validated_data["vat_number"], "CH0001")

    def test_validate_failure_on_no_vies(self):
        customer = get_customer()
        customer.save()
        serializer = InvoiceSerializer(data=self.data)
        self.assertFalse(serializer.is_valid())
        self.assertIn(
            ErrorDetail(string="VAT number for EU resident is not yet validated", code="invalid"),
            serializer.errors["non_field_errors"],
        )

    @freeze_time("2024-01-25T12:00:00Z")
    @patch("django_pain.serializers.uuid4")
    def test_create_and_serialize(self, uuid_mock):
        uuid_mock.return_value = UUID(int=5)
        customer = get_customer(vies_vat_valid=True, vies_checked_at=datetime(2024, 1, 1, tzinfo=timezone.utc))
        customer.save()
        serializer = InvoiceSerializer(data=self.data)
        self.assertTrue(serializer.is_valid())

        serializer.save()  # create invoice record in DB (calls `serializer.create`)

        # Get invoice from DB and serialize it for verification
        invoice = Invoice.objects.get()
        expected = {
            "uuid": "00000000-0000-0000-0000-000000000005",
            "app_id": "auctions",
            "customer": "CID-1",
            "name": None,
            "organization": "Test, s.r.o.",
            "street": ["Na Okraji 1"],
            "city": "Pokuston",
            "state_or_province": None,
            "postal_code": "050 10",
            "country_code": "CZ",
            "company_registration_number": "001234",
            "vat_number": "CZ001234",
            "created_at": "2024-01-25T12:00:00Z",
            "tax_at": "2024-01-20",
            "number": "902400001",  # properly generated invoice number from sequence
            "variable_symbol": "0902400012",
            "price_total_base": "41.0000000000",  # properly summed up
            "price_total_vat": "7.1000000000",  # properly summed up
            "price_total": "48.1000000000",  # properly summed up
            "price_to_pay": "0.0000000000",
            "currency": "CZK",
            "file_uid": None,
            "bank_account": "123456/0300",
            "payment_type": PaymentType.TRANSFER,
            "items": [
                # nested serialized data stays in OrderedDict form (which doesn't matter for further JSON rendering)
                OrderedDict(
                    {
                        "name": "Item 1",
                        "vat_rate": "10.0000",
                        "quantity": 1,
                        "price_per_unit": "11.0000000000",
                        "price_total_base": "11.0000000000",
                        "price_total_vat": "1.1000000000",
                        "price_total": "12.1000000000",
                    }
                ),
                OrderedDict(
                    {
                        "name": "Item 2",
                        "vat_rate": "20.0000",
                        "quantity": 2,
                        "price_per_unit": "15.0000000000",
                        "price_total_base": "30.0000000000",
                        "price_total_vat": "6.0000000000",
                        "price_total": "36.0000000000",
                    }
                ),
            ],
        }
        serializer = InvoiceSerializer(invoice)
        self.assertEqual(serializer.data, expected)

    @freeze_time("2023-01-25T12:00:00Z")
    def test_create_failed(self):
        customer = get_customer(vies_vat_valid=True, vies_checked_at=datetime(2024, 1, 1, tzinfo=timezone.utc))
        customer.save()
        serializer = InvoiceSerializer(data=self.data)
        self.assertTrue(serializer.is_valid())

        error_message = "No invoice number sequence available for `auctions` and `2023`"
        self.assertRaisesMessage(ValidationError, error_message, serializer.save)
