#
# Copyright (C) 2024  CZ.NIC, z. s. p. o.
#
# This file is part of FRED.
#
# FRED is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# FRED is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with FRED.  If not, see <https://www.gnu.org/licenses/>.

"""Tests for TEDB client."""

from datetime import date
from pathlib import Path
from unittest.mock import call, patch

from django.test import SimpleTestCase
from lxml import etree
from testfixtures import LogCapture
from zeep.exceptions import Error, Fault

from django_pain.vat_rate.tedb_client import TedbClient

LOG_SOURCE = "django_pain.vat_rate.tedb_client"


class TestVatRateDownloader(SimpleTestCase):
    """Test VAT rate downloader."""

    @classmethod
    def setUpClass(cls):
        wsdl = Path(__file__).parent / "data" / "VatRetrievalService.wsdl"
        cls.tedb_client = TedbClient(wsdl)
        cls.response_type = cls.tedb_client._client.get_type(
            "{urn:ec.europa.eu:taxud:tedb:services:v1:IVatRetrievalService:types}retrieveVatRatesRespType"
        )

    def setUp(self) -> None:
        self.log_handler = LogCapture(LOG_SOURCE, propagate=False)

    def tearDown(self):
        self.log_handler.uninstall()

    def test_get_vat_rates_standard(self):
        data = [
            {
                "memberState": "EL",
                "type": "STANDARD",
                "rate": {"type": "DEFAULT", "value": 24.0},
                "situationOn": date(2024, 1, 1),
            },
        ]
        with patch.object(self.tedb_client, "_client") as soap_client_mock:
            soap_client_mock.service.retrieveVatRates.return_value = self.response_type(vatRateResults=data)

            vat_rates = self.tedb_client.get_vat_rates(date(2024, 1, 1), date(2025, 1, 1), ["GR"])

            self.assertEqual(len(vat_rates), 1)
            item = vat_rates[0]
            self.assertEqual(item.memberState, "EL")
            self.assertEqual(item.type, "STANDARD")
            self.assertEqual(item.situationOn, date(2024, 1, 1))
            self.assertEqual(item.rate.type, "DEFAULT")
            self.assertEqual(item.rate.value, 24.0)
            soap_client_mock.service.retrieveVatRates.assert_called_once_with(
                **{"from": "2024-01-01", "to": "2025-01-01", "memberStates": {"isoCode": ["EL"]}}
            )
            self.log_handler.check(
                (
                    LOG_SOURCE,
                    "INFO",
                    "Calling `retrieveVatRates` service with: {'from': '2024-01-01', 'to': '2025-01-01', 'memberStates': {'isoCode': ['EL']}}",
                ),
                (
                    LOG_SOURCE,
                    "INFO",
                    "Received 1 VAT rate items from `retrieveVatRates` service",
                ),
                (LOG_SOURCE, "INFO", "Returning 1 VAT rate items from `retrieveVatRates` service"),
            )

    def test_get_vat_rates_reduced(self):
        data = [
            {
                "memberState": "SI",
                "type": "STANDARD",
                "rate": {"type": "DEFAULT", "value": 22.0},
                "situationOn": date(2024, 1, 1),
            },
        ]
        with patch.object(self.tedb_client, "_client") as soap_client_mock:
            soap_client_mock.service.retrieveVatRates.return_value = self.response_type(vatRateResults=data)

            vat_rates = self.tedb_client.get_vat_rates(date(2024, 1, 1), date(2025, 1, 1), ["SI"], ["36", "37.0"])

            self.assertEqual(len(vat_rates), 1)
            soap_client_mock.service.retrieveVatRates.assert_called_once_with(
                **{
                    "from": "2024-01-01",
                    "to": "2025-01-01",
                    "memberStates": {"isoCode": ["SI"]},
                    "cpaCodes": {"value": ["36", "37.0"]},
                }
            )

    @patch("django_pain.vat_rate.tedb_client.MAX_CPA_CODES", 1)  # limit of CPA codes in batch
    def test_get_vat_rates_reduced_split(self):
        data = [
            {
                "memberState": "SI",
                "type": "STANDARD",
                "rate": {"type": "DEFAULT", "value": 22.0},
                "situationOn": date(2024, 1, 1),
            },
        ]
        with patch.object(self.tedb_client, "_client") as soap_client_mock:
            response = self.response_type(vatRateResults=data)
            soap_client_mock.service.retrieveVatRates.side_effect = [response, response]

            vat_rates = self.tedb_client.get_vat_rates(date(2024, 1, 1), date(2025, 1, 1), ["SI"], ["36", "37.0"])

            self.assertEqual(len(vat_rates), 2)  # one item from each call
            self.assertEqual(  # called twice (each CPA code separately)
                soap_client_mock.service.retrieveVatRates.mock_calls,
                [
                    call(
                        **{
                            "from": "2024-01-01",
                            "to": "2025-01-01",
                            "memberStates": {"isoCode": ["SI"]},
                            "cpaCodes": {"value": ["36"]},
                        }
                    ),
                    call(
                        **{
                            "from": "2024-01-01",
                            "to": "2025-01-01",
                            "memberStates": {"isoCode": ["SI"]},
                            "cpaCodes": {"value": ["37.0"]},
                        }
                    ),
                ],
            )

    def test_is_usable(self):
        data = [
            {  # regional rate
                "memberState": "AT",
                "type": "REDUCED",
                "rate": {"type": "REDUCED_RATE", "value": 19.0},
                "situationOn": date(2024, 1, 1),
                "category": {
                    "identifier": "REGION",
                    "description": "Special reduced rate for specific regions (including articles 104, 105 and 120 of the VAT Directive)",
                },
                "comment": "Jungholz, Mittelberg",
            },
            {  # rate value not provided (similar case not met in real DB)
                "memberState": "AT",
                "type": "REDUCED",
                "rate": {
                    "type": "OUT_OF_SCOPE",
                },
                "situationOn": date(2024, 1, 1),
            },
            {
                "memberState": "AT",
                "type": "STANDARD",
                "rate": {"type": "DEFAULT", "value": 20.0},
                "situationOn": date(2024, 1, 1),
            },
            {  # exception - regional rate for Canary Islands
                "memberState": "ES",
                "type": "STANDARD",
                "rate": {"type": "DEFAULT", "value": 7.0},
                "situationOn": date(2024, 1, 1),
                "comment": "VAT - Canary Islands - ",
            },
            {
                "memberState": "ES",
                "type": "STANDARD",
                "rate": {"type": "DEFAULT", "value": 21.0},
                "situationOn": date(2024, 1, 1),
            },
        ]
        with patch.object(self.tedb_client, "_client") as soap_client_mock:
            soap_client_mock.service.retrieveVatRates.return_value = self.response_type(vatRateResults=data)

            vat_rates = self.tedb_client.get_vat_rates(date(2024, 1, 1), date(2025, 1, 1), ["AT", "ES"])

            self.assertEqual(len(vat_rates), 2)
            at_standard = vat_rates[0]
            self.assertEqual(
                (at_standard.memberState, at_standard.type, at_standard.rate.value), ("AT", "STANDARD", 20.0)
            )
            es_standard = vat_rates[1]
            self.assertEqual(
                (es_standard.memberState, es_standard.type, es_standard.rate.value), ("ES", "STANDARD", 21.0)
            )
            soap_client_mock.service.retrieveVatRates.assert_called_once_with(
                **{"from": "2024-01-01", "to": "2025-01-01", "memberStates": {"isoCode": ["AT", "ES"]}}
            )
            self.log_handler.check(
                (
                    LOG_SOURCE,
                    "INFO",
                    "Calling `retrieveVatRates` service with: {'from': '2024-01-01', 'to': '2025-01-01', 'memberStates': {'isoCode': ['AT', 'ES']}}",
                ),
                (
                    LOG_SOURCE,
                    "INFO",
                    "Received 5 VAT rate items from `retrieveVatRates` service",
                ),
                (LOG_SOURCE, "INFO", "Returning 2 VAT rate items from `retrieveVatRates` service"),
            )

    def test_message_no_details(self):
        error = Fault(message="TEDB-ERR-2 - Request is not valid", code="env:Client")
        self.assertEqual(
            self.tedb_client._error_message(error), "SOAP error: TEDB-ERR-2 - Request is not valid, env:Client"
        )

    def test_message(self):
        error_xml = (  # extracted from real SOAP response
            '<detail xmlns="urn:ec.europa.eu:taxud:tedb:services:v1:IVatRetrievalService:types">'
            '    <ns0:retrieveVatRatesFaultMsg xmlns:ns0="urn:ec.europa.eu:taxud:tedb:services:v1:IVatRetrievalService">'
            "        <error>"
            "            <code>00002</code>"
            '            <description>The Member State "SIX" does not exist.</description>'
            "        </error>"
            "        <error>"
            "            <code>00005</code>"
            '            <description>The CPA Code "37.01" does not exist.</description>'
            "        </error>"
            "    </ns0:retrieveVatRatesFaultMsg>"
            "</detail>"
        )
        error = Fault(message="TEDB-ERR-2 - Request is not valid", code="env:Client", detail=etree.XML(error_xml))
        expected = (
            "SOAP error: TEDB-ERR-2 - Request is not valid, env:Client "
            '(00002: The Member State "SIX" does not exist., 00005: The CPA Code "37.01" does not exist.)'
        )
        self.assertEqual(self.tedb_client._error_message(error), expected)

    def test_get_vat_rates_exception(self):
        with patch.object(self.tedb_client, "_client") as soap_client_mock:
            error = Fault(message="TEDB-ERR-2 - Request is not valid", code="env:Client")
            soap_client_mock.service.retrieveVatRates.side_effect = error

            self.assertRaisesMessage(
                Error,
                "SOAP error: TEDB-ERR-2 - Request is not valid, env:Client",
                self.tedb_client.get_vat_rates,
                date(2024, 1, 1),
                date(2025, 1, 1),
                ["GR"],
            )
