#
# Copyright (C) 2024  CZ.NIC, z. s. p. o.
#
# This file is part of FRED.
#
# FRED is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# FRED is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with FRED.  If not, see <https://www.gnu.org/licenses/>.

"""Tests for VAT rate updater."""

from datetime import date, datetime, timezone
from decimal import Decimal
from io import StringIO
from pathlib import Path
from typing import Any, Dict

from django.test import TestCase
from testfixtures import LogCapture

from django_pain.management.command_utils import Reporter
from django_pain.models.invoices import VatRate
from django_pain.vat_rate.tedb_client import TedbClient
from django_pain.vat_rate.updater import VatRateUpdater, VatRateUpdaterError


class TestVatRateUpdater(TestCase):
    """Test VAT rate updater."""

    @classmethod
    def setUpClass(cls) -> None:
        tedb_client = TedbClient(Path(__file__).parent / "data" / "VatRetrievalService.wsdl")
        cls.response_type = tedb_client._client.get_type(
            "{urn:ec.europa.eu:taxud:tedb:services:v1:IVatRetrievalService:types}retrieveVatRatesRespType"
        )
        return super().setUpClass()

    def setUp(self) -> None:
        self.updater = VatRateUpdater(Reporter(StringIO()))
        self.vat_rate_standard = VatRate(
            country_code="SI",
            cpa_code=None,
            valid_from=datetime(2024, 1, 1, tzinfo=timezone.utc),
            valid_to=None,
            value=Decimal(22.0),
        )
        self.vat_rate_reduced = VatRate(
            country_code="SI",
            cpa_code="62.0",
            valid_from=datetime(2024, 1, 1, tzinfo=timezone.utc),
            valid_to=None,
            value=Decimal(10.0),
        )
        self.log_handler = LogCapture("django_pain.vat_rate.updater", propagate=False)

    def tearDown(self) -> None:
        self.log_handler.uninstall()

    def _vat_rate_to_dict(self, vat_rate: VatRate) -> Dict[str, Any]:
        return {
            "country_code": str(vat_rate.country_code),
            "cpa_code": vat_rate.cpa_code,
            "valid_from": vat_rate.valid_from,
            "valid_to": vat_rate.valid_to,
            "value": vat_rate.value,
        }

    def test_apply_no_rates(self):
        self.updater.apply([], ["SI", "AT"], ["62.0", "37"], dry_run=True)

        self.assertEqual(self.updater._updated_vat_rates, [])

    def test_apply_on_empty_db(self):
        data = [
            {
                "memberState": "SI",
                "type": "STANDARD",
                "rate": {"type": "DEFAULT", "value": 22.0},
                "situationOn": date(2024, 1, 1),
            },
        ]
        vat_rates = self.response_type(vatRateResults=data).vatRateResults

        self.updater.apply(vat_rates, ["SI"], ["62.0"], dry_run=True)

        expected_data = [
            {
                "country_code": "SI",
                "cpa_code": None,
                "valid_from": datetime(2024, 1, 1, tzinfo=timezone.utc),
                "valid_to": None,
                "value": Decimal(22.0),
            },
        ]
        self.assertEqual([self._vat_rate_to_dict(i) for i in self.updater._updated_vat_rates], expected_data)

    def test_apply_no_dry_run(self):
        data = [
            {
                "memberState": "SI",
                "type": "STANDARD",
                "rate": {"type": "DEFAULT", "value": 22.0},
                "situationOn": date(2024, 1, 1),
            },
        ]
        vat_rates = self.response_type(vatRateResults=data).vatRateResults

        self.updater.apply(vat_rates, ["SI"], ["62.0"])

        expected_data = [
            {
                "country_code": "SI",
                "cpa_code": None,
                "valid_from": datetime(2024, 1, 1, tzinfo=timezone.utc),
                "valid_to": None,
                "value": Decimal(22.0),
            },
        ]
        queryset = VatRate.objects.all()
        self.assertEqual([self._vat_rate_to_dict(i) for i in queryset], expected_data)

    def test_apply_no_dry_run_no_rates(self):
        self.updater.apply([], ["SI", "AT"], ["62.0", "37"])

        self.assertIsNone(VatRate.objects.first())

    def test_apply_duplicate(self):
        data = [
            {
                "memberState": "SI",
                "type": "STANDARD",
                "rate": {"type": "DEFAULT", "value": 22.0},
                "situationOn": date(2024, 1, 1),
            },
            {
                "memberState": "SI",
                "type": "STANDARD",
                "rate": {"type": "DEFAULT", "value": 22.0},
                "situationOn": date(2024, 1, 1),
                "comment": "Import",
            },
        ]
        vat_rates = self.response_type(vatRateResults=data).vatRateResults

        self.updater.apply(vat_rates, ["SI"], ["62.0"], dry_run=True)

        expected_data = [
            {
                "country_code": "SI",
                "cpa_code": None,
                "valid_from": datetime(2024, 1, 1, tzinfo=timezone.utc),
                "valid_to": None,
                "value": Decimal(22.0),
            },
        ]
        self.assertEqual([self._vat_rate_to_dict(i) for i in self.updater._updated_vat_rates], expected_data)

    def test_apply_prolong_standard_rate(self):
        self.vat_rate_standard.save()
        data = [
            {
                "memberState": "SI",
                "type": "STANDARD",
                "rate": {"type": "DEFAULT", "value": 22.0},
                "situationOn": date(2024, 4, 1),
            },
            {
                "memberState": "SI",
                "type": "STANDARD",
                "rate": {"type": "DEFAULT", "value": 22.0},
                "situationOn": date(2024, 7, 1),
            },
        ]
        vat_rates = self.response_type(vatRateResults=data).vatRateResults

        self.updater.apply(vat_rates, ["SI"], ["62.0"], dry_run=True)

        self.assertEqual(self.updater._updated_vat_rates, [])

    def test_apply_change_standard_rate(self):
        self.vat_rate_standard.save()
        data = [
            {
                "memberState": "SI",
                "type": "STANDARD",
                "rate": {"type": "DEFAULT", "value": 21.0},
                "situationOn": date(2024, 4, 1),
            },
            {
                "memberState": "SI",
                "type": "STANDARD",
                "rate": {"type": "DEFAULT", "value": 20.0},
                "situationOn": date(2024, 7, 1),
            },
        ]
        vat_rates = self.response_type(vatRateResults=data).vatRateResults

        self.updater.apply(vat_rates, ["SI"], ["62.0"], dry_run=True)

        expected_data = [
            {
                "country_code": "SI",
                "cpa_code": None,
                "valid_from": datetime(2024, 1, 1, tzinfo=timezone.utc),
                "valid_to": datetime(2024, 4, 1, tzinfo=timezone.utc),
                "value": Decimal(22.0),
            },
            {
                "country_code": "SI",
                "cpa_code": None,
                "valid_from": datetime(2024, 4, 1, tzinfo=timezone.utc),
                "valid_to": datetime(2024, 7, 1, tzinfo=timezone.utc),
                "value": Decimal(21.0),
            },
            {
                "country_code": "SI",
                "cpa_code": None,
                "valid_from": datetime(2024, 7, 1, tzinfo=timezone.utc),
                "valid_to": None,
                "value": Decimal(20.0),
            },
        ]
        self.assertEqual([self._vat_rate_to_dict(i) for i in self.updater._updated_vat_rates], expected_data)

    def test_apply_greek_standard_rate(self):
        # Greece: ISO country code = GR, VAT country code = EL
        VatRate.objects.create(
            country_code="GR",
            cpa_code=None,
            valid_from=datetime(2024, 1, 1, tzinfo=timezone.utc),
            valid_to=None,
            value=Decimal(24.0),
        )
        data = [
            {
                "memberState": "EL",
                "type": "STANDARD",
                "rate": {"type": "DEFAULT", "value": 23.0},
                "situationOn": date(2024, 4, 1),
            },
        ]
        vat_rates = self.response_type(vatRateResults=data).vatRateResults

        self.updater.apply(vat_rates, ["GR"], ["62.0"], dry_run=True)

        expected_data = [
            {
                "country_code": "GR",
                "cpa_code": None,
                "valid_from": datetime(2024, 1, 1, tzinfo=timezone.utc),
                "valid_to": datetime(2024, 4, 1, tzinfo=timezone.utc),
                "value": Decimal(24.0),
            },
            {
                "country_code": "GR",
                "cpa_code": None,
                "valid_from": datetime(2024, 4, 1, tzinfo=timezone.utc),
                "valid_to": None,
                "value": Decimal(23.0),
            },
        ]
        self.assertEqual([self._vat_rate_to_dict(i) for i in self.updater._updated_vat_rates], expected_data)

    def test_apply_prolong_reduced_rate(self):
        self.vat_rate_standard.save()
        self.vat_rate_reduced.save()
        data = [
            {
                "memberState": "SI",
                "type": "STANDARD",
                "rate": {"type": "DEFAULT", "value": 22.0},
                "situationOn": date(2024, 4, 1),
            },
            {
                "memberState": "SI",
                "type": "REDUCED",
                "rate": {"type": "REDUCED_RATE", "value": 10.0},
                "situationOn": date(2024, 4, 1),
                "cpaCodes": {"code": [{"value": "62.0"}]},
            },
        ]
        vat_rates = self.response_type(vatRateResults=data).vatRateResults

        self.updater.apply(vat_rates, ["SI"], ["62.0"], dry_run=True)

        self.assertEqual(self.updater._updated_vat_rates, [])

    def test_apply_change_reduced_rate(self):
        self.vat_rate_standard.save()
        self.vat_rate_reduced.save()
        data = [
            {
                "memberState": "SI",
                "type": "STANDARD",
                "rate": {"type": "DEFAULT", "value": 22.0},
                "situationOn": date(2024, 4, 1),
            },
            {
                "memberState": "SI",
                "type": "REDUCED",
                "rate": {"type": "REDUCED_RATE", "value": 11.0},
                "situationOn": date(2024, 4, 1),
                "cpaCodes": {"code": [{"value": "62.0"}]},
            },
        ]
        vat_rates = self.response_type(vatRateResults=data).vatRateResults

        self.updater.apply(vat_rates, ["SI"], ["62.0"], dry_run=True)

        expected_data = [
            {
                "country_code": "SI",
                "cpa_code": "62.0",
                "valid_from": datetime(2024, 1, 1, tzinfo=timezone.utc),
                "valid_to": datetime(2024, 4, 1, tzinfo=timezone.utc),
                "value": Decimal(10.0),
            },
            {
                "country_code": "SI",
                "cpa_code": "62.0",
                "valid_from": datetime(2024, 4, 1, tzinfo=timezone.utc),
                "valid_to": None,
                "value": Decimal(11.0),
            },
        ]
        self.assertEqual([self._vat_rate_to_dict(i) for i in self.updater._updated_vat_rates], expected_data)

    def test_apply_close_and_open_reduced_rate(self):
        self.vat_rate_standard.save()
        self.vat_rate_reduced.save()
        data = [
            {
                "memberState": "SI",
                "type": "STANDARD",
                "rate": {"type": "DEFAULT", "value": 22.0},
                "situationOn": date(2024, 4, 1),
            },
            {
                "memberState": "SI",
                "type": "STANDARD",
                "rate": {"type": "DEFAULT", "value": 22.0},
                "situationOn": date(2024, 7, 1),
            },
            {
                "memberState": "SI",
                "type": "REDUCED",
                "rate": {"type": "REDUCED_RATE", "value": 11.0},
                "situationOn": date(2024, 7, 1),
                "cpaCodes": {"code": [{"value": "62.0"}]},
            },
        ]
        vat_rates = self.response_type(vatRateResults=data).vatRateResults

        self.updater.apply(vat_rates, ["SI"], ["62.0"], dry_run=True)

        expected_data = [
            {
                "country_code": "SI",
                "cpa_code": "62.0",
                "valid_from": datetime(2024, 1, 1, tzinfo=timezone.utc),
                "valid_to": datetime(2024, 4, 1, tzinfo=timezone.utc),
                "value": Decimal(10.0),
            },
            {
                "country_code": "SI",
                "cpa_code": "62.0",
                "valid_from": datetime(2024, 7, 1, tzinfo=timezone.utc),
                "valid_to": None,
                "value": Decimal(11.0),
            },
        ]
        self.assertEqual([self._vat_rate_to_dict(i) for i in self.updater._updated_vat_rates], expected_data)

    def test_apply_open_and_close_reduced_rate(self):
        self.vat_rate_standard.save()
        data = [
            {
                "memberState": "SI",
                "type": "STANDARD",
                "rate": {"type": "DEFAULT", "value": 22.0},
                "situationOn": date(2024, 4, 1),
            },
            {
                "memberState": "SI",
                "type": "REDUCED",
                "rate": {"type": "REDUCED_RATE", "value": 11.0},
                "situationOn": date(2024, 4, 1),
                "cpaCodes": {"code": [{"value": "62.0"}]},
            },
            {
                "memberState": "SI",
                "type": "STANDARD",
                "rate": {"type": "DEFAULT", "value": 22.0},
                "situationOn": date(2024, 7, 1),
            },
        ]
        vat_rates = self.response_type(vatRateResults=data).vatRateResults

        self.updater.apply(vat_rates, ["SI"], ["62.0"], dry_run=True)

        expected_data = [
            {
                "country_code": "SI",
                "cpa_code": "62.0",
                "valid_from": datetime(2024, 4, 1, tzinfo=timezone.utc),
                "valid_to": datetime(2024, 7, 1, tzinfo=timezone.utc),
                "value": Decimal(11.0),
            },
        ]
        self.assertEqual([self._vat_rate_to_dict(i) for i in self.updater._updated_vat_rates], expected_data)

    def test_apply_open_on_closed_reduced_rate(self):
        self.vat_rate_standard.save()
        VatRate.objects.create(
            country_code="SI",
            cpa_code="62.0",
            valid_from=datetime(2023, 1, 1, tzinfo=timezone.utc),
            valid_to=datetime(2024, 1, 1, tzinfo=timezone.utc),
            value=Decimal(10.0),
        )
        data = [
            {
                "memberState": "SI",
                "type": "STANDARD",
                "rate": {"type": "DEFAULT", "value": 22.0},
                "situationOn": date(2024, 4, 1),
            },
            {
                "memberState": "SI",
                "type": "REDUCED",
                "rate": {"type": "REDUCED_RATE", "value": 11.0},
                "situationOn": date(2024, 4, 1),
                "cpaCodes": {"code": [{"value": "62.0"}]},
            },
        ]
        vat_rates = self.response_type(vatRateResults=data).vatRateResults

        self.updater.apply(vat_rates, ["SI"], ["62.0"], dry_run=True)

        expected_data = [
            {
                "country_code": "SI",
                "cpa_code": "62.0",
                "valid_from": datetime(2024, 4, 1, tzinfo=timezone.utc),
                "valid_to": None,
                "value": Decimal(11.0),
            },
        ]
        self.assertEqual([self._vat_rate_to_dict(i) for i in self.updater._updated_vat_rates], expected_data)

    def test_apply_unexpected_reduced_rate(self):
        data = [
            {
                "memberState": "SI",
                "type": "REDUCED",
                "rate": {"type": "REDUCED_RATE", "value": 11.0},
                "situationOn": date(2024, 4, 1),
                "cpaCodes": {"code": [{"value": "62.0"}]},
            },
        ]
        vat_rates = self.response_type(vatRateResults=data).vatRateResults

        with self.assertRaises(VatRateUpdaterError):
            self.updater.apply(vat_rates, ["SI"], ["62.0"], dry_run=True)

        self.assertEqual(
            self.updater._errors, ["Unexpected VAT rate change for country SI and CPA code 62.0 on 2024-04-01"]
        )

    def test_apply_ambiguous_reduced_rate(self):
        data = [
            {
                "memberState": "SI",
                "type": "STANDARD",
                "rate": {"type": "DEFAULT", "value": 22.0},
                "situationOn": date(2024, 4, 1),
            },
            {
                "memberState": "SI",
                "type": "REDUCED",
                "rate": {"type": "REDUCED_RATE", "value": 11.0},
                "situationOn": date(2024, 4, 1),
                "cpaCodes": {"code": [{"value": "62.0"}]},
            },
            {
                "memberState": "SI",
                "type": "REDUCED",
                "rate": {"type": "SUPER_REDUCED_RATE", "value": 1.0},
                "situationOn": date(2024, 4, 1),
                "cpaCodes": {"code": [{"value": "62.0"}]},
                "comment": "With super cool features.",
            },
        ]
        vat_rates = self.response_type(vatRateResults=data).vatRateResults

        with self.assertRaises(VatRateUpdaterError):
            self.updater.apply(vat_rates, ["SI"], ["62.0"], dry_run=True)

        self.assertEqual(
            self.updater._errors, ["Ambiguous VAT rate for country SI and CPA code 62.0 on 2024-04-01: 11% vs 1%"]
        )

    def _assert_equal_iterable(self, iterable, expected):
        for expected_item in expected:  # check all expected items
            iter_item = next(iterable)
            self.assertEqual(  # check item
                (iter_item.memberState, iter_item.situationOn, iter_item.rate.value),
                expected_item,
            )
        with self.assertRaises(StopIteration):  # check iterable is exhausted
            next(iterable)

    def test_filter_vat_rates(self):
        data = [
            {
                "memberState": "AT",
                "type": "STANDARD",
                "rate": {"type": "DEFAULT", "value": 20.0},
                "situationOn": date(2024, 1, 1),
            },
            {
                "memberState": "AT",
                "type": "STANDARD",
                "rate": {"type": "DEFAULT", "value": 20.5},
                "situationOn": date(2024, 7, 1),
            },
            {
                "memberState": "ES",
                "type": "STANDARD",
                "rate": {"type": "DEFAULT", "value": 21.0},
                "situationOn": date(2024, 1, 1),
            },
            {
                "memberState": "ES",
                "type": "REDUCED",
                "rate": {"type": "REDUCED_RATE", "value": 10.0},
                "situationOn": date(2024, 1, 1),
                "cpaCodes": {
                    "code": [
                        {"value": "37"},
                        {"value": "38.01"},
                    ]
                },
            },
            {
                "memberState": "ES",
                "type": "REDUCED",
                "rate": {"type": "REDUCED_RATE", "value": 11.0},
                "situationOn": date(2024, 1, 1),
                "cpaCodes": {"code": [{"value": "39"}]},
            },
        ]
        vat_rates = self.response_type(vatRateResults=data).vatRateResults
        # Iterate stored VAT rates with different filters
        test_cases = [
            (("AT", None, None), [("AT", date(2024, 1, 1), 20.0), ("AT", date(2024, 7, 1), 20.5)]),
            (("AT", None, date(2024, 1, 2)), [("AT", date(2024, 7, 1), 20.5)]),
            (("AT", None, date(2024, 7, 2)), []),
            (("ES", "30", None), []),
            (("ES", "37", None), [("ES", date(2024, 1, 1), 10.0)]),
            (("ES", "38.01", None), [("ES", date(2024, 1, 1), 10.0)]),
            (("ES", "39", None), [("ES", date(2024, 1, 1), 11.0)]),
            (("ES", "37.15", None), [("ES", date(2024, 1, 1), 10.0)]),  # subcase of CPA code
            (("ES", "38", None), []),  # not enough specific CPA code
        ]
        for params, expected in test_cases:
            with self.subTest(params=params):
                self._assert_equal_iterable(self.updater._filter_vat_rates(vat_rates, *params), expected)
