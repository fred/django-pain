#
# Copyright (C) 2020-2024  CZ.NIC, z. s. p. o.
#
# This file is part of FRED.
#
# FRED is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# FRED is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with FRED.  If not, see <https://www.gnu.org/licenses/>.

"""Tests of the REST API."""

from collections import OrderedDict
from datetime import date, datetime, timezone
from decimal import Decimal
from typing import Any, Dict
from unittest.mock import Mock, call, patch
from uuid import UUID

from django.test import TestCase, override_settings
from djmoney.money import Money
from freezegun import freeze_time
from pycsob import conf as CSOB
from testfixtures import LogCapture

from django_pain.card_payment_handlers import (
    AbstractCardPaymentHandler,
    CartItem,
    CustomerData,
    PaymentHandlerConnectionError,
    RedirectUrl,
)
from django_pain.card_payment_handlers.csob import CSOBCardPaymentHandler  # noqa: F401
from django_pain.constants import PaymentState, PaymentType
from django_pain.models import BankPayment, Customer, InvoiceNumberSequence, PaymentRequest, VariableSymbolSequence
from django_pain.models.invoices import BillingItemType, VatRate
from django_pain.serializers import ExternalPaymentState, InvoiceSerializer
from django_pain.settings import get_card_payment_handler_instance
from django_pain.tests.mixins import CacheResetMixin
from django_pain.tests.utils import (
    create_payment_request,
    get_account,
    get_customer,
    get_payment,
    get_payment_request_data,
)


@freeze_time("2024-01-25T12:00:00Z")
@patch("django_pain.serializers.uuid4")
def _create_invoice(uuid_mock, id: int = 3):
    # Create payment request
    payment_request = create_payment_request()
    # Create invoice
    data: Dict[str, Any] = {
        "app_id": "auctions",
        "customer": "CID-1",
        "due_at": datetime(2024, 1, 25, 12, 0, 0, tzinfo=timezone.utc),
        "tax_at": date(2024, 1, 20),
        "paid_at": date(2024, 1, 20),
        "price_to_pay": Decimal("0.00"),
        "currency": "CZK",
        "variable_symbol": "0902400012",
        "bank_account": "123456/0300",
        "payment_type": PaymentType.TRANSFER,
        "items": [
            {
                "name": "Item 1",
                "item_type": "basic_rate",
                "vat_rate": Decimal("10.0"),
                "quantity": 1,
                "price_per_unit": Decimal("11.00"),
                "price_total_base": Decimal("11.00"),
                "price_total_vat": Decimal("1.10"),
                "price_total": Decimal("12.10"),
            },
            {
                "name": "Item 2",
                "item_type": "basic_rate",
                "vat_rate": Decimal("20.0"),
                "quantity": 2,
                "price_per_unit": Decimal("15.00"),
                "price_total_base": Decimal("30.00"),
                "price_total_vat": Decimal("6.00"),
                "price_total": Decimal("36.00"),
            },
        ],
    }
    InvoiceNumberSequence.objects.create(app_id="auctions", year=2024, prefix=9024, current_number=0)
    uuid_mock.return_value = UUID(int=id)
    customer = Customer.objects.get(uid="CID-1")
    customer.vies_vat_valid = True
    customer.vies_checked_at = datetime(2024, 1, 1, tzinfo=timezone.utc)
    customer.save()
    serializer = InvoiceSerializer(data=data)
    serializer.is_valid()
    invoice = serializer.save()
    payment_request.invoice = invoice
    payment_request.save()


def _get_serialized_invoice(id: int = 3):
    expected = {
        "uuid": str(UUID(int=id)),
        "app_id": "auctions",
        "customer": "CID-1",
        "name": None,
        "organization": "Test, s.r.o.",
        "street": ["Na Okraji 1"],
        "city": "Pokuston",
        "state_or_province": None,
        "postal_code": "050 10",
        "country_code": "CZ",
        "company_registration_number": "001234",
        "vat_number": "CZ001234",
        "created_at": "2024-01-25T12:00:00Z",
        "tax_at": "2024-01-20",
        "number": "902400001",
        "variable_symbol": "0902400012",
        "price_total_base": "41.0000000000",
        "price_total_vat": "7.1000000000",
        "price_total": "48.1000000000",
        "price_to_pay": "0.0000000000",
        "currency": "CZK",
        "file_uid": None,
        "bank_account": "123456/0300",
        "payment_type": "transfer",
        "items": [
            {
                "name": "Item 1",
                "vat_rate": "10.0000",
                "quantity": 1,
                "price_per_unit": "11.0000000000",
                "price_total_base": "11.0000000000",
                "price_total_vat": "1.1000000000",
                "price_total": "12.1000000000",
            },
            {
                "name": "Item 2",
                "vat_rate": "20.0000",
                "quantity": 2,
                "price_per_unit": "15.0000000000",
                "price_total_base": "30.0000000000",
                "price_total_vat": "6.0000000000",
                "price_total": "36.0000000000",
            },
        ],
    }
    return expected


class BrokenCardPaymentHandler(AbstractCardPaymentHandler):
    """Card payment handler for tests of payment requests."""

    def init_payment(self, amount, variable_symbol, processor, return_url, return_method, cart, language, **kwargs):
        """Raise exception."""
        raise PaymentHandlerConnectionError("Gateway is down")

    def update_payments_state(self, payment):
        """Do nothing."""

    def get_payment_url(self, payment):
        """Do nothing."""


@override_settings(
    ROOT_URLCONF="django_pain.tests.urls",
    PAIN_CARD_PAYMENT_HANDLERS={
        "csob": {
            "HANDLER": "django_pain.card_payment_handlers.csob.CSOBCardPaymentHandler",
            "GATEWAY": "default",
        },
    },
)
class TestBankPaymentRestAPI(CacheResetMixin, TestCase):
    def test_retrieve_not_exists(self):
        response = self.client.get("/api/private/bankpayment/no-i-do-not-exist/")
        self.assertEqual(response.status_code, 404)

    def test_retrieve_exists(self):
        account = get_account(account_number="123456", currency="CZK")
        account.save()
        payment = get_payment(
            identifier="1",
            account=account,
            counter_account_number="",
            payment_type=PaymentType.CARD_PAYMENT,
            state=PaymentState.INITIALIZED,
            card_handler="csob",
        )
        payment.save()

        result_mock = Mock()
        result_mock.payload = {"paymentStatus": CSOB.PAYMENT_STATUS_CANCELLED, "resultCode": CSOB.RETURN_CODE_OK}

        card_payment_handler = get_card_payment_handler_instance(payment.card_handler)
        with patch.object(card_payment_handler, "_client") as gateway_client_mock:
            gateway_client_mock.payment_status.return_value = result_mock
            response = self.client.get("/api/private/bankpayment/{}/".format(payment.uuid))

        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.data["state"], ExternalPaymentState.CANCELED)

    def test_retrieve_gateway_connection_error(self):
        account = get_account(account_number="123456", currency="CZK")
        account.save()
        payment = get_payment(
            identifier="1",
            account=account,
            counter_account_number="",
            payment_type=PaymentType.CARD_PAYMENT,
            state=PaymentState.READY_TO_PROCESS,
            card_handler="csob",
        )
        payment.save()

        card_payment_handler = get_card_payment_handler_instance(payment.card_handler)
        with patch.object(card_payment_handler, "_client") as gateway_client_mock:
            gateway_client_mock.payment_status.side_effect = PaymentHandlerConnectionError()
            response = self.client.get("/api/private/bankpayment/{}/".format(payment.uuid))

        self.assertEqual(response.status_code, 503)

    @override_settings(
        PAIN_PROCESSORS={"dummy": "django_pain.tests.commands.test_process_payments.DummyProcessedPaymentProcessor"}
    )
    def test_retrieve_process_paid(self):
        account = get_account(account_number="123456", currency="CZK")
        account.save()
        payment = get_payment(
            identifier="1",
            account=account,
            counter_account_number="",
            payment_type=PaymentType.CARD_PAYMENT,
            processor="dummy",
            state=PaymentState.INITIALIZED,
            card_handler="csob",
        )
        payment.save()

        result_mock = Mock()
        result_mock.payload = {"paymentStatus": CSOB.PAYMENT_STATUS_CONFIRMED, "resultCode": CSOB.RETURN_CODE_OK}
        rest_log_handler = LogCapture("django_pain.views.rest", propagate=False)

        card_payment_handler = get_card_payment_handler_instance(payment.card_handler)
        with patch.object(card_payment_handler, "_client") as gateway_client_mock:
            gateway_client_mock.payment_status.return_value = result_mock
            response = self.client.get("/api/private/bankpayment/{}/".format(payment.uuid))

        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.data["state"], ExternalPaymentState.PAID)
        self.assertEqual(BankPayment.objects.first().state, PaymentState.PROCESSED)
        rest_log_handler.check_present(
            ("django_pain.views.rest", "INFO", "Processing card payment with processor dummy."),
        )
        rest_log_handler.uninstall()

    @override_settings(
        PAIN_PROCESSORS={"dummy": "django_pain.tests.commands.test_process_payments.DummyNotProcessedPaymentProcessor"}
    )
    def test_retrieve_process_paid_error(self):
        account = get_account(account_number="123456", currency="CZK")
        account.save()
        payment = get_payment(
            identifier="1",
            account=account,
            counter_account_number="",
            payment_type=PaymentType.CARD_PAYMENT,
            processor="dummy",
            state=PaymentState.INITIALIZED,
            card_handler="csob",
        )
        payment.save()

        result_mock = Mock()
        result_mock.payload = {"paymentStatus": CSOB.PAYMENT_STATUS_CONFIRMED, "resultCode": CSOB.RETURN_CODE_OK}
        model_log_handler = LogCapture("django_pain.models.bank", propagate=False)
        rest_log_handler = LogCapture("django_pain.views.rest", propagate=False)

        card_payment_handler = get_card_payment_handler_instance(payment.card_handler)
        with patch.object(card_payment_handler, "_client") as gateway_client_mock:
            gateway_client_mock.payment_status.return_value = result_mock
            response = self.client.get("/api/private/bankpayment/{}/".format(payment.uuid))

        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.data["state"], ExternalPaymentState.PAID)
        self.assertEqual(BankPayment.objects.first().state, PaymentState.DEFERRED)
        model_log_handler.check(
            ("django_pain.models.bank", "INFO", f"Saving payment {payment.uuid} as DEFERRED with error None."),
        )
        rest_log_handler.check_present(
            ("django_pain.views.rest", "INFO", "Processing card payment with processor dummy."),
        )
        model_log_handler.uninstall()
        rest_log_handler.uninstall()

    @override_settings(
        PAIN_PROCESSORS={"dummy": "django_pain.tests.commands.test_process_payments.DummyToRefundPaymentProcessor"}
    )
    def test_retrieve_process_to_refund(self):
        account = get_account(account_number="123456", currency="CZK")
        account.save()
        payment = get_payment(
            identifier="1",
            account=account,
            counter_account_number="",
            payment_type=PaymentType.CARD_PAYMENT,
            processor="dummy",
            state=PaymentState.INITIALIZED,
            card_handler="csob",
        )
        payment.save()

        result_mock = Mock()
        result_mock.payload = {"paymentStatus": CSOB.PAYMENT_STATUS_CONFIRMED, "resultCode": CSOB.RETURN_CODE_OK}
        model_log_handler = LogCapture("django_pain.models.bank", propagate=False)
        rest_log_handler = LogCapture("django_pain.views.rest", propagate=False)

        card_payment_handler = get_card_payment_handler_instance(payment.card_handler)
        with patch.object(card_payment_handler, "_client") as gateway_client_mock:
            gateway_client_mock.payment_status.return_value = result_mock
            response = self.client.get("/api/private/bankpayment/{}/".format(payment.uuid))

        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.data["state"], ExternalPaymentState.PAID)
        self.assertEqual(BankPayment.objects.first().state, PaymentState.TO_REFUND)
        model_log_handler.check(
            ("django_pain.models.bank", "INFO", f"Saving payment {payment.uuid} as TO_REFUND."),
        )
        rest_log_handler.check_present(
            ("django_pain.views.rest", "INFO", "Processing card payment with processor dummy."),
        )
        model_log_handler.uninstall()
        rest_log_handler.uninstall()

    @override_settings(
        PAIN_PROCESSORS={"broken": "django_pain.tests.commands.test_process_payments.DummyBrokenProcessor"}
    )
    def test_retrieve_processor_exception(self):
        account = get_account(account_number="123456", currency="CZK")
        account.save()
        payment = get_payment(
            identifier="1",
            account=account,
            counter_account_number="",
            payment_type=PaymentType.CARD_PAYMENT,
            processor="broken",
            state=PaymentState.INITIALIZED,
            card_handler="csob",
        )
        payment.save()

        result_mock = Mock()
        result_mock.payload = {"paymentStatus": CSOB.PAYMENT_STATUS_CONFIRMED, "resultCode": CSOB.RETURN_CODE_OK}
        rest_log_handler = LogCapture("django_pain.views.rest", propagate=False)

        card_payment_handler = get_card_payment_handler_instance(payment.card_handler)
        with patch.object(card_payment_handler, "_client") as gateway_client_mock:
            gateway_client_mock.payment_status.return_value = result_mock
            response = self.client.get("/api/private/bankpayment/{}/".format(payment.uuid))

        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.data["state"], ExternalPaymentState.PAID)
        self.assertEqual(BankPayment.objects.first().state, PaymentState.READY_TO_PROCESS)
        rest_log_handler.check_present(
            ("django_pain.views.rest", "INFO", "Processing card payment with processor broken."),
            (
                "django_pain.views.rest",
                "ERROR",
                "Error occured while processing payment with processor broken: It is broken!.",
            ),
        )
        rest_log_handler.uninstall()

    def _test_create(self, post_data, account_number="123456", account_currency="CZK"):
        account = get_account(account_number=account_number, currency=account_currency)
        account.save()

        with patch("django_pain.card_payment_handlers.csob.CsobClient") as gateway_client_mock:
            gateway_client_mock.return_value.gateway_return.return_value = OrderedDict(
                [
                    ("payId", "unique_id_123"),
                    ("resultCode", 0),
                    ("resultMessage", "OK"),
                    ("paymentStatus", CSOB.PAYMENT_STATUS_INIT),
                    ("dttime", datetime(2020, 6, 10, 16, 47, 30, tzinfo=timezone.utc)),
                ]
            )
            gateway_client_mock.return_value.get_payment_process_url.return_value = "https://example.com"

            response = self.client.post("/api/private/bankpayment/", data=post_data)

        self.assertEqual(response.status_code, 201)

    def test_create_no_currency(self):
        post_data = {
            "amount": "1000",
            "variable_symbol": "130",
            "processor": "donations",
            "card_handler": "csob",
            "return_url": "https://donations.nic.cz/return/",
            "return_method": "POST",
            "language": "cs",
            "cart": '[{"name":"Dar","amount":1000,"description":"Longer description","quantity":1}]',
        }
        with LogCapture(("django_pain.serializers",), propagate=False) as log_handler:
            self._test_create(post_data)
        log_handler.check(
            (
                "django_pain.serializers",
                "WARNING",
                'Parameter "amount_currency" not set. Using default currency. Processor for this payment: "donations"',
            )
        )

    def test_create_custom_currency(self):
        post_data = {
            "amount": "100",
            "amount_currency": "EUR",
            "variable_symbol": "130",
            "processor": "donations",
            "card_handler": "csob",
            "return_url": "https://donations.nic.cz/return/",
            "return_method": "POST",
            "language": "cs",
            "cart": '[{"name":"Dar","amount":100,"description":"Longer description","quantity":1}]',
        }
        self._test_create(post_data, account_number="234567", account_currency="EUR")

    def test_create_gw_connection_error(self):
        account = get_account(account_number="234567", currency="EUR")
        account.save()

        with patch("django_pain.card_payment_handlers.csob.CsobClient") as gateway_client_mock:
            gateway_client_mock.side_effect = PaymentHandlerConnectionError()
            response = self.client.post(
                "/api/private/bankpayment/",
                data={
                    "amount": "1000",
                    "amount_currency": "EUR",
                    "variable_symbol": "130",
                    "processor": "donations",
                    "card_handler": "csob",
                    "return_url": "https://donations.nic.cz/return/",
                    "return_method": "POST",
                    "language": "cs",
                    "cart": '[{"name":"Dar","amount":1000,"description":"Longer description","quantity":1}]',
                },
            )

        self.assertEqual(response.status_code, 503)

    @override_settings(
        PAIN_CARD_PAYMENT_HANDLERS={
            "csob": {"HANDLER": "django_pain.tests.test_rest.CSOBCardPaymentHandler", "GATEWAY": "default"},
        },
        PAIN_CSOB_CUSTOM_HANDLER={},
    )
    @freeze_time("2023-07-25T12:00:00Z")
    def test_pass_email_custom_expiry(self):
        # test that 'email' and 'custom_expiry' are properly serialized and passed to handler's 'init_payment'
        post_data = {
            "amount": "100",
            "amount_currency": "EUR",
            "variable_symbol": "130",
            "processor": "donations",
            "card_handler": "csob",
            "return_url": "https://donations.nic.cz/return/",
            "return_method": "POST",
            "language": "cs",
            "cart": '[{"name":"Dar","amount":100,"description":"Longer description","quantity":1}]',
            "custom_expiry": "2023-07-31T12:00:00Z",
            "email": "user@example.org",
            "email_messages": True,
        }

        with patch("django_pain.tests.test_rest.CSOBCardPaymentHandler") as handler_mock:
            handler_mock.return_value.init_payment.return_value = BankPayment(), RedirectUrl()

            response = self.client.post("/api/private/bankpayment/", data=post_data)

            self.assertEqual(response.status_code, 201)
            calls = [
                call(name="csob", gateway="default"),
                call().init_payment(
                    amount=Money(amount=100, currency="EUR"),
                    variable_symbol="130",
                    processor="donations",
                    return_url="https://donations.nic.cz/return/",
                    return_method="POST",
                    cart=[CartItem(name="Dar", quantity=1, amount=Decimal(100), description="Longer description")],
                    language="cs",
                    email="user@example.org",
                    custom_expiry=datetime(2023, 7, 31, 12, 0, tzinfo=timezone.utc),
                ),
            ]
            self.assertEqual(handler_mock.mock_calls, calls)

    @override_settings(
        PAIN_CARD_PAYMENT_HANDLERS={
            "csob": {"HANDLER": "django_pain.tests.test_rest.CSOBCardPaymentHandler", "GATEWAY": "default"},
        },
        PAIN_CSOB_CUSTOM_HANDLER={},
    )
    @freeze_time("2024-02-15T12:00:00Z")
    def test_custom_expiry_overdue(self):
        post_data = {
            "amount": "100",
            "amount_currency": "EUR",
            "variable_symbol": "130",
            "processor": "donations",
            "card_handler": "csob",
            "return_url": "https://donations.nic.cz/return/",
            "return_method": "POST",
            "language": "cs",
            "cart": '[{"name":"Dar","amount":100,"description":"Longer description","quantity":1}]',
            "custom_expiry": "2024-04-15T12:00:00",  # + 60 days from "now"
        }

        response = self.client.post("/api/private/bankpayment/", data=post_data)

        self.assertEqual(response.status_code, 400)
        self.assertEqual(response.json(), {"custom_expiry": ["Time point outside allowed interval"]})

    @override_settings(
        PAIN_CARD_PAYMENT_HANDLERS={
            "csob": {"HANDLER": "django_pain.tests.test_rest.CSOBCardPaymentHandler", "GATEWAY": "default"},
        },
        PAIN_CSOB_CUSTOM_HANDLER={},
    )
    @freeze_time("2023-07-25T12:00:00Z")
    def test_pass_customer_data(self):
        # test that customer data are properly serialized and passed to handler's 'init_payment'
        post_data = {
            "amount": "100",
            "amount_currency": "EUR",
            "variable_symbol": "130",
            "processor": "donations",
            "card_handler": "csob",
            "return_url": "https://donations.nic.cz/return/",
            "return_method": "POST",
            "language": "cs",
            "cart": '[{"name":"Dar","amount":100,"description":"Longer description","quantity":1}]',
            "customer": "User",
            "phone_number": "+420 123 456 789",
            "email": "user@example.org",
        }

        with patch("django_pain.tests.test_rest.CSOBCardPaymentHandler") as handler_mock:
            handler_mock.return_value.init_payment.return_value = BankPayment(), RedirectUrl()

            response = self.client.post("/api/private/bankpayment/", data=post_data)

            self.assertEqual(response.status_code, 201)
            calls = [
                call(name="csob", gateway="default"),
                call().init_payment(
                    amount=Money(amount=100, currency="EUR"),
                    variable_symbol="130",
                    processor="donations",
                    return_url="https://donations.nic.cz/return/",
                    return_method="POST",
                    cart=[CartItem(name="Dar", quantity=1, amount=Decimal(100), description="Longer description")],
                    language="cs",
                    customer=CustomerData("User", "+420 123 456 789", "user@example.org"),
                ),
            ]
            self.assertEqual(handler_mock.mock_calls, calls)


@override_settings(ROOT_URLCONF="django_pain.tests.urls")
class TestCustomerRestAPI(CacheResetMixin, TestCase):
    # --- retrieve

    def test_retrieve_not_exists(self):
        response = self.client.get("/api/private/customer/no-i-do-not-exist/")

        self.assertEqual(response.status_code, 404)

    def test_retrieve_ok(self):
        customer = get_customer(uid="uid-1", email="test@example.cz", phone_number="+420 123 456 789")
        customer.save()

        response = self.client.get("/api/private/customer/uid-1/")

        self.assertEqual(response.status_code, 200)
        expected = {
            "uid": "uid-1",
            "name": None,
            "organization": "Test, s.r.o.",
            "street": ["Na Okraji 1"],
            "city": "Pokuston",
            "postal_code": "050 10",
            "state_or_province": None,
            "country_code": "CZ",
            "email": "test@example.cz",
            "phone_number": "+420 123 456 789",
            "company_registration_number": "001234",
            "vat_number": "CZ001234",
        }
        self.assertEqual(response.json(), expected)

    # --- create

    def test_create_ok(self):
        post_data: Dict[str, Any] = {
            "uid": "uid-1",
            "organization": "Test, s.r.o.",
            "street": ["Na Okraji 1"],
            "city": "Horní Dolní",
            "postal_code": "050 10",
            "country_code": "CZ",
            "email": "test@example.cz",
            "phone_number": "+420 123 456 789",
            "company_registration_number": "001234",
            "vat_number": "CZ001234",
        }

        response = self.client.post("/api/private/customer/", data=post_data, content_type="application/json")

        # Test DB record
        customer = Customer.objects.get(uid="uid-1")
        post_data.pop("street")  # Correct data to fit data model
        post_data.update({"street1": "Na Okraji 1", "street2": None, "street3": None})
        for item in post_data:
            self.assertEqual(getattr(customer, item), post_data[item])
        # Test response
        self.assertEqual(response.status_code, 201)
        expected = {
            "uid": "uid-1",
            "name": None,
            "organization": "Test, s.r.o.",
            "street": ["Na Okraji 1"],
            "city": "Horní Dolní",
            "state_or_province": None,
            "postal_code": "050 10",
            "country_code": "CZ",
            "email": "test@example.cz",
            "phone_number": "+420 123 456 789",
            "company_registration_number": "001234",
            "vat_number": "CZ001234",
        }
        self.assertEqual(response.json(), expected)

    def test_create_duplicate(self):
        post_data: Dict[str, Any] = {
            "uid": "uid-1",
            "organization": "Test, s.r.o.",
            "street": ["Na Okraji 1"],
            "city": "Horní Dolní",
            "postal_code": "050 10",
            "country_code": "CZ",
            "company_registration_number": "001234",
            "vat_number": "CZ001234",
        }
        customer = get_customer(uid="uid-1")
        customer.save()

        response = self.client.post("/api/private/customer/", data=post_data, content_type="application/json")

        self.assertEqual(response.status_code, 400)
        self.assertIn("Customer with this Customer identifier already exists.", response.json()["uid"])

    def test_create_invalid_data(self):
        post_data: Dict[str, Any] = {
            "uid": "uid-1",
            "organization": "Test, s.r.o.",
            "street": ["Na Okraji 1"],
            "city": "Horní Dolní",
            "postal_code": "050 10",
            "country_code": "XX",  # invalid country
        }

        response = self.client.post("/api/private/customer/", data=post_data, content_type="application/json")

        self.assertEqual(response.status_code, 400)
        self.assertIn('"XX" is not a valid choice.', response.json()["country_code"])

    # --- update

    def test_update_ok(self):
        customer = get_customer(uid="uid-1", organization="Firma, a.s.", street2="2. patro")
        customer.save()
        put_data: Dict[str, Any] = {
            "name": "Josef Starý",
            "street": ["Kopec 21"],
            "city": "Nádherná Ves",
            "state_or_province": "Krakonošovo",
            "postal_code": "700 00",
            "country_code": "SK",
        }

        response = self.client.put("/api/private/customer/uid-1/", data=put_data, content_type="application/json")

        # Test DB record
        customer = Customer.objects.get()
        put_data.pop("street")  # Correct data to fit data model
        put_data.update({"street1": "Kopec 21", "street2": None, "street3": None})
        for item in put_data:
            self.assertEqual(getattr(customer, item), put_data[item])  # updated items
        self.assertEqual(customer.organization, "Firma, a.s.")  # original item
        # Test response
        self.assertEqual(response.status_code, 200)
        expected = {
            "uid": "uid-1",
            "name": "Josef Starý",
            "organization": "Firma, a.s.",  # original item
            "street": ["Kopec 21"],
            "city": "Nádherná Ves",
            "state_or_province": "Krakonošovo",
            "postal_code": "700 00",
            "country_code": "SK",
            "email": None,
            "phone_number": None,
            "company_registration_number": "001234",  # original item
            "vat_number": "CZ001234",  # original item
        }
        self.assertEqual(response.json(), expected)

    def test_partial_update_ok(self):
        customer = get_customer(uid="uid-1", name=None)
        customer.save()
        patch_data: Dict[str, Any] = {"country_code": "GB"}

        response = self.client.patch("/api/private/customer/uid-1/", data=patch_data, content_type="application/json")

        customer = Customer.objects.get(uid="uid-1")
        self.assertEqual(customer.country_code, "GB")
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.json()["country_code"], "GB")

    def test_partial_update_constraint_failure(self):
        customer = get_customer(uid="uid-1", name=None)
        customer.save()
        # Try to clear `organization` when `name` is already empty
        patch_data: Dict[str, Any] = {"organization": None}

        response = self.client.patch("/api/private/customer/uid-1/", data=patch_data, content_type="application/json")

        self.assertEqual(response.status_code, 400)
        self.assertIn("Neither `name` nor `organization` is set", response.json()["non_field_errors"])

    def test_update_incomplete_data(self):
        customer = get_customer(uid="uid-1", organization="Firma, a.s.")
        customer.save()
        put_data: Dict[str, Any] = {
            "name": "Jan Novák",
            "street": ["Kopec 21"],
            "city": "Nádherná Ves",
            "postal_code": "700 00",
            # country_code required
        }

        response = self.client.put("/api/private/customer/uid-1/", data=put_data, content_type="application/json")

        self.assertEqual(response.status_code, 400)
        self.assertIn("This field is required.", response.json()["country_code"])

    def test_update_created_when_not_found(self):
        put_data: Dict[str, Any] = {
            "name": "Jan Novák",
            "street": ["Kopec 21"],
            "city": "Nádherná Ves",
            "postal_code": "700 00",
            "country_code": "CZ",
        }

        response = self.client.put("/api/private/customer/uid-1/", data=put_data, content_type="application/json")

        # Test DB record
        customer = Customer.objects.get(uid="uid-1")
        put_data.pop("street")  # Correct data to fit data model
        put_data.update({"street1": "Kopec 21", "street2": None, "street3": None})
        for item in put_data:
            self.assertEqual(getattr(customer, item), put_data[item])
        # Test response
        self.assertEqual(response.status_code, 201)
        expected = {
            "uid": "uid-1",
            "name": "Jan Novák",
            "organization": None,
            "street": ["Kopec 21"],
            "city": "Nádherná Ves",
            "state_or_province": None,
            "postal_code": "700 00",
            "country_code": "CZ",
            "email": None,
            "phone_number": None,
            "company_registration_number": None,
            "vat_number": None,
        }
        self.assertEqual(response.json(), expected)

    def test_partial_update_not_found(self):
        patch_data: Dict[str, Any] = {"name": "Jan Novák"}

        response = self.client.patch("/api/private/customer/uid-1/", data=patch_data, content_type="application/json")

        self.assertEqual(response.status_code, 404)

    def test_update_change_uid(self):
        customer = get_customer(uid="uid-1", organization="Firma, a.s.", street2="2. patro")
        customer.save()
        put_data: Dict[str, Any] = {
            "uid": "uid-2",  # attempt to change `uid`
            "name": "Josef Starý",
            "street": ["Kopec 21"],
            "city": "Nádherná Ves",
            "state_or_province": "Krakonošovo",
            "postal_code": "700 00",
            "country_code": "SK",
        }

        response = self.client.put("/api/private/customer/uid-1/", data=put_data, content_type="application/json")

        # Test DB record
        customer = Customer.objects.get()
        self.assertEqual(customer.uid, "uid-1")  # `uid` in request is ignored
        # Test response
        self.assertEqual(response.status_code, 200)


@override_settings(ROOT_URLCONF="django_pain.tests.urls")
class TestPaymentRequestRestAPI(CacheResetMixin, TestCase):
    def setUp(self):
        super().setUp()
        BillingItemType.objects.create(handle="basic_rate")
        VatRate.objects.create(
            country_code="CZ",
            cpa_code=None,
            valid_from=datetime(2023, 1, 1, tzinfo=timezone.utc),
            valid_to=None,
            value=Decimal("15.0"),
        )

    # --- retrieve

    def test_retrieve_not_exists(self):
        response = self.client.get("/api/private/paymentrequest/no-i-do-not-exist/")

        self.assertEqual(response.status_code, 404)

    def test_retrieve_ok(self):
        # Prepare data and save all objects to database
        create_payment_request()

        response = self.client.get("/api/private/paymentrequest/00000000-0000-0000-0000-000000000001/")

        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.json(), get_payment_request_data())

    # --- close

    def test_close_not_exists(self):
        response = self.client.post("/api/private/paymentrequest/no-i-do-not-exist/close/")

        self.assertEqual(response.status_code, 404)

    def test_close_ok(self):
        # Prepare data and save all objects to database
        pk = create_payment_request().pk

        with freeze_time("2023-09-08T10:45:00Z"):
            response = self.client.post("/api/private/paymentrequest/00000000-0000-0000-0000-000000000001/close/")

        # Check response
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.json(), get_payment_request_data(closed_at="2023-09-08T10:45:00Z"))
        # Check database
        payment_request = PaymentRequest.objects.get(pk=pk)
        self.assertEqual(payment_request.closed_at, datetime(2023, 9, 8, 10, 45, 0, tzinfo=timezone.utc))

    def test_close_already_closed(self):
        # Prepare data and save all objects to database
        create_payment_request(closed_at="2023-09-08T10:45:00Z")

        with freeze_time("2023-09-08T15:20:00Z"):
            response = self.client.post("/api/private/paymentrequest/00000000-0000-0000-0000-000000000001/close/")

        # Check response (`closed_at` at original value)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.json(), get_payment_request_data(closed_at="2023-09-08T10:45:00Z"))

    # --- invoice

    def test_get_invoice_no_payment_request(self):
        response = self.client.get("/api/private/paymentrequest/no-i-do-not-exist/invoice/")

        self.assertEqual(response.status_code, 404)

    def test_get_invoice_ok(self):
        # Create payment request and related invoice
        _create_invoice(id=3)

        response = self.client.get("/api/private/paymentrequest/00000000-0000-0000-0000-000000000001/invoice/")

        # Check response
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.json(), _get_serialized_invoice(id=3))

    def test_get_invoice_no_invoice(self):
        create_payment_request()
        response = self.client.get("/api/private/paymentrequest/00000000-0000-0000-0000-000000000001/invoice/")

        self.assertEqual(response.status_code, 404)

    # --- create

    @freeze_time("2023-09-01T12:00:00Z")
    def test_create_ok(self):
        create_payment_request(
            uuid="00000000-0000-0000-0000-000000000002",
            closed_at="2023-09-20T12:00:00Z",  # stored payment request is closed
        )

        post_data: Dict[str, Any] = {
            "uuid": "00000000-0000-0000-0000-000000000001",
            "app_id": "auctions",
            "customer": "CID-1",
            "due_at_displayed": "2023-10-01T12:00:00Z",
            "currency": "CZK",
            "variable_symbol": "0000009876",
            "bank_account": "123456/0300",
            "items": [
                {
                    "name": "Item 1",
                    "item_type": "basic_rate",
                    "quantity": 1,
                    "price_per_unit": "11.0000000000",
                    "price_with_vat": True,
                },
                {
                    "name": "Item 2",
                    "item_type": "basic_rate",
                    "quantity": 2,
                    "price_per_unit": "12.0000000000",
                    "price_with_vat": True,
                },
                {
                    "name": "Item 3",
                    "item_type": "basic_rate",
                    "quantity": 3,
                    "price_per_unit": "13.0000000000",
                    "price_with_vat": True,
                },
            ],
        }

        response = self.client.post("/api/private/paymentrequest/", data=post_data, content_type="application/json")

        self.assertEqual(response.status_code, 201)
        self.assertEqual(response.json(), get_payment_request_data(realized_payment=None))

    @freeze_time("2023-09-01T12:00:00Z")
    def test_create_variable_symbol_sequence_error(self):
        customer = get_customer()
        customer.save()
        account = get_account()
        account.save()

        post_data: Dict[str, Any] = {
            "uuid": "00000000-0000-0000-0000-000000000001",
            "app_id": "auctions",
            "customer": "CID-1",
            "due_at_displayed": "2023-10-01T12:00:00Z",
            "currency": "CZK",
            "bank_account": "123456/0300",
            "items": [
                {
                    "name": "Item 1",
                    "item_type": "basic_rate",
                    "quantity": 1,
                    "price_per_unit": "11.0000000000",
                    "price_with_vat": True,
                },
                {
                    "name": "Item 2",
                    "item_type": "basic_rate",
                    "quantity": 2,
                    "price_per_unit": "12.0000000000",
                    "price_with_vat": True,
                },
                {
                    "name": "Item 3",
                    "item_type": "basic_rate",
                    "quantity": 3,
                    "price_per_unit": "13.0000000000",
                    "price_with_vat": True,
                },
            ],
        }

        response = self.client.post("/api/private/paymentrequest/", data=post_data, content_type="application/json")

        self.assertEqual(response.status_code, 400)
        self.assertEqual(response.json(), {"detail": "No variable symbol sequence available for `auctions` and `2023`"})

    @freeze_time("2023-09-01T12:00:00Z")
    def test_create_no_matching_closed_vs(self):
        customer = get_customer()
        customer.save()
        account = get_account()
        account.save()

        post_data: Dict[str, Any] = {
            "uuid": "00000000-0000-0000-0000-000000000001",
            "app_id": "auctions",
            "customer": "CID-1",
            "due_at_displayed": "2023-10-01T12:00:00Z",
            "currency": "CZK",
            "variable_symbol": "0000009876",
            "bank_account": "123456/0300",
            "items": [
                {
                    "name": "Item 1",
                    "item_type": "basic_rate",
                    "quantity": 1,
                    "price_per_unit": "11.0000000000",
                    "price_with_vat": True,
                },
                {
                    "name": "Item 2",
                    "item_type": "basic_rate",
                    "quantity": 2,
                    "price_per_unit": "12.0000000000",
                    "price_with_vat": True,
                },
                {
                    "name": "Item 3",
                    "item_type": "basic_rate",
                    "quantity": 3,
                    "price_per_unit": "13.0000000000",
                    "price_with_vat": True,
                },
            ],
        }

        response = self.client.post("/api/private/paymentrequest/", data=post_data, content_type="application/json")

        self.assertEqual(response.status_code, 400)
        message = "Closed payment request with variable symbol `0000009876` not found"
        self.assertIn(message, response.json()["variable_symbol"])

    def test_create_duplicate_uuid(self):
        create_payment_request()

        post_data: Dict[str, Any] = {
            "uuid": "00000000-0000-0000-0000-000000000001",
            "app_id": "auctions",
            "customer": "CID-1",
            "due_at_displayed": "2023-10-01T12:00:00Z",
            "currency": "CZK",
            "variable_symbol": "0000009876",
            "bank_account": "123456/0300",
            "items": [
                {
                    "name": "Item 1",
                    "item_type": "basic_rate",
                    "quantity": 1,
                    "price_per_unit": "11.0000000000",
                    "price_with_vat": True,
                },
                {
                    "name": "Item 2",
                    "item_type": "basic_rate",
                    "quantity": 2,
                    "price_per_unit": "12.0000000000",
                    "price_with_vat": True,
                },
                {
                    "name": "Item 3",
                    "item_type": "basic_rate",
                    "quantity": 3,
                    "price_per_unit": "13.0000000000",
                    "price_with_vat": True,
                },
            ],
        }

        response = self.client.post("/api/private/paymentrequest/", data=post_data, content_type="application/json")

        self.assertEqual(response.status_code, 400)
        self.assertEqual(response.json()["uuid"], ["Payment request with this uuid already exists."])

    def test_create_duplicate_active_vs(self):
        create_payment_request()

        post_data: Dict[str, Any] = {
            "uuid": "00000000-0000-0000-0000-000000000002",  # different UUID
            "app_id": "auctions",
            "customer": "CID-1",
            "due_at_displayed": "2023-10-01T12:00:00Z",
            "currency": "CZK",
            "variable_symbol": "0000009876",
            "bank_account": "123456/0300",
            "items": [
                {
                    "name": "Item 1",
                    "item_type": "basic_rate",
                    "quantity": 1,
                    "price_per_unit": "11.0000000000",
                    "price_with_vat": True,
                },
                {
                    "name": "Item 2",
                    "item_type": "basic_rate",
                    "quantity": 2,
                    "price_per_unit": "12.0000000000",
                    "price_with_vat": True,
                },
                {
                    "name": "Item 3",
                    "item_type": "basic_rate",
                    "quantity": 3,
                    "price_per_unit": "13.0000000000",
                    "price_with_vat": True,
                },
            ],
        }

        response = self.client.post("/api/private/paymentrequest/", data=post_data, content_type="application/json")

        self.assertEqual(response.status_code, 400)
        message = "Duplicate variable symbol found in active payment request `00000000-0000-0000-0000-000000000001`"
        self.assertIn(message, response.json()["variable_symbol"])

    @freeze_time("2023-09-01T12:00:00Z")
    @override_settings(
        PAIN_PROCESSORS={"dummy": "django_pain.tests.utils.DummyPaymentProcessor"},
        PAIN_CARD_PAYMENT_HANDLERS={
            "test": {"HANDLER": "django_pain.tests.test_serializers.TestCardPaymentHandler", "GATEWAY": "default"},
        },
        PAIN_PAYMENT_REQUESTS={"auctions": {"CARD_HANDLER": "test", "PROCESSOR": "dummy"}},
        PAIN_CSOB_CUSTOM_HANDLER={},
    )
    def test_create_card_payment_ok(self):
        create_payment_request(
            uuid="00000000-0000-0000-0000-000000000002",
            closed_at="2023-09-20T12:00:00Z",  # stored payment request is closed
        )

        post_data: Dict[str, Any] = {
            "uuid": "00000000-0000-0000-0000-000000000001",
            "app_id": "auctions",
            "customer": "CID-1",
            "due_at_displayed": "2023-10-01T12:00:00Z",
            "currency": "CZK",
            "variable_symbol": "0000009876",
            "bank_account": "123456/0300",
            "items": [
                {
                    "name": "Item 1",
                    "item_type": "basic_rate",
                    "quantity": 1,
                    "price_per_unit": "11.0000000000",
                    "price_with_vat": True,
                },
                {
                    "name": "Item 2",
                    "item_type": "basic_rate",
                    "quantity": 2,
                    "price_per_unit": "12.0000000000",
                    "price_with_vat": True,
                },
                {
                    "name": "Item 3",
                    "item_type": "basic_rate",
                    "quantity": 3,
                    "price_per_unit": "13.0000000000",
                    "price_with_vat": True,
                },
            ],
            "create_card_payment": {"redirect_url": "https://callback.example.org/"},
        }

        response = self.client.post("/api/private/paymentrequest/", data=post_data, content_type="application/json")

        self.assertEqual(response.status_code, 201)
        expected = get_payment_request_data(
            card_gateway_url="https://gateway.example.org/0000009876",
            card_payment=str(UUID(int=9876)),
            realized_payment=None,
        )
        self.assertEqual(response.json(), expected)

    @freeze_time("2023-09-01T12:00:00Z")
    @override_settings(
        PAIN_PROCESSORS={"dummy": "django_pain.tests.utils.DummyPaymentProcessor"},
        PAIN_CARD_PAYMENT_HANDLERS={
            "test": {"HANDLER": "django_pain.tests.test_serializers.TestCardPaymentHandler", "GATEWAY": "default"},
        },
        PAIN_PAYMENT_REQUESTS={"auctions": {"CARD_HANDLER": "test", "PROCESSOR": "dummy"}},
        PAIN_CSOB_CUSTOM_HANDLER={},
    )
    def test_create_card_payment_gateway_error(self):
        create_payment_request(
            uuid="00000000-0000-0000-0000-000000000002",
            closed_at="2023-09-20T12:00:00Z",  # stored payment request is closed
        )

        post_data: Dict[str, Any] = {
            "uuid": "00000000-0000-0000-0000-000000000001",
            "app_id": "auctions",
            "customer": "CID-1",
            "due_at_displayed": "2023-10-01T12:00:00Z",
            "currency": "CZK",
            "variable_symbol": "0000009876",
            "bank_account": "123456/0300",
            "items": [
                {
                    "name": "Item 1",
                    "item_type": "basic_rate",
                    "quantity": 1,
                    "price_per_unit": "11.0000000000",
                    "price_with_vat": True,
                },
                {
                    "name": "Item 2",
                    "item_type": "basic_rate",
                    "quantity": 2,
                    "price_per_unit": "12.0000000000",
                    "price_with_vat": True,
                },
                {
                    "name": "Item 3",
                    "item_type": "basic_rate",
                    "quantity": 3,
                    "price_per_unit": "13.0000000000",
                    "price_with_vat": True,
                },
            ],
            "create_card_payment": {
                "expiry": "2023-10-01T12:00:00Z",
                "redirect_url": "https://callback.example.org/",
                "redirect_method": "GET",
            },
        }

        with patch("django_pain.tests.test_serializers.TestCardPaymentHandler.init_payment") as init_payment_mock:
            init_payment_mock.side_effect = PaymentHandlerConnectionError()

            response = self.client.post("/api/private/paymentrequest/", data=post_data, content_type="application/json")

            self.assertEqual(response.status_code, 503)
            expected = {
                "amount": Money("74.00", "CZK"),
                "variable_symbol": "0000009876",
                "processor": "dummy",
                "return_url": "https://callback.example.org/",
                "return_method": "GET",
                "cart": [
                    CartItem(name="Item 1", quantity=1, amount=Decimal(11.0), description=""),
                    CartItem(name="+2 ...", quantity=1, amount=Decimal(63.0), description=""),
                ],
                "language": "cs",
                "custom_expiry": datetime(2023, 10, 1, 12, 0, 0, tzinfo=timezone.utc),
                "customer": CustomerData("Test, s.r.o.", None, None),
            }
            init_payment_mock.assert_called_once_with(**expected)

    @freeze_time("2023-09-01T12:00:00Z")
    @override_settings(
        PAIN_PROCESSORS={"dummy": "django_pain.tests.utils.DummyPaymentProcessor"},
        PAIN_CARD_PAYMENT_HANDLERS={
            "test": {"HANDLER": "django_pain.tests.test_serializers.TestCardPaymentHandler", "GATEWAY": "default"},
        },
        PAIN_PAYMENT_REQUESTS={"auctions": {"CARD_HANDLER": "test", "PROCESSOR": "dummy"}},
        PAIN_CSOB_CUSTOM_HANDLER={"MAX_VALIDITY": 30 * 86400},
    )
    def test_create_card_payment_bad_custom_expiry(self):
        create_payment_request(
            uuid="00000000-0000-0000-0000-000000000002",
            closed_at="2023-09-20T12:00:00Z",  # stored payment request is closed
        )

        post_data: Dict[str, Any] = {
            "uuid": "00000000-0000-0000-0000-000000000001",
            "app_id": "auctions",
            "customer": "CID-1",
            "due_at_displayed": "2023-10-01T12:00:00Z",
            "currency": "CZK",
            "variable_symbol": "0000009876",
            "bank_account": "123456/0300",
            "items": [
                {
                    "name": "Item 1",
                    "item_type": "basic_rate",
                    "quantity": 1,
                    "price_per_unit": "11.0000000000",
                    "price_with_vat": True,
                },
            ],
            "create_card_payment": {
                "expiry": "2023-10-01T12:00:01Z",  # too late
                "redirect_url": "https://callback.example.org/",
                "redirect_method": "GET",
            },
        }

        response = self.client.post("/api/private/paymentrequest/", data=post_data, content_type="application/json")

        self.assertEqual(response.status_code, 400)
        self.assertEqual(response.json(), {"create_card_payment": {"expiry": ["Time point outside allowed interval"]}})

    @freeze_time("2023-09-01T12:00:00Z")
    @override_settings(
        PAIN_PROCESSORS={"dummy": "django_pain.tests.utils.DummyPaymentProcessor"},
        PAIN_CARD_PAYMENT_HANDLERS={
            "test": {"HANDLER": "django_pain.tests.test_serializers.TestCardPaymentHandler", "GATEWAY": "default"},
        },
        PAIN_PAYMENT_REQUESTS={"auctions": {"CARD_HANDLER": "test", "PROCESSOR": "dummy"}},
        PAIN_CSOB_CUSTOM_HANDLER={},
    )
    def test_create_card_payment_transaction_rollback(self):
        # Initialize database
        account = get_account()
        account.save()
        customer = get_customer()
        customer.save()
        VariableSymbolSequence.objects.create(app_id="auctions", year=2023, prefix=6023, current_number=100)

        post_data: Dict[str, Any] = {
            "uuid": "00000000-0000-0000-0000-000000000001",
            "app_id": "auctions",
            "customer": customer.uid,
            "due_at_displayed": "2023-10-01T12:00:00Z",
            "currency": "CZK",
            # No variable symbol provided => new one will be generated from sequence
            "bank_account": account.account_number,
            "items": [
                {
                    "name": "Item 1",
                    "item_type": "basic_rate",
                    "quantity": 1,
                    "price_per_unit": "11.0",
                    "price_with_vat": True,
                },
            ],
            "create_card_payment": {
                "expiry": "2023-10-01T12:00:00Z",
                "redirect_url": "https://callback.example.org/",
                "redirect_method": "GET",
            },
        }

        with patch("django_pain.tests.test_serializers.TestCardPaymentHandler.init_payment") as init_payment_mock:
            init_payment_mock.side_effect = PaymentHandlerConnectionError()

            response = self.client.post("/api/private/paymentrequest/", data=post_data, content_type="application/json")

            self.assertEqual(response.status_code, 503)
            # Test that database transaction is rolled back
            #   1. We start with VS sequence: prefix = 6023, current_value = 100 (see initial data)
            #   2. Call to `init_payment` is made with current_value 101, i.e. value is incremented and stored in DB at
            #      the time of method call
            self.assertEqual(init_payment_mock.call_args[1]["variable_symbol"], "6023000101")
            #   3. Check database state after API call - current_value is 100, i.e. rolled back successfully
            self.assertEqual(VariableSymbolSequence.objects.get().variable_symbol, "6023000100")

    @freeze_time("2023-09-01T12:00:00Z")
    def test_create_due_in_past(self):
        create_payment_request(
            uuid="00000000-0000-0000-0000-000000000002",
            closed_at="2023-09-20T12:00:00Z",  # stored payment request is closed
        )

        post_data: Dict[str, Any] = {
            "uuid": "00000000-0000-0000-0000-000000000001",
            "app_id": "auctions",
            "customer": "CID-1",
            "due_at_displayed": "2023-09-01T11:59:59Z",
            "currency": "CZK",
            "variable_symbol": "0000009876",
            "bank_account": "123456/0300",
            "items": [
                {
                    "name": "Item 1",
                    "item_type": "basic_rate",
                    "quantity": 1,
                    "price_per_unit": "11.0000000000",
                    "price_with_vat": True,
                },
                {
                    "name": "Item 2",
                    "item_type": "basic_rate",
                    "quantity": 2,
                    "price_per_unit": "12.0000000000",
                    "price_with_vat": True,
                },
                {
                    "name": "Item 3",
                    "item_type": "basic_rate",
                    "quantity": 3,
                    "price_per_unit": "13.0000000000",
                    "price_with_vat": True,
                },
            ],
        }

        response = self.client.post("/api/private/paymentrequest/", data=post_data, content_type="application/json")

        self.assertEqual(response.status_code, 400)
        self.assertEqual(response.json(), {"due_at_displayed": ["Time point must not be in the past"]})

    @freeze_time("2023-09-01T12:00:00Z")
    @override_settings(PAIN_SECRETARY_URL="http://localhost/api/", PAIN_FILEMAN_NETLOC="localhost:50053")
    def test_create_ok_with_pdf_rendered(self):
        create_payment_request(
            uuid="00000000-0000-0000-0000-000000000002",
            closed_at="2023-09-20T12:00:00Z",  # stored payment request is closed
        )

        post_data: Dict[str, Any] = {
            "uuid": "00000000-0000-0000-0000-000000000001",
            "app_id": "auctions",
            "customer": "CID-1",
            "due_at_displayed": "2023-10-01T12:00:00Z",
            "currency": "CZK",
            "variable_symbol": "0000009876",
            "bank_account": "123456/0300",
            "items": [
                {
                    "name": "Item 1",
                    "item_type": "basic_rate",
                    "quantity": 1,
                    "price_per_unit": "11.0000000000",
                    "price_with_vat": True,
                },
            ],
        }

        def fake_render_pdf(payment_request, secretary_client, file_storage):
            # Simulate storing a rendered PDF
            payment_request.file_uid = "pdf-1"
            payment_request.save(update_fields=["file_uid"])

        with patch.object(PaymentRequest, "render_pdf", new=fake_render_pdf):
            response = self.client.post("/api/private/paymentrequest/", data=post_data, content_type="application/json")

            self.assertEqual(response.status_code, 201)
            self.assertEqual(response.json()["file_uid"], "pdf-1")  # PDF rendered and stored

    @freeze_time("2023-09-01T12:00:00Z")
    @override_settings(PAIN_SECRETARY_URL="http://localhost/api/", PAIN_FILEMAN_NETLOC="localhost:50053")
    def test_create_ok_with_pdf_rendering_error(self):
        create_payment_request(
            uuid="00000000-0000-0000-0000-000000000002",
            closed_at="2023-09-20T12:00:00Z",  # stored payment request is closed
        )

        post_data: Dict[str, Any] = {
            "uuid": "00000000-0000-0000-0000-000000000001",
            "app_id": "auctions",
            "customer": "CID-1",
            "due_at_displayed": "2023-10-01T12:00:00Z",
            "currency": "CZK",
            "variable_symbol": "0000009876",
            "bank_account": "123456/0300",
            "items": [
                {
                    "name": "Item 1",
                    "item_type": "basic_rate",
                    "quantity": 1,
                    "price_per_unit": "11.0000000000",
                    "price_with_vat": True,
                },
            ],
        }

        with patch.object(PaymentRequest, "render_pdf") as render_mock:
            render_mock.side_effect = RuntimeError

            response = self.client.post("/api/private/paymentrequest/", data=post_data, content_type="application/json")

            self.assertEqual(response.status_code, 201)
            self.assertIsNone(response.json()["file_uid"])  # PDF not rendered/stored
            render_mock.assert_called_once()

    # --- add card payment

    def test_add_card_payment_not_exists(self):
        response = self.client.post("/api/private/paymentrequest/no-i-do-not-exist/cardpayment/")

        self.assertEqual(response.status_code, 404)

    def test_add_card_payment_closed(self):
        create_payment_request(closed_at=datetime(2023, 9, 5, tzinfo=timezone.utc))  # closed payment request
        post_data = {"redirect_url": "http://example.org"}

        response = self.client.post(
            "/api/private/paymentrequest/00000000-0000-0000-0000-000000000001/cardpayment/",
            data=post_data,
            content_type="application/json",
        )

        self.assertEqual(response.status_code, 400)
        self.assertEqual(response.json(), {"detail": "Payment request is closed"})

    def test_add_card_payment_bad_params(self):
        create_payment_request(realized_payment=None)
        post_data = {"redirect_method": "PUT"}

        response = self.client.post(
            "/api/private/paymentrequest/00000000-0000-0000-0000-000000000001/cardpayment/",
            data=post_data,
            content_type="application/json",
        )

        self.assertEqual(response.status_code, 400)
        expected = {"redirect_url": ["This field is required."], "redirect_method": ['"PUT" is not a valid choice.']}
        self.assertEqual(response.json(), expected)

    @freeze_time("2023-09-15T12:00:00Z")
    @override_settings(
        PAIN_PROCESSORS={"dummy": "django_pain.tests.utils.DummyPaymentProcessor"},
        PAIN_CARD_PAYMENT_HANDLERS={
            "test": {"HANDLER": "django_pain.tests.test_serializers.TestCardPaymentHandler", "GATEWAY": "default"},
        },
        PAIN_PAYMENT_REQUESTS={"auctions": {"CARD_HANDLER": "test", "PROCESSOR": "dummy"}},
        PAIN_CSOB_CUSTOM_HANDLER={},
    )
    def test_add_card_payment_ok(self):
        create_payment_request(realized_payment=None)  # without card payment
        post_data = {"redirect_url": "http://example.org", "expiry": "2023-10-01T12:00:00Z"}

        response = self.client.post(
            "/api/private/paymentrequest/00000000-0000-0000-0000-000000000001/cardpayment/",
            data=post_data,
            content_type="application/json",
        )

        self.assertEqual(response.status_code, 201)
        expected = get_payment_request_data(
            realized_payment=None,
            card_payment="00000000-0000-0000-0000-000000002694",
            card_gateway_url="https://gateway.example.org/0000009876",
        )
        self.assertEqual(response.json(), expected)
        card_payment = BankPayment.objects.get()
        self.assertEqual(card_payment.custom_expiry, datetime(2023, 10, 1, 12, 0, 0, tzinfo=timezone.utc))

    @override_settings(
        PAIN_PROCESSORS={"dummy": "django_pain.tests.utils.DummyPaymentProcessor"},
        PAIN_CARD_PAYMENT_HANDLERS={
            "test": {"HANDLER": "django_pain.tests.test_serializers.TestCardPaymentHandler", "GATEWAY": "default"},
        },
        PAIN_PAYMENT_REQUESTS={"auctions": {"CARD_HANDLER": "test", "PROCESSOR": "dummy"}},
        PAIN_CSOB_CUSTOM_HANDLER={},
    )
    def test_add_card_payment_replace(self):
        payment_request = create_payment_request(realized_payment=None)
        payment = get_payment(
            identifier="pid-1",
            account=payment_request.bank_account,
            counter_account_number="",
            payment_type=PaymentType.CARD_PAYMENT,
            state=PaymentState.CANCELED,
            card_handler="test",
        )
        payment.save()
        payment_request.card_payment = payment
        payment_request.save(update_fields=["card_payment"])  # with inactive card payment
        post_data = {"redirect_url": "http://example.org"}

        response = self.client.post(
            "/api/private/paymentrequest/00000000-0000-0000-0000-000000000001/cardpayment/",
            data=post_data,
            content_type="application/json",
        )

        self.assertEqual(response.status_code, 201)
        expected = get_payment_request_data(
            realized_payment=None,
            card_payment="00000000-0000-0000-0000-000000002694",
            card_gateway_url="https://gateway.example.org/0000009876",
        )
        self.assertEqual(response.json(), expected)

    @override_settings(
        PAIN_PROCESSORS={"dummy": "django_pain.tests.utils.DummyPaymentProcessor"},
        PAIN_CARD_PAYMENT_HANDLERS={
            "test": {"HANDLER": "django_pain.tests.test_serializers.TestCardPaymentHandler", "GATEWAY": "default"},
        },
        PAIN_PAYMENT_REQUESTS={"auctions": {"CARD_HANDLER": "test", "PROCESSOR": "dummy"}},
        PAIN_CSOB_CUSTOM_HANDLER={},
    )
    def test_add_card_payment_failure_active(self):
        payment_request = create_payment_request(realized_payment=None)
        payment = get_payment(
            identifier="pid-1",
            account=payment_request.bank_account,
            counter_account_number="",
            payment_type=PaymentType.CARD_PAYMENT,
            state=PaymentState.INITIALIZED,
            card_handler="test",
        )
        payment.save()
        payment_request.card_payment = payment
        payment_request.save(update_fields=["card_payment"])  # with active card payment
        post_data = {"redirect_url": "http://example.org"}

        response = self.client.post(
            "/api/private/paymentrequest/00000000-0000-0000-0000-000000000001/cardpayment/",
            data=post_data,
            content_type="application/json",
        )

        self.assertEqual(response.status_code, 400)
        self.assertEqual(response.json(), {"detail": "Payment request has active card payment"})

    @override_settings(
        PAIN_PROCESSORS={"dummy": "django_pain.tests.utils.DummyPaymentProcessor"},
        PAIN_CARD_PAYMENT_HANDLERS={
            "broken": {"HANDLER": "django_pain.tests.test_rest.BrokenCardPaymentHandler", "GATEWAY": "default"},
        },
        PAIN_PAYMENT_REQUESTS={"auctions": {"CARD_HANDLER": "broken", "PROCESSOR": "dummy"}},
        PAIN_CSOB_CUSTOM_HANDLER={},
    )
    def test_add_card_payment_gw_error(self):
        create_payment_request(realized_payment=None)  # without card payment
        post_data = {"redirect_url": "http://example.org"}

        response = self.client.post(
            "/api/private/paymentrequest/00000000-0000-0000-0000-000000000001/cardpayment/",
            data=post_data,
            content_type="application/json",
        )

        self.assertEqual(response.status_code, 400)
        self.assertEqual(response.json(), {"detail": "PaymentHandlerConnectionError('Gateway is down')"})


@override_settings(ROOT_URLCONF="django_pain.tests.urls")
class TestInvoiceRestAPI(CacheResetMixin, TestCase):
    # --- retrieve

    def test_retrieve_not_exists(self):
        response = self.client.get("/api/private/invoice/no-i-do-not-exist/")

        self.assertEqual(response.status_code, 404)

    def test_retrieve_ok(self):
        # Create payment request and related invoice
        _create_invoice()

        response = self.client.get("/api/private/invoice/902400001/")

        # Check response
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.json(), _get_serialized_invoice())
