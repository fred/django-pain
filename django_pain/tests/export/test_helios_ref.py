#
# Copyright (C) 2024  CZ.NIC, z. s. p. o.
#
# This file is part of FRED.
#
# FRED is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# FRED is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with FRED.  If not, see <https://www.gnu.org/licenses/>.

"""Tests for Helios reference container."""

from django.test import SimpleTestCase
from lxml import etree

from django_pain.export.helios_ref import HeliosRefContainer, HeliosRefItem


class TestHeliosRefItem(SimpleTestCase):
    """Tests for `HeliosRefItem`."""

    def test_init_error(self):
        with self.assertRaises(ValueError):
            HeliosRefItem("group", {})

    def test_repr(self):
        item = HeliosRefItem("group", {"Number": 1, "Value1": "Test"})
        self.assertEqual(repr(item), "<HeliosRefItem(0) @ [group][1]>")

    def test_str(self):
        item = HeliosRefItem("group", {"Number": 1, "Value1": "Test"})
        self.assertEqual(str(item), "FK_1")

    def test_serialize(self):
        xml = "<Polozka><Klic>FK_1</Klic><Number>1</Number><Value1>Test</Value1></Polozka>"
        item = HeliosRefItem("group", {"Number": 1, "Value1": "Test"})
        root = etree.Element("root")
        item.serialize(root)

        self.assertEqual(etree.tostring(root.find("Polozka")).decode(), xml)

    def test_serialize_empty_item(self):
        xml = "<Polozka><Klic>FK_1</Klic></Polozka>"
        item = HeliosRefItem("group", {"Key": None})
        root = etree.Element("root")
        item.serialize(root)

        self.assertEqual(etree.tostring(root.find("Polozka")).decode(), xml)

    def test_add_data_new(self):
        xml = "<Polozka><Klic>FK_1</Klic><Number>1</Number><New>Test</New></Polozka>"
        item = HeliosRefItem("group", {"Number": 1})
        item.add_data("New", "Test")
        root = etree.Element("root")
        item.serialize(root)

        self.assertEqual(etree.tostring(root.find("Polozka")).decode(), xml)

    def test_add_data_same(self):
        xml = "<Polozka><Klic>FK_1</Klic><Number>1</Number><New>Test</New></Polozka>"
        item = HeliosRefItem("group", {"Number": 1, "New": "Test"})
        item.add_data("New", "Test")
        root = etree.Element("root")
        item.serialize(root)

        self.assertEqual(etree.tostring(root.find("Polozka")).decode(), xml)

    def test_add_data_error(self):
        item = HeliosRefItem("group", {"Number": 1, "Value1": "Test"})
        with self.assertRaises(ValueError):
            item.add_data("Value1", "New")


class TestHeliosRefContainer(SimpleTestCase):
    """Tests for `HeliosRefContainer`."""

    def test_insert_serialize(self):
        xml = (
            "<Reference>"
            "<group_1>"
            "<Polozka><Klic>FK_1</Klic><number>1</number></Polozka>"
            "<Polozka><Klic>FK_2</Klic><number>2</number></Polozka>"
            "</group_1>"
            "<group_2>"
            "<Polozka><Klic>FK_3</Klic><name>one</name><value>11</value></Polozka>"
            "<Polozka><Klic>FK_4</Klic><name>two</name><value>22</value></Polozka>"
            "</group_2>"
            "<group_3>"
            "<Polozka><Klic>FK_5</Klic></Polozka>"
            "</group_3>"
            "</Reference>"
        )
        container = HeliosRefContainer()
        # insert some items
        container.insert("group_1", {"number": 1})
        container.insert("group_1", {"number": 2})
        container.insert("group_2", {"name": "one", "value": 11})
        container.insert("group_2", {"name": "two", "value": 22})
        container.insert("group_3", {"key": None})
        # insert some duplicate items
        container.insert("group_1", {"number": 2})
        container.insert("group_2", {"name": "one", "value": 11})
        container.insert("group_3", {"key": None})

        root = etree.Element("root")
        container.serialize(root)

        self.assertEqual(etree.tostring(root.find("Reference")).decode(), xml)

    def test_serialize_empty(self):
        container = HeliosRefContainer()

        root = etree.Element("root")
        container.serialize(root)

        self.assertEqual(etree.tostring(root).decode(), "<root/>")

    def test_insert_error(self):
        container = HeliosRefContainer()
        with self.assertRaises(ValueError):
            container.insert("group", {})
