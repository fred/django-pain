#
# Copyright (C) 2024  CZ.NIC, z. s. p. o.
#
# This file is part of FRED.
#
# FRED is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# FRED is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with FRED.  If not, see <https://www.gnu.org/licenses/>.

"""Test for invoice export to Helios."""

from datetime import date, datetime, timezone
from decimal import Decimal
from pathlib import Path

from django.test import SimpleTestCase, TestCase, TransactionTestCase
from lxml import etree

from django_pain.export.helios import AuctionsInvoiceSerializer, BaseInvoiceSerializer, HeliosData, HeliosExporter
from django_pain.export.helios_ref import HeliosRefContainer
from django_pain.models import Customer, Invoice, InvoiceItem
from django_pain.tests.utils import create_invoice, get_customer
from django_pain.utils import round_money_amount


class TestBaseInvoiceSerializer(SimpleTestCase):
    """Test common functionality of `BaseInvoiceSerializer`."""

    class MinimalInvoiceSerializer(BaseInvoiceSerializer):
        """Serializer with header handler only."""

        def _invoice_header(self, invoice: Invoice) -> HeliosData:  # pragma: no cover
            return tuple()

    def test_serialize_data(self):
        data: HeliosData = (
            ("string_item", "Bla, bla, bla ..."),
            ("int_item", 151),
            ("int_item", 152),  # repeated item
            ("bool_item", True),
            ("none_item", None),  # skipped
            ("datetime_item", datetime(2024, 6, 1, 12, 0, 0, tzinfo=timezone.utc)),  # formatted datetime
            ("date_item", date(2024, 6, 15)),  # formatted date
            ("empty_string", ""),  # with no value (i.e. not with empty string)
            (
                "child_element",
                (
                    ("tag_1", "tag 1 value"),
                    ("tag_2", 2),
                ),
            ),
        )
        root = etree.Element("root")
        invoice_serializer = self.MinimalInvoiceSerializer(HeliosRefContainer())

        invoice_serializer._serialize_data(data, root)

        xml = (
            "<root>"
            "<string_item>Bla, bla, bla ...</string_item>"
            "<int_item>151</int_item>"
            "<int_item>152</int_item>"
            "<bool_item>True</bool_item>"
            "<datetime_item>2024-06-01 12:00:00</datetime_item>"
            "<date_item>2024-06-15 00:00:00</date_item>"
            "<empty_string/>"
            "<child_element>"
            "<tag_1>tag 1 value</tag_1>"
            "<tag_2>2</tag_2>"
            "</child_element>"
            "</root>"
        )
        self.assertEqual(etree.tostring(root).decode(), xml)


class TestInvoiceSerializer(TestCase):
    """Test of handling invoice in serializers."""

    class EmptyInvoiceSerializer(BaseInvoiceSerializer):
        """Serializer with no usable handlers."""

    class HeaderOnlyInvoiceSerializer(BaseInvoiceSerializer):
        """Serializer with header handler only."""

        def _invoice_header(self, invoice: Invoice) -> HeliosData:
            return tuple()

    class HeaderItemInvoiceSerializer(BaseInvoiceSerializer):
        """Serializer with header and item handler."""

        def _invoice_header(self, invoice: Invoice) -> HeliosData:
            return tuple()

        def _invoice_item_basic_rate(self, invoice: Invoice, item: InvoiceItem) -> HeliosData:
            return tuple()

    def setUp(self):
        create_invoice()  # invoice with `auctions` app id and two items of `basic_rate` type
        self.invoice = Invoice.objects.get()

    def test_no_header_handler(self):
        with self.assertRaises(TypeError):
            self.EmptyInvoiceSerializer(HeliosRefContainer())  # type: ignore[abstract]

    def test_no_item_handler(self):
        serializer = self.HeaderOnlyInvoiceSerializer(HeliosRefContainer())

        with self.assertRaisesMessage(
            ValueError,
            "Processing of invoice item type `basic_rate` not defined in `HeaderOnlyInvoiceSerializer`",
        ):
            serializer.serialize(self.invoice)

    def test_ok(self):
        serializer = self.HeaderItemInvoiceSerializer(HeliosRefContainer())

        node = serializer.serialize(self.invoice)

        self.assertEqual(
            etree.tostring(node).decode(), "<PohyboveDoklady><TabPohybyZbozi/><TabPohybyZbozi/></PohyboveDoklady>"
        )


class TestHeliosExporter(TestCase):
    """Tests for Helios exporter."""

    class ExampleInvoiceSerializer(BaseInvoiceSerializer):
        """Example invoice serializer with minimal implementation."""

        def _invoice_header(self, invoice: Invoice) -> HeliosData:
            return (("Cislo", invoice.number),)

        def _invoice_item_basic_rate(self, invoice: Invoice, item: InvoiceItem) -> HeliosData:
            ref = self.ref_container.insert("TabDruhPolozky", {"Druh": item.item_type})
            return (
                ("Jmeno", item.name),
                ("DruhPolozky", ref),
            )

    def test_export_ok(self):
        exporter = HeliosExporter({"auctions": self.ExampleInvoiceSerializer})
        exporter.add_invoice(create_invoice())
        xml = exporter.export()

        expected = (
            "<?xml version='1.0' encoding='UTF-8'?>\n"
            '<HeliosIQ_1 xmlns="urn:schemas-microsoft-com:xml-data">\n'
            "  <header>\n"
            "    <manifest>\n"
            "      <document>\n"
            "        <name>PohyboveDoklady</name>\n"
            "        <description>internal-format-HELIOS:lcs_cz:PohyboveDoklady</description>\n"
            "        <version>020020140302</version>\n"
            "      </document>\n"
            "    </manifest>\n"
            "  </header>\n"
            "  <body>\n"
            "    <PohyboveDoklady>\n"
            "      <Cislo>902400001</Cislo>\n"
            "      <TabPohybyZbozi>\n"
            "        <Jmeno>Item 1</Jmeno>\n"
            "        <DruhPolozky>FK_1</DruhPolozky>\n"
            "      </TabPohybyZbozi>\n"
            "      <TabPohybyZbozi>\n"
            "        <Jmeno>Item 2</Jmeno>\n"
            "        <DruhPolozky>FK_1</DruhPolozky>\n"
            "      </TabPohybyZbozi>\n"
            "    </PohyboveDoklady>\n"
            "    <Reference>\n"
            "      <TabDruhPolozky>\n"
            "        <Polozka>\n"
            "          <Klic>FK_1</Klic>\n"
            "          <Druh>basic_rate</Druh>\n"
            "        </Polozka>\n"
            "      </TabDruhPolozky>\n"
            "    </Reference>\n"
            "  </body>\n"
            "</HeliosIQ_1>"
        )
        self.assertEqual(xml.decode(), expected)

    def test_export_no_serializer(self):
        exporter = HeliosExporter({})  # no serializer for `auctions`

        with self.assertRaisesMessage(ValueError, "No invoice serializer defined for `auctions`"):
            exporter.add_invoice(create_invoice())


class TestAuctionsInvoiceExport(TransactionTestCase):
    """Tests of invoice export from auctions."""

    reset_sequences = True

    def _create_invoice(self, customer: Customer, price_total: Decimal, vat_rate: Decimal) -> Invoice:
        price_total = round_money_amount(price_total)
        price_base = round_money_amount(price_total / (Decimal(1) + Decimal(0.01) * vat_rate))
        kwargs = {
            "customer": customer,
            "variable_symbol": "402400023",
            "due_at": datetime(2024, 2, 10, 8, 43, 12, tzinfo=timezone.utc),
            "tax_at": date(2024, 1, 31),
            "paid_at": date(2024, 1, 31),
            "bank_account": "450044/0300",
            "items": [
                {
                    "name": "example.org",
                    "item_type": "domain_registration_right",
                    "vat_rate": vat_rate,
                    "quantity": 1,
                    "price_per_unit": price_base,
                    "price_total_base": price_base,
                    "price_total_vat": price_total - price_base,
                    "price_total": price_total,
                },
            ],
        }
        return create_invoice(**kwargs)  # auctions specific invoice

    def _read_xml(self, file_name: str) -> bytes:
        xml_file_name = Path(__file__).parent / "data" / file_name
        with open(xml_file_name, "rb") as file:
            return file.read()

    def _test_export(self, invoice: Invoice, expected_xml: bytes) -> None:
        exporter = HeliosExporter({"auctions": AuctionsInvoiceSerializer})
        exporter.add_invoice(invoice)

        self.assertEqual(exporter.export(), expected_xml)

    def test_export_cz_with_vat_number(self):
        data = {
            "name": None,
            "organization": "ATAL, s.r.o.",
            "street1": "Lesní 47",
            "city": "Tábor-Horky",
            "postal_code": "390 01",
            "country_code": "CZ",
            "company_registration_number": "47216719",
            "vat_number": "CZ47216719",
        }
        invoice = self._create_invoice(get_customer(**data), Decimal("1210.00"), Decimal("21.0"))

        self._test_export(invoice, self._read_xml("auctions-1.xml"))

    def test_export_cz_no_vat_number(self):
        data = {
            "name": "Jan Nováček",
            "organization": None,
            "street1": "Skalka 905",
            "city": "Neratovice",
            "postal_code": "277 11",
            "country_code": "CZ",
            "company_registration_number": None,
            "vat_number": None,
        }
        invoice = self._create_invoice(get_customer(**data), Decimal("540.00"), Decimal("21.0"))

        self._test_export(invoice, self._read_xml("auctions-2.xml"))

    def test_export_eu_with_vat(self):
        data = {
            "name": None,
            "organization": "Custom Chrome Europe GmbH",
            "street1": "Carl-von-Ossietzky-Str. 8",
            "city": "Grolsheim",
            "postal_code": "D-55459",
            "country_code": "DE",
            "company_registration_number": "811309246",
            "vat_number": "DE811309246",
        }
        invoice = self._create_invoice(get_customer(**data), Decimal("2148.00"), Decimal("0.0"))

        self._test_export(invoice, self._read_xml("auctions-3.xml"))

    def test_export_eu_no_vat(self):
        data = {
            "name": "Marika Janschke",
            "organization": None,
            "street1": "Salzburgerstrasse 16c",
            "city": "Lofer",
            "postal_code": "A-6080",
            "country_code": "AT",
            "company_registration_number": None,
            "vat_number": None,
        }
        invoice = self._create_invoice(get_customer(**data), Decimal("1433.00"), Decimal("20.0"))

        self._test_export(invoice, self._read_xml("auctions-4.xml"))

    def test_export_non_eu(self):
        data = {
            "name": None,
            "organization": "Instra Corporation (Europe) Ltd",
            "street1": "31 Arden Close",
            "street2": "Bradley",
            "city": "Stoke Bristol",
            "postal_code": "32 8AX",
            "country_code": "GB",
            "company_registration_number": "55110054610",
            "vat_number": "ABN55110054610",
        }
        invoice = self._create_invoice(get_customer(**data), Decimal("1000.00"), Decimal("0.0"))

        self._test_export(invoice, self._read_xml("auctions-5.xml"))
