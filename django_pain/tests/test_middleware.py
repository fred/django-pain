#
# Copyright (C) 2024  CZ.NIC, z. s. p. o.
#
# This file is part of FRED.
#
# FRED is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# FRED is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with FRED.  If not, see <https://www.gnu.org/licenses/>.

"""Tests of middleware."""

from unittest.mock import patch

from django.test import TestCase, override_settings
from testfixtures import LogCapture

from django_pain.middleware import _binary_content
from django_pain.tests.mixins import CacheResetMixin
from django_pain.tests.utils import get_customer


@override_settings(ROOT_URLCONF="django_pain.tests.urls")
class TestTrafficLoggingMiddleware(CacheResetMixin, TestCase):
    """Tests of logging performed by middleware."""

    def setUp(self):
        self.rest_log_handler = LogCapture("django_pain.middleware", propagate=False)

    def tearDown(self):
        self.rest_log_handler.uninstall()

    def test_binary_content(self):
        bin_data = b"0123456789abcdefghijklmnopqrstuvwxyz"
        data = [
            (0, "b''... (36 more bytes)"),
            (10, "b'0123456789'... (26 more bytes)"),
            (36, "b'0123456789abcdefghijklmnopqrstuvwxyz'"),
        ]
        for length, expected in data:
            with self.subTest(length=length):
                self.assertEqual(_binary_content(bin_data, length), expected)

    def test_binary_content_none(self):
        self.assertEqual(_binary_content(None, 10), "None")

    @patch("django_pain.middleware.get_call_id")
    def test_get_404(self, id_mock):
        id_mock.return_value = "Ab1k"

        self.client.get("/api/private/customer/no-i-do-not-exist/")

        self.rest_log_handler.check_present(
            ("django_pain.middleware", "INFO", "<<< [Ab1k] GET /api/private/customer/no-i-do-not-exist/"),
            ("django_pain.middleware", "DEBUG", "<<< [Ab1k] b''"),
            ("django_pain.middleware", "INFO", ">>> [Ab1k] 404"),
            ("django_pain.middleware", "DEBUG", '>>> [Ab1k] b\'{"detail":"Not found."}\''),
        )

    @patch("django_pain.middleware.get_call_id")
    def test_patch(self, id_mock):
        customer = get_customer(uid="uid-1", name="Alice Angry")
        customer.save()
        id_mock.return_value = "Ab1k"
        patch_data = {"name": "Bob Brown"}

        self.client.patch("/api/private/customer/uid-1/", data=patch_data, content_type="application/json")

        self.rest_log_handler.check_present(
            ("django_pain.middleware", "INFO", "<<< [Ab1k] PATCH /api/private/customer/uid-1/"),
            (
                "django_pain.middleware",
                "DEBUG",
                '<<< [Ab1k] b\'{"name": "Bob Brown"}\' ' 'Content-Type: "application/json"',
            ),
            ("django_pain.middleware", "INFO", ">>> [Ab1k] 200"),
            (
                "django_pain.middleware",
                "DEBUG",
                '>>> [Ab1k] b\'{"uid":"uid-1","name":"Bob Brown",'
                '"organization":"Test, s.r.o.","street":["Na Okraji 1"],"city":"Pokuston","state_or_province":null,'
                '"postal_code":"050 10","country_code":"CZ","email":null,"phone_number":null,'
                '"company_registration_number":"001234","vat_number":"CZ001234"}\'',
            ),
        )
