#
# Copyright (C) 2018-2024  CZ.NIC, z. s. p. o.
#
# This file is part of FRED.
#
# FRED is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# FRED is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with FRED.  If not, see <https://www.gnu.org/licenses/>.

"""Test utils."""

import uuid
from datetime import date, datetime, timezone
from decimal import Decimal
from typing import Any, Dict

from djmoney.money import Money
from freezegun import freeze_time

from django_pain.card_payment_handlers import (
    AbstractCardPaymentHandler,
    PaymentHandlerConnectionError,
    PaymentHandlerError,
)
from django_pain.constants import InvoiceType, PaymentState, PaymentType
from django_pain.models import (
    BankAccount,
    BankPayment,
    ClientRef,
    Customer,
    Invoice,
    InvoiceRef,
    PaymentRequest,
    PaymentRequestItem,
)
from django_pain.models.invoices import InvoiceNumberSequence
from django_pain.processors import AbstractPaymentProcessor
from django_pain.serializers import InvoiceSerializer
from django_pain.settings import is_eu_member


class DummyPaymentProcessor(AbstractPaymentProcessor):
    """Dummy payment processor."""

    default_objective = "Dummy objective"

    def process_payments(self, payments):
        """Do nothing."""

    def assign_payment(self, payment, client_id):
        """Do nothing."""


class DummyCardPaymentHandler(AbstractCardPaymentHandler):
    """Dummy card payment handler."""

    def init_payment(self, amount, variable_symbol, processor, return_url, return_method, cart, language, **kwargs):
        """Do nothing."""

    def update_payments_state(self, payment):
        """Update payment state."""
        payment.state = PaymentState.READY_TO_PROCESS
        payment.save()


class DummyCardPaymentHandlerExc(DummyCardPaymentHandler):
    """Dummy card payment handler which throws connectoin exception."""

    def init_payment(self, amount, variable_symbol, processor, return_url, return_method, cart, language, **kwargs):
        """Do nothing."""

    def update_payments_state(self, payment):
        """Raise exception."""
        raise PaymentHandlerError("Card Handler Error")


class DummyCardPaymentHandlerConnExc(DummyCardPaymentHandler):
    """Dummy card payment handler which throws connectoin exception."""

    def init_payment(self, amount, variable_symbol, processor, return_url, return_method, cart, language, **kwargs):
        """Do nothing."""

    def update_payments_state(self, payment):
        """Raise exception."""
        raise PaymentHandlerConnectionError("Gateway connection error")


def get_account(**kwargs: Any) -> BankAccount:
    """Create bank account object."""
    default = {
        "account_number": "123456/0300",
        "account_name": "Account",
        "currency": "CZK",
    }
    default.update(kwargs)
    return BankAccount(**default)


def get_payment(**kwargs: Any) -> BankPayment:
    """Create payment object."""
    default = {
        "identifier": "PAYMENT1",
        "account": None,
        "transaction_date": date(2018, 5, 9),
        "counter_account_number": "098765/4321",
        "counter_account_name": "Another account",
        "amount": Money("42.00", "CZK"),
    }
    default.update(kwargs)
    return BankPayment(**default)


def get_invoice_ref(**kwargs: Any) -> InvoiceRef:
    """Create invoice object."""
    default = {
        "number": "1111122222",
        "remote_id": 0,
        "invoice_type": InvoiceType.ADVANCE,
    }
    default.update(kwargs)
    return InvoiceRef(**default)


def get_client_ref(**kwargs: Any) -> ClientRef:
    """Create client object."""
    default = {
        "handle": "HANDLE",
        "remote_id": 0,
    }
    default.update(kwargs)
    return ClientRef(**default)


def get_customer(**kwargs: Any) -> Customer:
    """Create customer object."""
    default = {
        "uid": "CID-1",
        "name": None,
        "organization": "Test, s.r.o.",
        "street1": "Na Okraji 1",
        "street2": None,
        "street3": None,
        "city": "Pokuston",
        "postal_code": "050 10",
        "country_code": "CZ",
        "email": None,
        "phone_number": None,
        "company_registration_number": "001234",
        "vat_number": "CZ001234",
        "vies_vat_valid": None,
        "vies_checked_at": None,
    }
    default.update(kwargs)
    return Customer(**default)


def get_payment_request(**kwargs: Any) -> PaymentRequest:
    """Create payment request object."""
    customer = kwargs.pop("customer", get_customer())
    bank_account = kwargs.pop("bank_account", get_account())
    default = {
        "uuid": uuid.UUID(int=1),
        "app_id": "auctions",
        "customer": customer,
        "name": customer.name,
        "organization": customer.organization,
        "street1": customer.street1,
        "street2": customer.street2,
        "street3": customer.street3,
        "city": customer.city,
        "state_or_province": customer.state_or_province,
        "postal_code": customer.postal_code,
        "country_code": customer.country_code,
        "company_registration_number": customer.company_registration_number,
        "vat_number": customer.vat_number,
        "created_at": datetime(2023, 9, 1, tzinfo=timezone.utc),
        "due_at_displayed": datetime(2023, 10, 1, 12, 0, 0, tzinfo=timezone.utc),
        "variable_symbol": "0000009876",
        "price_to_pay": Decimal("25.60"),
        "currency": "CZK",
        "bank_account": bank_account,
    }
    default.update(kwargs)
    return PaymentRequest(**default)


@freeze_time("2023-09-01T12:00:00Z")
def create_payment_request(**kwargs) -> PaymentRequest:
    """Create payment request.

    Create "reasonable" payment request including items, calculated total price, etc. and save to database.
    Some attributes of the created object may be overrided.
    """
    customer = kwargs.pop("customer", get_customer())
    customer.save()
    account = kwargs.pop("bank_account", get_account())
    account.save()
    realized_payment = kwargs.pop(
        "realized_payment",
        get_payment(uuid=uuid.UUID(int=28), account=account, transaction_date=date(2023, 9, 15), variable_symbol="603"),
    )
    if realized_payment:
        realized_payment.save()
    data: Dict[str, Any] = {"customer": customer, "bank_account": account, "realized_payment": realized_payment}
    data.update(**kwargs)
    payment_request = get_payment_request(**data)
    payment_request.save()
    total_price = Decimal(0)
    for i in range(1, 4):
        item = PaymentRequestItem(
            payment_request=payment_request,
            name=f"Item {i}",
            item_type="basic_rate",
            quantity=i,
            price_per_unit=Decimal(10 + i),
        )
        item.price_total = item.price_per_unit * item.quantity
        total_price += item.price_total
        item.save()
    payment_request.price_to_pay = total_price
    payment_request.save(update_fields=["price_to_pay"])
    return payment_request


def get_payment_request_data(**kwargs) -> Dict[str, Any]:
    """Get payment request serialization.

    Complementary function to `create_payment_request` provides serialized contents of payment request object
    intended as expected pattern for comparison. Attributes overriden there should be adjusted accordingly.
    """
    expected: Dict[str, Any] = {
        "uuid": "00000000-0000-0000-0000-000000000001",
        "app_id": "auctions",
        "customer": "CID-1",
        "name": None,
        "organization": "Test, s.r.o.",
        "street": ["Na Okraji 1"],
        "city": "Pokuston",
        "state_or_province": None,
        "postal_code": "050 10",
        "country_code": "CZ",
        "company_registration_number": "001234",
        "vat_number": "CZ001234",
        "created_at": "2023-09-01T12:00:00Z",
        "due_at": None,
        "due_at_displayed": "2023-10-01T12:00:00Z",
        "closed_at": None,
        "price_to_pay": "74.0000000000",
        "currency": "CZK",
        "variable_symbol": "0000009876",
        "bank_account": "123456/0300",
        "card_payment": None,
        "realized_payment": "00000000-0000-0000-0000-00000000001c",
        "card_gateway_url": None,
        "file_uid": None,
        "invoice_number": None,
        "qr_code": "SPD*1.0*ACC:CZ4403000000000000123456*AM:74.00*CC:CZK*X-VS:0000009876*MSG:CZ.NIC-Item 1",
        "items": [
            {
                "name": "Item 1",
                "item_type": "basic_rate",
                "quantity": 1,
                "price_per_unit": "11.0000000000",
                "price_total": "11.0000000000",
            },
            {
                "name": "Item 2",
                "item_type": "basic_rate",
                "quantity": 2,
                "price_per_unit": "12.0000000000",
                "price_total": "24.0000000000",
            },
            {
                "name": "Item 3",
                "item_type": "basic_rate",
                "quantity": 3,
                "price_per_unit": "13.0000000000",
                "price_total": "39.0000000000",
            },
        ],
    }
    expected.update(**kwargs)
    return expected


def get_payment_request_context_data(**kwargs) -> Dict[str, Any]:
    """Get payment request context serialization.

    Complementary function to `create_payment_request` provides serialized contents of payment request object
    intended as expected pattern for comparison. Attributes overriden there should be adjusted accordingly.
    """
    expected: Dict[str, Any] = {
        "name": None,
        "organization": "Test, s.r.o.",
        "street": ["Na Okraji 1"],
        "city": "Pokuston",
        "state_or_province": None,
        "postal_code": "050 10",
        "country": "Czechia",
        "company_registration_number": "001234",
        "vat_number": "CZ001234",
        "variable_symbol": "0000009876",
        "created_at": "2023-09-01",
        "due_at": "2023-09-30",
        "bank_account": "123456/0300",
        "price_paid": "0.00",
        "price_to_pay": "74.00",
        "currency": "CZK",
        "iban": "CZ44 0300 0000 0000 0012 3456",
        "qr_code": "SPD*1.0*ACC:CZ4403000000000000123456*AM:74.00*CC:CZK*X-VS:0000009876*MSG:CZ.NIC-Item 1",
        "bank_name": "Československá obchodní banka, a.s.",
        "bank_swift": "CEKOCZPP",
        "items": [
            {"name": "Item 1", "item_type": "basic_rate", "quantity": 1, "price_total": "11.00"},
            {"name": "Item 2", "item_type": "basic_rate", "quantity": 2, "price_total": "24.00"},
            {"name": "Item 3", "item_type": "basic_rate", "quantity": 3, "price_total": "39.00"},
        ],
    }
    expected.update(**kwargs)
    return expected


def get_invoice(**kwargs: Any) -> Invoice:
    """Create invoice object."""
    customer = kwargs.pop("customer", get_customer())
    default = {
        "uuid": uuid.UUID(int=1),
        "app_id": "auctions",
        "customer": customer,
        "name": customer.name,
        "organization": customer.organization,
        "street1": customer.street1,
        "street2": customer.street2,
        "street3": customer.street3,
        "city": customer.city,
        "state_or_province": customer.state_or_province,
        "postal_code": customer.postal_code,
        "country_code": customer.country_code,
        "company_registration_number": customer.company_registration_number,
        "vat_number": customer.vat_number,
        "created_at": datetime(2023, 9, 15, tzinfo=timezone.utc),
        "tax_at": date(2023, 9, 15),
        "number": "902300008",
        "variable_symbol": "0902300023",
        "price_total_base": Decimal("1000.00"),
        "price_total_vat": Decimal("200.00"),
        "price_total": Decimal("1200.00"),
        "price_to_pay": Decimal("0.00"),
        "currency": "CZK",
        "bank_account": "123456/0300",
        "payment_type": str(PaymentType.TRANSFER),
    }
    default.update(kwargs)
    return Invoice(**default)


@freeze_time("2024-01-31T15:00:00Z")
def create_invoice(**kwargs) -> Invoice:
    """Create invoice instance."""
    customer = kwargs.pop("customer", get_customer())
    vies_check = customer.vat_number is not None and is_eu_member(customer.country_code)
    customer.vies_vat_valid = True if vies_check else None
    customer.vies_checked_at = datetime(2024, 1, 1, tzinfo=timezone.utc) if vies_check else None
    customer.save()
    data: Dict[str, Any] = {
        "app_id": "auctions",
        "customer": customer.uid,
        "due_at": datetime(2024, 1, 25, 12, 0, 0, tzinfo=timezone.utc),
        "tax_at": date(2024, 1, 20),
        "paid_at": date(2024, 1, 20),
        "price_to_pay": Decimal("0.00"),
        "currency": "CZK",
        "variable_symbol": "0902400012",
        "bank_account": "123456/0300",
        "payment_type": PaymentType.TRANSFER,
        "items": [
            {
                "name": "Item 1",
                "item_type": "basic_rate",
                "vat_rate": Decimal("10.0"),
                "quantity": 1,
                "price_per_unit": Decimal("11.00"),
                "price_total_base": Decimal("11.00"),
                "price_total_vat": Decimal("1.10"),
                "price_total": Decimal("12.10"),
            },
            {
                "name": "Item 2",
                "item_type": "basic_rate",
                "vat_rate": Decimal("20.0"),
                "quantity": 2,
                "price_per_unit": Decimal("15.00"),
                "price_total_base": Decimal("30.00"),
                "price_total_vat": Decimal("6.00"),
                "price_total": Decimal("36.00"),
            },
        ],
    }
    data.update(**kwargs)
    InvoiceNumberSequence.objects.get_or_create(app_id="auctions", year=2024, prefix=9024)
    serializer = InvoiceSerializer(data=data)
    serializer.is_valid()
    return serializer.save()


def get_invoice_context_data(**kwargs) -> Dict[str, Any]:
    default = {
        "name": None,
        "organization": "Test, s.r.o.",
        "street": ["Na Okraji 1"],
        "city": "Pokuston",
        "state_or_province": None,
        "postal_code": "050 10",
        "country": "Czechia",
        "company_registration_number": "001234",
        "vat_number": "CZ001234",
        "number": "902400001",
        "variable_symbol": "0902400012",
        "created_at": "2024-01-31",
        "tax_at": "2024-01-20",
        "price_total_base": "41.00",
        "price_total_vat": "7.10",
        "price_total": "48.10",
        "price_to_pay": "0.00",
        "currency": "CZK",
        "bank_account": "123456/0300",
        "iban": "CZ44 0300 0000 0000 0012 3456",
        "bank_name": "Československá obchodní banka, a.s.",
        "bank_swift": "CEKOCZPP",
        "payment_type": "transfer",
        "reverse_charge": False,
        "items": [
            {
                "name": "Item 1",
                "item_type": "basic_rate",
                "vat_rate": "10.0000",
                "quantity": 1,
                "price_per_unit": "11.00",
                "price_total_base": "11.00",
                "price_total_vat": "1.10",
                "price_total": "12.10",
            },
            {
                "name": "Item 2",
                "item_type": "basic_rate",
                "vat_rate": "20.0000",
                "quantity": 2,
                "price_per_unit": "15.00",
                "price_total_base": "30.00",
                "price_total_vat": "6.00",
                "price_total": "36.00",
            },
        ],
    }
    default.update(**kwargs)
    return default
