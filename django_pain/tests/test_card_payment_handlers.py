#
# Copyright (C) 2020-2024  CZ.NIC, z. s. p. o.
#
# This file is part of FRED.
#
# FRED is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# FRED is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with FRED.  If not, see <https://www.gnu.org/licenses/>.

"""Tests of card payment handlers."""

from collections import OrderedDict
from copy import deepcopy
from datetime import date, datetime, timedelta, timezone as tz
from decimal import Decimal
from unittest.mock import Mock, patch, sentinel

import requests
from django.core.exceptions import ImproperlyConfigured
from django.test import TestCase, override_settings
from django.utils import timezone
from djmoney.money import Money
from freezegun import freeze_time
from hermes import Email, Reference
from hermes.client import SendFailed
from pycsob import conf as CSOB
from testfixtures import LogCapture

from django_pain.card_payment_handlers import (
    AbstractCardPaymentHandler,
    CartItem,
    CSOBCardPaymentHandler,
    CSOBCustomPaymentHandler,
    PaymentHandlerAccountError,
    PaymentHandlerConnectionError,
    PaymentHandlerError,
    PaymentHandlerLimitError,
    RedirectUrl,
    check_payment_handler,
)
from django_pain.constants import PaymentState, PaymentType
from django_pain.models import BankPayment
from django_pain.settings import SETTINGS
from django_pain.tests.utils import get_account, get_payment


class TestCSOBGatewaySettings(TestCase):
    """Test proper usage of gateway settings in handlers."""

    def test_ok(self):
        handler = CSOBCardPaymentHandler("csob", "default")
        self.assertEqual(handler._gateway, SETTINGS.csob_gateways["default"])

    def test_invalid_gateway(self):
        with self.assertRaisesMessage(ImproperlyConfigured, "Key `undefined` not found in PAIN_CSOB_GATEWAYS"):
            CSOBCardPaymentHandler("csob", "undefined")


class TestCSOBCardPaymentHandlerStatus(TestCase):
    """Test CSOBCardPaymentHandler.payment_status method."""

    def test_get_client(self):
        handler = CSOBCardPaymentHandler("csob", gateway="default")
        client = handler.client
        # repeated calls should get the same client instance
        client2 = handler.client
        self.assertEqual(client, client2)

    @freeze_time("2023-09-01T12:00:00Z")
    def test_update_payment_state_confirmed(self):
        account = get_account(account_number="123456", currency="CZK")
        account.save()
        payment = get_payment(
            identifier="1",
            account=account,
            counter_account_number="",
            payment_type=PaymentType.CARD_PAYMENT,
            state=PaymentState.INITIALIZED,
            card_handler="csob",
            transaction_date=None,
        )
        payment.save()

        result_mock = Mock()
        result_mock.payload = {"paymentStatus": CSOB.PAYMENT_STATUS_CONFIRMED, "resultCode": CSOB.RETURN_CODE_OK}

        handler = CSOBCardPaymentHandler("csob", gateway="default")
        with patch.object(handler, "_client") as gateway_client_mock:
            gateway_client_mock.payment_status.return_value = result_mock

            handler.update_payments_state(payment)

        self.assertEqual(payment.state, PaymentState.READY_TO_PROCESS)
        self.assertEqual(payment.transaction_date, date(2023, 9, 1))

    @freeze_time("2023-09-01T12:00:00Z")
    def test_update_payment_state_initialized(self):
        account = get_account(account_number="123456", currency="CZK")
        account.save()
        payment = get_payment(
            identifier="1",
            account=account,
            counter_account_number="",
            payment_type=PaymentType.CARD_PAYMENT,
            state=PaymentState.INITIALIZED,
            card_handler="csob",
            transaction_date=None,
        )
        payment.save()

        result_mock = Mock()
        result_mock.payload = {"paymentStatus": CSOB.PAYMENT_STATUS_PROCESS, "resultCode": CSOB.RETURN_CODE_OK}

        handler = CSOBCardPaymentHandler("csob", gateway="default")
        with patch.object(handler, "_client") as gateway_client_mock:
            gateway_client_mock.payment_status.return_value = result_mock

            handler.update_payments_state(payment)

        self.assertEqual(payment.state, PaymentState.INITIALIZED)
        self.assertIsNone(payment.transaction_date)

    def test_update_payment_state_no_update_not_initialized(self):
        account = get_account(account_number="123456", currency="CZK")
        account.save()
        payment = get_payment(
            identifier="1",
            account=account,
            counter_account_number="",
            payment_type=PaymentType.CARD_PAYMENT,
            state=PaymentState.PROCESSED,
            card_handler="csob",
        )
        payment.save()

        result_mock = Mock()
        result_mock.payload = {"paymentStatus": CSOB.PAYMENT_STATUS_CANCELLED, "resultCode": CSOB.RETURN_CODE_OK}

        handler = CSOBCardPaymentHandler("csob", gateway="default")
        with patch.object(handler, "_client") as gateway_client_mock:
            gateway_client_mock.payment_status.return_value = result_mock

            handler.update_payments_state(payment)

        self.assertEqual(payment.state, PaymentState.PROCESSED)

    def test_update_payment_state_not_ok(self):
        account = get_account(account_number="123456", currency="CZK")
        account.save()
        payment = get_payment(
            identifier="1",
            account=account,
            counter_account_number="",
            payment_type=PaymentType.CARD_PAYMENT,
            state=PaymentState.INITIALIZED,
            card_handler="csob",
            transaction_date=None,
        )
        payment.save()

        result_mock = Mock()
        result_mock.payload = {"resultCode": CSOB.RETURN_CODE_MERCHANT_BLOCKED}

        with LogCapture() as log:
            handler = CSOBCardPaymentHandler("csob", gateway="default")
            with patch.object(handler, "_client") as gateway_client_mock:
                gateway_client_mock.payment_status.return_value = result_mock
                self.assertRaisesRegex(
                    PaymentHandlerError, "payment_status resultCode != OK", handler.update_payments_state, payment
                )
                log.check(
                    (
                        "django_pain.card_payment_handlers.csob",
                        "ERROR",
                        "payment_status resultCode != OK: {'resultCode': 120}",
                    ),
                )
            self.assertEqual(payment.state, PaymentState.INITIALIZED)

    def test_update_payment_state_connection_error(self):
        account = get_account(account_number="123456", currency="CZK")
        account.save()
        payment = get_payment(
            identifier="1",
            account=account,
            counter_account_number="",
            payment_type=PaymentType.CARD_PAYMENT,
            state=PaymentState.INITIALIZED,
            card_handler="csob",
            transaction_date=None,
        )
        payment.save()

        handler = CSOBCardPaymentHandler("csob", gateway="default")
        with patch.object(handler, "_client") as gateway_client_mock:
            gateway_client_mock.payment_status.side_effect = requests.ConnectionError()

            self.assertRaises(PaymentHandlerConnectionError, handler.update_payments_state, payment)


class TestCSOBCardPaymentHandlerInit(TestCase):
    """Test CSOBCardPaymentHandler.init_payment method."""

    def test_init_payment_connection_error(self):
        account = get_account(account_number="123456", currency="CZK")
        account.save()

        handler = CSOBCardPaymentHandler("csob", gateway="default")
        with patch.object(handler, "_client") as gateway_client_mock:
            gateway_client_mock.payment_init.side_effect = requests.ConnectionError()
            self.assertRaises(
                PaymentHandlerConnectionError,
                handler.init_payment,
                Money(100, "CZK"),
                "123",
                "csob",
                "https://example.com",
                "POST",
                [],
                "cs",
            )

    def test_init_payment_ok(self):
        account = get_account(account_number="123456", currency="CZK")
        account.save()

        handler = CSOBCardPaymentHandler("csob", gateway="default")
        with patch.object(handler, "_client") as gateway_client_mock:
            gateway_client_mock.gateway_return.return_value = OrderedDict(
                [
                    ("payId", "unique_id_123"),
                    ("resultCode", CSOB.RETURN_CODE_OK),
                    ("resultMessage", "OK"),
                    ("paymentStatus", CSOB.PAYMENT_STATUS_INIT),
                    ("dttime", datetime(2020, 6, 10, 16, 47, 30, tzinfo=tz.utc)),
                ]
            )
            gateway_client_mock.get_payment_process_url.return_value = sentinel.url
            payment, redirect_url = handler.init_payment(
                Money(100, "CZK"),
                "123",
                "donations",
                "https://example.com",
                "POST",
                [CartItem("Gift for FRED", 1, Decimal(100), "Gift for the best FRED")],
                "cs",
            )
            self.assertIsNone(payment.transaction_date)

        gateway_client_mock.payment_init.assert_called_once_with(
            order_no="123",
            total_amount=100 * 100,
            currency="CZK",
            return_url="https://example.com",
            description="Dummy value",
            cart=[{"name": "Gift for FRED", "quantity": 1, "amount": 10000, "description": "Gift for the best FRED"}],
            return_method="POST",
            language="cs",
            pay_operation="payment",
            pay_method="card",
        )

        self.assertEqual(redirect_url, RedirectUrl(sentinel.url))
        self.assertEqual(payment.identifier, "unique_id_123")
        self.assertEqual(payment.state, PaymentState.INITIALIZED)
        self.assertEqual(payment.card_payment_state, CSOB.PAYMENT_STATUSES[CSOB.PAYMENT_STATUS_INIT])
        self.assertEqual(payment.amount, Money(100, "CZK"))
        self.assertEqual(payment.processor, "donations")
        self.assertEqual(payment.card_handler, "csob")

    def test_init_payment_select_account_by_currency(self):
        account_czk = get_account(account_number="123456", currency="CZK")
        account_czk.save()
        account_eur = get_account(account_number="234567", currency="EUR")
        account_eur.save()

        test_data = (
            ("CZK", "123", account_czk),
            ("EUR", "456", account_eur),
        )

        handler = CSOBCardPaymentHandler("csob", gateway="default")

        for currency, pay_id, _ in test_data:
            with patch.object(handler, "_client") as gateway_client_mock:
                gateway_client_mock.gateway_return.return_value = OrderedDict(
                    [
                        ("payId", "unique_id_" + pay_id),
                        ("resultCode", CSOB.RETURN_CODE_OK),
                        ("resultMessage", "OK"),
                        ("paymentStatus", CSOB.PAYMENT_STATUS_INIT),
                        ("dttime", datetime(2020, 6, 10, 16, 47, 30, tzinfo=tz.utc)),
                    ]
                )
                handler.init_payment(
                    Money(100, currency),
                    pay_id,
                    "donations",
                    "https://example.com",
                    "POST",
                    [CartItem("Gift for FRED", 1, Decimal(100), "Gift for the best FRED")],
                    "cs",
                )

        for currency, pay_id, account in test_data:
            payment = BankPayment.objects.get(identifier="unique_id_" + pay_id)
            self.assertEqual(payment.amount, Money(100, currency))
            self.assertEqual(payment.account, account)

    def test_init_payment_non_existent_account(self):
        account_czk = get_account(account_number="123456", currency="CZK")
        account_czk.save()

        handler = CSOBCardPaymentHandler("csob", gateway="default")

        with patch.object(handler, "_client") as gateway_client_mock:
            gateway_client_mock.gateway_return.return_value = OrderedDict(
                [
                    ("payId", "unique_id_123"),
                    ("resultCode", CSOB.RETURN_CODE_OK),
                    ("resultMessage", "OK"),
                    ("paymentStatus", CSOB.PAYMENT_STATUS_INIT),
                    ("dttime", datetime(2020, 6, 10, 16, 47, 30, tzinfo=tz.utc)),
                ]
            )
            message = 'CSOBCardPaymentHandler configured with non-existing account "234567"'
            with self.assertRaisesRegex(PaymentHandlerAccountError, message):
                handler.init_payment(
                    Money(100, "EUR"),
                    "123",
                    "donations",
                    "https://example.com",
                    "POST",
                    [CartItem("Gift for FRED", 1, Decimal(100), "Gift for the best FRED")],
                    "cs",
                )

    def test_init_payment_account_not_set(self):
        account_czk = get_account(account_number="123456", currency="CZK")
        account_czk.save()
        account_eur = get_account(account_number="234567", currency="EUR")
        account_eur.save()

        handler = CSOBCardPaymentHandler("csob", gateway="default")

        with patch.object(handler, "_client") as gateway_client_mock:
            gateway_client_mock.gateway_return.return_value = OrderedDict(
                [
                    ("payId", "unique_id_123"),
                    ("resultCode", CSOB.RETURN_CODE_OK),
                    ("resultMessage", "OK"),
                    ("paymentStatus", CSOB.PAYMENT_STATUS_INIT),
                    ("dttime", datetime(2020, 6, 10, 16, 47, 30, tzinfo=tz.utc)),
                ]
            )
            with self.assertRaisesRegex(PaymentHandlerAccountError, "No account for currency USD"):
                handler.init_payment(
                    Money(100, "USD"),
                    "123",
                    "donations",
                    "https://example.com",
                    "POST",
                    [CartItem("Gift for FRED", 1, Decimal(100), "Gift for the best FRED")],
                    "cs",
                )

    def test_init_payment_not_ok(self):
        account = get_account(account_number="123456", currency="CZK")
        account.save()

        handler = CSOBCardPaymentHandler("csob", gateway="default")
        with patch.object(handler, "_client") as gateway_client_mock:
            gateway_client_mock.gateway_return.return_value = OrderedDict(
                [
                    ("payId", "unique_id_123"),
                    ("resultCode", CSOB.RETURN_CODE_MERCHANT_BLOCKED),
                    ("resultMessage", "Merchant blocked"),
                    ("dttime", datetime(2020, 6, 10, 16, 47, 30, tzinfo=tz.utc)),
                ]
            )
            self.assertRaisesRegex(
                PaymentHandlerError,
                "resultCode != OK",
                handler.init_payment,
                Money(100, "CZK"),
                "123",
                "donations",
                "https://example.com",
                "POST",
                [CartItem("Gift for FRED", 1, Decimal(100), "Gift for the best FRED")],
                "cs",
            )

    def test_init_payment_wrong_status(self):
        account = get_account(account_number="123456", currency="CZK")
        account.save()

        handler = CSOBCardPaymentHandler("csob", gateway="default")
        with patch.object(handler, "_client") as gateway_client_mock:
            gateway_client_mock.gateway_return.return_value = OrderedDict(
                [
                    ("payId", "unique_id_123"),
                    ("resultCode", CSOB.RETURN_CODE_OK),
                    ("resultMessage", "OK"),
                    ("paymentStatus", CSOB.PAYMENT_STATUS_CANCELLED),
                    ("dttime", datetime(2020, 6, 10, 16, 47, 30, tzinfo=tz.utc)),
                ]
            )
            self.assertRaisesRegex(
                PaymentHandlerError,
                "paymentStatus != PAYMENT_STATUS_INIT",
                handler.init_payment,
                Money(100, "CZK"),
                "123",
                "donations",
                "https://example.com",
                "POST",
                [CartItem("Gift for FRED", 1, Decimal(100), "Gift for the best FRED")],
                "cs",
            )

    def test_init_payment_exceeded_gw_limit(self):
        account = get_account(account_number="123456", currency="CZK")
        account.save()

        handler = CSOBCardPaymentHandler("csob", gateway="default")
        with patch.object(handler, "_client") as gateway_client_mock:
            gateway_client_mock.gateway_return.return_value = OrderedDict(
                [
                    ("payId", ""),
                    ("resultCode", 120),
                    ("resultMessage", "Merchant trx limit exceeded"),
                    ("dttime", datetime(2020, 6, 10, 16, 47, 30, tzinfo=tz.utc)),
                ]
            )
            self.assertRaisesRegex(
                PaymentHandlerLimitError,
                "Amount 100000 CZK exceeds gateway limit",
                handler.init_payment,
                Money(100_000, "CZK"),
                "123",
                "donations",
                "https://example.com",
                "POST",
                [CartItem("Gift for FRED", 1, Decimal(100_000), "Gift for the best FRED")],
                "cs",
            )

    def test_check_limit_ok(self):
        data = [
            Money(500000, "CZK"),  # with limit set
            Money(20000, "EUR"),  # unlimited by default
        ]

        gw_settings = deepcopy(SETTINGS.csob_gateways)
        gw_settings["default"]["ACCOUNT_LIMITS"].update({"CZK": 500000})
        with override_settings(PAIN_CSOB_GATEWAYS=gw_settings):
            handler = CSOBCardPaymentHandler("csob", gateway="default")

            for amount in data:
                with self.subTest(amount=amount):
                    handler._check_limit(amount)

    def test_check_limit_exceeded(self):
        data = [
            Money(500001, "CZK"),
            Money(20001, "EUR"),
        ]

        gw_settings = deepcopy(SETTINGS.csob_gateways)
        gw_settings["default"]["ACCOUNT_LIMITS"].update({"CZK": 500000, "EUR": 20000})
        with override_settings(PAIN_CSOB_GATEWAYS=gw_settings):
            handler = CSOBCardPaymentHandler("csob", gateway="default")

            for amount in data:
                with self.subTest(amount=amount):
                    self.assertRaises(PaymentHandlerLimitError, handler._check_limit, amount)


@override_settings(PAIN_CSOB_CUSTOM_HANDLER={"MIN_VALIDITY": 3600, "MAX_VALIDITY": 86400})
@freeze_time("2023-01-01T00:00:00Z")
class TestCSOBCustomPaymentHandlerInit(TestCase):
    """Test CSOBCustomPaymentHandler.init_payment method."""

    init_params = {
        "amount": Money(100, "CZK"),
        "variable_symbol": "123",
        "processor": "donations",
        "return_url": "https://example.com",
        "return_method": "POST",
        "cart": [CartItem("Gift for FRED", 1, Decimal(100), "Gift for the best FRED")],
        "language": "cs",
    }

    def test_ok(self):
        account = get_account(account_number="123456", currency="CZK")
        account.save()

        handler = CSOBCustomPaymentHandler("csob_custom", gateway="default")
        handler._email_client = Mock()
        handler._email_client.send.return_value = "msg-1"

        with patch.object(handler, "_client") as gateway_client_mock:
            gateway_client_mock.gateway_return.return_value = OrderedDict(
                [
                    ("payId", "unique_id_123"),
                    ("resultCode", CSOB.RETURN_CODE_OK),
                    ("resultMessage", "OK"),
                    ("paymentStatus", CSOB.PAYMENT_STATUS_INIT),
                    ("customerCode", "XYZ789"),
                    ("dttime", datetime.now()),
                ]
            )

            payment, redirect_url = handler.init_payment(**self.init_params, email="user@example.org")

            self.assertEqual(payment.card_handler, "csob_custom")
            self.assertEqual(payment.email, "user@example.org")
            self.assertEqual(
                payment.custom_expiry, datetime(2023, 1, 2, 0, 0, 0, tzinfo=tz.utc)
            )  # max validity by default
            self.assertEqual(payment.customer_code, "XYZ789")
            self.assertEqual(payment.email_sent, datetime(2023, 1, 1, 0, 0, 0, tzinfo=tz.utc))  # email sent just now
            self.assertEqual(redirect_url, RedirectUrl("https://platebnibrana.csob.cz/zaplat/XYZ789"))

            gateway_client_mock.payment_init.assert_called_once_with(
                order_no="123",
                total_amount=100 * 100,
                currency="CZK",
                return_url="https://example.com",
                description="Dummy value",
                cart=[
                    {
                        "name": "Gift for FRED",
                        "quantity": 1,
                        "amount": 100 * 100,
                        "description": "Gift for the best FRED",
                    }
                ],
                return_method="POST",
                language="cs",
                pay_operation="customPayment",
                pay_method="card",
                custom_expiry="20230102000000",
            )
            handler._email_client.send.assert_called_once()

    def test_ok_without_email(self):
        account = get_account(account_number="123456", currency="CZK")
        account.save()

        handler = CSOBCustomPaymentHandler("csob_custom", gateway="default")
        handler._email_client = Mock()

        with patch.object(handler, "_client") as gateway_client_mock:
            gateway_client_mock.gateway_return.return_value = OrderedDict(
                [
                    ("payId", "unique_id_123"),
                    ("resultCode", CSOB.RETURN_CODE_OK),
                    ("resultMessage", "OK"),
                    ("paymentStatus", CSOB.PAYMENT_STATUS_INIT),
                    ("customerCode", "XYZ789"),
                    ("dttime", datetime.now()),
                ]
            )

            handler.init_payment(**self.init_params)

            gateway_client_mock.payment_init.assert_called_once()
            handler._email_client.send.assert_not_called()

    def test_ok_with_custom_expiry(self):
        account = get_account(account_number="123456", currency="CZK")
        account.save()

        handler = CSOBCustomPaymentHandler("csob_custom", gateway="default")
        handler._email_client = Mock()
        handler._email_client.send.return_value = "msg-1"

        with patch.object(handler, "_client") as gateway_client_mock:
            gateway_client_mock.gateway_return.return_value = OrderedDict(
                [
                    ("payId", "unique_id_123"),
                    ("resultCode", CSOB.RETURN_CODE_OK),
                    ("resultMessage", "OK"),
                    ("paymentStatus", CSOB.PAYMENT_STATUS_INIT),
                    ("customerCode", "XYZ789"),
                    ("dttime", datetime.now()),
                ]
            )

            payment, _ = handler.init_payment(
                **self.init_params,
                email="user@example.org",
                custom_expiry=datetime(2023, 1, 1, 12, 0, 0, tzinfo=tz.utc),
            )

            self.assertEqual(payment.custom_expiry, datetime(2023, 1, 1, 12, 0, 0, tzinfo=tz.utc))

    def test_no_customer_code(self):
        account = get_account(account_number="123456", currency="CZK")
        account.save()

        handler = CSOBCustomPaymentHandler("csob_custom", gateway="default")

        with patch.object(handler, "_client") as gateway_client_mock:
            gateway_client_mock.gateway_return.return_value = OrderedDict(
                [
                    ("payId", "unique_id_123"),
                    ("resultCode", CSOB.RETURN_CODE_OK),
                    ("resultMessage", "OK"),
                    ("paymentStatus", CSOB.PAYMENT_STATUS_INIT),
                    ("dttime", datetime.now()),
                ]
            )

            with self.assertRaisesMessage(PaymentHandlerError, "No customer code generated for custom payment"):
                handler.init_payment(**self.init_params, email="user@example.org")


@override_settings(PAIN_CSOB_CUSTOM_HANDLER={})
class TestSendCustomerCode(TestCase):
    """Test sending email in CSOBCustomPaymentHandler."""

    @freeze_time("2023-08-01T09:15:00Z")
    def setUp(self):
        account = get_account(account_number="123456", currency="CZK")
        account.save()
        payment_data = {
            "identifier": "PAYMENT1",
            "payment_type": PaymentType.CARD_PAYMENT,
            "account": account,
            "amount": Money("42.00", "CZK"),
            "description": "Gift for FRED",
            "state": PaymentState.INITIALIZED,
            "variable_symbol": "2023000001",
            "card_handler": "csob_custom",
            "email": "user@example.org",
            "custom_expiry": datetime(2023, 8, 10, 15, 30, 0, tzinfo=tz.utc),
            "customer_code": "XYZ789",
        }
        payment = BankPayment(**payment_data)
        payment.save()

        self.handler = CSOBCustomPaymentHandler("csob_custom", gateway="default")
        self.email_client = Mock()
        self.handler._email_client = self.email_client

    @freeze_time("2023-08-05T12:00:00Z")
    def test_send_ok(self):
        self.email_client.send.return_value = "msg-1"
        with LogCapture() as log:
            payment = BankPayment.objects.get()
            self.handler._send_customer_code(payment)
            # Check payment is updated
            payment = BankPayment.objects.get()
            self.assertEqual(payment.email_sent, datetime(2023, 8, 5, 12, 0, 0, tzinfo=tz.utc))
            # Check email client call
            expected_context = {
                "payment": "PAYMENT1",
                "url_base": "https://platebnibrana.csob.cz/zaplat/",
                "customer_code": "XYZ789",
                "description": "Gift for FRED",
                "amount": "42.00 Kč",
                "variable_symbol": "2023000001",
                "expiry": "2023-08-10T15:30:00+00:00",
                "resend": False,
            }
            expected_email = Email(
                recipient="user@example.org",
                subject_template="csob-customer-code-subject.txt",
                body_template="csob-customer-code-body.txt",
                context=expected_context,
            )
            expected_references = [Reference(type="payment", value=str(payment.uuid))]
            self.email_client.send.assert_called_once_with(expected_email, references=expected_references)
            # Check log message
            log.check(
                (
                    "django_pain.card_payment_handlers.csob",
                    "DEBUG",
                    "email with customer code for payment PAYMENT1 sent (message id: msg-1)",
                ),
            )

    @freeze_time("2023-08-05T12:00:00Z")
    def test_send_failed(self):
        self.email_client.send.side_effect = SendFailed
        with LogCapture() as log:
            payment = BankPayment.objects.get()
            self.handler._send_customer_code(payment)
            # Check payment is not updated
            payment = BankPayment.objects.get()
            self.assertIsNone(payment.email_sent)
            # Check email client call
            self.email_client.send.assert_called_once()
            # Check log message
            log.check(
                (
                    "django_pain.card_payment_handlers.csob",
                    "ERROR",
                    "failed to sent email with customer code for payment PAYMENT1",
                ),
            )

    @freeze_time("2023-08-15T12:00:00Z")  # payment is overdue
    def test_send_expired(self):
        with LogCapture() as log:
            payment = BankPayment.objects.get()
            self.handler._send_customer_code(payment)
            self.email_client.send.assert_not_called()
            log.check()

    def test_update_email_not_sent_yet(self):
        # Send email in `update_payments_state`
        payment = BankPayment.objects.get()
        # payment.state is initialized and payment.email_sent is None, i.e. email ready to sent

        with patch.object(self.handler, "_send_customer_code") as send_mock, patch.object(
            self.handler, "_update_payments_state"
        ) as update_mock:
            self.handler.update_payments_state(payment=payment)

            update_mock.assert_called_once()
            send_mock.assert_called_once_with(payment)

    @freeze_time("2023-08-05T12:00:00Z")
    def test_update_email_already_sent(self):
        # Don't send email in `update_payments_state` because already sent
        payment = BankPayment.objects.get()
        payment.email_sent = timezone.now()  # email already sent

        with patch.object(self.handler, "_send_customer_code") as send_mock, patch.object(
            self.handler, "_update_payments_state"
        ) as update_mock:
            self.handler.update_payments_state(payment=payment)

            update_mock.assert_called_once()
            send_mock.assert_not_called()

    @freeze_time("2023-08-05T12:00:00Z")
    def test_update_resend_email(self):
        # Resend email in `update_payments_state`
        payment = BankPayment.objects.get()
        payment.email_sent = timezone.now()  # email already sent

        with patch.object(self.handler, "_send_customer_code") as send_mock, patch.object(
            self.handler, "_update_payments_state"
        ) as update_mock, patch.object(self.handler, "_should_resend_email") as resend_mock:
            resend_mock.return_value = True

            self.handler.update_payments_state(payment=payment)

            update_mock.assert_called_once()
            send_mock.assert_called_once_with(payment, resend=True)

    @freeze_time("2023-08-05T12:00:00Z")
    def test_update_without_email(self):
        # Resend email in `update_payments_state`
        payment = BankPayment.objects.get()
        payment.email = None

        with patch.object(self.handler, "_send_customer_code") as send_mock, patch.object(
            self.handler, "_update_payments_state"
        ) as update_mock, patch.object(self.handler, "_should_resend_email") as resend_mock:
            self.handler.update_payments_state(payment=payment)

            update_mock.assert_called_once()
            send_mock.assert_not_called()
            resend_mock.assert_not_called()

    def test_update_payment_bad_state(self):
        # Don't send email in `update_payments_state` because payment canceled by bank
        payment = BankPayment.objects.get()
        payment.state = PaymentState.CANCELED

        with patch.object(self.handler, "_send_customer_code") as send_mock, patch.object(
            self.handler, "_update_payments_state"
        ) as update_mock:
            self.handler.update_payments_state(payment=payment)

            update_mock.assert_called_once()
            send_mock.assert_not_called()


class TestShouldResendEmail(TestCase):
    """Test resend email evaluation in CSOBCustomPaymentHandler."""

    @freeze_time("2023-06-01T00:00:00Z")
    def setUp(self):
        account = get_account(account_number="123456", currency="CZK")
        account.save()
        payment_data = {
            "identifier": "PAYMENT1",
            "payment_type": PaymentType.CARD_PAYMENT,
            "account": account,
            "amount": Money("42.00", "CZK"),
            "description": "Gift for FRED",
            "state": PaymentState.INITIALIZED,
            "variable_symbol": "2023000001",
            "processor": "test_resend",
            "card_handler": "csob_custom",
            "email": "user@example.org",
            "email_sent": timezone.now(),
            "custom_expiry": datetime(2023, 7, 20, 0, 0, 0, tzinfo=tz.utc),
            "customer_code": "XYZ789",
        }
        payment = BankPayment(**payment_data)
        payment.save()

    def _test_should_resend_case(self, handler: CSOBCustomPaymentHandler, timepoint: datetime, expected: bool):
        payment = BankPayment.objects.get()
        should_resend = handler._should_resend_email(payment)
        if should_resend:
            payment.email_sent = timezone.now()  # mark email as sent just now
            payment.save()
        self.assertEqual(should_resend, expected)

    @override_settings(
        PAIN_CSOB_CUSTOM_HANDLER={
            "RESEND_EMAIL": {"PROCESSORS": ["test_resend"], "TIME_DELTAS": [10, 12, 25, -3], "MIN_DELTA": 5}
        }
    )
    def test_should_resend_timeline(self):
        data = [
            (1, datetime(2023, 6, 10, tzinfo=tz.utc), False),  # no event
            (2, datetime(2023, 6, 11, tzinfo=tz.utc), True),  # event on +10 days
            (3, datetime(2023, 6, 13, tzinfo=tz.utc), False),  # event on +12 days, but 'min_delta' not satisfied
            (5, datetime(2023, 6, 22, tzinfo=tz.utc), False),  # no event
            (6, datetime(2023, 6, 26, tzinfo=tz.utc), True),  # event on +25 days
            (7, datetime(2023, 7, 18, tzinfo=tz.utc), True),  # event on -3 days
            (8, datetime(2023, 7, 25, tzinfo=tz.utc), False),  # no event
        ]
        handler = CSOBCustomPaymentHandler("csob_custom", gateway="default")

        for number, timepoint, expected in data:
            with freeze_time(timepoint):
                with self.subTest(number=number):
                    self._test_should_resend_case(handler, timepoint, expected)

    @override_settings(
        PAIN_CSOB_CUSTOM_HANDLER={"RESEND_EMAIL": {"PROCESSORS": ["test_resend"], "TIME_DELTAS": [10], "MIN_DELTA": 10}}
    )
    def test_should_resend_timeline_with_delay(self):
        data = [
            (1, datetime(2023, 6, 10, 23, 59, 59, tzinfo=tz.utc), False),  # no event
            (2, datetime(2023, 6, 11, 0, 0, 0, tzinfo=tz.utc), True),  # event on +10 days
        ]
        payment = BankPayment.objects.get()
        payment.email_sent += timedelta(seconds=3)  # email sent with some delay
        payment.save()
        handler = CSOBCustomPaymentHandler("csob_custom", gateway="default")

        for number, timepoint, expected in data:
            with freeze_time(timepoint):
                with self.subTest(number=number):
                    self._test_should_resend_case(handler, timepoint, expected)

    @override_settings(PAIN_CSOB_CUSTOM_HANDLER={"RESEND_EMAIL": {"TIME_DELTAS": [-3]}})
    def test_should_resend_all_processors(self):
        data = [
            (1, datetime(2023, 6, 5, tzinfo=tz.utc), False),  # no event
            (2, datetime(2023, 7, 18, tzinfo=tz.utc), True),  # event on -3 days
        ]
        handler = CSOBCustomPaymentHandler("csob_custom", gateway="default")

        for number, timepoint, expected in data:
            with freeze_time(timepoint):
                with self.subTest(number=number):
                    self._test_should_resend_case(handler, timepoint, expected)

    @override_settings(PAIN_CSOB_CUSTOM_HANDLER={"RESEND_EMAIL": {"PROCESSORS": ["no_resend"], "TIME_DELTAS": [-3]}})
    def test_should_resend_no_processor(self):
        data = [
            (1, datetime(2023, 6, 5, tzinfo=tz.utc), False),  # no event
            (2, datetime(2023, 7, 18, tzinfo=tz.utc), False),  # event on -3 days but no match in processors
        ]
        handler = CSOBCustomPaymentHandler("csob_custom", gateway="default")

        for number, timepoint, expected in data:
            with freeze_time(timepoint):
                with self.subTest(number=number):
                    self._test_should_resend_case(handler, timepoint, expected)


@override_settings(PAIN_CSOB_CUSTOM_HANDLER={})
class TestCSOBCustomPaymentHandlerGetPaymentUrl(TestCase):
    """Test retrieving URL for custom payments."""

    @freeze_time("2023-08-01T09:15:00Z")
    def setUp(self):
        account = get_account()
        account.save()
        payment_data = {
            "identifier": "PAYMENT1",
            "payment_type": PaymentType.CARD_PAYMENT,
            "account": account,
            "amount": Money("42.00", "CZK"),
            "description": "Gift for FRED",
            "state": PaymentState.INITIALIZED,
            "variable_symbol": "2023000001",
            "card_handler": "csob_custom",
            "email": "user@example.org",
            "custom_expiry": datetime(2023, 9, 15, 15, 30, 0, tzinfo=tz.utc),
            "customer_code": "XYZ789",
        }
        payment = BankPayment(**payment_data)
        payment.save()

        self.handler = CSOBCustomPaymentHandler("csob_custom", gateway="default")

    @freeze_time("2023-08-20T12:00:00Z")
    def test_get_payment_url_ok(self):
        payment = BankPayment.objects.get()
        self.assertEqual(self.handler.get_payment_url(payment), "https://platebnibrana.csob.cz/zaplat/XYZ789")

    @freeze_time("2023-08-20T12:00:00Z")
    def test_get_payment_url_processed(self):
        payment = BankPayment.objects.get()
        payment.state = PaymentState.PROCESSED
        self.assertIsNone(self.handler.get_payment_url(payment))

    @freeze_time("2023-11-20T12:00:00Z")
    def test_get_payment_url_expired(self):
        payment = BankPayment.objects.get()
        self.assertIsNone(self.handler.get_payment_url(payment))


class TestCheckPaymentHandlerDecorator(TestCase):
    """Test docorator to check handler method parameters."""

    class DummyHandler(AbstractCardPaymentHandler):
        def init_payment(self, *args, **kwargs):  # pragma: no cover
            raise NotImplementedError

        @check_payment_handler
        def update_payments_state(self, payment: BankPayment) -> None:
            pass

    @staticmethod
    def _get_card_payment(**kwargs):
        default = {
            "identifier": "PAYMENT1",
            "payment_type": PaymentType.CARD_PAYMENT,
            "card_handler": "dummy",
            "account": None,
            "amount": Money("42.00", "CZK"),
        }
        default.update(kwargs)
        return BankPayment(**default)

    def test_decorator_ok(self):
        payment = self._get_card_payment()
        handler = self.DummyHandler("dummy", "default")

        handler.get_payment_url(payment)  # positional parameter
        handler.get_payment_url(payment=payment)  # keyword parameter
        handler.update_payments_state(payment)
        handler.update_payments_state(payment=payment)

    def test_decorator_raises(self):
        payment = self._get_card_payment(card_handler="some_other")
        handler = self.DummyHandler("dummy", "default")
        message = "Card handler got incompatible payment: got `some_other`, expected `dummy`."

        self.assertRaisesMessage(ValueError, message, handler.get_payment_url, payment)
        self.assertRaisesMessage(ValueError, message, handler.get_payment_url, payment=payment)
        self.assertRaisesMessage(ValueError, message, handler.update_payments_state, payment)
        self.assertRaisesMessage(ValueError, message, handler.update_payments_state, payment=payment)
