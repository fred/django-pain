#
# Copyright (C) 2018-2024  CZ.NIC, z. s. p. o.
#
# This file is part of FRED.
#
# FRED is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# FRED is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with FRED.  If not, see <https://www.gnu.org/licenses/>.

"""Test admin filters."""

from datetime import datetime, timezone
from uuid import UUID

from django.contrib import admin
from django.contrib.auth.models import User
from django.test import RequestFactory, TestCase, override_settings
from django.urls import reverse
from freezegun import freeze_time

from django_pain.admin import BankPaymentAdmin
from django_pain.constants import PaymentState
from django_pain.models import BankAccount, BankPayment
from django_pain.tests.utils import get_account, get_customer, get_payment, get_payment_request


class TestChoicesFieldListFilter(TestCase):
    """Test ChoicesFieldListFilter."""

    def setUp(self):
        self.request_factory = RequestFactory()
        self.admin = User.objects.create_superuser("admin", "admin@example.com", "password")
        account = BankAccount(account_number="123456/7890", currency="CZK")
        account.save()
        get_payment(identifier="PAYMENT_1", account=account, state=PaymentState.INITIALIZED).save()
        get_payment(identifier="PAYMENT_2", account=account, state=PaymentState.READY_TO_PROCESS).save()
        get_payment(identifier="PAYMENT_3", account=account, state=PaymentState.DEFERRED).save()
        get_payment(identifier="PAYMENT_4", account=account, state=PaymentState.PROCESSED).save()
        get_payment(identifier="PAYMENT_5", account=account, state=PaymentState.CANCELED).save()

    def test_filter_default(self):
        modeladmin = BankPaymentAdmin(BankPayment, admin.site)
        request = self.request_factory.get("/", {})
        request.user = self.admin
        changelist = modeladmin.get_changelist_instance(request)
        filterspec = changelist.get_filters(request)[0][0]
        choices = list(filterspec.choices(changelist))
        self.assertEqual(
            choices,
            (
                [
                    {"selected": True, "query_string": "?", "display": "Realized"},
                    {"selected": False, "query_string": "?state__exact=all", "display": "All"},
                    {"selected": False, "query_string": "?state__exact=initialized", "display": "initialized"},
                    {
                        "selected": False,
                        "query_string": "?state__exact=ready_to_process",
                        "display": "ready to process",
                    },
                    {"selected": False, "query_string": "?state__exact=processed", "display": "processed"},
                    {"selected": False, "query_string": "?state__exact=deferred", "display": "not identified"},
                    {"selected": False, "query_string": "?state__exact=exported", "display": "exported"},
                    {"selected": False, "query_string": "?state__exact=canceled", "display": "canceled"},
                    {"selected": False, "query_string": "?state__exact=to_refund", "display": "to refund"},
                    {"selected": False, "query_string": "?state__exact=refunded", "display": "refunded"},
                ]
            ),
        )
        self.assertQuerySetEqual(
            changelist.get_queryset(request).values_list("identifier", flat=True),
            ["PAYMENT_2", "PAYMENT_3", "PAYMENT_4"],
            ordered=False,
            transform=str,
        )

    def test_filter_all(self):
        modeladmin = BankPaymentAdmin(BankPayment, admin.site)
        request = self.request_factory.get("/?state__exact=all", {})
        request.user = self.admin
        changelist = modeladmin.get_changelist_instance(request)
        filterspec = changelist.get_filters(request)[0][0]
        choices = list(filterspec.choices(changelist))
        self.assertEqual(
            choices,
            (
                [
                    {"selected": False, "query_string": "?", "display": "Realized"},
                    {"selected": True, "query_string": "?state__exact=all", "display": "All"},
                    {"selected": False, "query_string": "?state__exact=initialized", "display": "initialized"},
                    {
                        "selected": False,
                        "query_string": "?state__exact=ready_to_process",
                        "display": "ready to process",
                    },
                    {"selected": False, "query_string": "?state__exact=processed", "display": "processed"},
                    {"selected": False, "query_string": "?state__exact=deferred", "display": "not identified"},
                    {"selected": False, "query_string": "?state__exact=exported", "display": "exported"},
                    {"selected": False, "query_string": "?state__exact=canceled", "display": "canceled"},
                    {"selected": False, "query_string": "?state__exact=to_refund", "display": "to refund"},
                    {"selected": False, "query_string": "?state__exact=refunded", "display": "refunded"},
                ]
            ),
        )
        self.assertQuerySetEqual(
            changelist.get_queryset(request).values_list("identifier", flat=True),
            ["PAYMENT_1", "PAYMENT_2", "PAYMENT_3", "PAYMENT_4", "PAYMENT_5"],
            ordered=False,
            transform=str,
        )

    def test_filter_one_state(self):
        modeladmin = BankPaymentAdmin(BankPayment, admin.site)
        request = self.request_factory.get("/?state__exact=initialized", {})
        request.user = self.admin
        changelist = modeladmin.get_changelist_instance(request)
        filterspec = changelist.get_filters(request)[0][0]
        choices = list(filterspec.choices(changelist))
        self.assertEqual(
            choices,
            (
                [
                    {"selected": False, "query_string": "?", "display": "Realized"},
                    {"selected": False, "query_string": "?state__exact=all", "display": "All"},
                    {"selected": True, "query_string": "?state__exact=initialized", "display": "initialized"},
                    {
                        "selected": False,
                        "query_string": "?state__exact=ready_to_process",
                        "display": "ready to process",
                    },
                    {"selected": False, "query_string": "?state__exact=processed", "display": "processed"},
                    {"selected": False, "query_string": "?state__exact=deferred", "display": "not identified"},
                    {"selected": False, "query_string": "?state__exact=exported", "display": "exported"},
                    {"selected": False, "query_string": "?state__exact=canceled", "display": "canceled"},
                    {"selected": False, "query_string": "?state__exact=to_refund", "display": "to refund"},
                    {"selected": False, "query_string": "?state__exact=refunded", "display": "refunded"},
                ]
            ),
        )
        self.assertQuerySetEqual(
            changelist.get_queryset(request).values_list("identifier", flat=True),
            ["PAYMENT_1"],
            ordered=False,
            transform=str,
        )


@override_settings(ROOT_URLCONF="django_pain.tests.urls")
class TestPaymentRequestStatusFilter(TestCase):
    """Test PaymentRequestStatusFilter."""

    def setUp(self):
        self.request_factory = RequestFactory()
        self.admin = User.objects.create_superuser("admin", "admin@example.com", "password")
        account = get_account()
        account.save()
        customer = get_customer()
        customer.save()
        payment = get_payment(account=account)
        payment.save()
        data = [
            {"uuid": UUID(int=1), "variable_symbol": "3023000001"},  # waiting/overdue (depends on time)
            {
                "uuid": UUID(int=2),
                "variable_symbol": "3023000002",
                "closed_at": datetime(2023, 9, 10, tzinfo=timezone.utc),
                "realized_payment": payment,
            },  # realized (+ closed)
            {
                "uuid": UUID(int=3),
                "variable_symbol": "3023000003",
                "closed_at": datetime(2023, 9, 10, tzinfo=timezone.utc),
            },  # closed only
        ]
        for i in data:
            payment_request = get_payment_request(customer=customer, bank_account=account, **i)
            payment_request.save()

    def test_default(self):
        self.client.force_login(self.admin)
        response = self.client.get(reverse("admin:django_pain_paymentrequest_changelist"))
        self.assertContains(response, "3 Payment requests")

    def test_all(self):
        self.client.force_login(self.admin)
        response = self.client.get(reverse("admin:django_pain_paymentrequest_changelist") + "?")
        self.assertContains(response, "3 Payment requests")

    @freeze_time("2023-09-15T12:00:00Z")
    def test_waiting(self):
        self.client.force_login(self.admin)
        response = self.client.get(reverse("admin:django_pain_paymentrequest_changelist") + "?status=waiting")
        self.assertContains(response, "3023000001")
        self.assertContains(response, "1 Payment request")

    @freeze_time("2023-10-15T12:00:00Z")
    def test_overdue(self):
        self.client.force_login(self.admin)
        response = self.client.get(reverse("admin:django_pain_paymentrequest_changelist") + "?status=overdue")
        self.assertContains(response, "3023000001")
        self.assertContains(response, "1 Payment request")

    def test_realized(self):
        self.client.force_login(self.admin)
        response = self.client.get(reverse("admin:django_pain_paymentrequest_changelist") + "?status=realized")
        self.assertContains(response, "3023000002")
        self.assertContains(response, "1 Payment request")

    def test_closed(self):
        self.client.force_login(self.admin)
        response = self.client.get(reverse("admin:django_pain_paymentrequest_changelist") + "?status=closed")
        self.assertContains(response, "3023000003")
        self.assertContains(response, "1 Payment request")
