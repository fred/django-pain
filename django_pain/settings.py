#
# Copyright (C) 2018-2024  CZ.NIC, z. s. p. o.
#
# This file is part of FRED.
#
# FRED is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# FRED is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with FRED.  If not, see <https://www.gnu.org/licenses/>.

"""django_pain settings."""

from datetime import timedelta
from functools import lru_cache
from os import R_OK
from typing import Any, Optional

import appsettings
from django.core.exceptions import ImproperlyConfigured, ValidationError
from django.core.validators import RegexValidator, URLValidator
from django.utils import module_loading
from django_countries import countries
from filed import FileClient, Storage
from frgal import make_credentials
from frgal.aio import SyncGrpcProxy
from hermes import EmailMessengerClient
from typist import HTTPTokenAuth, SecretaryClient

from .utils import full_class_name


class NamedDictSetting(appsettings.DictSetting):
    """Dictionary of names and DictSetting. The Dictsetting is shared between the keys.

    The parameters are the same for each key but the values to which the marameters are set may be different.
    """

    def __init__(self, settings: dict, *args, **kwargs):
        super().__init__(*args, key_type=str, value_type=dict, **kwargs)
        self.settings = settings

    def transform(self, value):
        """Transform the values of each setting group."""
        transformed_value = {}
        for key, group in value.items():
            transformed_group = dict()
            for k, v in group.items():
                setting = self.settings[k]
                transformed_group[k] = setting.transform(v)
            transformed_value[key] = transformed_group
        return transformed_value

    def validate(self, value):
        """Run custom validation on the setting value."""
        if not isinstance(value, dict):
            raise ValidationError("{} must be {}, not {}".format(self.full_name, dict, value.__class__))
        super().validate(value)

    def get_value(self):
        """Get setting value and add missing defaults."""
        value = super().get_value()

        # Use default value for missing subsettings (which are not required)
        for group in value.values():
            for key, subsetting in self.settings.items():
                if key not in group and not subsetting.required:
                    group[key] = subsetting.default_value

        return value

    def check(self):
        """Validate the setting value including the subsettings."""
        super().check()

        # We can not use super().check() to validate subgroups so we do it manually.
        exceptions = []
        try:
            for name, group in self.raw_value.items():
                try:
                    self._check_group(group)
                except ValidationError as error:
                    exceptions.extend(error.messages)
        except AttributeError as e:
            if self.required:  # pragma: no cover  # We don't have required NamedDictSetting at the moment
                exceptions.append(e)
        if exceptions:
            raise ImproperlyConfigured(
                "Setting {} is improperly configured.\n".format(self.full_name) + "\n".join(exceptions)
            )

    def _check_group(self, group):
        if not {k for k in group.keys()}.issubset({k for k in self.settings.keys()}):
            raise ValidationError("Invalid keys.")  # Extra key

        for key, subsetting in self.settings.items():
            try:
                value = group[key]
                subsetting.validate(value)
                subsetting.run_validators(value)
            except KeyError:
                if subsetting.required:
                    raise ValidationError("Invalid keys.")  # Missing required key
                pass


class ClassSetting(appsettings.StringSetting):
    """Class as dotted path string is checked to be of specified type."""

    def __init__(self, checked_class_str, *args, **kwargs):
        self.checked_class_str = checked_class_str
        super().__init__(*args, **kwargs)

    def transform(self, value):
        """Transform value from dotted string into module object."""
        return module_loading.import_string(value)

    def validate(self, value):
        """Check if value is subclass of class specified in checked_class_str."""
        cls = self.transform(value)
        checked_class = module_loading.import_string(self.checked_class_str)
        if not issubclass(cls, checked_class):
            raise ValidationError("{} is not subclass of {}".format(full_class_name(cls), checked_class.__name__))


class NamedClassSetting(appsettings.DictSetting):
    """Dictionary of names and dotted paths to classes setting. Class is checked to be specified type."""

    def __init__(self, checked_class_str, *args, **kwargs):
        self.checked_class_str = checked_class_str
        super().__init__(*args, **kwargs)

    def transform(self, value):
        """Transform value from dotted strings into module objects."""
        return {key: module_loading.import_string(value) for (key, value) in value.items()}

    def validate(self, value):
        """Check if properly configured.

        Value has to be dictionary with following properties:
          * all keys must be strings
          * all values must be subclasses of class specified in checked_class_str
        """
        if not isinstance(value, dict):
            raise ValidationError("{} must be {}, not {}".format(self.full_name, dict, value.__class__))
        if not all(isinstance(key, str) for key in value.keys()):
            raise ValidationError("All keys of {} must be {}".format(self.full_name, str))
        value = self.transform(value)

        checked_class = module_loading.import_string(self.checked_class_str)
        for name, cls in value.items():
            if not issubclass(cls, checked_class):
                raise ValidationError("{} is not subclass of {}".format(full_class_name(cls), checked_class.__name__))


class CallableListSetting(appsettings.ListSetting):
    """Contains list of dotted paths referring to callables."""

    def transform(self, value):
        """Translate dotted path to callable."""
        return [module_loading.import_string(call) for call in value]

    def validate(self, value):
        """Check whether dotted path refers to callable."""
        if not all(isinstance(val, str) for val in value):
            raise ValidationError("{} must be a list of dotted paths to callables".format(self.full_name))

        transformed_value = self.transform(value)
        for call in transformed_value:
            if not callable(call):
                raise ValidationError("{} must be a list of dotted paths to callables".format(self.full_name))


class DayDeltaSetting(appsettings.IntegerSetting):
    """Time interval in days entered as integer."""

    def transform(self, value):
        """Tranforms integer value in days to `timedelta`."""
        return timedelta(days=value)


def http_with_slash_validator(value: str) -> None:
    """Validate string to be HTTP(S) based URL with trailing slash."""
    URLValidator(schemes=["http", "https"])(value)
    if not value.endswith("/"):
        raise ValidationError("Missing trailing slash")


def country_code_validator(value: str) -> None:
    """Validate two-letter country code."""
    if value not in countries:
        raise ValidationError("Invalid country code")


def card_handler_validator(value: str) -> None:
    """Validate that value corresponds to key in `card_payment_handlers` settings."""
    if not SETTINGS.card_payment_handlers or value not in SETTINGS.card_payment_handlers:
        raise ValidationError(f"Card handler `{value}` not found in `PAIN_CARD_PAYMENT_HANDLERS`")


def processor_validator(value: str) -> None:
    """Validate that value corresponds to key in `processors` settings."""
    if not SETTINGS.processors or value not in SETTINGS.processors:
        raise ValidationError(f"Processor `{value}` not found in `PAIN_PROCESSORS`")


def timeout_validator(value: Any) -> None:
    """Validate timeouts - must contain a number or tuple of two numbers."""
    if isinstance(value, (float, int)):
        return
    if isinstance(value, tuple) and len(value) == 2 and all(isinstance(v, (float, int)) for v in value):
        return
    raise ValidationError(
        "Value %(value)s must be a float, int or a tuple with 2 float or int items.", params={"value": value}
    )


EU_MEMBERS = [  # as of December 2023
    "AT",  # Austria
    "BE",  # Belgium
    "BG",  # Bulgaria
    "CY",  # Cyprus
    "CZ",  # Czech Republic
    "DE",  # Germany
    "DK",  # Denmark
    "EE",  # Estonia
    "ES",  # Spain
    "FI",  # Finland
    "FR",  # France
    "GR",  # Greece
    "HR",  # Croatia
    "HU",  # Hungary
    "IE",  # Ireland
    "IT",  # Italy
    "LT",  # Lithuania
    "LU",  # Luxembourg
    "LV",  # Latvia
    "MT",  # Malta
    "NL",  # Netherlands
    "PL",  # Poland
    "PT",  # Portugal
    "RO",  # Romania
    "SE",  # Sweden
    "SI",  # Slovenia
    "SK",  # Slovakia
]


class PainSettings(appsettings.AppSettings):
    """Specific settings for django-pain app."""

    # Dictionary of names and dotted paths to processor classes setting.
    processors = NamedClassSetting(
        "django_pain.processors.AbstractPaymentProcessor", required=True, key_type=str, value_type=str
    )

    # Location of process_payments command lock file.
    process_payments_lock_file = appsettings.StringSetting(default="/tmp/pain_process_payments.lock")

    # Whether variable symbol should be trimmed of leading zeros.
    trim_varsym = appsettings.BooleanSetting(default=False)

    # List of dotted paths to callables that takes BankPayment object as their argument and return (possibly) changed
    # BankPayment.
    #
    # These callables are called right before the payment is saved during the import. Especially, these callable can
    # raise ValidationError in order to avoid saving payment to the database.
    import_callbacks = CallableListSetting(item_type=str)

    downloaders = NamedDictSetting(
        dict(
            DOWNLOADER=appsettings.ObjectSetting(required=True),
            PARSER=appsettings.ObjectSetting(required=True),
            DOWNLOADER_PARAMS=appsettings.DictSetting(required=True, key_type=str),
        ),
        required=False,
    )

    # Configuration of card payment handlers
    card_payment_handlers = NamedDictSetting(
        dict(
            HANDLER=ClassSetting("django_pain.card_payment_handlers.AbstractCardPaymentHandler", required=True),
            GATEWAY=appsettings.StringSetting(required=True),
        ),
        required=False,
    )

    # CSOB payment gateway configurations
    csob_gateways = NamedDictSetting(
        dict(
            API_URL=appsettings.StringSetting(validators=(http_with_slash_validator,), required=True),
            API_PUBLIC_KEY=appsettings.FileSetting(required=True, mode=R_OK),
            MERCHANT_ID=appsettings.StringSetting(required=True),
            MERCHANT_PRIVATE_KEY=appsettings.FileSetting(required=True, mode=R_OK),
            ACCOUNT_NUMBERS=appsettings.DictSetting(required=True, key_type=str, value_type=str),  # currency: account
            ACCOUNT_LIMITS=appsettings.DictSetting(key_type=str, value_type=int),  # currency: limit
        ),
        required=False,
    )

    # CSOB custom payment handler settings
    csob_custom_handler = appsettings.NestedDictSetting(
        dict(
            min_validity=appsettings.IntegerSetting(default=86400),  # in seconds
            max_validity=appsettings.IntegerSetting(default=59 * 86400 - 3600),  # in seconds
            custom_payment_url=appsettings.StringSetting(
                default="https://platebnibrana.csob.cz/zaplat/", validators=(http_with_slash_validator,)
            ),
            messenger_netloc=appsettings.StringSetting(default="localhost:50051"),
            messenger_ssl_cert=appsettings.FileSetting(default=None, mode=R_OK),
            resend_email=appsettings.NestedDictSetting(
                dict(
                    # List of processors which are taken into account for resending emails ('None' for all)
                    processors=appsettings.ListSetting(default=None, item_type=str),
                    # Time intervals in days for resending email
                    # positive values are relative to payment creation, negative values relative to payment expiry
                    time_deltas=appsettings.NestedListSetting(
                        inner_setting=DayDeltaSetting(), default=list, call_default=True
                    ),
                    # Time interval after last email in days for which another timepoints are disabled
                    min_delta=DayDeltaSetting(default=3, transform_default=True),
                ),
                default=None,
            ),
        ),
        default=None,
    )

    # EU members (using two-letter country code)
    eu_members = appsettings.NestedListSetting(
        inner_setting=appsettings.StringSetting(validators=(country_code_validator,)),
        default=EU_MEMBERS,
    )

    # VIES requester VAT number (required by VIES REST API, default is that of CZ.NIC)
    vies_requester = appsettings.StringSetting(validators=(RegexValidator(r"^[A-Z]{2}[0-9A-Z]+$"),), required=True)

    # Payment requests settings
    payment_requests = NamedDictSetting(
        dict(
            CARD_HANDLER=appsettings.StringSetting(validators=(card_handler_validator,), required=True),
            PROCESSOR=appsettings.StringSetting(validators=(processor_validator,), required=True),
        ),
        required=False,
    )

    # Secretary settings
    secretary_url = appsettings.StringSetting(validators=(http_with_slash_validator,), default=None)
    secretary_timeout = appsettings.Setting(default=3.05, validators=(timeout_validator,))
    secretary_token = appsettings.StringSetting(default=None)

    # Fileman settings
    fileman_netloc = appsettings.StringSetting(default=None)
    fileman_ssl_cert = appsettings.FileSetting(default=None, mode=R_OK)

    class Meta:
        """Meta class."""

        setting_prefix = "pain_"


SETTINGS = PainSettings()


@lru_cache()
def get_processor_class(processor: str):
    """Get processor class."""
    cls = SETTINGS.processors.get(processor)
    if cls is None:
        raise ValueError("{} is not present in PAIN_PROCESSORS setting".format(processor))
    else:
        return cls


@lru_cache()
def get_processor_instance(processor: str):
    """Get processor class instance."""
    processor_class = get_processor_class(processor)
    return processor_class()


@lru_cache()
def get_processor_objective(processor: str):
    """Get processor objective."""
    proc = get_processor_instance(processor)
    return proc.default_objective


@lru_cache()
def get_card_payment_handler_class(card_payment_handler: str):
    """Get CardPaymentHandler class."""
    try:
        cls = SETTINGS.card_payment_handlers[card_payment_handler]["HANDLER"]
        return cls
    except KeyError as error:
        raise ValueError(
            "{} is not present in PAIN_CARD_PAYMENT_HANDLERS setting".format(card_payment_handler)
        ) from error


@lru_cache()
def get_card_payment_handler_instance(card_payment_handler: str):
    """Get card_payment_handler class instance."""
    card_payment_handler_class = get_card_payment_handler_class(card_payment_handler)
    return card_payment_handler_class(
        name=card_payment_handler,
        gateway=SETTINGS.card_payment_handlers[card_payment_handler]["GATEWAY"],
    )


@lru_cache()
def get_email_client_instance() -> SyncGrpcProxy:
    """Return an email client instance (with synchronous method calls)."""
    client = EmailMessengerClient(
        SETTINGS.csob_custom_handler["messenger_netloc"],
        make_credentials(SETTINGS.csob_custom_handler["messenger_ssl_cert"]),
    )
    return SyncGrpcProxy(client)


def is_eu_member(country_code: str) -> bool:
    """Test country for EU membership."""
    return country_code in SETTINGS.eu_members


@lru_cache()
def get_secretary_client_instance() -> Optional[SecretaryClient]:
    """Return an instance of secretary client."""
    if SETTINGS.secretary_url is None:
        return None
    auth = None
    if SETTINGS.secretary_token:
        auth = HTTPTokenAuth(SETTINGS.secretary_token)
    return SecretaryClient(SETTINGS.secretary_url, auth=auth, timeout=SETTINGS.secretary_timeout)


@lru_cache()
def get_file_storage_instance() -> Optional[Storage]:
    """Return an instance of file storage."""
    if SETTINGS.fileman_netloc is None:
        return None
    file_client = FileClient(SETTINGS.fileman_netloc, credentials=make_credentials(SETTINGS.fileman_ssl_cert))
    return Storage(file_client)
