#
# Copyright (C) 2018-2024  CZ.NIC, z. s. p. o.
#
# This file is part of FRED.
#
# FRED is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# FRED is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with FRED.  If not, see <https://www.gnu.org/licenses/>.

"""Application-wide constants."""

from enum import Enum, IntEnum, unique

try:
    from enum import StrEnum  # type: ignore[attr-defined, unused-ignore]
except ImportError:  # pragma: no cover
    from backports.strenum import StrEnum  # type: ignore[assignment, unused-ignore]

from django.utils.translation import gettext_lazy as _

# Number of decimal places of currency amounts.
# Bitcoin has 8, so 10 should be enough for most practical purposes.
CURRENCY_PRECISION = 10


@unique
class PaymentType(StrEnum):
    """Payment types constants."""

    TRANSFER = "transfer"
    CARD_PAYMENT = "card_payment"


@unique
class PaymentState(StrEnum):
    """Payment states constants."""

    INITIALIZED = "initialized"
    READY_TO_PROCESS = "ready_to_process"
    PROCESSED = "processed"
    DEFERRED = "deferred"
    EXPORTED = "exported"
    CANCELED = "canceled"
    TO_REFUND = "to_refund"
    REFUNDED = "refunded"


@unique
class InvoiceType(str, Enum):
    """Invoice type constants."""

    ADVANCE = "advance"
    ACCOUNT = "account"


@unique
class ProcessPaymentResultType(Enum):
    """Result type of payment processing."""

    NOT_PROCESSED = 0  # evaluates to `False` in boolean context
    TO_REFUND = 1
    PROCESSED = 2


@unique
class PaymentProcessingError(str, Enum):
    """Payment processing error constants."""

    DUPLICITY = "duplicity"
    INSUFFICIENT_AMOUNT = "insufficient_amount"
    EXCESSIVE_AMOUNT = "excessive_amount"
    OVERDUE = "overdue"
    MANUALLY_BROKEN = "manually_broken"
    TOO_OLD = "too_old"


@unique
class MessageReference(str, Enum):
    """Type of reference when sending messages."""

    PAYMENT = "payment"


@unique
class MessageTemplate(str, Enum):
    """Enum of templates."""

    CSOB_CUSTOMER_CODE_SUBJECT = "csob-customer-code-subject.txt"
    CSOB_CUSTOMER_CODE_BODY = "csob-customer-code-body.txt"


@unique
class PdfTemplate(str, Enum):
    """Templates for rendering to PDF."""

    PAYMENT_REQUEST_PDF_TEMPLATE_CZ = "pain-payment-request-cs.html"
    PAYMENT_REQUEST_PDF_TEMPLATE_EN = "pain-payment-request-en.html"
    INVOICE_PDF_TEMPLATE_CZ = "pain-invoice-cs.html"
    INVOICE_PDF_TEMPLATE_EN = "pain-invoice-en.html"


@unique
class PaymentRequestStatus(str, Enum):
    """Status of payment request."""

    label: str  # value showed in views, subject to translation

    def __new__(cls, value: str, label: str = "") -> "PaymentRequestStatus":
        """Create new instance."""
        obj = str.__new__(cls, value)
        obj._value_ = value
        obj.label = label
        return obj

    WAITING = ("waiting", _("Waiting"))
    OVERDUE = ("overdue", _("Overdue"))
    REALIZED = ("realized", _("Realized"))
    CLOSED = ("closed", _("Closed"))


# Data for Czech banks - selection only
BANK_INSTITUTIONS = {
    "0300": {"country": "CZ", "alpha": "CEKO", "swift": "CEKOCZPP", "name": "Československá obchodní banka, a.s."},
    "2010": {"country": "CZ", "alpha": "FIOB", "swift": "FIOBCZPP", "name": "Fio banka, a.s."},
    "5500": {"country": "CZ", "alpha": "RZBC", "swift": "RZBCCZPP", "name": "Raiffeisenbank, a.s."},
}


@unique
class VerbosityLevel(IntEnum):
    """Verbosity level for reporting."""

    NONE = 0
    BASIC = 1
    EXTENDED = 2
    FULL = 3
