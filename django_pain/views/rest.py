#
# Copyright (C) 2020-2024  CZ.NIC, z. s. p. o.
#
# This file is part of FRED.
#
# FRED is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# FRED is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with FRED.  If not, see <https://www.gnu.org/licenses/>.

"""REST API module."""

import logging
from copy import deepcopy

from django.db import transaction
from django.http import Http404
from django.utils import timezone
from rest_framework import mixins, routers, status, viewsets
from rest_framework.decorators import action
from rest_framework.response import Response

from django_pain.card_payment_handlers import PaymentHandlerConnectionError, PaymentHandlerError
from django_pain.constants import PaymentState, PaymentType
from django_pain.models import BankPayment, Customer, Invoice, PaymentRequest
from django_pain.processors import PaymentProcessorError
from django_pain.serializers import (
    BankPaymentSerializer,
    CardPaymentSerializer,
    CustomerSerializer,
    InvoiceSerializer,
    PaymentRequestSerializer,
)
from django_pain.settings import (
    SETTINGS,
    get_card_payment_handler_instance,
    get_file_storage_instance,
    get_processor_instance,
    get_secretary_client_instance,
)

LOGGER = logging.getLogger(__name__)


class BankPaymentViewSet(mixins.CreateModelMixin, mixins.RetrieveModelMixin, viewsets.GenericViewSet):
    """BankPayment API for create and retrieve."""

    queryset = BankPayment.objects.filter(payment_type=PaymentType.CARD_PAYMENT).select_for_update()
    serializer_class = BankPaymentSerializer
    lookup_field = "uuid"

    def _process_payment(self, payment):
        processor = get_processor_instance(payment.processor)
        LOGGER.info("Processing card payment with processor %s.", payment.processor)
        try:
            result = list(processor.process_payments([deepcopy(payment)]))[0]
            payment.save_processing_result(result)
        except PaymentProcessorError as error:
            LOGGER.error("Error occured while processing payment with processor %s: %s.", payment.processor, str(error))

    @transaction.atomic()
    def retrieve(self, request, *args, **kwargs):
        """Update payment state and return update payment."""
        payment = self.get_object()
        old_payment_state = payment.state

        card_payment_handler = get_card_payment_handler_instance(payment.card_handler)
        try:
            card_payment_handler.update_payments_state(payment)
        except PaymentHandlerConnectionError:
            return Response(status=status.HTTP_503_SERVICE_UNAVAILABLE)

        if old_payment_state == PaymentState.INITIALIZED and payment.state == PaymentState.READY_TO_PROCESS:
            self._process_payment(payment)

        serializer = BankPaymentSerializer(payment)
        return Response(serializer.data)

    @transaction.atomic()
    def create(self, request, *args, **kwargs):
        """Create new payment."""
        try:
            return super().create(request, *args, **kwargs)
        except PaymentHandlerConnectionError:
            return Response(status=status.HTTP_503_SERVICE_UNAVAILABLE)


class CustomerViewSet(
    mixins.RetrieveModelMixin, mixins.CreateModelMixin, mixins.UpdateModelMixin, viewsets.GenericViewSet
):
    """API for `Customer` model."""

    queryset = Customer.objects.all().select_for_update()
    serializer_class = CustomerSerializer
    lookup_field = "uid"

    @transaction.atomic
    def retrieve(self, request, *args, **kwargs):
        """Retrive customer."""
        return super().retrieve(request, *args, **kwargs)

    @transaction.atomic
    def create(self, request, *args, **kwargs):
        """Create new customer."""
        return super().create(request, *args, **kwargs)

    @transaction.atomic
    def update(self, request, *args, **kwargs):
        """Update customer."""
        # Serializer for full update (and potential create) requires `uid` in its input data. Take it from URL
        # parameter which also prevents change of `uid` during update.
        uid = kwargs.get(self.lookup_field)  # when routed here it is sure that `uid` is successfully parsed
        request.data["uid"] = uid  # malformed data in request raise parsing error before being available
        try:
            return super().update(request, *args, **kwargs)
        except Http404:
            # If regular update fails with "not found" try to create resource. There is no need to extra check
            # permissions in our use case. Do nothing more for partial update (PATCH method).
            if request.method == "PUT":
                return super().create(request, *args, **kwargs)
            else:
                raise


class PaymentRequestViewSet(mixins.RetrieveModelMixin, mixins.CreateModelMixin, viewsets.GenericViewSet):
    """API for `PaymentRequest` model."""

    queryset = PaymentRequest.objects.all().select_for_update()
    serializer_class = PaymentRequestSerializer
    lookup_field = "uuid"

    @transaction.atomic
    def retrieve(self, request, *args, **kwargs):
        """Retrive payment request."""
        return super().retrieve(request, *args, **kwargs)

    @transaction.atomic
    @action(methods=["POST"], detail=True)
    def close(self, request, *args, **kwargs):
        """Close payment request (i.e. set `closed_at` field to current datetime)."""
        payment_request: PaymentRequest = self.get_object()

        if not payment_request.closed_at:
            payment_request.closed_at = timezone.now()
            payment_request.save(update_fields=["closed_at"])

        serializer = self.serializer_class(payment_request)
        return Response(data=serializer.data)

    def _is_card_payment_active(self, payment_request, card_handler):
        """Check if current card payment exists and is ready to pay."""
        payment = payment_request.card_payment
        if payment:
            card_handler.update_payments_state(payment)
            payment.refresh_from_db()
            return payment.state == PaymentState.INITIALIZED
        else:
            return False

    @transaction.atomic
    @action(methods=["POST"], detail=True, url_path="cardpayment")
    def add_card_payment(self, request, *args, **kwargs):
        """Add (or replace cancelled) card payment to payment request."""
        payment_request: PaymentRequest = self.get_object()
        input_serializer = CardPaymentSerializer(data=request.data)
        input_serializer.is_valid(raise_exception=True)

        if payment_request.closed_at:
            return Response(status=status.HTTP_400_BAD_REQUEST, data={"detail": "Payment request is closed"})

        try:
            card_handler = get_card_payment_handler_instance(
                SETTINGS.payment_requests[payment_request.app_id]["CARD_HANDLER"]
            )

            # Check status of current card payment
            if self._is_card_payment_active(payment_request, card_handler):
                return Response(
                    status=status.HTTP_400_BAD_REQUEST, data={"detail": "Payment request has active card payment"}
                )

            # Prepare parameters and create card payment on gateway
            PaymentRequestSerializer.add_card_payment(
                payment_request,
                input_serializer.validated_data["redirect_url"],
                input_serializer.validated_data["redirect_method"],
                input_serializer.validated_data["expiry"],
                card_handler,
            )
        except PaymentHandlerError as e:  # bank gateway error (connection, security, limit, etc.)
            return Response(status=status.HTTP_400_BAD_REQUEST, data={"detail": repr(e)})

        output_serializer = self.serializer_class(payment_request)
        return Response(status=status.HTTP_201_CREATED, data=output_serializer.data)

    @transaction.atomic
    @action(detail=True)
    def invoice(self, request, *args, **kwargs):
        """Get invoice related to payment request."""
        payment_request = self.get_object()
        invoice = payment_request.invoice
        if invoice is None:
            raise Http404
        serializer = InvoiceSerializer(invoice)
        return Response(data=serializer.data)

    def perform_create(self, serializer):
        """Save payment request instance."""
        super().perform_create(serializer)
        payment_request = serializer.instance
        # New stuff - try to render PDF and store its identifier, silently drop exceptions on the way.
        secretary_client = get_secretary_client_instance()
        file_storage = get_file_storage_instance()
        if secretary_client is not None and file_storage is not None:
            # Try to render and store PDF, update payment request when successful
            try:
                payment_request.render_pdf(secretary_client, file_storage)
            except Exception:
                pass  # silently drop exceptions

    def create(self, request, *args, **kwargs):
        """Create new payment request."""
        try:
            with transaction.atomic():
                return super().create(request, *args, **kwargs)
        except PaymentHandlerConnectionError:
            return Response(status=status.HTTP_503_SERVICE_UNAVAILABLE)


class InvoiceViewSet(mixins.RetrieveModelMixin, viewsets.GenericViewSet):
    """API for `Invoice` model."""

    queryset = Invoice.objects.all()
    serializer_class = InvoiceSerializer
    lookup_field = "number"

    @transaction.atomic
    def retrieve(self, request, *args, **kwargs):
        """Retrive invoice."""
        return super().retrieve(request, *args, **kwargs)


ROUTER = routers.DefaultRouter()
ROUTER.register(r"bankpayment", BankPaymentViewSet)
ROUTER.register(r"customer", CustomerViewSet)
ROUTER.register(r"paymentrequest", PaymentRequestViewSet)
ROUTER.register(r"invoice", InvoiceViewSet)
