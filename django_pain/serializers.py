#
# Copyright (C) 2020-2024  CZ.NIC, z. s. p. o.
#
# This file is part of FRED.
#
# FRED is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# FRED is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with FRED.  If not, see <https://www.gnu.org/licenses/>.

"""Serializers for REST API."""

import logging
import re
from datetime import date, datetime, timedelta
from decimal import Decimal
from enum import Enum, unique
from re import match as regex_match
from typing import Any, Dict, List, Mapping, Optional, OrderedDict, Sequence, Union
from uuid import uuid4

from django.conf import settings
from django.utils import timezone
from djmoney.money import Money
from rest_framework import serializers

from django_pain.card_payment_handlers import (
    AbstractCardPaymentHandler,
    CartItem,
    CustomerData,
    PaymentHandlerLimitError,
)
from django_pain.constants import PaymentState, PaymentType
from django_pain.models import (
    BankAccount,
    BankPayment,
    Customer,
    Invoice,
    InvoiceItem,
    PaymentRequest,
    PaymentRequestItem,
)
from django_pain.models.invoices import (
    BillingItemType,
    InvoiceNumberSequence,
    VariableSymbolSequence,
    VatRate,
)
from django_pain.settings import SETTINGS, get_card_payment_handler_instance, is_eu_member
from django_pain.utils import local_date, round_money_amount, vat_number_to_country_code

LOGGER = logging.getLogger(__name__)

# International phone number - not very restrictive regular expression
#  - country code prefix (optional): +, 0, 00
#  - country code: 1 to 3 digits
#  - country code separator: space, dot
#  - subscriber number: digits with separators (space, dot, dash) in between
INTL_PHONE_PATTERN = re.compile(r"^(\+|0|00)?\d{1,3}[ \.]\d([ \.-]?\d)+$")


@unique
class ExternalPaymentState(str, Enum):
    """Card Payment state used by REST API."""

    INITIALIZED = "initialized"
    PAID = "paid"
    CANCELED = "canceled"


CARD_PAYMENT_STATE_MAPPING = {
    PaymentState.INITIALIZED: ExternalPaymentState.INITIALIZED,
    PaymentState.READY_TO_PROCESS: ExternalPaymentState.PAID,
    PaymentState.PROCESSED: ExternalPaymentState.PAID,
    PaymentState.DEFERRED: ExternalPaymentState.PAID,
    PaymentState.EXPORTED: ExternalPaymentState.PAID,
    PaymentState.CANCELED: ExternalPaymentState.CANCELED,
    PaymentState.TO_REFUND: ExternalPaymentState.PAID,
    PaymentState.REFUNDED: ExternalPaymentState.PAID,
}


def custom_expiry_validator(expiry: datetime) -> None:
    """Validate custom expiry."""
    now = timezone.now()
    min_expiry = timedelta(seconds=SETTINGS.csob_custom_handler["min_validity"])
    max_expiry = timedelta(seconds=SETTINGS.csob_custom_handler["max_validity"])
    if not (now + min_expiry <= expiry <= now + max_expiry):
        raise serializers.ValidationError("Time point outside allowed interval")


class BankPaymentSerializer(serializers.ModelSerializer):
    """Serializer for BankPayment."""

    return_url = serializers.URLField(write_only=True)
    return_method = serializers.ChoiceField(choices=("GET", "POST"), write_only=True)
    cart = serializers.JSONField(write_only=True, required=True)
    language = serializers.CharField(write_only=True)
    identifier = serializers.CharField(read_only=True)
    state = serializers.SerializerMethodField(read_only=True)
    gateway_redirect_url = serializers.SerializerMethodField(read_only=True)
    custom_expiry = serializers.DateTimeField(required=False, write_only=True, validators=(custom_expiry_validator,))
    customer = serializers.CharField(required=False, write_only=True)
    phone_number = serializers.RegexField(required=False, write_only=True, regex=INTL_PHONE_PATTERN)
    email = serializers.EmailField(required=False, write_only=True)
    email_messages = serializers.BooleanField(default=False, write_only=True)

    class Meta:
        model = BankPayment
        fields = [
            "uuid",
            "identifier",
            "amount",
            "variable_symbol",
            "processor",
            "card_handler",
            "return_url",
            "return_method",
            "cart",
            "language",
            "gateway_redirect_url",
            "state",
            "custom_expiry",
            "customer",
            "phone_number",
            "email",
            "email_messages",
        ]

    def __init__(self, *args, **kwargs) -> None:
        self.gateway_redirect_url = None
        super().__init__(*args, **kwargs)

    def get_state(self, obj: BankPayment) -> ExternalPaymentState:
        """Get simple state for REST API from BankPayment state attribute."""
        return CARD_PAYMENT_STATE_MAPPING[obj.state]

    def get_gateway_redirect_url(self, obj: BankPayment) -> Optional[str]:
        """Get redriect URL of the payment gateway."""
        return self.gateway_redirect_url

    def validate_cart(self, value: List[Dict[str, Any]]) -> List[CartItem]:
        """Validate cart is properly set."""
        if not (1 <= len(value) <= 2):
            raise serializers.ValidationError("`cart` must have one or two item(s)!")
        cart_items = []
        for item in value:
            if len(item["name"]) > 20:
                raise serializers.ValidationError("`cartitem/name` must not exceed 20 characters!")
            cart_item = CartItem(**item)
            cart_items.append(cart_item)
        return cart_items

    def validate(self, data: OrderedDict) -> OrderedDict:
        """Overall checks throughout the fields."""
        data = self._validate_amount(data)
        data = self._validate_amount_cart(data)
        return data

    def _validate_amount(self, data: OrderedDict) -> OrderedDict:
        """Validate amount is of type Money.

        If amount_currency is not specified we get Decimal instead of Money.
        We can not validate amount in the dedicated function because we want to log info about the processor.
        """
        value = data["amount"]
        if not isinstance(value, Money):
            processor = data.get("processor", "")
            message = 'Parameter "amount_currency" not set. Using default currency. Processor for this payment: "{}"'
            LOGGER.warning(message.format(processor))
            data["amount"] = Money(amount=value, currency=settings.DEFAULT_CURRENCY)
        return data

    def _validate_amount_cart(self, data: OrderedDict) -> OrderedDict:
        """Validate cart and amount are consistent."""
        cart_total_amount = sum((item.amount * item.quantity for item in data["cart"]))
        if cart_total_amount != data["amount"].amount:
            raise serializers.ValidationError("Sum of `cart` items amounts must be equal to payments `amount`")
        return data

    def create(self, validated_data: OrderedDict) -> BankPayment:
        """Create the payment using initialization of its CardHandler."""
        # Compose customer data
        customer_name = validated_data.pop("customer", None)
        customer_phone = validated_data.pop("phone_number", None)
        if customer_name:
            validated_data["customer"] = CustomerData(customer_name, customer_phone, validated_data.get("email"))
        if not validated_data.pop("email_messages"):
            validated_data.pop("email", None)
        # Create payment on bank gateway
        card_handler: AbstractCardPaymentHandler = get_card_payment_handler_instance(validated_data.pop("card_handler"))
        payment, self.gateway_redirect_url = card_handler.init_payment(**validated_data)
        return payment


class StreetField(serializers.Field):
    """Serializer for `street` field.

    Street is stored as three separate items in data model (`street1`, `street2` and `street3`)
    while passed as a list of (non-empty) strings in API. At least one non-empty string is required.
    """

    def street_validator(self, value: Any) -> None:
        """Validate list of one to three non-empty strings."""
        if not isinstance(value, list) or len(value) < 1 or len(value) > 3:
            raise serializers.ValidationError("List of one to three strings is required")
        for i in value:
            if not isinstance(i, str) or not i:
                raise serializers.ValidationError("List items should be non-empty strings")

    def to_representation(self, value: Union[Customer, PaymentRequest]) -> List[str]:
        """Convert to list of strings."""
        result = [value.street1]  # `street1` is non-empty
        for i in (value.street2, value.street3):
            if i:
                result.append(i)
            else:
                break
        return result

    def to_internal_value(self, data: List[str]) -> Dict[str, Optional[str]]:
        """Validate and convert from list of strings."""
        self.street_validator(data)
        result = {
            "street1": data[0],
            "street2": data[1] if len(data) > 1 else None,
            "street3": data[2] if len(data) > 2 else None,
        }
        return result


class CustomerSerializer(serializers.ModelSerializer):
    """Serializer for `Customer` model."""

    street = StreetField(source="*")
    phone_number = serializers.RegexField(required=False, regex=INTL_PHONE_PATTERN)

    class Meta:
        model = Customer
        fields = [
            "uid",
            "name",
            "organization",
            "street",
            "city",
            "state_or_province",
            "postal_code",
            "country_code",
            "email",
            "phone_number",
            "company_registration_number",
            "vat_number",
        ]

    def validate(self, data: OrderedDict) -> OrderedDict:
        """Perform object-wide validations."""
        # In case of partial update `data` may not be enough to perform object-wide validation properly and
        # should be combined with stored model instance data.
        if self.partial and self.instance:
            full_data = self.to_representation(self.instance)
            full_data.update(data)
        else:
            full_data = data
        if not full_data.get("name") and not full_data.get("organization"):
            raise serializers.ValidationError("Neither `name` nor `organization` is set")

        # Validate VAT number format for EU countries
        vat_number = full_data.get("vat_number")
        country_code = full_data.get("country_code")
        if vat_number and is_eu_member(country_code):
            # VAT number should be prefixed with two-letter country code, format of the rest is not normalized
            # across EU countries (see https://en.wikipedia.org/wiki/VAT_identification_number).
            pattern = re.compile(r"^(?!GR)[A-Z]{2}[0-9A-Z]+$")
            if not pattern.match(vat_number) or not is_eu_member(vat_number_to_country_code(vat_number)):
                raise serializers.ValidationError("Malformed VAT number for EU country")

        # Store empty VAT number as `None`
        if vat_number == "":
            data["vat_number"] = None

        # Clear VIES validation info as it could become invalid after the update.
        data["vies_vat_valid"] = None
        data["vies_checked_at"] = None

        return data


class PaymentRequestItemSerializer(serializers.ModelSerializer):
    """Serializer for `PaymentRequstItem` model."""

    price_with_vat = serializers.BooleanField(write_only=True)

    class Meta:
        model = PaymentRequestItem
        fields = ["name", "item_type", "quantity", "price_per_unit", "price_total", "price_with_vat"]
        read_only_fields = ["price_total"]

    def validate(self, data: OrderedDict) -> OrderedDict:
        """Perform object-wide validations and processing."""
        price_with_vat = data.pop("price_with_vat")
        data["price_per_unit"] = round_money_amount(data["price_per_unit"])

        # Validate item type
        q = BillingItemType.objects.filter(handle=data["item_type"])
        if not q:
            raise serializers.ValidationError("Unknown item type")

        # Add VAT to price if required
        if not price_with_vat:
            raise serializers.ValidationError("Items without VAT are not supported")

        # Calculate total price
        data["price_total"] = data["quantity"] * data["price_per_unit"]

        return data


def not_in_past_validator(timestamp: Optional[datetime]) -> None:
    """Validate that timestamp in not in the past."""
    if timestamp is not None and timestamp < timezone.now():
        raise serializers.ValidationError("Time point must not be in the past")


def vs_format_validator(variable_symbol: str) -> None:
    """Validate format of variable symbol."""
    # Check format
    pattern = r"^[0-9]{0,10}$"
    if regex_match(pattern, variable_symbol) is None:
        raise serializers.ValidationError("Invalid format")


def variable_symbol_validator(variable_symbol: str) -> None:
    """Validate variable symbol."""
    # Ensure there is no active payment request with the same symbol
    q = PaymentRequest.objects.filter(closed_at=None, variable_symbol=variable_symbol).select_for_update()
    if q:
        raise serializers.ValidationError(f"Duplicate variable symbol found in active payment request `{q.get().uuid}`")
    # Ensure there is a closed payment request with the same symbol
    # to prevent possible conflict with the symbol sequence.
    q = PaymentRequest.objects.filter(closed_at__isnull=False, variable_symbol=variable_symbol)
    if not q:
        raise serializers.ValidationError(f"Closed payment request with variable symbol `{variable_symbol}` not found")


class CardPaymentSerializer(serializers.Serializer):
    """Serializer for card payment parameters."""

    expiry = serializers.DateTimeField(allow_null=True, default=None, validators=(custom_expiry_validator,))
    redirect_url = serializers.URLField()
    redirect_method = serializers.ChoiceField(choices=["GET", "POST"], default="GET")


class PaymentRequestSerializer(serializers.ModelSerializer):
    """Serializer for `PaymentRequst` model."""

    customer = serializers.SlugRelatedField(slug_field="uid", queryset=Customer.objects.all())
    street = StreetField(source="*", read_only=True)
    bank_account = serializers.SlugRelatedField(slug_field="account_number", queryset=BankAccount.objects.all())
    variable_symbol = serializers.CharField(
        max_length=10, allow_null=True, default=None, validators=[vs_format_validator, variable_symbol_validator]
    )  # None => generate VS
    due_at = serializers.DateTimeField(allow_null=True, required=False, validators=(not_in_past_validator,))
    due_at_displayed = serializers.DateTimeField(validators=(not_in_past_validator,))
    card_payment = serializers.CharField(source="card_payment.uuid", read_only=True, allow_null=True)
    realized_payment = serializers.CharField(source="realized_payment.uuid", read_only=True, allow_null=True)
    card_gateway_url = serializers.SerializerMethodField(read_only=True)
    invoice_number = serializers.CharField(source="invoice.number", read_only=True, allow_null=True)
    qr_code = serializers.SerializerMethodField(read_only=True)

    items = PaymentRequestItemSerializer(many=True, min_length=1)

    create_card_payment = CardPaymentSerializer(default=None, allow_null=True, write_only=True)

    class Meta:
        model = PaymentRequest
        fields = [
            "uuid",
            "app_id",
            "customer",
            "name",
            "organization",
            "street",
            "city",
            "state_or_province",
            "postal_code",
            "country_code",
            "company_registration_number",
            "vat_number",
            "created_at",
            "due_at",
            "due_at_displayed",
            "closed_at",
            "price_to_pay",
            "currency",
            "variable_symbol",
            "bank_account",
            "card_payment",
            "realized_payment",
            "card_gateway_url",
            "items",
            "create_card_payment",
            "file_uid",
            "invoice_number",
            "qr_code",
        ]
        read_only_fields = [
            "name",
            "organization",
            "street",
            "city",
            "state_or_province",
            "postal_code",
            "country_code",
            "company_registration_number",
            "vat_number",
            "created_at",
            "closed_at",
            "price_to_pay",
            "file_uid",
        ]

    def get_card_gateway_url(self, obj: PaymentRequest) -> Optional[str]:
        """Get URL on the bank payment gateway to pay the request."""
        result = None
        if obj.card_payment:
            handler = get_card_payment_handler_instance(obj.card_payment.card_handler)
            result = handler.get_payment_url(obj.card_payment)

        return result

    def get_qr_code(self, obj: PaymentRequest) -> str:
        """Get SPAYD string for QR payment."""
        return obj._get_qr_code()

    def validate(self, data: OrderedDict) -> OrderedDict:
        """Perform object-wide validations and processing."""
        # Validate account currency compatibility
        if data.get("bank_account") and (data["currency"] != data["bank_account"].currency):
            raise serializers.ValidationError("Selected currency is not compatible with the bank account")

        # Avoid mix of invoiceable and non-invoiceable items
        item_types = {i["item_type"] for i in data["items"]}
        queryset = BillingItemType.objects.filter(handle__in=item_types).values_list("not_invoiceable", flat=True)
        if True in queryset and False in queryset:
            raise serializers.ValidationError("Mix of invoiceable and non-invoiceable items is not allowed")

        # Copy current customer data
        customer = data["customer"]
        data["name"] = customer.name
        data["organization"] = customer.organization
        data["street1"] = customer.street1
        data["street2"] = customer.street2
        data["street3"] = customer.street3
        data["city"] = customer.city
        data["state_or_province"] = customer.state_or_province
        data["postal_code"] = customer.postal_code
        data["country_code"] = customer.country_code
        data["company_registration_number"] = customer.company_registration_number
        data["vat_number"] = customer.vat_number

        # Calculate total price as sum of all items
        data["price_to_pay"] = sum(i["price_total"] for i in data["items"])

        return data

    def create(self, validated_data: OrderedDict[str, Any]) -> PaymentRequest:
        """Create payment request object with all its items."""
        items = validated_data.pop("items")
        create_card_payment = validated_data.pop("create_card_payment")

        # Variable symbol processing
        if validated_data["variable_symbol"] is None:
            # Variable symbol is to be generated
            app_id = validated_data["app_id"]
            year = timezone.now().year
            try:
                validated_data["variable_symbol"] = VariableSymbolSequence.objects.next_value(app_id, year)
            except VariableSymbolSequence.DoesNotExist:
                raise serializers.ValidationError(
                    {"detail": f"No variable symbol sequence available for `{app_id}` and `{year}`"},
                )

        # Save objects to database
        payment_request = PaymentRequest.objects.create(**validated_data)
        for item in items:
            PaymentRequestItem.objects.create(payment_request=payment_request, **item)

        # Create card payment on bank gateway if required
        if create_card_payment is not None:
            settings = SETTINGS.payment_requests[validated_data["app_id"]]

            # Payment request is created without card payment when amount exceeds limit given by bank and
            # no error is reported in such case.
            try:
                self.add_card_payment(
                    payment_request,
                    create_card_payment["redirect_url"],
                    create_card_payment["redirect_method"],
                    create_card_payment["expiry"],
                    get_card_payment_handler_instance(settings["CARD_HANDLER"]),
                )
            except PaymentHandlerLimitError:
                pass

        return payment_request

    @staticmethod
    def make_cart(items: Sequence[Mapping[str, Any]]) -> List[CartItem]:
        """Convert payment request items to cart for card payment."""
        # Cart must have one or two values, description up to 20 chars
        item = items[0]  # `items` is sure to be non-empty
        result = [
            CartItem(
                name=item["name"][:20],
                quantity=item["quantity"],
                amount=item["price_per_unit"] * item["quantity"],
                description="",
            ),
        ]
        items_left = len(items) - 1
        if items_left == 1:
            item = items[1]
            result.append(
                CartItem(
                    name=item["name"][:20],
                    quantity=item["quantity"],
                    amount=item["price_per_unit"] * item["quantity"],
                    description="",
                )
            )
        elif items_left > 1:  # accumulate left items into one auxuliary cart item
            result.append(
                CartItem(
                    name=f"+{items_left} ...",
                    quantity=1,
                    amount=sum(i["price_total"] for i in items[1:]),
                    description="",
                )
            )
        return result

    @staticmethod
    def add_card_payment(
        payment_request: PaymentRequest,
        return_url: str,
        return_method: str,
        expiry: Optional[datetime],
        card_handler: AbstractCardPaymentHandler,
    ):
        """Prepare parameters and create card payment on gateway."""
        params = {
            "amount": Money(payment_request.price_to_pay, payment_request.currency),
            "variable_symbol": payment_request.variable_symbol,
            "processor": SETTINGS.payment_requests[payment_request.app_id]["PROCESSOR"],
            "return_url": return_url,
            "return_method": return_method,
            "cart": PaymentRequestSerializer.make_cart(
                [
                    dict(name=i.name, quantity=i.quantity, price_per_unit=i.price_per_unit, price_total=i.price_total)
                    for i in payment_request.items.all()
                ]
            ),
            "language": "cs",
            "customer": CustomerData(
                ", ".join([i for i in (payment_request.name, payment_request.organization) if i]),
                payment_request.customer.phone_number,
                payment_request.customer.email,
            ),
        }
        if expiry:
            params["custom_expiry"] = expiry

        payment_request.card_payment, _ = card_handler.init_payment(**params)  # replace with/add new card payment
        payment_request.save(update_fields=["card_payment"])


class PaymentTooOldException(Exception):
    """Payment is too old to be processed automatically."""


class PaymentRequestForInvoiceSerializer(serializers.ModelSerializer):
    """Read only serializer for `PaymentRequst` model to create data suitable for invoice creation."""

    customer = serializers.SlugRelatedField(slug_field="uid", queryset=Customer.objects.all())
    due_at = serializers.SerializerMethodField()
    tax_at = serializers.SerializerMethodField()
    paid_at = serializers.SerializerMethodField()
    price_to_pay = serializers.SerializerMethodField()
    bank_account = serializers.SlugRelatedField(slug_field="account_number", queryset=BankAccount.objects.all())
    payment_type = serializers.SerializerMethodField()
    items = serializers.SerializerMethodField()

    class Meta:
        model = PaymentRequest
        fields = [
            "customer",
            "app_id",
            "due_at",
            "tax_at",
            "paid_at",
            "price_to_pay",
            "currency",
            "variable_symbol",
            "bank_account",
            "payment_type",
            "items",
        ]
        read_only_fields = fields

    def __init__(self, obj: PaymentRequest) -> None:
        # This form of constructor is enough for read-only serializer
        super().__init__(obj)

        # Check prerequisites for serialization into invoice data
        if obj.realized_payment is None:
            raise serializers.ValidationError("Payment is not realized yet")

        # Evaluate tax point
        self._tax_point = self._get_tax_point(obj.realized_payment.transaction_date, local_date(timezone.now()))

    def get_due_at(self, obj: PaymentRequest) -> datetime:
        """Return due datetime."""
        return obj.due_at_displayed

    @staticmethod
    def _get_tax_point(transaction_date: date, today: date) -> date:
        """Evaluate tax point (time of supply)."""
        if transaction_date > today:
            raise ValueError("Payment realized in future")  # should never happen
        if today - transaction_date <= timedelta(days=15):
            return transaction_date
        last_in_previous_month = today.replace(day=1) - timedelta(days=1)
        first_in_previous_month = last_in_previous_month.replace(day=1)
        if transaction_date > last_in_previous_month:  # realized this month
            return today
        # Realized before this month
        if transaction_date < first_in_previous_month:
            raise PaymentTooOldException("Payment before start of previous month")
        # Payment realized during previous month
        assert first_in_previous_month <= transaction_date <= last_in_previous_month
        if today.day > 15:
            raise PaymentTooOldException("Payment during previous month, more than 15 days old")
        return last_in_previous_month

    def get_tax_at(self, obj: PaymentRequest) -> date:
        """Return tax point (time of supply)."""
        return self._tax_point

    def get_paid_at(self, obj: PaymentRequest) -> date:
        """Return paid date."""
        return obj.realized_payment.transaction_date

    def get_price_to_pay(self, obj: PaymentRequest) -> Decimal:
        """Return total price to pay."""
        # Payment is realized, i.e. there is nothing more to pay
        return Decimal("0.00")

    def get_payment_type(self, obj: PaymentRequest) -> PaymentType:
        """Return payment type of realized payment."""
        return obj.realized_payment.payment_type

    def get_items(self, obj: PaymentRequest) -> List[Dict[str, Any]]:
        """Return payment request items."""
        if obj.vat_number and is_eu_member(obj.country_code) and obj.customer.vies_vat_valid is None:
            # Invoice can only be create once proper tax evaluation is possible
            raise serializers.ValidationError("VAT number for EU resident is not yet validated")

        items = obj.items.all()
        return [self._get_item(item, obj.customer.tax_country_code, obj.customer.reverse_charge_mode) for item in items]

    def _get_item(self, item: PaymentRequestItem, tax_country_code: str, reverse_charge_mode: bool) -> Dict[str, Any]:
        """Evaluate VAT and calculate remaining prices for payment request item."""
        tax_date = self._tax_point
        # Get item info (VAT rate, etc.) from code list
        billing_item_type = BillingItemType.objects.get(handle=item.item_type)
        if billing_item_type.not_invoiceable:
            raise serializers.ValidationError(f"`{item.item_type}` is non-invoiceable")
        try:
            vat_rate = Decimal("0.0") if reverse_charge_mode else billing_item_type.vat_rate(tax_country_code, tax_date)
        except VatRate.DoesNotExist:
            raise serializers.ValidationError(f"No applicable VAT rate found for `{item.item_type}` at {tax_date}")

        # Evaluate VAT and other prices
        # Input data:
        #   item.price_total - total price including VAT
        #   item.quantity - number of pieces
        #   vat_rate - VAT rate to be applied
        # Results:
        #   price_total - total price including VAT
        #   price_total_base - total base price
        #   price_total_vat - total VAT
        #   price_per_unit - unit base price
        price_total = item.price_total  # sure to be properly rounded
        price_total_base = round_money_amount(price_total / (Decimal(1) + Decimal("0.01") * vat_rate))
        price_total_vat = price_total - price_total_base
        price_per_unit = round_money_amount(price_total_base / item.quantity)  # informative value only

        return {
            "name": item.name,
            "item_type": item.item_type,
            "vat_rate": vat_rate,
            "price_per_unit": price_per_unit,
            "quantity": item.quantity,
            "price_total_base": price_total_base,
            "price_total_vat": price_total_vat,
            "price_total": price_total,
        }


class InvoiceItemSerializer(serializers.ModelSerializer):
    """Serializer for `InvoiceItem` model."""

    item_type = serializers.CharField(allow_blank=True, write_only=True)

    class Meta:
        model = InvoiceItem
        fields = [
            "name",
            "item_type",
            "vat_rate",
            "quantity",
            "price_per_unit",
            "price_total_base",
            "price_total_vat",
            "price_total",
        ]


class InvoiceSerializer(serializers.ModelSerializer):
    """Serializer for `Invoice` model."""

    customer = serializers.SlugRelatedField(slug_field="uid", queryset=Customer.objects.all())
    street = StreetField(source="*", read_only=True)
    due_at = serializers.DateTimeField(write_only=True)
    paid_at = serializers.DateField(write_only=True)

    items = InvoiceItemSerializer(many=True, min_length=1)

    class Meta:
        model = Invoice
        fields = [
            "uuid",
            "app_id",
            "customer",
            "name",
            "organization",
            "street",
            "city",
            "state_or_province",
            "postal_code",
            "country_code",
            "company_registration_number",
            "vat_number",
            "created_at",
            "due_at",
            "tax_at",
            "paid_at",
            "number",
            "variable_symbol",
            "price_total_base",
            "price_total_vat",
            "price_total",
            "price_to_pay",
            "currency",
            "file_uid",
            "bank_account",
            "payment_type",
            "items",
        ]
        read_only_fields = [
            "uuid",
            "name",
            "organization",
            "street",
            "city",
            "state_or_province",
            "postal_code",
            "country_code",
            "company_registration_number",
            "vat_number",
            "created_at",
            "number",
            "price_total_base",
            "price_total_vat",
            "price_total",
            "file_uid",
        ]

    def validate(self, data: OrderedDict) -> OrderedDict:
        """Perform object-wide validations and processing."""
        # Copy current customer data
        customer = data["customer"]
        data["name"] = customer.name
        data["organization"] = customer.organization
        data["street1"] = customer.street1
        data["street2"] = customer.street2
        data["street3"] = customer.street3
        data["city"] = customer.city
        data["state_or_province"] = customer.state_or_province
        data["postal_code"] = customer.postal_code
        data["country_code"] = customer.country_code
        data["company_registration_number"] = customer.company_registration_number
        data["vat_number"] = customer.vat_number

        # VAT number for EU residents should be VIES validated
        if customer.vat_number and is_eu_member(customer.country_code):
            if customer.vies_vat_valid is None:
                raise serializers.ValidationError("VAT number for EU resident is not yet validated")
            if not customer.vies_vat_valid:
                data["vat_number"] = None  # provided VAT number is not usable for invoice tax purposes

        # Calculate total prices as sums of all items
        data["price_total_base"] = sum(i["price_total_base"] for i in data["items"])
        data["price_total_vat"] = sum(i["price_total_vat"] for i in data["items"])
        data["price_total"] = sum(i["price_total"] for i in data["items"])

        return data

    def create(self, validated_data: OrderedDict[str, Any]) -> Invoice:
        """Create payment request object with all its items."""
        items = validated_data.pop("items")

        # Generate random identifier
        validated_data["uuid"] = uuid4()
        # Generate invoice number
        app_id = validated_data["app_id"]
        year = timezone.now().year
        try:
            validated_data["number"] = InvoiceNumberSequence.objects.next_value(app_id, year)
        except InvoiceNumberSequence.DoesNotExist:
            raise serializers.ValidationError(
                {"detail": f"No invoice number sequence available for `{app_id}` and `{year}`"},
            )

        # Save objects to database
        invoice = Invoice.objects.create(**validated_data)
        for item in items:
            InvoiceItem.objects.create(invoice=invoice, **item)

        return invoice
