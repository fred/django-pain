# Generated by Django 4.2.11 on 2024-04-16 10:19

from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ("django_pain", "0046_add_invoice_item_type"),
    ]

    operations = [
        migrations.AddField(
            model_name="invoice",
            name="due_at",
            field=models.DateTimeField(blank=True, null=True, verbose_name="Due time"),
        ),
        migrations.AddField(
            model_name="invoice",
            name="paid_at",
            field=models.DateField(blank=True, null=True, verbose_name="Paid date"),
        ),
    ]
