# Generated by Django 3.2.22 on 2023-10-20 07:47

import django.core.validators
from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ("django_pain", "0033_state_or_province"),
    ]

    operations = [
        migrations.CreateModel(
            name="VariableSymbolSequence",
            fields=[
                ("id", models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name="ID")),
                ("app_id", models.TextField(verbose_name="Application ID")),
                (
                    "year",
                    models.IntegerField(
                        default=2023,
                        validators=[
                            django.core.validators.MinValueValidator(1),
                            django.core.validators.MaxValueValidator(9999),
                        ],
                        verbose_name="Year",
                    ),
                ),
                (
                    "prefix",
                    models.IntegerField(
                        validators=[
                            django.core.validators.MinValueValidator(0),
                            django.core.validators.MaxValueValidator(9999),
                        ],
                        verbose_name="Prefix",
                    ),
                ),
                ("current_number", models.IntegerField(default=0, editable=False)),
            ],
            options={
                "db_table": "django_pain_payment_request_variable_symbol_sequence",
            },
        ),
        migrations.AddConstraint(
            model_name="variablesymbolsequence",
            constraint=models.CheckConstraint(
                check=models.Q(("year__gte", 1), ("year__lte", 9999)), name="vs_year_range"
            ),
        ),
        migrations.AddConstraint(
            model_name="variablesymbolsequence",
            constraint=models.CheckConstraint(
                check=models.Q(("prefix__gte", 0), ("prefix__lte", 9999)), name="vs_prefix_4_digits"
            ),
        ),
        migrations.AddConstraint(
            model_name="variablesymbolsequence",
            constraint=models.CheckConstraint(
                check=models.Q(("current_number__gte", 0), ("current_number__lte", 999999)),
                name="vs_current_number_6_digits",
            ),
        ),
        migrations.AlterUniqueTogether(
            name="variablesymbolsequence",
            unique_together={("app_id", "year", "prefix")},
        ),
    ]
