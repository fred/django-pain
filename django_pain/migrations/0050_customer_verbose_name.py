# Generated by Django 4.2.11 on 2024-06-06 12:38

import django.db.models.deletion
from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ("django_pain", "0049_vat_rate"),
    ]

    operations = [
        migrations.AlterField(
            model_name="invoice",
            name="customer",
            field=models.ForeignKey(
                editable=False,
                on_delete=django.db.models.deletion.RESTRICT,
                related_name="+",
                to="django_pain.customer",
                verbose_name="Customer",
            ),
        ),
        migrations.AlterField(
            model_name="paymentrequest",
            name="customer",
            field=models.ForeignKey(
                editable=False,
                on_delete=django.db.models.deletion.RESTRICT,
                related_name="+",
                to="django_pain.customer",
                verbose_name="Customer",
            ),
        ),
    ]
