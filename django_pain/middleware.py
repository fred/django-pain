#
# Copyright (C) 2024  CZ.NIC, z. s. p. o.
#
# This file is part of FRED.
#
# FRED is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# FRED is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with FRED.  If not, see <https://www.gnu.org/licenses/>.

"""Custom middleware module."""

import logging
from typing import Callable, Optional

from django.http import HttpRequest, HttpResponse, StreamingHttpResponse
from django.http.response import HttpResponseBase
from django.template.response import SimpleTemplateResponse
from frgal.utils import get_call_id

LOGGER = logging.getLogger(__name__)
LOGGER_MAX_BINARY_LENGTH = 2000


def _binary_content(data: Optional[bytes], max_length: int) -> str:
    if data is None or len(data) <= max_length:
        return str(data)
    else:
        return f"{data[:max_length]!r}... ({len(data) - max_length} more bytes)"


def _content_type(content_type: Optional[str]) -> str:
    return f' Content-Type: "{content_type}"' if content_type else ""


class TrafficLoggingMiddleware:
    """Request and response logging for selected endpoints."""

    def __init__(self, get_response: Callable[[HttpRequest], HttpResponseBase]):
        self.get_response = get_response
        self._paths = (  # URL prefixes that are subject to traffic logging
            "/api/private/",
        )

    def __call__(self, request: HttpRequest) -> HttpResponseBase:
        """Middleware handler."""
        to_log = any((request.path.startswith(i) for i in self._paths))
        if to_log:
            token = get_call_id()  # token to allow grouping of log records for a single REST call
            LOGGER.info("<<< [%s] %s %s", token, request.method, request.get_full_path())
            LOGGER.debug(
                "<<< [%s] %s%s",
                token,
                _binary_content(request.body, LOGGER_MAX_BINARY_LENGTH),
                _content_type(request.content_type),
            )

        response = self.get_response(request)

        if to_log:
            LOGGER.info(">>> [%s] %d", token, response.status_code)
            if isinstance(response, StreamingHttpResponse):  # pragma: no cover
                LOGGER.debug(">>> [%s] Streaming response")
            elif isinstance(response, SimpleTemplateResponse) and not response.is_rendered:  # pragma: no cover
                # This branch should be left unused when the middleware is properly placed outside all other
                # response modifying middleware
                pass
            elif isinstance(response, HttpResponse):  # pragma: no branch
                LOGGER.debug(">>> [%s] %s", token, _binary_content(response.content, LOGGER_MAX_BINARY_LENGTH))

        return response
