#
# Copyright (C) 2024  CZ.NIC, z. s. p. o.
#
# This file is part of FRED.
#
# FRED is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# FRED is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with FRED.  If not, see <https://www.gnu.org/licenses/>.

"""Command for exporting invoices."""

import logging

from django.core.management.base import BaseCommand, CommandError, CommandParser, no_translations
from django.db import transaction

from django_pain.export.helios import AuctionsInvoiceSerializer, HeliosExporter
from django_pain.models import Invoice
from django_pain.utils import parse_datetime_safe, round_money_amount

LOGGER = logging.getLogger(__name__)


class Command(BaseCommand):
    """Export invoices to XML."""

    help = "Export invoices to XML."

    def add_arguments(self, parser: CommandParser) -> None:
        """Command takes optional arguments restricting processed time interval."""
        parser.add_argument(
            "-f",
            "--from",
            dest="time_from",
            type=parse_datetime_safe,
            required=True,
            help="ISO datetime for lower limit of invoices",
        )
        parser.add_argument(
            "-t",
            "--to",
            dest="time_to",
            type=parse_datetime_safe,
            required=True,
            help="ISO datetime for upper limit of invoices",
        )

    @no_translations
    def handle(self, *args, **options) -> None:
        """Run command."""
        LOGGER.info("Command `export_invoice` started with interval %s to %s", options["time_from"], options["time_to"])

        exporter = HeliosExporter({"auctions": AuctionsInvoiceSerializer})
        failed = 0

        with transaction.atomic():
            queryset = Invoice.objects.filter(
                created_at__gte=options["time_from"], created_at__lt=options["time_to"]
            ).order_by("number")

            # Add invoices to export
            for invoice in queryset:
                try:
                    exporter.add_invoice(invoice)
                    LOGGER.debug(
                        "Invoice %s (%s %s, VS: %s) processed",
                        invoice.number,
                        round_money_amount(invoice.price_total),
                        invoice.currency,
                        invoice.variable_symbol,
                    )
                except Exception as e:
                    failed += 1
                    LOGGER.error("Invoice %s not exported: %r", invoice.number, e)

        # Carry out export to XML and send it to stdout
        xml = exporter.export()
        self.stdout.write(xml.decode("utf-8"), ending="")

        LOGGER.info("Invoices exported: %d, failed: %d", len(queryset) - failed, failed)
        LOGGER.info("Command `export_invoice` finished")
        if failed > 0:
            raise CommandError(f"Failed to export some invoices: {failed}")
