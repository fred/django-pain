#
# Copyright (C) 2024  CZ.NIC, z. s. p. o.
#
# This file is part of FRED.
#
# FRED is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# FRED is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with FRED.  If not, see <https://www.gnu.org/licenses/>.

"""Command for exporting report of realized non-invoiceable payments."""

import logging
from csv import writer
from io import StringIO

from django.core.management.base import BaseCommand, CommandParser, no_translations
from django.db import transaction
from django.db.models import Exists, OuterRef, Subquery

from django_pain.models import PaymentRequest
from django_pain.models.invoices import BillingItemType, PaymentRequestItem
from django_pain.utils import parse_datetime_safe, round_money_amount

LOGGER = logging.getLogger(__name__)


class Command(BaseCommand):
    """Export non-invoiceable payments to CSV."""

    help = "Export non-invoiceable payments to CSV."

    def add_arguments(self, parser: CommandParser) -> None:
        """Command takes optional arguments restricting processed time interval."""
        parser.add_argument("-a", "--application", dest="app_id", required=True, help="Application")
        parser.add_argument(
            "-f",
            "--from",
            dest="time_from",
            type=parse_datetime_safe,
            required=True,
            help="ISO datetime for lower limit of invoices",
        )
        parser.add_argument(
            "-t",
            "--to",
            dest="time_to",
            type=parse_datetime_safe,
            required=True,
            help="ISO datetime for upper limit of invoices",
        )

    @no_translations
    def handle(self, *args, **options) -> None:
        """Run command."""
        LOGGER.info(
            "Command `export_non_invoice` started for application `%s` with interval %s to %s",
            options["app_id"],
            options["time_from"],
            options["time_to"],
        )

        buffer = StringIO(newline="")
        csv_writer = writer(buffer)
        # CSV header
        names = (
            "identifier",
            "transaction date",
            "variable symbol",
            "amount",
            "currency",
            "customer",
            "description",
            "payment type",
            "counter account",
        )
        csv_writer.writerow(names)

        with transaction.atomic():
            # "not invoiceable" items in payment request
            not_invoiceable_list = BillingItemType.objects.filter(not_invoiceable=True)
            not_invoiceable_items = PaymentRequestItem.objects.filter(
                payment_request_id=OuterRef("pk"),
                item_type__in=Subquery(not_invoiceable_list.values("handle")),
            )
            # Main query with filters
            queryset = PaymentRequest.objects.filter(
                Exists(not_invoiceable_items),  # non-invoiceable items prevent invoice creation
                app_id=options["app_id"],  # relates to specified application
                closed_at__gte=options["time_from"],  # closed within specified interval
                closed_at__lt=options["time_to"],
                realized_payment__isnull=False,  # properly paid
            ).select_related("realized_payment")

            for payment_request in queryset:
                payment = payment_request.realized_payment
                fields = (
                    payment.identifier,
                    payment.transaction_date.isoformat(),
                    payment.variable_symbol,
                    str(round_money_amount(payment.amount.amount)),
                    payment.amount.currency,
                    ", ".join(i for i in (payment_request.name, payment_request.organization) if i),
                    ", ".join(i.name for i in payment_request.items.all()),
                    payment.payment_type,
                    payment.counter_account_number,
                )
                csv_writer.writerow(fields)

        items = len(queryset)
        if items != 0:
            self.stdout.write(buffer.getvalue(), ending="")

        LOGGER.info("Payments exported: %d", items)
        LOGGER.info("Command `export_non_invoice` finished")
