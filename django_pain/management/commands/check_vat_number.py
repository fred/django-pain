#
# Copyright (C) 2024  CZ.NIC, z. s. p. o.
#
# This file is part of FRED.
#
# FRED is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# FRED is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with FRED.  If not, see <https://www.gnu.org/licenses/>.

"""Command to check VAT number of customers."""

import logging
from datetime import timedelta
from json.decoder import JSONDecodeError
from typing import Optional

from django.core.management.base import BaseCommand, CommandParser, no_translations
from django.db import transaction
from django.db.models import Q
from django.utils import timezone
from requests import RequestException, post

from django_pain.models import Customer
from django_pain.settings import SETTINGS, is_eu_member

LOGGER = logging.getLogger(__name__)

VIES_URL = "https://ec.europa.eu/taxation_customs/vies/rest-api/check-vat-number"


class ViesUnresolved(Exception):
    """VIES request was not resolved (service unavailable, overloaded, network error, bad data format, etc.)."""


def validate_eu_vat_number(vat_number: Optional[str], *, timeout: float) -> Optional[bool]:
    """Validate VAT number at VIES using REST API.

    Returns:
      - `None` when not applicable (not a VAT subject, non-EU resident)
      - boolean result when found/not found at VIES

    Raises `ViesUnresolved` when VIES failed to provide info.
    """
    # VIES is only usable for EU based subjects with VAT number assigned
    if not vat_number or not is_eu_member(vat_number[0:2]):
        return None  # VIES related info is no more applicable

    # REST API call
    # Data formats could be found at https://ec.europa.eu/taxation_customs/vies/#/technical-information
    # Some real data for requester fields is required.
    request_data = {
        "countryCode": vat_number[0:2],
        "vatNumber": vat_number[2:],
        "requesterMemberStateCode": SETTINGS.vies_requester[0:2],
        "requesterNumber": SETTINGS.vies_requester[2:],
    }
    try:
        response = post(VIES_URL, json=request_data, timeout=timeout)
        response.raise_for_status()
        response_data = response.json()
        return response_data["valid"]  # Boolean `valid` field is only present if request is successfully resolved
    except (KeyError, JSONDecodeError, RequestException) as e:
        raise ViesUnresolved(repr(e)) from e


class Command(BaseCommand):
    """Check VAT number of customers."""

    help = "Check VAT number of customers."

    def add_arguments(self, parser: CommandParser) -> None:
        """Add command arguments."""
        parser.add_argument(
            "-i", "--interval", type=int, default=14, help="Interval for periodic check in days (default: %(default)s)"
        )
        parser.add_argument(
            "-t", "--timeout", type=float, default=10.0, help="VIES request timeout in seconds (default: %(default)s)"
        )

    @no_translations
    def handle(self, *args, **options) -> None:
        """Run command."""
        LOGGER.info("Command `check_vat_number` started")
        self._api_timeout = options["timeout"]

        # The queryset below selects "candidates" for a check which is then carried out atomically
        # (i.e. individual checks always run with current and immutable data). It may happen that some
        # customer is created/updated in the meantime and skips the check, it will be processed during
        # the next run. Locking the whole query into transactions would be very inpractical as the VIES
        # checks may be slow and many functions could be blocked for a substantial period of time.
        limit = timezone.now() - timedelta(days=options["interval"])
        queryset = Customer.objects.filter(
            vat_number__isnull=False,  # with VAT number
            country_code__in=SETTINGS.eu_members,  # EU resident
        ).filter(
            Q(vies_checked_at__isnull=True) | Q(vies_checked_at__lt=limit),  # not checked yet or due to re-check
        )
        for customer in queryset:
            self._check_customer(customer.uid)
        LOGGER.info("Customers processed: %d", len(queryset))

        LOGGER.info("Command `check_vat_number` finished")

    @transaction.atomic
    def _check_customer(self, customer_uid: str) -> None:
        """Check one customer."""
        customer = Customer.objects.select_for_update().get(uid=customer_uid)
        try:
            vat_valid = validate_eu_vat_number(customer.vat_number, timeout=self._api_timeout)
            customer.update_vies_info(vat_valid)
            if vat_valid is None:
                LOGGER.debug("Customer %s - VIES info cleared", customer.uid)
            else:
                LOGGER.debug("Customer %s - VIES info resolved, valid: %s", customer.uid, vat_valid)
        except ViesUnresolved as e:
            LOGGER.debug("Customer %s - VIES info not resolved: %s", customer.uid, e)
