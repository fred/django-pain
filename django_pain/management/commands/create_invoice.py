#
# Copyright (C) 2024  CZ.NIC, z. s. p. o.
#
# This file is part of FRED.
#
# FRED is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# FRED is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with FRED.  If not, see <https://www.gnu.org/licenses/>.

"""Command for creating invoices from payment requests."""

import logging

from django.core.management.base import BaseCommand, CommandError, no_translations
from django.db import transaction
from rest_framework.serializers import ValidationError

from django_pain.models import Invoice, PaymentRequest
from django_pain.serializers import InvoiceSerializer, PaymentRequestForInvoiceSerializer, PaymentTooOldException
from django_pain.settings import get_file_storage_instance, get_secretary_client_instance

LOGGER = logging.getLogger(__name__)


class Command(BaseCommand):
    """Create invoices from payment requests."""

    help = "Create invoices from payment requests."

    def __init__(self, *args, **kwargs) -> None:
        super().__init__(*args, **kwargs)
        self._secretary_client = get_secretary_client_instance()
        self._file_storage = get_file_storage_instance()

    @no_translations
    def handle(self, *args, **options) -> None:
        """Run command."""
        failed = 0
        LOGGER.info("Command `create_invoice` started")

        with transaction.atomic():
            q = PaymentRequest.objects.filter_for_invoice().select_for_update()
            for payment_request in q:
                try:
                    invoice = self._create_invoice(payment_request)
                    LOGGER.debug("Invoice %s created from payment request %s", invoice.number, payment_request.uuid)
                    if invoice.file_uid is not None:
                        LOGGER.debug("PDF for %s created", invoice.number)
                except (ValidationError, PaymentTooOldException) as e:
                    failed += 1
                    LOGGER.error("Invoice not created from payment request %s: %s", payment_request.uuid, e)

        LOGGER.info("Command `create_invoice` finished")
        if failed > 0:
            raise CommandError(f"Failed to create {failed} invoice(s)")

    def _create_invoice(self, payment_request: PaymentRequest) -> Invoice:
        """Create invoice from payment request."""
        invoice_data = PaymentRequestForInvoiceSerializer(payment_request).data
        serializer = InvoiceSerializer(data=invoice_data)
        serializer.is_valid(raise_exception=True)
        invoice = serializer.save()
        # Set payment request to invoice link
        payment_request.invoice = invoice
        payment_request.save(update_fields=["invoice"])
        # Create invoice PDF (silently drop errors)
        if self._secretary_client is not None and self._file_storage is not None:
            try:
                invoice.render_pdf(self._secretary_client, self._file_storage)
            except Exception:
                pass

        return invoice
