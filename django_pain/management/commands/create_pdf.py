#
# Copyright (C) 2023-2024  CZ.NIC, z. s. p. o.
#
# This file is part of FRED.
#
# FRED is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# FRED is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with FRED.  If not, see <https://www.gnu.org/licenses/>.

"""Command for creating PDF documents."""

import logging
from enum import Enum

from django.core.management.base import BaseCommand, CommandError, CommandParser, no_translations
from django.db import transaction
from django.utils import timezone
from filed import Storage
from typist import SecretaryClient

from django_pain.models import Invoice, PaymentRequest
from django_pain.settings import get_file_storage_instance, get_secretary_client_instance

LOGGER = logging.getLogger(__name__)


class PdfType(Enum):
    """Types of PDF to create."""

    PDF_TYPE_PAYMENT_REQUEST = "payment_request"
    PDF_TYPE_INVOICE = "invoice"


class Command(BaseCommand):
    """Create PDF documents."""

    help = "Create PDF documents."

    def add_arguments(self, parser: CommandParser) -> None:
        """Add command arguments."""
        parser.add_argument(
            "-t", "--type", choices=[i.value for i in PdfType], required=True, action="append", help="Document type"
        )

    @no_translations
    def handle(self, *args, **options) -> None:
        """Run command."""
        failed = 0
        LOGGER.info("Command `create_pdf` started")

        # Initialize clients to create and store PDF documents
        secretary_client = get_secretary_client_instance()
        file_storage = get_file_storage_instance()
        if secretary_client is None or file_storage is None:
            raise CommandError("Settings for secretary and file manager are required to create PDF documents")

        # Process payment requests
        if PdfType.PDF_TYPE_PAYMENT_REQUEST.value in options["type"]:
            failed += self._handle_payment_requests(secretary_client, file_storage)
        # Process invoices
        if PdfType.PDF_TYPE_INVOICE.value in options["type"]:
            failed += self._handle_invoices(secretary_client, file_storage)

        LOGGER.info("Command `create_pdf` finished")
        if failed > 0:
            raise CommandError(f"Failed to create some PDFs: {failed}")

    @transaction.atomic
    def _handle_payment_requests(self, secretary_client: SecretaryClient, file_storage: Storage) -> int:
        """Create and store PDFs for payment requests."""
        LOGGER.info("Creating payment request PDFs ...")
        queryset = PaymentRequest.objects.filter(
            closed_at__isnull=True,  # active payment request
            due_at_displayed__gte=timezone.now(),  # not overdue
            file_uid__isnull=True,  # PDF not created yet
        ).select_for_update()
        failed = 0
        for payment_request in queryset:
            try:
                payment_request.render_pdf(secretary_client, file_storage)
                LOGGER.debug("PDF for %s created", payment_request.uuid)
            except Exception as e:
                failed += 1
                LOGGER.error("PDF for %s not created: %r", payment_request.uuid, e)
        LOGGER.info("Payment request PDFs created: %d, failed: %d", len(queryset) - failed, failed)
        return failed

    @transaction.atomic
    def _handle_invoices(self, secretary_client: SecretaryClient, file_storage: Storage) -> int:
        """Create and store PDFs for invoices."""
        LOGGER.info("Creating invoice PDFs ...")
        queryset = Invoice.objects.filter(file_uid__isnull=True).select_for_update()
        failed = 0
        for invoice in queryset:
            try:
                invoice.render_pdf(secretary_client, file_storage)
                LOGGER.debug("PDF for %s created", invoice.number)
            except Exception as e:
                failed += 1
                LOGGER.error("PDF for %s not created: %r", invoice.number, e)
        LOGGER.info("Invoice PDFs created: %d, failed: %d", len(queryset) - failed, failed)
        return failed
