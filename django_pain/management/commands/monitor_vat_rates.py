#
# Copyright (C) 2024  CZ.NIC, z. s. p. o.
#
# This file is part of FRED.
#
# FRED is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# FRED is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with FRED.  If not, see <https://www.gnu.org/licenses/>.

"""Command for monitoring and update of VAT rates."""

from logging import getLogger
from typing import Sequence

from django.core.management.base import BaseCommand, CommandError, CommandParser, no_translations

from django_pain.constants import VerbosityLevel
from django_pain.management.command_utils import Reporter
from django_pain.models.invoices import BillingItemType
from django_pain.settings import SETTINGS
from django_pain.utils import parse_date_safe, timezone
from django_pain.vat_rate.tedb_client import VAT_RATES_WSDL, TedbClient, VatRateResult
from django_pain.vat_rate.updater import VatRateUpdater

LOGGER = getLogger(__name__)


class Command(BaseCommand):
    """Monitor VAT rates."""

    help = "Monitor and update VAT rates."

    def add_arguments(self, parser: CommandParser) -> None:
        """Add command arguments."""
        parser.add_argument(
            "-f",
            "--from",
            dest="date_from",
            type=parse_date_safe,
            required=True,
            help="Start of interval as ISO date",
        )
        parser.add_argument(
            "-t",
            "--to",
            dest="date_to",
            type=parse_date_safe,
            required=True,
            help="End of interval as ISO date",
        )
        country_group = parser.add_mutually_exclusive_group()
        country_group.add_argument(
            "--include-countries",
            type=(lambda x: set(x.split(","))),
            help="Comma separated list of countries to include",
        )
        country_group.add_argument(
            "--exclude-countries",
            type=(lambda x: set(x.split(","))),
            help="Comma separated list of countries to exclude",
        )
        cpa_code_group = parser.add_mutually_exclusive_group()
        cpa_code_group.add_argument(
            "--include-cpa_codes",
            type=(lambda x: set(x.split(","))),
            help="Comma separated list of CPA codes to include",
        )
        cpa_code_group.add_argument(
            "--exclude-cpa_codes",
            type=(lambda x: set(x.split(","))),
            help="Comma separated list of CPA codes to exclude",
        )
        parser.add_argument(
            "-u", "--update", action="store_true", help="Update VAT rates in database (monitor only by default)"
        )

    @no_translations
    def handle(self, *args, **options) -> None:
        """Run command."""
        LOGGER.info("Command `monitor_vat_rates` started")
        LOGGER.info("Options: %s", options)
        reporter = Reporter(self.stdout, options["verbosity"])
        reporter.write("VAT rate monitoring/update", VerbosityLevel.BASIC)
        reporter.write("==========================", VerbosityLevel.BASIC)
        reporter.write(f"Started: {timezone.now()}", VerbosityLevel.BASIC)
        reporter.write("Options:", VerbosityLevel.EXTENDED)
        reporter.write(f"  --from={options['date_from']}, --to={options['date_to']}", VerbosityLevel.EXTENDED)
        reporter.write(
            f"  --include-countries={options['include_countries']}, --exclude-countries={options['exclude_countries']}",
            VerbosityLevel.EXTENDED,
        )
        reporter.write(
            f"  --include-cpa-codes={options['include_cpa_codes']}, --exclude-cpa-codes={options['exclude_cpa_codes']}",
            VerbosityLevel.EXTENDED,
        )
        reporter.write(f"  --update={options['update']}", VerbosityLevel.EXTENDED)
        reporter.write("", VerbosityLevel.BASIC)

        # Prepare country and CPA code lists
        if options["include_countries"]:
            countries = sorted(options["include_countries"])
        else:
            countries = [i for i in SETTINGS.eu_members if i not in (options["exclude_countries"] or ())]
        if options["include_cpa_codes"]:
            cpa_codes = sorted(options["include_cpa_codes"])
        else:
            queryset = BillingItemType.objects.filter(not_invoiceable=False).values_list("cpa_code", flat=True)
            # Using auxiliary set as DISTINCT ON is not supported in SQLite
            cpa_codes = sorted({i for i in queryset if i not in (options["exclude_cpa_codes"] or ())})

        reporter.write(f"Interval: {options['date_from']} - {options['date_to']}", VerbosityLevel.BASIC)
        reporter.write(f"Countries: {', '.join(countries)}", VerbosityLevel.BASIC)
        reporter.write(f"CPA codes: {', '.join(cpa_codes)}", VerbosityLevel.BASIC)
        reporter.write(f"Mode: {'update' if options['update'] else 'dry-run'}", VerbosityLevel.BASIC)
        reporter.write("", VerbosityLevel.BASIC)

        # Retrive VAT rates from SOAP service, process them and update database
        tedb_client = TedbClient(VAT_RATES_WSDL)
        updater = VatRateUpdater(reporter)
        try:
            reporter.write("Retrieving VAT rates", VerbosityLevel.BASIC)
            reporter.write("--------------------", VerbosityLevel.BASIC)
            vat_rates = tedb_client.get_vat_rates(options["date_from"], options["date_to"], countries, cpa_codes)
            self._report_vat_rates(vat_rates, reporter)
            reporter.write("", VerbosityLevel.BASIC)

            reporter.write("Processing VAT rates", VerbosityLevel.BASIC)
            reporter.write("--------------------", VerbosityLevel.BASIC)
            updater.apply(vat_rates, countries, cpa_codes, dry_run=not options["update"])
            reporter.write("", VerbosityLevel.BASIC)
        except Exception as e:
            reporter.write(f"Finished with error(s): {timezone.now()}", VerbosityLevel.BASIC)
            LOGGER.error("Command `monitor_vat_rates` finished with error(s)")
            raise CommandError(repr(e))

        reporter.write(f"Finished: {timezone.now()}", VerbosityLevel.BASIC)
        LOGGER.info("Command `monitor_vat_rates` finished")

    def _report_vat_rates(self, vat_rates: Sequence[VatRateResult], reporter: Reporter) -> None:
        reporter.write(f"VAT rate items retrieved: {len(vat_rates)}", VerbosityLevel.BASIC)
        for number, item in enumerate(vat_rates, 1):
            reporter.write(f"--- Item {number}:\n{item}", VerbosityLevel.EXTENDED)
