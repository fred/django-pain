#
# Copyright (C) 2024  CZ.NIC, z. s. p. o.
#
# This file is part of FRED.
#
# FRED is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# FRED is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with FRED.  If not, see <https://www.gnu.org/licenses/>.

"""Command utilities."""

from io import TextIOBase
from typing import Any, Iterable, Optional
from warnings import warn

from django_pain.constants import VerbosityLevel
from django_pain.models import BankAccount, BankPayment

try:
    from teller.parsers import BankStatementParser, SourceType
    from teller.statement import BankStatement, Payment
except ImportError:
    warn("Failed to import teller library.", UserWarning)
    BankStatement = object  # type: ignore
    BankStatementParser = Any
    Payment = object  # type: ignore
    SourceType = Any


class TellerBankPaymentParser:
    """Use parser from `teller` to get payments from file source."""

    def __init__(self, parser: BankStatementParser) -> None:
        self._parser = parser

    def parse_payments(self, source: SourceType, encoding: Optional[str] = None) -> Iterable[BankPayment]:
        """Parse bank payments from source."""
        statement = self._parser.parse_file(source, encoding=encoding)
        return self._convert_to_models(statement) if len(statement.payments) > 0 else ()

    def _convert_to_models(self, statement: BankStatement) -> Iterable[BankPayment]:
        account_number = statement.account_number
        try:
            account = BankAccount.objects.get(account_number=account_number)
        except BankAccount.DoesNotExist:
            raise ValueError("Bank account {} does not exist.".format(account_number))
        payments = []
        for payment_data in statement:
            payment = self._payment_from_data_class(account, payment_data)
            payments.append(payment)
        return payments

    def _payment_from_data_class(self, account: BankAccount, payment: Payment) -> BankPayment:
        """Convert `Payment` data class from `teller` to Django model."""
        result = BankPayment(
            identifier=payment.identifier,
            account=account,
            transaction_date=payment.transaction_date,
            counter_account_number=self._value_or_blank(payment.counter_account),
            counter_account_name=self._value_or_blank(payment.name),
            amount=payment.amount,
            description=self._value_or_blank(payment.description),
            constant_symbol=self._value_or_blank(payment.constant_symbol),
            variable_symbol=self._value_or_blank(payment.variable_symbol),
            specific_symbol=self._value_or_blank(payment.specific_symbol),
        )
        return result

    def _value_or_blank(self, value: Optional[str]) -> str:
        return "" if value is None else value


class Reporter:
    """Utility for reporting with verbosity level."""

    def __init__(self, stream: TextIOBase, verbosity: int = VerbosityLevel.BASIC) -> None:
        self._stream = stream
        self._verbosity = verbosity

    def write(self, message: str, verbosity: int = VerbosityLevel.BASIC) -> None:
        """Write message to output stream."""
        if verbosity <= self._verbosity:
            self._stream.write(message)
