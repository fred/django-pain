#
# Copyright (C) 2018-2024  CZ.NIC, z. s. p. o.
#
# This file is part of FRED.
#
# FRED is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# FRED is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with FRED.  If not, see <https://www.gnu.org/licenses/>.

"""Various utils."""

import re
from datetime import date, datetime
from decimal import ROUND_HALF_UP, Decimal
from typing import Optional

from django.utils import timezone
from django.utils.dateparse import parse_date, parse_datetime
from djmoney.settings import DECIMAL_PLACES

from django_pain.constants import PaymentProcessingError, ProcessPaymentResultType

# Czech bank account number: optional prefix number + prefix separator, base number, slash, bank code
CZ_ACCOUNT_PATTERN = re.compile(r"((?P<prefix>\d{2,6})\s*-\s*)?(?P<base>\d{2,10})\s*\/\s*(?P<bank>\d{4})")


def full_class_name(cls):
    """Return full class name includeing the module path."""
    return "{}.{}".format(cls.__module__, cls.__qualname__)


def parse_date_safe(value: str) -> date:
    """Parse date using Django utils, but raise an exception when unsuccessful."""
    result = parse_date(value)
    if result is None:
        raise ValueError("Could not parse date.")
    return result


def parse_datetime_safe(value: str) -> datetime:
    """Parse date_time using Django utils, but raise an exception when unsuccessful."""
    result = parse_datetime(value)
    if result is None:
        raise ValueError("Could not parse date_time.")
    return result


def round_money_amount(amount: Decimal, places: int = DECIMAL_PLACES, rounding: str = ROUND_HALF_UP) -> Decimal:
    """Round money amount to given places and using given rounding method.

    Provided defaults should be OK for Czechia, at least (it may be worth noting that currency-specific decimal
    places are not implemented even in django-money which uses hard-coded value for money formatting).
    """
    q = Decimal(10) ** -places
    return amount.quantize(q, rounding=rounding)


def local_date(value: datetime) -> date:
    """Get date from timestamp considering local time zone."""
    return value.astimezone(timezone.get_current_timezone()).date()


def local_date_start(value: date) -> datetime:
    """Get datetime of 00:00 local time from date."""
    return datetime(value.year, value.month, value.day, tzinfo=timezone.get_current_timezone())


class ProcessPaymentResult(object):
    """Result of payment processing."""

    def __init__(self, result: ProcessPaymentResultType, error: Optional[PaymentProcessingError] = None) -> None:
        """Initialize the result.

        Args:
            result: PROCESSED if payment was successfully processed, TO_REFUND if bound for refunding,
                    NOT_PROCESSED otherwise.
            error: Optional payment processing error code.
        """
        self.result = result
        self.error = error

    def __eq__(self, other) -> bool:
        """Compare processing results by their value."""
        if isinstance(other, ProcessPaymentResult):
            return self.result == other.result and self.error == other.error
        return False

    @property
    def processed(self) -> bool:
        """Return whether the payment is processed.

        The state with `NOT_PROCESSED` result and no error represents case in which processor didn't handle
        the payment (and left it for other processors or handling at a later time).
        """
        return self.result != ProcessPaymentResultType.NOT_PROCESSED or self.error is not None

    @property
    def unprocessed(self) -> bool:
        """Return whether the payment is not processed."""
        return self.result == ProcessPaymentResultType.NOT_PROCESSED and self.error is None


def bank_of_account(account: str) -> str:
    """Get bank code from account number."""
    # Beware: format of account number is Czechia specific!
    match = CZ_ACCOUNT_PATTERN.match(account)
    if match is None:
        raise ValueError("Invalid bank account number: {}".format(account))
    return match.group("bank")


def account_to_iban(account: str, human: bool = False) -> str:
    """Convert account number to IBAN format."""
    # Beware: national format to IBAN conversion is Czechia specific!

    match = CZ_ACCOUNT_PATTERN.match(account)
    if match is None:
        raise ValueError("Invalid bank account number: {}".format(account))
    # IBAN format for Czechia at https://www.cnb.cz/cs/platebni-styk/iban/iban-mezinarodni-format-cisla-uctu/
    iban_account = "{bank}{prefix:0>6}{base:0>10}".format(
        prefix=match.group("prefix") or 0,
        base=match.group("base"),
        bank=match.group("bank"),
    )
    # Compute checksum
    check_string = iban_account + "123500"  # from 'CZ00', C => 12 Z => 35
    check_number = 98 - int(check_string) % 97
    # Compose result
    iban = "CZ{:0>2d}{}".format(check_number, iban_account)
    if human:
        iban = " ".join([iban[i : i + 4] for i in range(0, len(iban), 4)])  # form with space separated groups
    return iban


def vat_number_to_country_code(vat_number: str) -> str:
    """Return ISO country code from VAT number."""
    # Note: functions should be used in EU context where VAT number is sure to have two-letter country prefix
    prefix = vat_number[:2]
    # Greece is the only EU country with tax country code different from ISO country code
    return "GR" if prefix == "EL" else prefix


def country_code_to_vat_prefix(country_code: str) -> str:
    """Return VAT number prefix for a country."""
    # Note: functions should be used in EU context where VAT number is sure to have two-letter country prefix
    return "EL" if country_code == "GR" else country_code
