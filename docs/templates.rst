===================
Templates structure
===================

This document describes structure of templates used by ``django-pain``.

.. contents:: Table of Contents
   :depth: 2

The following abbreviations are used to describe details:

* ``key`` – detail key
* ``type`` – detail type
* ``value`` – description of the value

``payment_request``
===================

PDF file with payment request data.
The file is created and stored by file manager and is intended to be sent to a payee as an email attachment.

* Template: ``pain-payment-request-cs.html`` for Czech residents
* Template: ``pain-payment-request-en.html`` for foreign residents

Context
-------

.. csv-table::
   :header: key, type, value
   :width: 100%

   name, "str | None", customer's name
   organization, "str | None", customer's organization
   street, list[str], address - street
   city, str, address - city
   state_or_province, "str | None", address - state or province
   postal_code, str, address - postal code
   country, str, address - country
   company_registration_number, "str | None", registration number of subject (like IČO in Czechia)
   vat_number, "str | None", VAT number of subject (like DIČ in Czechia)
   variable_symbol, str, bank transfer variable symbol
   created_at, str, date of creation of payment request (in ISO format)
   due_at, str, due date of payment request (in ISO format)
   bank_name, str, name of bank
   bank_account, str, bank account to pay to
   iban, str, bank account in IBAN format
   bank_swift, str, SWIFT code of bank
   price_paid, str, price paid in advance (amount only) - always 0.00
   price_to_pay, str, price to pay (amount only) including VAT
   currency, str, payment currency
   qr_code, str, payment data in SPAYD format for QR code creation
   items, "list[dict[str, Any]]", items of payment request (for format see following table)

Payment request item

.. csv-table::
   :header: key, type, value
   :width: 100%

   name, str, description of payment request item
   item_type, str, type of payment request item
   quantity, int, number of pieces
   price_total, str, total price (amount only) including VAT for all pieces

``invoice``
===========

PDF file with invoice data.
The file is created and stored by file manager and is intended to be sent to a customer as an email attachment.

* Template: ``pain-invoice-cs.html`` for Czech residents
* Template: ``pain-invoice-en.html`` for foreign residents

Context
-------

.. csv-table::
   :header: key, type, value
   :width: 100%

   name, "str | None", customer's name
   organization, "str | None", customer's organization
   street, list[str], address - street
   city, str, address - city
   state_or_province, "str | None", address - state or province
   postal_code, str, address - postal code
   country, str, address - country
   company_registration_number, "str | None", registration number of subject (like IČO in Czechia)
   vat_number, "str | None", VAT number of subject (like DIČ in Czechia)
   number, str, invoice number
   variable_symbol, str, bank transfer variable symbol
   created_at, str, date of creation of invoice (in ISO format)
   tax_at, str, time of supply (in ISO format)
   price_total_base, str, total price without VAT (amount only)
   price_total_vat, str, total VAT  (amount only)
   price_total, str, total price including VAT (amount only)
   price_to_pay, str, remaining price to pay (amount only)
   currency, str, payment currency
   bank_name, str, name of bank
   bank_account, str, bank account to pay to
   iban, str, bank account in IBAN format
   bank_swift, str, SWIFT code of bank
   payment_type, str, type of payment ("transfer" or "card_payment")
   items, "list[dict[str, Any]]", items of invoice (for format see following table)

Invoice item

.. csv-table::
   :header: key, type, value
   :width: 100%

   name, str, description of invoice item
   item_type, str, type of invoice item
   vat_rate, str, VAT rate in per cents
   quantity, int, number of pieces
   price_per_unit, str, unit price without VAT (amount only)
   price_total_base, str, price without VAT for all pieces (amount only)
   price_total_vat, str, VAT for all pieces (amount only)
   price_total, str, price including VAT for all pieces (amount only)
