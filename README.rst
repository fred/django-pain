==============================
 Django Payments and Invoices
==============================

Django application for processing bank payments and invoices.

Bank statements are passed through a parser and imported to a database.
So far, there is only one parser available, ``TransprocXMLParser``,
used to process bank statements collected and transformed by `fred-transproc`_.
Parsers may also be implemented as ``django-pain`` plugins.

Imported payments are regularly processed by payment processors.
The processors are usually implemented as ``django-pain`` plugins, implementing
payment processor interface.

Bank accounts and payments may be managed through a Django admin site.

.. _fred-transproc: https://github.com/CZ-NIC/fred-transproc


Installation
============

You need to add ``django_pain.apps.DjangoPainConfig`` and ``django_lang_switch.apps.DjangoLangSwitchConfig``
to your ``INSTALLED_APPS``. In order for user interface to work, you also need to add the Django admin site.
See `Django docs`__ for detailed description.

__ https://docs.djangoproject.com/en/dev/ref/contrib/admin/

You also need to include ``django_pain``, ``admin`` and ``django_lang_switch`` urls into your project ``urls.py``:

.. code-block:: python

    urlpatterns = [
        ...
        path('pain/', include('django_pain.urls')),
        path('admin/', admin.site.urls),
        path('django_lang_switch/', include('django_lang_switch.urls')),
        ...
    ]

After installing the ``django-pain`` package and configuring your Django project, you need to generate migrations.
Call ``django-admin makemigrations``.
These migrations depend on ``LANGUAGES`` and ``CURRENCIES`` settings_, so think carefully how you set them.
If you decide to change these settings in the future, you need to generate the migrations again.

Also, JavaScript files use the new ECMAScript 2017 notation and need to be transpiled
in order to work in most of current browsers.
Assuming you have ``Node.js`` and ``npm`` installed,
you need to install necessary packages first by calling ``npm install`` from the command line.
After that, you can transpile the JavaScript code by calling ``npm run build``.

.. _settings: `Other related settings`_

Requirements
============

All requirements are resolved during package installation, see ``install_requires`` in ``setup.cfg``.

If you wish to use LDAP authentication, you can use django-python3-ldap__.

__ https://github.com/etianen/django-python3-ldap


Plugins
=======

``auctions-pain``
-----------------

https://gitlab.office.nic.cz/others/auctions-pain

Provides payment processor for domain auctions.

``donations-pain``
------------------

https://gitlab.office.nic.cz/django-apps/donations-pain

Provides payment processor for donations to CZ.NIC projects and other activities.

``fred-pain``
-------------

https://gitlab.office.nic.cz/fred/pain

Provides payment processor for FRED.

``payments-pain``
-----------------

https://gitlab.office.nic.cz/ginger/payments-pain

Provides payment processor for Ginger Payments (and therefore the Academy).


Settings
========

In order for ``django-pain`` to work, you need to define some settings in your ``settings.py``.

``PAIN_PROCESSORS``
-------------------

A required setting containing a dictionary of payment processor names and dotted paths to payment processors classes.
The payments are offered to the payment processors in that order.

Example configuration:

.. code-block:: python

    PAIN_PROCESSORS = {
        'fred': 'fred_pain.processors.FredPaymentProcessor',
        'payments': 'payments_pain.processors.PaymentsPaymentProcessor',
        'ignore': 'django_pain.processors.IgnorePaymentProcessor',
    }

You should not change processor names unless you have a very good reason.
In that case, you also need to take care of changing processor names saved in the database.

When you change this setting (including the initial setup), you have to run ``django-admin migrate``.
Permissions for manual assignment to individual payment processors are created in this step.

``PAIN_PROCESS_PAYMENTS_LOCK_FILE``
-----------------------------------

Path to the lock file for the ``process_payments`` command.
The default value is ``/tmp/pain_process_payments.lock``.

``PAIN_TRIM_VARSYM``
--------------------

Boolean setting.
If ``True``, bank statement parser removes leading zeros from the variable symbol.
Default is ``False``.

``PAIN_DOWNLOADERS``
--------------------

A setting containing dotted paths to payment downloader and parser classes and downloader parameters.
There should be a separate entry in the dictionary for each bank from which payments should be downloaded.
The downloaders and parsers are provided in `teller` library.

Example configuration:

.. code-block:: python

    PAIN_DOWNLOADERS = {
        'test_bank': {'DOWNLOADER': 'teller.downloaders.TestStatementDownloader',
                      'PARSER': 'teller.downloaders.TestStatementParser',
                      'DOWNLOADER_PARAMS': {'base_url': 'https://bank.test', 'password': 'letmein'}}
    }

``PAIN_IMPORT_CALLBACKS``
-------------------------

List setting containing dotted paths to callables.

Each value of the list should be dotted path refering to callable that takes BankPayment object as its argument and
returns (possibly) changed BankPayment. This callable is called right before the payment is saved during the import.
Especially, this callable can throw ValidationError in order to avoid saving payment to the database.
Default value is empty list.

``PAIN_CARD_PAYMENT_HANDLERS``
------------------------------

A setting that assigns names to card payment handlers.

Handlers implement bank and payment method specific functionality of card payment operations. Keys in a dictionary stand
for handler names, values consist of handler dotted path (`HANDLER`) and gateway name (`GATEWAY`). CSOB handlers
read additional gateway parameters from related section in ``PAIN_CSOB_GATEWAYS``. Card payment operations are started with
an appropriate handler name as a required parameter.

Example configuration:

.. code-block:: python

    PAIN_CARD_PAYMENT_HANDLERS = {
        'csob': {'HANDLER': 'django_pain.card_payment_handlers.csob.CSOBCardPaymentHandler', 'GATEWAY': 'default'},
        'csob_custom': {'HANDLER': 'django_pain.card_payment_handlers.csob.CSOBCustomPaymentHandler', 'GATEWAY': 'default'},
        'csob_auctions': {'HANDLER': 'django_pain.card_payment_handlers.csob.CSOBCustomPaymentHandler', 'GATEWAY': 'auctions'},
    }

``PAIN_CSOB_GATEWAYS``
----------------------

Settings for CSOB payment gateways as a dictionary with parameters for each gateway.

`API_URL`, `API_PUBLIC_KEY`, `MERCHANT_ID` and `MERCHANT_PRIVATE_KEY` contain parameters provided by CSOB bank.
`ACCOUNT_NUMBERS` is a mapping assigning accounts to curency codes. CSOB payment gateway may
accept payments in different currencies. For each currency there has to be an account held in that currency associated
with the gateway. The associated accounts have to be specified in `ACCOUNT_NUMBERS` setting so we are able to choose the
correct account when recording the payments in the application. The values of the mapping are account numbers as
recorded in the database. Optional `ACCOUNT_LIMITS` mapping allows to handle a payment exceeding limit appropriately
in advance (instead of generic transaction failure).

Example configuration:

.. code-block:: python

    PAIN_CSOB_GATEWAYS = {
        'default': {
            'API_URL': 'https://api.platebnibrana.csob.cz/api/v1.9/',
            'API_PUBLIC_KEY': 'path/to/public_key.txt',
            'MERCHANT_ID': 'abc123',
            'MERCHANT_PRIVATE_KEY': 'path/to/private_key1.txt',
            'ACCOUNT_NUMBERS': {'CZK': '123456', 'EUR': '234567'},
        },
        'auctions': {
            'API_URL': 'https://api.platebnibrana.csob.cz/api/v1.9/',
            'API_PUBLIC_KEY': 'path/to/public_key.txt',
            'MERCHANT_ID': 'def456',
            'MERCHANT_PRIVATE_KEY': 'path/to/private_key2.txt',
            'ACCOUNT_NUMBERS': {'CZK': '987654'},
            'ACCOUNT_LIMITS': {'CZK': 500000},
        },
    }

``PAIN_CSOB_CUSTOM_HANDLER``
----------------------------

Settings specific for CSOB custom payment handler. `MIN_VALIDITY` and `MAX_VALIDITY` define boundaries of validity
of custom payment (provided in `custom_expiry` parameter to `init_payment` method call). These are integer values
in seconds with defaults of 1 day for the minimum and slightly less than 59 days for the maximum (because of limit
defined by CSOB). Optional URL parameter `CUSTOM_PAYMENT_URL` is used together with payment customer code to
form the CSOB gateway link and defaults to https://platebnibrana.csob.cz/zaplat/. Agent for dispatching e-mails
is initialized with network location (i.e. host and port) in `MESSENGER_NETLOC` (``localhost:50051`` by default)
and security credentials (i.e. path to TLS certificate) in `MESSENGER_SSL_CERT` (empty, i.e. insecure connection
by default). Re-sending emails (disabled by default) can be configured with `RESEND_EMAIL` dictionary: list
`TIME_DELTAS` contains integers defining times to send remainder (in days, positive values are relative to
payment creation, negative values relative to payment expiry, defaults to empty list), `MIN_DELTA` is the minimum
interval between two emails (in days, defaults to 3 days) and `PROCESSORS` is a list of payment processors with
remainders active (defaults to ``None``, i.e. enabled regardless of processor).

Example of full configuration:

.. code-block:: python

    PAIN_CSOB_CUSTOM_HANDLER = {
        'MIN_VALIDITY': 14400,
        'MAX_VALIDITY': 864000,
        'CUSTOM_PAYMENT_URL': 'https://platebnibrana.csob.cz/zaplat/',
        'MESSENGER_NETLOC': 'localhost:50051',
        'MESSENGER_SSL_CERT': 'path/to/certificate.pem',
        'RESEND_EMAIL': {
            'TIME_DELTAS': [-3],
            'MIN_DELTA': 2,
            'PROCESSORS': ['auctions'],
        },
    }

Please note that CSOB custom payment handler uses settings in ``PAIN_CSOB_CARD`` section as well.

``PAIN_EU_MEMBERS``
-------------------

List of countries that are members of the EU. Items are two-letter strings with ISO-3166 country codes. The default
is a list of 27 countries as of EU members in December 2023.

``PAIN_VIES_REQUESTER``
-----------------------

VIES system is used for validation of VAT numbers of customers. Its REST API requires a valid VAT number of
requester (this is a required setting).

``PAIN_PAYMENT_REQUESTS``
-------------------------

Setting for payment requests consists of dictionary of application (by its ``app_id``) and related values
provided again as dictionary with keys: `CARD_HANDLER` (value is name of card payment handler configured in
`PAIN_CARD_PAYMENT_HANDLERS`) and `PROCESSOR` (value is name of processor configured in `PAIN_PROCESSORS`).

Example:

.. code-block:: python

    PAIN_PAYMENT_REQUESTS = {
        'auctions': {'CARD_HANDLER': 'csob_auctions', 'PROCESSOR': 'auctions'},
    }

``PAIN_FILEMAN_NETLOC``
------------------------

Network location in form ``host:port`` of the gRPC server with fileman service.

``PAIN_FILEMAN_SSL_CERT``
--------------------------

Path to file with TLS certificate. Default value is ``None``, which means insecure connection.

``PAIN_SECRETARY_URL``
-----------------------

URL of django-secretary service (including the path to the API and trailing slash).

``PAIN_SECRETARY_TIMEOUT``
-----------------------------------

A timeout for connection to django-secretary service. For possible values see documentation of `requests`_ library,
default value is ``3.05`` (in seconds).

.. _requests: https://requests.readthedocs.io/

``PAIN_SECRETARY_TOKEN``
---------------------------------

Token for authentication to django-secretary service. Default value is ``None``, which means no authentication.


Other related settings
======================

Plugins usually have settings of their own, see the plugin docs.
Apart from that, there are several settings that don't have to be set, but it's really advisable to do so.

``CURRENCIES``
--------------

A list of currency codes used in the application.
The default is the list of all available currencies (which is pretty long).

Example configuration:

.. code-block:: python

    CURRENCIES = ['CZK', 'EUR', 'USD']

This setting comes from django-money_ app. Changing this setting requires generating migrations.

.. _django-money: https://github.com/django-money/django-money

``DEFAULT_CURRENCY``
--------------------

The currency code of the default currency.
It should be one of the currencies defined in the ``CURRENCIES`` setting.
The default is ``XYZ``.

Example configuration:

.. code-block:: python

    DEFAULT_CURRENCY = 'CZK'

This setting comes from django-money_ app. Changing this setting requires generating migrations.

``LANGUAGES``
-------------

See `Django docs`__.
It is advisable to set this only to languages you intend to support.
``django-pain`` natively comes with English and Czech.

__ https://docs.djangoproject.com/en/dev/ref/settings/#languages

Currency formatting
-------------------

In case Django does not format currencies correctly according to its locale setting it may be necessary to define
the formatting rules manually:

.. code-block:: python

    from moneyed.localization import _FORMATTER as money_formatter
    from decimal import ROUND_HALF_UP
    money_formatter.add_formatting_definition(
        'cs', group_size=3, group_separator=' ', decimal_point=',',
        positive_sign='',  trailing_positive_sign='',
        negative_sign='-', trailing_negative_sign='',
        rounding_method=ROUND_HALF_UP
    )

First argument of `add_formatting_definition` should be a properly formatted `locale name`_ from the ``LANGUAGES``
setting.

.. _locale name: https://docs.djangoproject.com/en/dev/topics/i18n/#term-locale-name

This setting comes from py-moneyed_ library.

.. _py-moneyed: https://github.com/limist/py-moneyed


Commands
========

``import_payments``
-------------------

.. code-block::

    import_payments --parser PARSER [input file [input file ...]]

Import payments from the bank.
A bank statement should be provided on the standard input or in a file as a positional parameter.

The mandatory argument ``PARSER`` must be a dotted path to a payment-parser class compatible with ``teller``
such as ``teller.parsers.CSOBParser`` and optional ``ENCODING`` (e.g. ``cp1250``) defines encoding of input
file(s).

``download_payments``
---------------------

.. code-block::

    download_payments [--start START] [--end END] [--downloader DOWNLOADER]

Download payments from the banks.

There are two optional arguments ``--start`` and ``--end`` which set the download interval for which the banks will be
queried. Both parameters should be entered as date and time in ISO format.
Default value for ``END`` is today.
Default value for ``START`` is seven days before ``END``.

Example ``download_payments --start 2020-09-01T00:00 --end 2020-10-31T23:59``

Optional repeatable parameter ``--downloader`` selects which downloaders defined in the ``PAIN_DOWNLOADERS`` settings
will be used. If the parameter is omitted all defined downloaders will be used.

Example ``download_payments --downloader somebank --downloader someotherbank``

``list_payments``
-----------------

.. code-block::

    list_payments [--exclude-accounts ACCOUNTS]
                  [--include-accounts ACCOUNTS]
                  [--limit LIMIT] [--state STATE]

List bank payments.

The options ``--exclude-accounts`` and ``--include-accounts`` are mutually exclusive
and expect a comma-separated list of bank account numbers.

Option ``--state`` can be either ``initialized``, ``ready_to_process``, ``processed``, ``deferred``, ``exported``,
``canceled``, ``to_refund`` or ``refunded``.

If ``--limit LIMIT`` is set, the command will list at most ``LIMIT`` payments.
If there are any non-listed payments, the command will announce their count.

``process_payments``
--------------------

.. code-block::

    process_payments [--from TIME_FROM] [--to TIME_TO]
                     [--exclude-accounts ACCOUNTS]
                     [--include-accounts ACCOUNTS]

Process unprocessed payments with predefined payment processors.

The command takes all payments in the states ``ready_to_process`` or ``deferred``
and offers them to the individual payment processors.
If any processor accepts the payment, then payment's state is switched to ``processed``.
Otherwise, its state is switched to ``deferred``.

The options ``--from`` and ``--to`` limit payments to be processed by their creation date.
They expect an ISO-formatted datetime value.

The options ``--exclude-accounts`` and ``--include-accounts`` are mutually exclusive
and expect a comma-separated list of bank account numbers.

``get_card_payments_states``
----------------------------

.. code-block::

    get_card_payments_states [--from TIME_FROM] [--to TIME_TO]

Check and update states of card payments.

The command takes all card payments in the ``initialized`` state and checks their current state
with the bank payment gateway. Payment states are updated accordingly. Each bank/payment method
uses a dedicated handler of its own for communication with the bank.

The options ``--from`` and ``--to`` limit payments to be processed by their creation date.
They expect an ISO-formatted datetime value.

``check_vat_number``
--------------------

.. code-block::

    check_vat_number [--interval DAYS] [--timeout TIMEOUT]

Check customers in a database for valid VAT numbers with external system. It checks new (i.e. not
processed yet) customers as well as regularly re-checks other customers (interval defaults to 14 days).
Timeout value (integer or float in seconds) is used for calls to a VAT number validation service
(default is 10.0 s).

``create_invoice``
------------------

.. code-block::

    create_invoice

Create invoice for newly realized payment requests where applicable. Evaluate proper VAT rate for
each item, time supply point and calculate total base price, VAT and price including VAT. Invoice numbers
for each application should form a uninterrupted sequence within a year.

``create_pdf``
--------------

.. code-block::

    create_pdf [--type=payment_request] [--type=invoice]

Create a PDF document for objects of given types (i.e. payment requests, invoices or both) and store
them in the file repository. File identifier is stored with the object (is available via REST API),
such objects are skipped from processing during subsequent runs.

``export_invoice``
------------------

.. code-block::

    export_invoice --from TIME_FROM --to TIME_TO

Export invoices created within provided time interval into XML format suitable for further processing
by Helios accounting software.

Resulted data are directed to standard output which makes it easy to process them further by the
calling process.

The required parameters ``--from`` and ``--to`` are expected as ISO-formatted datetime values.

``export_non_invoice``
----------------------

.. code-block::

    export_non_invoice --application APPLICATION --from TIME_FROM --to TIME_TO

Export payment requests for the application (like ``auctions`` e.g.) realized within time interval that
are not a subject of invoicing into CSV format for further processing.

Resulted data are directed to standard output which makes it easy to process them further by the
calling process.

The required parameters ``--from`` and ``--to`` are expected as ISO-formatted datetime values.

``monitor_vat_rates``
---------------------

.. code-block::

    monitor_vat_rates --from DATE_FROM --to DATE_TO
                      [--include-countries LIST_OF_COUNTRIES | --exclude-countries LIST_COUNTRIES]
                      [--include-cpa_codes LIST_OF_CODES | --exclude-cpa-codes LIST_OF_CODES]
                      [--update]

Monitor VAT rates (both standard and reduced based on CPA code) for EU countries and apply changes to VAT
rate database. Search within provided interval (with dates in ISO format) and optionally filter results with
include/exclude parameters (by default all EU countries and all CPA codes present in PAIN database are processed).
Without explicit ``--update`` the PAIN database is not modified (i.e. monitoring only mode by default).

Text report of monitoring (and optional database update) is sent to standard output, its contents depends on
selected verbosity level.

Changes
=======

See CHANGELOG_.

.. _CHANGELOG: CHANGELOG.rst
